<?php

namespace App\Http\Requests\Frontend\Auth\Product;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\Auth\Product;
use Illuminate\Http\Request;

/**
 * Class StoreStep2ProductRequest.
 */
class StoreStep2ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return $this->user()->isAdmin();
        //TODO this can be used by a regular user as well
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        //dd($request->route('product'));
        
        Validator::extend('custom_gtin_conflict', function($attribute, $value, $parameters) 
        {
            $gtin_exists = 0;
            //dd($parameters[0]);
            if($value){

                if (Product::where('gtin_1', '=', $value)->where('id', '<>', $parameters[0])->exists()) {
                    $gtin_exists++;
                }
                
                if (Product::where('gtin_2', '=', $value)->where('id','<>', $parameters[0])->exists()) {
                    $gtin_exists++;
                }
    
                if (Product::where('gtin_3', '=', $value)->where('id','<>', $parameters[0])->exists()) {
                    $gtin_exists++;
                }

            }

            return ($gtin_exists==0);

        }); 

        return [
            'gtin_1'  => 'required|max:13|custom_gtin_conflict:'.$request->route('product')->id,
            'gtin_2'  => 'sometimes|max:13|custom_gtin_conflict:'.$request->route('product')->id,
            'gtin_3'  => 'sometimes|max:13|custom_gtin_conflict:'.$request->route('product')->id,
            'lower_level_measurement_unit_qty_1'  => 'required|numeric|min:1|max:10000',
            'lower_level_measurement_unit_qty_2'  => 'sometimes|numeric|min:1|max:10000',
            'lower_level_measurement_unit_qty_3'  => 'sometimes|numeric|min:1|max:10000',
            'length'  => 'nullable|numeric',
            'width'  => 'nullable|numeric',
            'height'  => 'nullable|numeric',
            'weight'  => 'nullable|numeric',
            'volume'  => 'nullable|numeric',
            // 'name'  => 'required',
            // 'description'  => 'required',
            // 'gtin_1'  => 'required',
            // 'lower_level_pack_hierarchy_measurement_unit_id_1'  => 'required|numeric',
            // 'ind_measurement_unit_is_order_unit_1'  => 'required',
            // 'gtin_2'  => 'required',
            // 'lower_level_pack_hierarchy_measurement_unit_id_2'  => 'required|numeric',
            // 'gtin_3'  => 'required',
            // 'min_remain_shelf_life'  => 'required|numeric',
            // 'old_material_number'  => 'max:191',
            // 'vendor_material_number'  => 'max:191',
            // 'price' => 'required|numeric',
            // 'order_price_measurement_unit_id' => 'required',
            // 'image_original' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'custom_gtin_conflict' => 'Codigo de Barra GTIN (EAN/UPC) ya existe en el sistema',
        ];
    }

}
