{{ html()->modelForm($product, 'PATCH', route('frontend.auth.product.update', $product->id))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.frontend.access.products.management')
                        <small class="text-muted">@lang('labels.frontend.access.products.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                <div class="form-group row">
                            {{ html()->label(__('labels.products.form.label.product_name'))->class('col-md-2 form-control-label')->for('name') }}

                            <div class="col-md-10">
                                {{ html()->text('name')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('labels.products.form.label.description'))->class('col-md-2 form-control-label')->for('description') }}

                            <div class="col-md-10">
                                {{ html()->text('description')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('GTIN 1 (EAN/UPC)'))->class('col-md-2 form-control-label')->for('gtin_1') }}

                            <div class="col-md-10">
                                {{ html()->text('gtin_1')
                                    ->class('form-control')
                                    ->attribute('maxlength', 13)
                                    ->required() }}
                            </div><!--col-->

                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('Cantidad de Unidades de Medida/Nivel Inferior 1'))->class('col-md-2 form-control-label')->for('lower_level_pack_hierarchy_measurement_unit_id_1') }}

                            <div class="col-md-10">
                                {{ html()->text('lower_level_pack_hierarchy_measurement_unit_id_1')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                            <label class="col-md-2 form-control-label">Unidad de medida es unidad base 1</label>

                            <div class="col-md-10">
                                <div class="form-check form-check-inline mr-1">
                                {{ html()->radio('ind_measurement_unit_is_order_unit_1')->value('1')->checked(old('ind_measurement_unit_is_order_unit_1', $product->ind_measurement_unit_is_order_unit_1 === '1')) }}
                                 <label class="form-check-label" for="ind_measurement_unit_is_order_unit_1_true">Si</label>
                                </div>
                                <div class="form-check form-check-inline mr-1">
                                {{ html()->radio('ind_measurement_unit_is_order_unit_1')->value('0')->checked(old('ind_measurement_unit_is_order_unit_1', $product->ind_measurement_unit_is_order_unit_1 === '0')) }}
                                <label class="form-check-label" for="ind_measurement_unit_is_order_unit_1_false">No</label>
                                </div>
                            </div><!--col-->
                        </div><!--form-group TODO ADD OLD PARAMETER IN ALL FORM FIELDS-->

                        <div class="form-group row">
                            {{ html()->label(__('GTIN 2 (EAN/UPC)'))->class('col-md-2 form-control-label')->for('gtin_2') }}

                            <div class="col-md-10">
                                {{ html()->text('gtin_2')
                                    ->class('form-control')
                                    ->attribute('maxlength', 13)
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->


                        <div class="form-group row">
                            {{ html()->label(__('Cantidad de Unidades de Medida/Nivel Inferior'))->class('col-md-2 form-control-label')->for('lower_level_pack_hierarchy_measurement_unit_id_2') }}

                            <div class="col-md-10">
                                {{ html()->text('lower_level_pack_hierarchy_measurement_unit_id_2')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->


                        <div class="form-group row">
                            {{ html()->label(__('GTIN 3 (EAN/UPC)'))->class('col-md-2 form-control-label')->for('gtin_3') }}

                            <div class="col-md-10">
                                {{ html()->text('gtin_3')
                                    ->class('form-control')
                                    ->attribute('maxlength', 13)
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->


                        <div class="form-group row">
                            {{ html()->label(__('Tiempo minimo de duracion restante'))->class('col-md-2 form-control-label')->for('min_remain_shelf_life') }}
                            <div class="col-md-10">
                                {{ html()->text('min_remain_shelf_life')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('Número de material antiguo'))->class('col-md-2 form-control-label')->for('old_material_number') }}
                            <div class="col-md-10">
                                {{ html()->text('old_material_number')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('Número de material del proveedor'))->class('col-md-2 form-control-label')->for('vendor_material_number') }}
                            <div class="col-md-10">
                                {{ html()->text('vendor_material_number')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('Precio'))->class('col-md-2 form-control-label')->for('price') }}

                            <div class="col-md-10">
                                {{ html()->text('price')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group--> 
                        <!--TODO:labels from lang files--> 
                        <div class="form-group row">
                            {{ html()->label(__('labels.products.form.label.order_price_measurement_unit_id'))->class('col-md-2 form-control-label')->for('order_price_measurement_unit_id') }}
                            <div class="col-md-10">
                                {{ html()->select('order_price_measurement_unit_id',$packaging_measurement_units)
                                    ->class('form-control')
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('Imagen'))->class('col-md-2 form-control-label')->for('picture') }}
                            <div class="col-md-10">
                            <img id="picture" name="picture" src="{{ $product->picture }}" class="user-profile-image" width="300px" />
                            </div><!--col-->
                        </div><!--form-group-->


                        <div class="form-group row">
                            {{ html()->label(__('Imagen'))->class('col-md-2 form-control-label')->for('image_original') }}
                            <div class="col-md-10">
                                {{ html()->file('image_original')
                                    ->class('form-control')
                                     }}
                            </div><!--col-->
                        </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('frontend.auth.product.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}