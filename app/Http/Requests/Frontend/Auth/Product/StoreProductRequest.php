<?php

namespace App\Http\Requests\Frontend\Auth\Product;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreProductRequest.
 */
class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return $this->user()->isAdmin();
        //TODO this can be used by a regular user as well
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'product_name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'suggested_price' => 'required|gt:price',
            'department_id' => 'required',
            'min_remain_shelf_life'  => 'required',
            'purchase_tax_code_id' => 'required',
            //'material_tax_type_id' => 'required',
            'iva_code_id' => 'required',
            'brand_id'  => 'required',
            'manufacturer_id'  => 'required',
            'ind_include_discount'  => 'sometimes|nullable',
            'discount_percentage' => 'sometimes|nullable|numeric',
            'ind_limited_time_discount' => 'sometimes|nullable|numeric',
            'discount_start_date' => 'sometimes|nullable|date | date_format:"Y-m-d"',
            'discount_end_date' => 'sometimes|nullable|date | date_format:"Y-m-d"|after_or_equal:discount_start_date',
            'old_material_number' => 'sometimes|nullable' ,
            'vendor_material_number' => 'sometimes|nullable',


            // 'name'  => 'required',
            // 'description'  => 'required',
            // 'gtin_1'  => 'required',
            // 'lower_level_pack_hierarchy_measurement_unit_id_1'  => 'required|numeric',
            // 'ind_measurement_unit_is_order_unit_1'  => 'required',
            // 'gtin_2'  => 'required',
            // 'lower_level_pack_hierarchy_measurement_unit_id_2'  => 'required|numeric',
            // 'gtin_3'  => 'required',
            // 'min_remain_shelf_life'  => 'required|numeric',
            // 'old_material_number'  => 'max:191',
            // 'vendor_material_number'  => 'max:191',
            // 'price' => 'required|numeric',
            // 'order_price_measurement_unit_id' => 'required',
            // 'image_original' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
}
