<?php

namespace App\Models\Auth\Traits\Attribute;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

/**
 * Trait MessageAttribute.
 */
trait MessageAttribute
{

    /**
     * @return string
     */
    public function getStatusIconAttribute()
    {
        switch ($this->read) {
            case 1:
            return "<span class='badge badge-success'><i class='fa fa-check'></i>".__('Leido').'</span>';

            default:
            return "<span class='badge badge-warning'><i class='fa fa-star'></i>".__('No Leido').'</span>';
        }
    }


    /**
     * @return string
     */
    public function getStatusBadgeAttribute()
    {
        $created = new Carbon($this->created_at);
        $now = Carbon::now();
        $difference = ($created->diff($now)->days < 1) ? '1 dia' : $created->diffForHumans($now);
        

        $badge ='';
        
            switch (true) {
                case $created->diff($now)->days <= 7:
                    $badge = "<span class='badge badge-success'>".$created->diff($now)->days.' dias </span>';
                    break;
                case $created->diff($now)->days <= 15:
                    $badge = "<span class='badge badge-warning'>".$created->diff($now)->days.' dias </span>';
                    break;
                default:
                    $badge = "<span class='badge badge-danger'>".$created->diff($now)->days.' dias </span>';
                    break;
            }


        return $badge;

        
    }

        /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        $link ='';
        
        if (auth()->check()) { 
            if(Auth::user()->hasRole('proveedor')) {
                return '<a href="'.route('frontend.auth.product.message.show',$this->id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-info"><i class="fas fa-eye"></i></a>';        
            }else
            {
                return '<a href="'.route('admin.auth.product.message.show', $this->id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-primary"><i class="fas fa-eye"></i></a>';
            }
        }
        return $link;

        
    }


}
