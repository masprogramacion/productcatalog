<?php

namespace App\Http\Requests\Frontend\Auth\Product;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateProductImagesRequest.
 */
class UpdateImagesProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return $this->user()->isAdmin();
         //TODO this can be used by a regular user as well
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'image_front_1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_back_1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }


}
