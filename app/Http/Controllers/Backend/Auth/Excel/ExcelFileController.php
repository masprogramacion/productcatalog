<?php

namespace App\Http\Controllers\Backend\Auth\Excel;
use App\Http\Controllers\Controller;
use App\Models\Auth\Product;
use Excel;
use Illuminate\Http\Request;
use App\Repositories\Backend\Auth\ProductRepository;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

/**
 * Class ExcelFileController.
 */
class ExcelFileController extends Controller
{

        /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * ProductController constructor.
     *
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

     /**
     * @param ManageProductRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        //DB::enableQueryLog(); 
        //$totalData = Product::verified()->count();
        //dump($totalData);
        //dd(DB::getQueryLog()); // Show results of log\

        return view('backend.auth.product.export');
    }


     /**
     * @param ManageProductRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function upload_index(Request $request)
    {

        return view('backend.auth.product.upload');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function download(Request $request)
    {
        //$ids = $request->ids;
        // if (is_array($param_ids)){
        //     $ids = implode( ",", $param_ids );
        // }else {
        //     $ids = explode( ",", $param_ids );
        // }
        //return response()->json(['success2'=>$ids]);
        //return response()->json(['success2'=>$ids]);


        //$ids=explode(",", $iddata);
        //return response()->json(['success'=>$input]);

        // $type = 'xlsx';
        // return Excel::create('itsolutionstuff_example', function($excel) use ($data) {
        //     $excel->sheet('mySheet', function($sheet) use ($data)
        //     {
        //         $sheet->fromArray($data);
        //     });
        // })->download($type);



        // Excel::load( public_path( 'MM41_Plantilla_Creación de Materiales_GS1.xlsx' ), function ( $file )
        // {
        //        $file->sheet( 'Sheet1', function ( $sheet )
        //             {
        //                 $sheet->setCellValue( 'D6', '22' );
        //             } );
        // } )->export( 'my-shiny-new-excel-file' );


        // GET ALL PRODUCTS WITH THE FINAL STATUS


        //LOOP OVER RESULTS AND SET VALUES IN FIELDS

        $array_ids = explode( ",", $request->ids );
        Product::whereIn('id', $array_ids)->update(array('downloaded' => '1'));


        $path = public_path( 'MM41_Plantilla_Creacion_de_Materiales_GS1.xlsx' );
        //dd($path);
        Excel::load($path, function($doc) use ($request) {

            $sheet = $doc->setActiveSheetIndex(0);

            //$result = Product::whereNotNull('payment_verified_at')->whereIn('id', $ids);

            $result = DB::table('products')->select('products.*', 'brands.name as brand_name', 'manufacturers.name as manufacturer_name', 
            'sap_data.sap_account_number as sap_account_number',
            'material_types.code as material_type_code',
            'material_tax_types.code as material_tax_type_code',
            'tax_codes.code as purchase_tax_code_id_code',
            'purchase_organizations.code as purchase_organization_code',
            'purchase_groups.code as purchase_group_code', 'iva_codes.code as iva_code',
            'pmu1.international as alt_measure_unit_stockkeep_unit_id_1_code', 
            'pmu2.international as alt_measure_unit_stockkeep_unit_id_2_code', 
            'pmu3.international as alt_measure_unit_stockkeep_unit_id_3_code',  
            'gtin_types1.code as gtin_type_id_1_code',
            'gtin_types2.code as gtin_type_id_2_code',
            'gtin_types3.code as gtin_type_id_3_code',
            'pmu4.international as lower_level_pack_hierarchy_measurement_unit_id_1_code', 
            'pmu5.international as lower_level_pack_hierarchy_measurement_unit_id_2_code', 
            'pmu6.international as lower_level_pack_hierarchy_measurement_unit_id_3_code', 
            'pmu7.international as order_price_measurement_unit_id_code')
            ->leftJoin('brands', 'products.brand_id', '=', 'brands.id' )
            ->leftJoin('sap_data', 'products.user_id', '=', 'sap_data.user_id' )
            ->leftJoin('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
            ->leftJoin('iva_codes', 'products.iva_code_id', '=', 'iva_codes.id')
            ->leftJoin('material_types', 'products.material_type_id', '=', 'material_types.id')
            ->leftJoin('material_tax_types', 'products.material_tax_type_id', '=', 'material_tax_types.id')
            ->leftJoin('tax_codes', 'products.purchase_tax_code_id', '=', 'tax_codes.id')
            //->leftJoin('material_groups', 'products.material_group_id', '=', 'material_types.id')
            ->leftJoin('purchase_organizations', 'products.purchase_organization_id', '=', 'purchase_organizations.id')
            ->leftJoin('purchase_groups', 'products.purchase_group_id', '=', 'purchase_groups.id')
            ->leftJoin('gtin_types as gtin_types1', 'products.gtin_type_id_1', '=', 'gtin_types1.id')
            ->leftJoin('gtin_types as gtin_types2', 'products.gtin_type_id_2', '=', 'gtin_types2.id')
            ->leftJoin('gtin_types as gtin_types3', 'products.gtin_type_id_3', '=', 'gtin_types3.id')
            ->leftJoin('packaging_measurement_units as pmu1', 'products.alt_measure_unit_stockkeep_unit_id_1', '=', 'pmu1.id')
            ->leftJoin('packaging_measurement_units as pmu2', 'products.alt_measure_unit_stockkeep_unit_id_2', '=', 'pmu2.id')
            ->leftJoin('packaging_measurement_units as pmu3', 'products.alt_measure_unit_stockkeep_unit_id_3', '=', 'pmu3.id')
            ->leftJoin('packaging_measurement_units as pmu4', 'products.lower_level_pack_hierarchy_measurement_unit_id_1', '=', 'pmu4.id')
            ->leftJoin('packaging_measurement_units as pmu5', 'products.lower_level_pack_hierarchy_measurement_unit_id_2', '=', 'pmu5.id')
            ->leftJoin('packaging_measurement_units as pmu6', 'products.lower_level_pack_hierarchy_measurement_unit_id_3', '=', 'pmu6.id')
            ->leftJoin('packaging_measurement_units as pmu7', 'products.order_price_measurement_unit_id', '=', 'pmu7.id')               
            ->whereNotNull('products.payment_verified_at')
            ->when($request->ids, function($query) use ($request){
                 $array_ids = explode( ",", $request->ids );
                return $query->whereIn('products.id', $array_ids);
            })->get();

            //return response()->json(['success1'=>$request->ids]);

         //dd($product);
            $row_number=1;

            foreach($result as $r){
                $row_number++;
                $sheet->setCellValue('A'.$row_number, $r->material_type_code." "); //RMMW1-MTART - Tipo de Material
                //$sheet->setCellValue('B'.$row_number, $r->material_group_code); //RMMW1-MATKL - Grupo de Articulos
                $sheet->setCellValue('B'.$row_number, $r->category_id." "); //RMMW1-MATKL - Grupo de Articulos CATEGORIA
                $sheet->setCellValue('C'.$row_number, $r->purchase_organization_code." "); //RMMW1-EKORG - Organizacion de Compras
                $sheet->setCellValue('D'.$row_number, $r->sap_account_number." "); //RMMW1-LIFNR - Número de Cuenta del Proveedor  //NOTA: QUE ID DE CUENTA ES ESTE??
                $sheet->setCellValue('E'.$row_number, $r->short_text_description); //MAKT-MAKTX - Texto breve del material
                $sheet->setCellValue('F'.$row_number, $r->alt_measure_unit_stockkeep_unit_id_1_code); //SMEINH-MEINH(01) Unidad medida alternativa p.unidad medida almacén
                
                $gtin_1 = substr($r->gtin_1, 0, -1);

                $sheet->setCellValue('G'.$row_number, $gtin_1 . " "); //SMEINH-EAN11(01) Número de artículo europeo (EAN) Nota: se deja vacio si se desea autogenerar en SAP
                $sheet->setCellValue('H'.$row_number, $r->gtin_type_id_1_code); //SMEINH-NUMTP(01) Tipo de codigo del Número de Artículo Europeo

                
                if($r->ind_automatic_digits_ean_1 == 1) {
                    $sheet->setCellValue('I'.$row_number, 'X'); //RMMZU-AUTO_PRFZ(01) Ind.: determ. automát. dígitos control para EAN
                }

                // EMPIEZAN CAMPOS PARA LA SEGUNDA (2da) ALTERNATIVA DE EMBALAJE
                if($r->alt_measure_unit_stockkeep_unit_id_2_code != null){

                    $sheet->setCellValue('J'.$row_number, $r->alt_measure_unit_stockkeep_unit_id_2_code); //SMEINH-MEINH(02) Unidad medida alternativa p.unidad medida almacén
                    $sheet->setCellValue('K'.$row_number, $r->lower_level_measurement_unit_qty_2); //SMEINH-AZSUB(02) Cantidad de unidades de medida inferiores
                    $sheet->setCellValue('L'.$row_number, $r->lower_level_pack_hierarchy_measurement_unit_id_2_code); //SMEINH-MESUB(02) Unidad de medida inferior en jerarquía embalaje
                    if($r->ind_measurement_unit_is_order_unit_2 == 1) {
                        $sheet->setCellValue('M'.$row_number, 'X'); //SMEINH-KZBSTME(02) Ind.: unidad medida es unidad medida pedido
                    }
                    if($r->ind_measurement_unit_is_delivery_or_issue_unit_2 == 1) {
                        $sheet->setCellValue('N'.$row_number, 'X'); //SMEINH-KZAUSME(02) Ind.: UM es UM entrega / UM salida
                    }
    
                    $gtin_2 = substr($r->gtin_2, 0, -1);
                    $sheet->setCellValue('O'.$row_number, $gtin_2 . " "); //SMEINH-EAN11(02) Número de artículo europeo (EAN) Nota: se deja vacio si se desea autogenerar en SAP
                    $sheet->setCellValue('P'.$row_number, $r->gtin_type_id_2_code); //SMEINH-NUMTP(02) Tipo de codigo del Número de Artículo Europeo
                    $sheet->setCellValue('Q'.$row_number, 'X'); //RMMZU-AUTO_PRFZ(02) Ind.: determ. automát. dígitos control para EAN

    
                    // TERMIMAN CAMPOS PARA LA SEGUNDA (2da) ALTERNATIVAS DE EMBALAJE
                }

                if($r->alt_measure_unit_stockkeep_unit_id_3_code != null){
                    // EMPIEZAN CAMPOS PARA LA TERCERA (3ra) ALTERNATIVA DE EMBALAJE
                    $sheet->setCellValue('R'.$row_number, $r->alt_measure_unit_stockkeep_unit_id_3_code); //SMEINH-MEINH(03) Unidad medida alternativa p.unidad medida almacén
                    $sheet->setCellValue('S'.$row_number, $r->lower_level_measurement_unit_qty_3); //SMEINH-AZSUB(03) Cantidad de unidades de medida inferiores
                    $sheet->setCellValue('T'.$row_number, $r->lower_level_pack_hierarchy_measurement_unit_id_2_code); //SMEINH-MESUB(03) Unidad de medida inferior en jerarquía embalaje
                    
                    if($r->ind_measurement_unit_is_order_unit_3 == 1) {
                        $sheet->setCellValue('U'.$row_number, 'X'); //SMEINH-KZBSTME(03) Ind.: unidad medida es unidad medida pedido
                    }
                    if($r->ind_measurement_unit_is_delivery_or_issue_unit_3 == 1) {
                        $sheet->setCellValue('V'.$row_number, 'X'); //SMEINH-KZAUSME(03) Ind.: UM es UM entrega / UM salida
                    }
                    $gtin_3 = substr($r->gtin_3, 0, -1);
                    $sheet->setCellValue('W'.$row_number, $gtin_3 . " "); //SMEINH-EAN11(03) Número de artículo europeo (EAN) Nota: se deja vacio si se desea autogenerar en SAP
                    $sheet->setCellValue('X'.$row_number, $r->gtin_type_id_3_code); //SMEINH-NUMTP(03) Tipo de codigo del Número de Artículo Europeo
                    $sheet->setCellValue('Y'.$row_number, 'X'); //RMMZU-AUTO_PRFZ(03) Ind.: determ. automát. dígitos control para EAN
                    // TERMINAN CAMPOS PARA LA TERCERA (3ra) ALTERNATIVA DE EMBALAJE
                }



                $sheet->setCellValue('Z'.$row_number, $r->material_tax_type_code . " "); //MARA-TAKLV Clasificación fiscal material
                $sheet->setCellValue('AA'.$row_number, $r->brand_id. " "); //MARA-BRAND_ID Marca
                $sheet->setCellValue('AB'.$row_number, $r->min_remain_shelf_life); //MARA-MHDRZ Tiempo mínimo de duración restante
                $sheet->setCellValue('AC'.$row_number, $r->old_material_number); //MARA-BISMT Número de material antiguo
                $sheet->setCellValue('AD'.$row_number, Carbon::parse($r->available_deliverable_from)->format('dmY')); //EINA-LIFAB Suministrable desde
                $sheet->setCellValue('AE'.$row_number, Carbon::parse($r->available_deliverable_until)->format('dmY')); //EINA-LIFBI Suministrable hasta
                $sheet->setCellValue('AF'.$row_number, $r->vendor_material_number); //EINA-IDNLF Número de material del proveedor
                $ind_regular_vendor = '';
                if($r->ind_regular_vendor =='1'){
                    $ind_regular_vendor = 'X';
                }
                $sheet->setCellValue('AG'.$row_number, $ind_regular_vendor); //EINA-RELIF Proveedor regular
                $sheet->setCellValue('AH'.$row_number, $r->purchase_group_code); //EINE-EKGRP Grupo de compras
                $sheet->setCellValue('AI'.$row_number, $r->iva_code); //EINE-MWSKZ Indicador IVA
                $sheet->setCellValue('AJ'.$row_number, $r->price); //EINE-NETPR Precio neto en info de compras
                $sheet->setCellValue('AK'.$row_number, $r->order_price_measurement_unit_id_code); //EINE-BPRME Unidad medida de precio de pedido
                $sheet->setCellValue('AL'.$row_number, $r->purchase_tax_code_id_code); //MG03STEUMM-TAXIM Identificador de impuestos material (Compras)
                //$sheet->setCellValue('AM'.$row_number, $r->language_id); //MAMT-SPRAS(01) Clave del idioma
                $sheet->setCellValue('AM'.$row_number, 'ES'); //MAMT-SPRAS(01) Clave del idioma
                
                if($r->ind_measurement_unit_is_order_unit_1 != null) 
                {
                    $sheet->setCellValue('AN'.$row_number, $r->alt_measure_unit_stockkeep_unit_id_1_code); //MAMT-MEINH(01) Unidad medida alternativa p.unidad medida almacén //NOTA:IGUAL A SMEINH-MEINH(01)
                }
                if($r->ind_measurement_unit_is_order_unit_2 != null) 
                {
                    $sheet->setCellValue('AN'.$row_number, $r->alt_measure_unit_stockkeep_unit_id_2_code); //MAMT-MEINH(01) Unidad medida alternativa p.unidad medida almacén //NOTA:IGUAL A SMEINH-MEINH(01)
                }
                if($r->ind_measurement_unit_is_order_unit_3 != null) 
                {
                    $sheet->setCellValue('AN'.$row_number, $r->alt_measure_unit_stockkeep_unit_id_3_code); //MAMT-MEINH(01) Unidad medida alternativa p.unidad medida almacén //NOTA:IGUAL A SMEINH-MEINH(01)
                }

                $sheet->setCellValue('AO'.$row_number, $r->short_text_description); //MAMT-MAKTM(01) Texto breve de material corresp.a unidad de medida  //NOTA: IGUAL A short_text_description
            }

            
        
        })->export('xlsx');


        //return back()->with('success', 'Insert Record successfully.');



    }


    public function getProducts(Request $request){
		//print_r($request->all());
        
        //\Log::info($request);


        $columns = array(
			0 => 'name',
			1 => 'description',
            2 => 'created_at',
            3 => 'downloaded'
		);
		
		$totalData = Product::verified()->count();
		$limit = $request->input('length');
		$start = $request->input('start');
		$order = $columns[$request->input('order.0.column')];
		$dir = $request->input('order.0.dir');
        
        //\Log::info($order);
        
       
		if(empty($request->input('search.value'))){
			$posts = Product::verified()->offset($start)
					->limit($limit)
					->orderBy($order,$dir)
					->get();
			$totalFiltered = Product::verified()->count();
		}else{
			$search = $request->input('search.value');
                            $posts = Product::verified()
                            ->where(function($query) use ($search)  {
                                $query->where('name', 'like', "%{$search}%")
                                ->orWhere('description','like',"%{$search}%");
                            })
							->offset($start)
							->limit($limit)
							->orderBy($order, $dir)
							->get();
            $totalFiltered = Product::verified()
                            ->where(function($query) use ($search)  {
                                $query->where('name', 'like', "%{$search}%")
                                ->orWhere('description','like',"%{$search}%");
                            })
							->count();
		}		
					
		
		$data = array();
		
		if($posts){
			foreach($posts as $r){
                $nestedData['id'] = $r->id;
				$nestedData['name'] = $r->name;
				$nestedData['description'] = $r->description;
				$nestedData['created_at'] = date('d-m-Y H:i:s',strtotime($r->created_at));
				$nestedData['downloaded'] = $r->downloaded;
				$data[] = $nestedData;
			}
		}
		
		$json_data = array(
			"draw"			=> intval($request->input('draw')),
			"recordsTotal"	=> intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"			=> $data
		);
		
		echo json_encode($json_data);
	}


    /**
     * @param ManageProductRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function upload_excel(Request $request)
    {


        if($request->hasFile('excel_file')){
            $path = $request->file('excel_file')->getRealPath();
            $data = \Excel::load($path)->get();
            if($data->count()){
                foreach ($data as $key => $value) {
                    $arr[] = ['name' => $value->name, 'details' => $value->details];
                }
                if(!empty($arr)){
                    //UPDATE PRODUCT WITH PROCESSED EXCEL FILE
                    
                }
            }
        }
        
        
    }

}