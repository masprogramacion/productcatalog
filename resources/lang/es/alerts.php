<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => 'Rol creado satisfactoriamente.',
            'deleted' => 'Rol eliminado satisfactoriamente.',
            'updated' => 'Rol actualizado satisfactoriamente.',
        ],

        'users' => [
            'cant_resend_confirmation' => 'La aplicación esta actualmente configurada para aprobación manual de usuarios.',
            'confirmation_email'  => 'Un nuevo mensaje de confirmación ha sido enviado a tu correo.',
            'confirmed'              => 'El usuario fue confirmado exitosamente.',
            'created'             => 'Usuario creado satisfactoriamente.',
            'deleted'             => 'Usuario eliminado satisfactoriamente.',
            'deleted_permanently' => 'Usuario eliminado de forma permanente.',
            'restored'            => 'Usuario restaurado satisfactoriamente.',
            'session_cleared'     => 'La sesión del usuario se borró exitosamente.',
            'social_deleted' => 'Cuenta social removida exitosamente.',
            'unconfirmed' => 'El usuario fue desconfirmado exitosamente',
            'updated'             => 'Usuario actualizado satisfactoriamente.',
            'updated_password'    => 'Contraseña actualizada satisfactoriamente.',
        ],

        'vendors' => [
            'cant_resend_confirmation' => 'La aplicación está actualmente configurada para aprobación manual de usuarios.',
            'confirmation_email'  => 'Un nuevo mensaje de confirmación ha sido enviado a tu correo.',
            'confirmed'              => 'El usuario fue confirmado exitosamente.',
            'created'             => 'Proveedor creado satisfactoriamente.',
            'deleted'             => 'Proveedor eliminado satisfactoriamente.',
            'deleted_permanently' => 'Proveedor eliminado de forma permanente.',
            'restored'            => 'Proveedor restaurado satisfactoriamente.',
            'session_cleared'     => 'La sesión del usuario se borró exitosamente.',
            'social_deleted' => 'Cuenta social removida exitosamente.',
            'unconfirmed' => 'El usuario fue desconfirmado exitosamente',
            'updated'             => 'Proveedor actualizado satisfactoriamente.',
            'updated_password'    => 'Contraseña actualizada satisfactoriamente.',
        ],

        'products' => [

            'confirmed'              => 'El producto fue confirmado exitosamente.',
            'created'             => 'Producto creado satisfactoriamente.',
            'deleted'             => 'Producto eliminado satisfactoriamente.',
            'deleted_permanently' => 'Producto eliminado de forma permanente.',
            'restored'            => 'Producto restaurado satisfactoriamente.',
            'unconfirmed' => 'El Producto fue desconfirmado exitosamente',
            'updated'             => 'Producto actualizado satisfactoriamente.',
        ],


        'catalogs' => [


            'created'             => 'Registro de Catálogo creado satisfactoriamente.',
            'deleted'             => 'Registro de Catálogo eliminado satisfactoriamente.',
            'deleted_permanently' => 'Registro de Catálogo eliminado de forma permanente.',
            'restored'            => 'Registro de Catálogo restaurado satisfactoriamente.',
            'updated'             => 'Registro de Catálogo actualizado satisfactoriamente.',

        ],
    ],

    'frontend' => [
        'user' => [
            'missing_information' => 'Tu información está incompleta. Debes completarla',
        ],
        'contact' => [
            'sent' => 'Tu información fue enviada exitosamente. Responderemos tan pronto sea posible al e-mail que proporcionaste.',
        ],
        'products' => [

            'confirmed'              => 'El producto fue confirmado exitosamente.',
            'created'             => 'Producto creado satisfactoriamente.',
            'deleted'             => 'Producto eliminado satisfactoriamente.',
            'deleted_permanently' => 'Producto eliminado de forma permanente.',
            'restored'            => 'Producto restaurado satisfactoriamente.',
            'unconfirmed' => 'El Producto fue desconfirmado exitosamente',
            'updated_now_upload_pictures'             => 'Producto actualizado satisfactoriamente. Sólo falta subir las fotos del producto. Ud. debe subir ambas fotos para que su producto sea revisado.',
            'sent_for_approval'             => 'Producto actualizado satisfactoriamente y ha sido enviado para ser revisado por el departamento correspondiente.',
        ],
    ],
];
