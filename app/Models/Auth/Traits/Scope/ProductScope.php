<?php

namespace App\Models\Auth\Traits\Scope;

/**
 * Class ProductScope.
 */
trait ProductScope
{
    /**
     * @param $query
     * @param bool $user_id
     *
     * @return mixed
     */
    public function scopeUserId($query, $user_id = true)
    {
        return $query->where('user_id', $user_id);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeVerified($query)
    {
        return $query->whereNotNull('payment_verified_at');
    }



}