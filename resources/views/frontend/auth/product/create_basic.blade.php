@extends('frontend.layouts.app2')

@section('content')


<div class="container">
  <div class="py-1 text-center">
    <h2>CREACION DE PRODUCTO NO CATALOGADO</h2>
    <!--<p class="lead"></p>-->
  </div>

  <div class="row">
    <div class="col-md-4 order-md-2 mb-4">
    </div>

    <div class="col-md-12 mb-4">
     
        {{ html()->form('POST', route('frontend.auth.product.store'))->class('form-horizontal needs-validation')->attributes(['id'=>'form-create-product','novalidate'=>'novalidate'])->open() }}
        <hr class="mb-4">
        <h4 class="mb-3">Clasificacion General de Productos</h4>
        

        <div class="row">

        
            <div class="col-sm-5">
                              <label class="form-control-label">Asigne un Estatus</label>
                              <select class="custom-select d-block w-100" name="product_material_preset_id" id="product_material_preset_id">
                                <option value="" selected disabled>Por favor escoja una opción</option>
                                <option value="0">CREADO</option>
                                <option value="1">EN REVISION</option>
                                <option value="1">CATALOGADO</option>
                              </select>
                              <small class="text-muted"></small>
                          <div class="invalid-feedback"></div>
            </div>
            <div class="col-sm-2">
            </div>

        </div>
        

        <hr class="mb-4">


        <div class="row">
          <div class="col-md-5 mb-3">
            {{ html()->label(__('labels.products.form.label.product_name'))->for('product_name')}}
            {{ html()->text('product_name')
                                    ->class('form-control')
                                    ->attribute('maxlength', 100)
                                    ->required()
                                    ->autofocus() }}
            <small class="text-muted">@lang('labels.products.form.helptext.product_name')</small>
            <div class="invalid-feedback">
            @lang('validation.required', ['attribute' => __('labels.products.form.label.product_name')])
            {{ $errors->first('name') }} <!--TODO add backend errors logic to show errors in each field-->
            </div>
          </div>
          <div class="col-md-7 mb-3">
            {{ html()->label(__('labels.products.form.label.description'))->for('description')}}
            {{ html()->textarea('description')
                                      ->class('form-control')
                                      ->attribute('maxlength', 255)
                                      ->required()}}
            <small class="text-muted">@lang('labels.products.form.helptext.description')</small>
            <div class="invalid-feedback">
            @lang('validation.required', ['attribute' => __('labels.products.form.label.description')])
            </div>
          </div>
        </div>

        <div class="row">
          
          <div class="col-md-4 mb-3">
            {{ html()->label('Marca')->class('form-control-label')->for('brand_id') }}
            {{ html()->select('brand_id',$brands)
                                    ->class('custom-select')
                                    ->required() }}
            <small class="text-muted">@lang('labels.products.form.helptext.brand_id')</small>
            <div class="invalid-feedback">
            @lang('validation.required', ['attribute' => __('labels.products.form.label.brand_id')]).
            </div>
          </div>

          <div class="col-md-4 mb-3">

              {{ html()->label(__('labels.products.form.label.store_id'))->class('form-control-label') }}
              {{ html()->select('store_id',$stores)
                          ->class('custom-select d-block w-100')
                          ->required() }}
              <small class="text-muted"></small>
              <div class="invalid-feedback">
              {{ $errors->first('store_id') }}
              </div>

          </div>

          <div class="col-md-4 mb-3">

          </div>

         </div>


         <div class="row">
          


          <div class="col-md-6 mb-3">
              {{ html()->label(__('CODIGO DE BARRA - GTIN (EAN/UPC)'))->class('form-control-label')->for('gtin_1') }}

              {{ html()->text('gtin_1')
                      ->class('form-control')
                      ->attribute('maxlength', 13)
                      ->required() }}

          </div>
          <div class="col-md-4 mb-3">
          {{ html()->label(__('labels.products.form.label.base_measurement_unit_id'))->class('form-control-label') }}
                            {{ html()->select('base_measurement_unit_id',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->required() }}
                            <small class="text-muted">@lang('labels.products.form.helptext.base_measurement_unit_id')</small>
                        <div class="invalid-feedback">
                        {{ $errors->first('base_measurement_unit_id') }}
                        </div>
          </div>
          <div class="col-md-2 mb-3">

          </div>


         </div>


        <hr class="mb-4">

        <h4 class="mb-3">Fotos del Producto</h4>
        <div class="row"> <!--images-->
        

        <div class="col-sm-3">
                <div class="card">
                          <img class="card-img-top" id="preview_image_front_1" name="preview_image_front_1"  src="{{ asset('img/products/camera_icon.png') }}" alt="@lang('labels.products.form.alt.select_front_image')">
                          <div class="card-body">
                              <h5 class="card-title">Imagen Frontal</h5>
                              <p class="card-text"></p>
                              <div class="custom-file">
                                  {{ html()->file('image_front_1')
                                      ->class('custom-file-input')
                                          }}
                                  <label class="custom-file-label" for="image_front_1">Seleccione...</label>
                              </div>
                          </div>
                </div>
        </div>
        <div class="col-sm-3">        
                <div class="card">
                        <img class="card-img-top" id="preview_image_back_1" name="preview_image_back_1" src="{{ asset('img/products/camera_icon.png') }}" alt="@lang('labels.products.form.alt.select_back_image')">
                        <div class="card-body">
                              <h5 class="card-title">Imagen Posterior</h5>
                              <p class="card-text"></p>
                              <div class="custom-file">
                                  {{ html()->file('image_back_1')
                                      ->class('custom-file-input')
                                          }}
                                  <label class="custom-file-label" for="image_back_1">Seleccione...</label>
                              </div>
                        </div>
                </div>
        </div>
      </div><!--images-->

        <hr class="mb-4">
        <!--
        <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.auth.product.index'), __('buttons.general.cancel')) }}
                    </div>

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div>
         </div>
         -->
        <button class="btn btn-primary btn-lg btn-block" id="btnSubmit" type="button">Crear el Producto</button>
      </form>
    </div>
  </div>


</div>



@endsection
@push('after-scripts')
<script> 

$(document).ready(function(){

  var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

  $('#discount_start_date').datepicker({

            format: '{{ config('app.date_format_js') }}' ,
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fortawesome',
            minDate: today,
            maxDate: function () {
                return $('#discount_end_date').val();
            }
          });
  $('#discount_end_date').datepicker({
    format: '{{ config('app.date_format_js') }}',
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fortawesome',
            minDate: function () {
                return $('#discount_start_date').val();
            } 
  });


  //$(".select2").select2();
  $('[data-toggle="tooltip"]').tooltip();

  $('#collapseInner').on('show.bs.collapse', function( event ) {
    event.stopPropagation();
  })
  $('#collapseInner').on('hide.bs.collapse', function( event ) {
      event.stopPropagation();
  })

  $('#btnSubmit').on("click", validateForm);

  function readURL(input,previewId) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $(previewId).attr('src', e.target.result);

                    }
                    reader.readAsDataURL(input.files[0]);
                }
        }

        $("#image_front_1").change(function(){
           readURL(this, '#preview_image_front_1');
        });

        $("#image_back_1").change(function(){
           readURL(this, '#preview_image_back_1');
        });


});


function validateForm() {
   
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName("needs-validation");
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.classList.add("was-validated");
    });

    let product_name = document.querySelector("#product_name").checkValidity();
    let description = document.querySelector("#description").checkValidity();
    let price = document.querySelector("#price").checkValidity();
    let suggested_price = document.querySelector("#suggested_price").checkValidity();
    let min_remain_shelf_life = document.querySelector("#min_remain_shelf_life").checkValidity();
    let old_material_number = document.querySelector("#old_material_number").checkValidity();
    let vendor_material_number = document.querySelector("#vendor_material_number").checkValidity();
    let purchase_tax_code_id = document.querySelector("#purchase_tax_code_id").checkValidity();
    //let material_tax_type_id = document.querySelector("#material_tax_type_id").checkValidity();
    let iva_code_id = document.querySelector("#iva_code_id").checkValidity();

    let no_errors  = product_name && description && price && suggested_price && min_remain_shelf_life && old_material_number && vendor_material_number &&  purchase_tax_code_id  && iva_code_id;
    
    //if there is a discount, validate value
    let no_discount_errors = true;
    if($('#ind_include_discount').is(':checked')){
        no_discount_errors = document.querySelector("#discount_percentage").checkValidity();
    }
    //if there is a limited discount, validate start and end date
    let no_date_errors = true;
    if($('#ind_limited_time_discount').is(':checked')){
      let discount_start_date = document.querySelector("#discount_start_date").checkValidity();
      let discount_end_date = document.querySelector("#discount_end_date").checkValidity();
      if(discount_start_date && discount_end_date){
        let tmp_discount_start_date = document.querySelector("#discount_start_date");
        //alert(tmp_discount_start_date);
      }else {
        no_date_errors = false;
      }
    }


    if (no_errors && no_discount_errors && no_date_errors) {
      $('#form-create-product').submit();
      $(this).attr('disabled','disabled');
    }else {

          //var formElements = document.getElementById('form-create-product').querySelectorAll('textarea, input:not([type=hidden])');
          var errorElements = document.getElementById('form-create-product').querySelectorAll('input.form-control:invalid');
          if(errorElements.length > 0){
            //console.log(errorElements);
            //var scrollTop = $(window).scrollTop();
            var elementOffset = $(errorElements[0]).offset().top - 100;
            //console.log(elementOffset);
            //var currentElementOffset = (elementOffset - scrollTop);
            //console.log(currentElementOffset);
            $('html, body').animate({
                scrollTop: elementOffset
              }, 2000, function() {
                $(errorElements[0]).focus();
            });
          }


    }
  }

</script>  
@endpush