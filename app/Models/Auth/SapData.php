<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\Traits\Relationship\SapDataRelationship;

class SapData extends Model
{
    use SapDataRelationship;

        /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sap_data';

      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sap_account_number'
    ];



}
