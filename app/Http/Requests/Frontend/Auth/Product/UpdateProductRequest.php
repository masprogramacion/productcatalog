<?php

namespace App\Http\Requests\Frontend\Auth\Product;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateProductRequest.
 */
class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return $this->user()->isAdmin();
         //TODO this can be used by a regular user as well
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required',
            'description'  => 'required|max:40',
            'gtin_1'  => 'required|max:13',
            //'lower_level_pack_hierarchy_measurement_unit_id_1'  => 'required|numeric',
            'ind_measurement_unit_is_order_unit_1'  => 'required',
            'gtin_2'  => 'someimes|max:13',
            //'lower_level_pack_hierarchy_measurement_unit_id_2'  => 'required|numeric',
            'gtin_3'  => 'sometimes|max:13',
            'min_remain_shelf_life'  => 'required|numeric',
            'old_material_number'  => 'max:191',
            'vendor_material_number'  => 'max:191',
            'price' => 'required|numeric',
            //'order_price_measurement_unit_id' => 'required',
            'image_original' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
}
