<?php

use App\Helpers\General\Timezone;
use App\Helpers\General\HtmlHelper;

/*
 * Global helpers file with misc functions.
 */
if (! function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (! function_exists('gravatar')) {
    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (! function_exists('timezone')) {
    /**
     * Access the timezone helper.
     */
    function timezone()
    {
        return resolve(Timezone::class);
    }
}

if (! function_exists('include_route_files')) {

    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function include_route_files($folder)
    {
        try {
            $rdi = new recursiveDirectoryIterator($folder);
            $it = new recursiveIteratorIterator($rdi);

            while ($it->valid()) {
                if (! $it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
                    require $it->key();
                }

                $it->next();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

if (! function_exists('home_route')) {

    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function home_route()
    {
       
        //TODO CHECK FOR EXPIRED SESSION ERROR  NOTE: THIS MUST BE FIXED WHEN THE GUARD/MIDDLEWARE IS ADDED
        // if (!Auth::check()) {
			
        //     Session::flash('message', trans('errors.session_label'));
        //     Session::flash('type', 'warning');
          
        //     return redirect()->route('home');
          
        //   }
        //dd(auth()->user());
        if (auth()->check()) { // checks for guard 'web'
            //if (auth()->user()->can('view backend')) {
            if(auth()->user()->hasRole('proveedor')) {   
                return 'frontend.user.dashboard';
            } else {
                return 'admin.dashboard';
            }
        }

        //TODO: FOR LDAP LOGIN IMPLEMENTATION
        /*
        if (auth('admin')->check()) {  //check for authenticated user in 
            if (auth('admin')->user()->hasPermissionTo('view backend')) {
                return 'admin.dashboard';
            } else {
                return 'frontend.user.dashboard';
            }
        }
        */

        return 'frontend.index';
    }
}

if (! function_exists('style')) {

    /**
     * @param       $url
     * @param array $attributes
     * @param null  $secure
     *
     * @return mixed
     */
    function style($url, $attributes = [], $secure = null)
    {
        return resolve(HtmlHelper::class)->style($url, $attributes, $secure);
    }
}

if (! function_exists('script')) {

    /**
     * @param       $url
     * @param array $attributes
     * @param null  $secure
     *
     * @return mixed
     */
    function script($url, $attributes = [], $secure = null)
    {
        return resolve(HtmlHelper::class)->script($url, $attributes, $secure);
    }
}

if (! function_exists('form_cancel')) {

    /**
     * @param        $cancel_to
     * @param        $title
     * @param string $classes
     *
     * @return mixed
     */
    function form_cancel($cancel_to, $title, $classes = 'btn btn-danger btn-sm')
    {
        return resolve(HtmlHelper::class)->formCancel($cancel_to, $title, $classes);
    }
}

if (! function_exists('form_submit')) {

    /**
     * @param        $title
     * @param string $classes
     *
     * @return mixed
     */
    function form_submit($title, $classes = 'btn btn-success btn-sm pull-right')
    {
        return resolve(HtmlHelper::class)->formSubmit($title, $classes);
    }
}

if (! function_exists('camelcase_to_word')) {

    /**
     * @param $str
     *
     * @return string
     */
    function camelcase_to_word($str)
    {
        return implode(' ', preg_split('/
          (?<=[a-z])
          (?=[A-Z])
        | (?<=[A-Z])
          (?=[A-Z][a-z])
        /x', $str));
    }
}


if (! function_exists('get_gtin_type')) {

    /** TODO REPLACE FUNCTION WITH DATABASE VALUES
     * @param $str
     * @param $is_internal
     *
     * @return mixed
     */
    function get_gtin_type($str)
    {
        $result = ['id'=>'1','code'=>'VC', 'name'=>'Interno']; // for default case
        
        $str_len = strlen($str);

        if($str_len == 0){
            return $result;
        }else {

            if($str_len == 13) {
                $result = ['id'=>'2','code'=>'HE', 'name'=>'EAN de 13 caracteres'];
            }else{
                $result = ['id'=>'3','code'=>'UC', 'name'=>'EAN distinto a 13 caracteres'];
            }
            return $result;
        }
        
    }
}
