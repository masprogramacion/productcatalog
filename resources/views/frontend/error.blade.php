@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <i class="fas fa-home"></i> @lang('navs.general.home')
                </div>
                <div class="card-body">
                    Se ha producido un error. Nuestro soporte tecnico ha sido informado. 
                </div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->

@endsection
