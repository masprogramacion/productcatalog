{{ html()->modelForm($logged_in_user, 'PATCH', route('frontend.user.info.update.general'))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}

    <div class="row">
        <div class="col">
            <div class="form-group">
                {{ html()->label(__('labels.frontend.user.info.tabs.content.general.name'))->for('name') }}
                {{ html()->text('name')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.name'))
                    ->attribute('maxlength', 191)
                    ->autofocus()
                    ->required() }}
            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->

    <div class="row">
        <div class="col">
            <div class="form-group">
                {{ html()->label(__('labels.frontend.user.info.tabs.content.general.first_name'))->for('first_name') }}

                {{ html()->text('first_name')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.first_name'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->

    <div class="row">
        <div class="col">
            <div class="form-group">
                {{ html()->label(__('labels.frontend.user.info.tabs.content.general.last_name'))->for('last_name') }}

                {{ html()->text('last_name')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.last_name'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->

    @if ($logged_in_user->canChangeEmail())
        <div class="row">
            <div class="col">
                <div class="alert alert-info">
                    <i class="fas fa-info-circle"></i> @lang('strings.frontend.user.change_email_notice')
                </div>

                <div class="form-group">
                    {{ html()->label(__('labels.frontend.user.info.tabs.content.general.email'))->for('email') }}
                    {{ html()->email('email')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.frontend.email'))
                        ->attribute('maxlength', 191)
                        ->required() }}
                </div><!--form-group-->
            </div><!--col-->
        </div><!--row-->
    @endif

    @if ($logged_in_user->operationsDocument)
    <div class="row">
            <div class="col">
                <div class="form-group">
                <a href="{{ route('frontend.user.info.documents.download', $logged_in_user->operationsDocument->uuid) }}?rand={{sha1(time())}}" class="btn btn-primary active" role="button" aria-pressed="true"><i class="fa fa-download"></i> @lang('labels.frontend.user.info.tabs.content.general.download')</a> @lang('labels.frontend.user.info.tabs.content.general.file_name') {{$logged_in_user->operationsDocument->title}}
                </div><!--form-group-->
            </div><!--col-->
    </div><!--row-->
    <div class="row">
            <div class="col">
                <div class="form-group">
                {{ html()->label(__('labels.frontend.user.info.tabs.content.general.operations_document'))->for('operations_document') }}
                <div class="custom-file">
                        {{ html()->file('operations_document')
                            ->class('custom-file-input') }}
                        {{ html()->label(__('validation.attributes.frontend.operations_document'))->for('operations_document')->class('custom-file-label') }}
                    </div>
                </div><!--form-group-->
            </div><!--col-->
    </div><!--row-->
    @else
    <div class="row">
            <div class="col">
                <div class="form-group">
                {{ html()->label(__('labels.frontend.user.info.tabs.content.general.operations_document'))->for('operations_document') }}
                <div class="custom-file">
                        {{ html()->file('operations_document')
                            ->class('custom-file-input')
                            ->required() }}
                        {{ html()->label(__('validation.attributes.frontend.operations_document'))->for('operations_document')->class('custom-file-label') }}
                    </div>
                </div><!--form-group-->
            </div><!--col-->
    </div><!--row-->
    @endif



    <div class="row">
        <div class="col">
            <div class="form-group mb-0 clearfix">
                {{ form_submit(__('labels.general.buttons.update')) }}
            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->
{{ html()->closeModelForm() }}