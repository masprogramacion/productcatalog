{{ html()->modelForm($product, 'PATCH', route('admin.auth.product.update.images', $product->id))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-images-product','novalidate'=>'novalidate','enctype'=>'multipart/form-data'])->open() }}

<div class="col">


  <hr class="mb-4">
  <h4 class="mb-3">Imagenes</h4>

    <div class="col-sm-12">
        <div class="row"> <!--images-->

            <div class="col-sm-3">
                    <div class="card">
                                <img class="card-img-top" id="preview_image_front_1" name="preview_image_front_1"  src="{{ $product->imageFront }}" alt="@lang('labels.products.form.alt.select_front_image')">
                                <div class="card-body">
                                    <h5 class="card-title">Imagen Frontal</h5>
                                    <p class="card-text"></p>
                                    <div class="custom-file">
                                        {{ html()->file('image_front_1')
                                            ->class('custom-file-input')
                                                }}
                                        <label class="custom-file-label" for="image_front_1">Seleccione...</label>
                                    </div>
                                </div>
                    </div>
            </div>
            <div class="col-sm-3">        
                    <div class="card">
                            <img class="card-img-top" id="preview_image_back_1" name="preview_image_back_1" src="{{ $product->imageBack  }}" alt="@lang('labels.products.form.alt.select_back_image')">
                            <div class="card-body">
                                    <h5 class="card-title">Imagen Posterior</h5>
                                    <p class="card-text"></p>
                                    <div class="custom-file">
                                        {{ html()->file('image_back_1')
                                            ->class('custom-file-input')
                                                }}
                                        <label class="custom-file-label" for="image_back_1">Seleccione...</label>
                                    </div>
                            </div>
                    </div>
            </div>
        </div><!--images-->
    </div>
    
    <div class="card-footer clearfix">
                <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.product.index'), __('buttons.general.goback')) }}
                </div><!--col-->

                <div class="col text-right">
                    <button class="btn btn-success btn-sm pull-right" id="btnSubmitImages" type="button">@lang('buttons.general.crud.update')</button>
                </div><!--row-->
            </div><!--row-->
    </div><!--card-footer-->


</div><!--table-responsive-->
{{ html()->closeModelForm() }}