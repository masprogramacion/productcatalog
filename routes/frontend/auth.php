<?php

use App\Http\Controllers\Frontend\Auth\LoginController;
use App\Http\Controllers\Frontend\Auth\AdminLoginController;
use App\Http\Controllers\Frontend\Auth\RegisterController;
use App\Http\Controllers\Frontend\Auth\SocialLoginController;
use App\Http\Controllers\Frontend\Auth\ResetPasswordController;
use App\Http\Controllers\Frontend\Auth\ConfirmAccountController;
use App\Http\Controllers\Frontend\Auth\ForgotPasswordController;
use App\Http\Controllers\Frontend\Auth\UpdatePasswordController;
use App\Http\Controllers\Frontend\Auth\PasswordExpiredController;
use App\Http\Controllers\Frontend\Auth\Product\ProductController;
use App\Http\Controllers\Frontend\Auth\Product\ProductStatusController;

/*
 * Frontend Access Controllers
 * All route names are prefixed with 'frontend.auth'.
 */
Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function () {

    /*
    * These routes require the user to be logged in
    */
    Route::group(['middleware' => 'auth'], function () {
        Route::get('logout', [LoginController::class, 'logout'])->name('logout');

        //For when admin is logged in as user from backend
        Route::get('logout-as', [LoginController::class, 'logoutAs'])->name('logout-as');

        // These routes can not be hit if the password is expired
        Route::group(['middleware' => 'password_expires'], function () {
            // Change Password Routes
            Route::get('password', [UpdatePasswordController::class, 'index'])->name('password.index');
            Route::patch('password/update', [UpdatePasswordController::class, 'update'])->name('password.update');
        });



        /*
         * Product Show Deleted'
         */
        Route::get('product/deleted', [ProductStatusController::class, 'getDeleted'])->name('product.deleted');



        /*
         * Product crud
         */
        Route::get('product', [ProductController::class, 'index'])->name('product.index');


        Route::group(['middleware' => 'prevent-back-history'], function () {
            Route::get('product/create', [ProductController::class, 'create'])->name('product.create');
            Route::get('product/create/{product}/step2/', [ProductController::class, 'create_step2'])->name('product.create.step2');
            Route::get('product/create/{product}/step3/', [ProductController::class, 'create_step3'])->name('product.create.step3');
            Route::patch('product/create/{product}/step2/', [ProductController::class, 'update_step2'])->name('product.update.step2');
            Route::patch('product/create/{product}/step3/', [ProductController::class, 'update_step3'])->name('product.update.step3');
        });

        Route::post('product', [ProductController::class, 'store'])->name('product.store');
        Route::post('product/search', [ProductController::class, 'search'])->name('product.search');
        Route::get('product/search', [ProductController::class, 'search'])->name('product.search');
        Route::get('product/message', [ProductController::class, 'view_messages'])->name('product.message.index');
        Route::get('product/message/{message}/show', [ProductController::class, 'show_message'])->name('product.message.show');
        
        Route::group(['prefix' => 'product/{product}'], function () {
            Route::get('/', [ProductController::class, 'show'])->name('product.show');
            Route::get('edit', [ProductController::class, 'edit'])->name('product.edit');


            Route::delete('/', [ProductController::class, 'destroy'])->name('product.destroy');
            Route::patch('general/', [ProductController::class, 'update_general'])->name('product.update.general');
            Route::patch('packaging/', [ProductController::class, 'update_packaging'])->name('product.update.packaging');
            Route::patch('images/', [ProductController::class, 'update_images'])->name('product.update.images');
            Route::patch('resend/', [ProductController::class, 'resend'])->name('product.resend');

            // Delete
            Route::get('delete', [ProductStatusController::class, 'delete'])->name('product.delete-permanently');
            // Restore
            Route::get('restore', [ProductStatusController::class, 'restore'])->name('product.restore');

        });

        Route::group(['prefix' => 'product/images'], function () {
            Route::post('/', [ProductController::class, 'imagesupdate'])->name('product.imagesupdate');

        });


        // Password expired routes
        if (is_numeric(config('access.users.password_expires_days'))) {
            Route::get('password/expired', [PasswordExpiredController::class, 'expired'])->name('password.expired');
            Route::patch('password/expired', [PasswordExpiredController::class, 'update'])->name('password.expired.update');
        }
    });

    /*
     * These routes require no user to be logged in
     */
    Route::group(['middleware' => 'guest'], function () {
        // Authentication Routes
        Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
        Route::post('login', [LoginController::class, 'login'])->name('login.post');
   
        // // Socialite Routes
        // Route::get('login/{provider}', [SocialLoginController::class, 'login'])->name('social.login');
        // Route::get('login/{provider}/callback', [SocialLoginController::class, 'login']);

        // Registration Routes
        if (config('access.registration')) {
            Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
            Route::post('register', [RegisterController::class, 'register'])->name('register.post');
        }

        // Confirm Account Routes
        Route::get('account/confirm/{token}', [ConfirmAccountController::class, 'confirm'])->name('account.confirm');
        Route::get('account/confirm/resend/{uuid}', [ConfirmAccountController::class, 'sendConfirmationEmail'])->name('account.confirm.resend');

        // Password Reset Routes
        Route::get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.email');
        Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email.post');

        Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset.form');
        Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.reset');
    });


    /*
     * These routes require no user to be logged in using the 'admin' guard TODO FOR LDAP GUARD
     */

    // Route::group(['middleware' => 'guest:admin'], function () {
    //     // Authentication Routes
    //     Route::get('superadmin-login', [AdminLoginController::class, 'showLoginForm'])->name('superadmin.login');
    //     Route::post('superadmin-login', [AdminLoginController::class, 'login'])->name('superadmin.login.post');
       
    // });



    // Route::group(['middleware' => 'auth:admin'], function () {
    //     Route::get('superadmin-logout', [AdminLoginController::class, 'logout'])->name('superadmin.logout');

    //     //For when admin is logged in as user from backend
    //     Route::get('superadmin-logout-as', [AdminLoginController::class, 'logoutAs'])->name('superadmin.logout-as');

    // });


});






