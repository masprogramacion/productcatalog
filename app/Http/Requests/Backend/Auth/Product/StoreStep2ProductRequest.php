<?php

namespace App\Http\Requests\Backend\Auth\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Models\Auth\Product;
use Illuminate\Http\Request;

/**
 * Class StoreStep2ProductRequest.
 */
class StoreStep2ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //dd('here1');
        return true;
         //TODO this can be used by a regular user as well
        //return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        Validator::extend('custom_gtin_conflict', function($attribute, $value, $parameters) 
        {
            $gtin_exists = 0;

            if(strlen($value)>0){

                if (Product::where('gtin_1', '=', $value)->where('id', '<>', $parameters[0])->exists()) {
                    $gtin_exists++;
                }
    
                if (Product::where('gtin_2', '=', $value)->where('id','<>', $parameters[0])->exists()) {
                    $gtin_exists++;
                }
                if (Product::where('gtin_3', '=', $value)->where('id','<>', $parameters[0])->exists()) {
                    $gtin_exists++;
                }
            }

            return ($gtin_exists==0);

        }); 


        return [
            'gtin_1'  => 'required|max:13|custom_gtin_conflict:'.$this->route('product')->id,
            'gtin_2'  => 'sometimes|max:13|custom_gtin_conflict:'.$this->route('product')->id,
            'gtin_3'  => 'sometimes|max:13|custom_gtin_conflict:'.$this->route('product')->id,
            'lower_level_measurement_unit_qty_1'  => 'required|numeric|min:1|max:10000',
            'lower_level_measurement_unit_qty_2'  => 'sometimes|numeric|min:1|max:10000',
            'lower_level_measurement_unit_qty_3'  => 'sometimes|numeric|min:1|max:10000',
        ];
    }

    public function messages()
    {
        return [
            'custom_gtin_conflict' => 'Codigo de Barra GTIN (EAN/UPC) ya existe en el sistema',
        ];
    }
}
