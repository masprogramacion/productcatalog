@extends('backend.layouts.app')

@section('title', __('labels.backend.access.products.management') . ' | ' . __('labels.backend.access.products.create'))

@section('content')


<div class="container-fluid">
  <ul class="list-unstyled multi-steps">
    <li>@lang('labels.products.create.step1')</li>
    <li>@lang('labels.products.create.step2')</li>
    <li>@lang('labels.products.create.step3')</li>
  </ul>
</div>

<!--
<ul class="progress-indicator">
            <li class="completed">
                <span class="bubble"></span>
                <i class="fa fa-check-circle"></i>
                PASO 1
            </li>
            <li class="completed">
                <span class="bubble"></span>
                <i class="fa fa-check-circle"></i>
                PASO 2
            </li>
            <li class="completed">
                <span class="bubble"></span>
                PASO 3
            </li>
</ul>
-->

<div class="container">

  <div class="py-5 text-center">
    <h2>PRODUCTO ENVIADO!!</h2>
    <!--<p class="lead">Debe completar todos los campos requeridos.</p>-->
  </div>
  <div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Bien hecho!</h4>
  <p>CUALES SON LOS SIGUIENTES PASOS?</p>
  <hr>
  <p class="mb-0">@lang('labels.products.create.alert_success_product_submitted')</p>
  </div>
  <button class="btn btn-primary btn-lg btn-block" type="submit">Regresar</button>
  

  <hr class="mb-4">
</div><!--container-->



@endsection
@push('after-scripts')
 <script> 

      

</script>  
@endpush
