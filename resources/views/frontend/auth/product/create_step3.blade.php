@extends('frontend.layouts.app2')

@section('content')


@section('content')


<div class="container-fluid">
  <ul class="list-unstyled multi-steps">
    <li>PASO 1</li>
    <li>PASO 2</li>
    <li class="is-active">PASO 3</li>
  </ul>
</div>

<!--
<ul class="progress-indicator">
            <li class="completed">
                <span class="bubble"></span>
                <i class="fa fa-check-circle"></i>
                PASO 1
            </li>
            <li class="completed">
                <span class="bubble"></span>
                <i class="fa fa-check-circle"></i>
                PASO 2
            </li>
            <li class="completed">
                <span class="bubble"></span>
                PASO 3
            </li>
</ul>
-->

<div class="container">

  <div class="py-5 text-center">
    <h2>PASO 3 DE 3</h2>
    <!--<p class="lead">Debe completar todos los campos requeridos.</p>-->
  </div>

  {{ html()->modelForm($product, 'PATCH', route('frontend.auth.product.update.step3', $product))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-images-product','novalidate'=>'novalidate','enctype'=>'multipart/form-data'])->open() }}
  <hr class="mb-4">

  <h4 class="mb-3">Fotos del Producto</h4>
  <div class="row"> <!--images-->
  

  <div class="col-sm-3">
          <div class="card">
                    <img class="card-img-top" id="preview_image_front_1" name="preview_image_front_1"  src="{{ asset('img/products/camera_icon.png') }}" alt="@lang('labels.products.form.alt.select_front_image')">
                    <div class="card-body">
                        <h5 class="card-title">Imagen Frontal (Maximo 2MB)</h5>
                        <p class="card-text"></p>
                        <div class="custom-file">
                            {{ html()->file('image_front_1')
                                ->class('custom-file-input')
                                ->required()
                                    }}
                            <label class="custom-file-label" for="image_front_1">Seleccione...</label>
                            <div class="invalid-feedback">Este campo es obligatorio</div>
                        </div>
                        
                    </div>
          </div>
  </div>
  <div class="col-sm-3">        
          <div class="card">
                  <img class="card-img-top" id="preview_image_back_1" name="preview_image_back_1" src="{{ asset('img/products/camera_icon.png') }}" alt="@lang('labels.products.form.alt.select_back_image')">
                  <div class="card-body">
                        <h5 class="card-title">Imagen Posterior (Maximo 2MB y debe ser visible el codigo de barra en la foto)</h5>
                        <p class="card-text"></p>
                        <div class="custom-file">
                            {{ html()->file('image_back_1')
                                ->class('custom-file-input')
                                ->required()
                                    }}
                            <label class="custom-file-label" for="image_back_1">Seleccione...</label>
                            <div class="invalid-feedback">Este campo es obligatorio</div>
                        </div>
                        
                  </div>
          </div>
  </div>
</div><!--images-->

  <button class="btn btn-primary btn-lg btn-block" id="btnSubmit" type="button">Enviar el Producto para su Revisión</button>
  
  {{ html()->form()->close() }}
  <hr class="mb-4">
</div><!--container-->



@endsection
@push('after-scripts')
<script> 


$('[data-toggle="tooltip"]').tooltip();

$('#btnSubmit').on("click", validateForm);

$('form').submit(function(e) {
    $(':disabled').each(function(e) {
        $(this).removeAttr('disabled');
    })
});


function validateForm() {


// Fetch all the forms we want to apply custom Bootstrap validation styles to
var forms = document.getElementsByClassName("needs-validation");
// Loop over them and prevent submission
var validation = Array.prototype.filter.call(forms, function(form) {
  form.classList.add("was-validated");
});

let image_front_1 = document.querySelector('#image_front_1').checkValidity();
let image_back_1 = document.querySelector('#image_back_1').checkValidity();
//alert(image_front_1);



if (image_front_1 && image_back_1) {
    console.log('valid');
    $('#form-update-images-product *').filter(':input').each(function(){
      $(this).prop('disabled', false);
    });
    $('#form-update-images-product').submit();
    $(this).attr('disabled','disabled');

}else {

  var errorElements = document.getElementById('form-update-images-product').querySelectorAll('input.form-control:invalid');

  if(errorElements.length > 0){
      //console.log(errorElements);
      //var scrollTop = $(window).scrollTop();
      var elementOffset = $(errorElements[0]).offset().top - 100;
      //console.log(elementOffset);
      //var currentElementOffset = (elementOffset - scrollTop);
      //console.log(currentElementOffset);
      $('html, body').animate({
          scrollTop: elementOffset
        }, 2000, function() {
          $(errorElements[0]).focus();
      });
    }



}
}

function readURL(input,previewId) {
    if (input.files && input.files[0]) {

        var a=(input.files[0].size);

        if(a/(1024*1024) > 2){ // make it in MB so divide by 1024*1024
            alert('La imagen debe ser menor a 2 MB');
            return false;
        }


        var reader = new FileReader();
        
        reader.onload = function (e) {
            $(previewId).attr('src', e.target.result);

        }
        reader.readAsDataURL(input.files[0]);
    }
  }

$("#image_front_1").change(function(){
    readURL(this, '#preview_image_front_1');
});

$("#image_back_1").change(function(){
    readURL(this, '#preview_image_back_1');
});


</script>  
@endpush
