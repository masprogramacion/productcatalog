<?php

namespace App\Models\Catalog\Traits\Attribute;

use Illuminate\Support\Facades\Hash;

/**
 * Trait BankAttribute.
 */
trait BankAttribute
{

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        //return '<input type="button" value="Update" class="btn btn-sm btn-danger update" data-id="'.$this->id.'" >';
        //<button class="btn btn-light" type="button"><span class="fas fa-edit"></span></button>
        return '<button class="btn btn-brand btn-success update" data-id="'.  $this->id .'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'"><i class="fa fa-edit"></i></button>';
        //return '<a href="'.route('admin.auth.catalog.bank.update', $this). '" data-id="'.  $this->id .'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        
        return '<a href="'.route('admin.auth.catalog.bank.destroy', $this).'"
                data-method="delete"
                data-trans-button-cancel="'.__('buttons.general.cancel').'"
                data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                data-trans-title="'.__('strings.backend.general.are_you_sure').'"
                class="dropdown-item">'.__('buttons.general.crud.delete').'</a> ';


    }

    /**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="'.route('admin.auth.catalog.bank.delete-permanently', $this).'" name="confirm_item" class="btn btn-danger"><i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="'.__('buttons.backend.access.users.delete_permanently').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="'.route('admin.auth.catalog.bank.restore', $this).'" name="confirm_item" class="btn btn-info"><i class="fas fa-sync" data-toggle="tooltip" data-placement="top" title="'.__('buttons.backend.access.users.restore_user').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return '
				<div class="btn-group" role="group" aria-label="'.__('labels.backend.access.users.user_actions').'">
				  '.$this->restore_button.'
				  '.$this->delete_permanently_button.'
				</div>';
        }

        return '
    	<div class="btn-group" role="group" aria-label="'.__('labels.backend.access.users.user_actions').'">
          '.$this->edit_button.'
          '.$this->delete_button.'
		</div>';
    }


}
