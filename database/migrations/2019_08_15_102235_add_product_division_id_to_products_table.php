<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductDivisionIdToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {

            // 1. Create new column
            // You probably want to make the new column nullable
            $table->integer('product_division_id')->unsigned()->nullable()->after('description');
            $table->integer('product_division_id_verified_by')->unsigned()->nullable()->after('description');
            $table->timestamp('product_division_id_verified_at')->nullable()->after('description');
            $table->integer('store_id')->unsigned()->nullable()->after('manufacturer_internal_code');

            // 2. Create foreign key constraints
            $table->foreign('product_division_id')->references('id')->on('product_divisions')->onDelete('SET NULL');
            $table->foreign('product_division_id_verified_by')->references('id')->on('users')->onDelete('SET NULL');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {

            // 1. Drop foreign key constraints
            $table->dropForeign(['product_division_id']);
            $table->dropForeign(['product_division_id_verified_by']);

            // 2. Drop the column
            $table->dropColumn('product_division_id');
            $table->dropColumn('product_division_id_verified_by');
            $table->dropColumn('product_division_id_verified_at');
            $table->dropColumn('store_id');

        });
    }
}
