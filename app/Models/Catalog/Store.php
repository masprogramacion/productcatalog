<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = ['name'];
}
