<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\Auth\WorkflowStatus;
use App\Models\Catalog\Brand;
use App\Models\Catalog\Manufacturer;
use App\Models\Catalog\Store;

/**
 * Class ProductRelationship.
 */

trait ProductRelationship
{

    /**
     * @return mixed
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
    */
    public function workflowStatus()
    {
        return $this->hasMany(WorkflowStatus::class);    
    }


        /**
     * @return mixed
    */
    public function store()
    {
        return $this->belongsToMany(Store::class, 'product_store');   
    }

}
