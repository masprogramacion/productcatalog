@if(Auth::user()->can('create products acompras') or Auth::user()->can('create products dmaestros'))
<div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">
    <a href="{{ route('admin.auth.product.create') }}" class="btn btn-success ml-1" data-toggle="tooltip" title="@lang('labels.general.create_new')"><i class="fas fa-plus-circle"></i> @lang('labels.general.create_new_product')</a>
</div><!--btn-toolbar-->
@endif

@if(Auth::user()->can('create uncatalogued products'))
<div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">
    <a href="{{ route('admin.auth.product.create.uncatalogued') }}" class="btn btn-success ml-1" data-toggle="tooltip" title="@lang('labels.general.create_new')"><i class="fas fa-plus-circle"></i> @lang('labels.general.create_new_product')</a>
</div><!--btn-toolbar-->
@endif
