<?php

namespace App\Listeners\Frontend\Auth;

use Carbon\Carbon;

/**
 * Class AdminEventListener.
 */
class AdminEventListener
{
    /**
     * @param $event
     */
    public function onLoggedIn($event)
    {
        $ip_address = request()->getClientIp();

        // Update the logging in admins time & IP
        $event->admin->fill([
            'last_login_at' => Carbon::now()->toDateTimeString(),
            'last_login_ip' => $ip_address,
        ]);

        // Update the timezone via IP address
        $geoip = geoip($ip_address);

        if ($event->admin->timezone !== $geoip['timezone']) {
            // Update the admins timezone
            $event->admin->fill([
                'timezone' => $geoip['timezone'],
            ]);
        }

        $event->admin->save();

        \Log::info('Admin Logged In: '.$event->admin->full_name);
    }

    /**
     * @param $event
     */
    public function onLoggedOut($event)
    {
        \Log::info('Admin Logged Out: '.$event->admin->full_name);
    }

    // /**
    //  * @param $event
    //  */
    // public function onRegistered($event)
    // {
    //     \Log::info('User Registered: '.$event->user->full_name);
    // }

    // public function onConfirmed($event)
    // {
    //     \Log::info('User Confirmed: '.$event->user->full_name);
    // }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Frontend\Auth\AdminLoggedIn::class,
            'App\Listeners\Frontend\Auth\AdminEventListener@onLoggedIn'
        );

        $events->listen(
            \App\Events\Frontend\Auth\AdminLoggedOut::class,
            'App\Listeners\Frontend\Auth\AdminEventListener@onLoggedOut'
        );

        // $events->listen(
        //     \App\Events\Frontend\Auth\UserRegistered::class,
        //     'App\Listeners\Frontend\Auth\UserEventListener@onRegistered'
        // );

        // $events->listen(
        //     \App\Events\Frontend\Auth\UserProviderRegistered::class,
        //     'App\Listeners\Frontend\Auth\UserEventListener@onProviderRegistered'
        // );

        // $events->listen(
        //     \App\Events\Frontend\Auth\UserConfirmed::class,
        //     'App\Listeners\Frontend\Auth\UserEventListener@onConfirmed'
        // );
    }
}
