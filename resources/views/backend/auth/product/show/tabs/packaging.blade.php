{{ html()->modelForm($product, 'PATCH', route('frontend.auth.product.update.packaging', $product->id))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-packaging-product','novalidate'=>'novalidate'])->open() }}

<div class="col">

    <div class="col-sm-12">
    <div class="card bg-light mb-3" >
              <div class="card-header"><h3>@lang('labels.products.title.measurement_unit')</h3>
                <div class="row">
                  <div class="col-sm-4">
                            {{ html()->label(__('labels.products.form.label.base_measurement_unit_id'))->class('form-control-label') }}
                            {{ html()->select('base_measurement_unit_id',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->attributes(['disabled'=>'disabled'])  }}
                            <small class="text-muted">@lang('labels.products.form.helptext.base_measurement_unit_id')</small>
                        <div class="invalid-feedback">
                        {{ $errors->first('base_measurement_unit_id') }}
                        </div>
                  </div>
                  <div class="col-sm-7"></div>
                  <div class="col-sm-1"></div>
                </div>
              </div>
              <div class="card-body"  id="sections">

              <div class="row" id="row_to_clone"><!--multiple fields row-->
                <div class="col-sm-12">
                  <div class="row"><!--1st row-->

                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.alt_measure_unit_stockkeep_unit_id'))->class('form-control-label') }}
                            {{ html()->select('alt_measure_unit_stockkeep_unit_id_1',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->attributes(['disabled'=>'disabled'])  }}

                            <small class="text-muted">@lang('labels.products.form.helptext.alt_measure_unit_stockkeep_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('alt_measure_unit_stockkeep_unit_id_1') }}
                            </div>
                    </div>
                    <div class="form-group col-sm-2">
                        {{ html()->label(__('labels.products.form.label.lower_level_measurement_unit_qty'))->class('form-control-label')}}
                        {{ html()->input('number','lower_level_measurement_unit_qty_1')->class('form-control')
                            ->attributes(['disabled'=>'disabled']) }}
                        <small class="text-muted">@lang('labels.products.form.helptext.lower_level_measurement_unit_qty')</small>
                          <div class="invalid-feedback">
                          {{ $errors->first('lower_level_measurement_unit_qty_1') }}
                          </div>
                    </div>
                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.lower_level_pack_hierarchy_measurement_unit_id'))->class('form-control-label') }}
                            {{ html()->select('lower_level_pack_hierarchy_measurement_unit_id_1',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->attributes(['disabled'=>'disabled'])
                                           }}

                            <small class="text-muted">@lang('labels.products.form.helptext.lower_level_pack_hierarchy_measurement_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('lower_level_pack_measurement_unit_id') }}
                            </div>
                    </div>

                    <div class="form-group col-sm-3">
                          {{ html()->label(__('labels.products.form.label.gtin_1'))->class('form-control-label')}}
                          {{ html()->text('gtin_1')->class('form-control')
                          ->attributes(['disabled'=>'disabled'])
                          ->required() }}
                          <small class="text-muted">@lang('labels.products.form.helptext.gtin_1')</small>
                          <div class="invalid-feedback">
                              @lang('validation.required', ['attribute' => __('labels.products.form.label.gtin_1')]).
                      </div>
                    </div>
                    <div class="col-sm-1">
                      <div id="hdnButton" class="d-none">
                        <button class="btn btn-brand btn-danger" type="button" style="margin-bottom: 4px" data-toggle="tooltip" data-placement="top" title="Presione - para eliminar esta presentacion"><i class="fa fa-minus"></i></button>
                      </div>
                    </div>

                  </div><!--1st row ends-->
                  <div class="row"><!--2nd row-->
                    <div class="col-sm-4">
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_order_unit_1')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_order_unit_1','id'=>'ind_measurement_unit_is_order_unit_1', 'disabled'=>'disabled'])
                            }}
                            <label class="custom-control-label" label="label_ind_measurement_unit_is_order_unit_1" for="ind_measurement_unit_is_order_unit_1">@lang('labels.products.form.label.ind_measurement_unit_is_order_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_order_unit_1"></small>
                        </div>
                    </div>

                    <div class="col-sm-4">    
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_delivery_or_issue_unit_1')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_delivery_or_issue_unit_1','id'=>'ind_measurement_unit_is_delivery_or_issue_unit_1','disabled'=>'disabled'])
                            }}
                            <label class="custom-control-label" id="label_measurement_unit_is_delivery_or_issue_unit_1" for="ind_measurement_unit_is_delivery_or_issue_unit_1">@lang('labels.products.form.label.ind_measurement_unit_is_delivery_or_issue_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_delivery_or_issue_unit_1"></small>
                        </div>
                    </div>

                  </div><!--2nd row ends-->
                  <div class="row">
                    <div class="col-sm-12">
                      <hr class="mb-4">                    
                    </div>
                  </div>
                </div>    
              </div><!--multiple fields row-->

              @if($product->alt_measure_unit_stockkeep_unit_id_2 !== null)
              <div class="row" id="row_id_2"><!--row id -->
                <div class="col-sm-12">
                  <div class="row"><!--1st row-->

                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.alt_measure_unit_stockkeep_unit_id'))->class('form-control-label') }}
                            {{ html()->select('alt_measure_unit_stockkeep_unit_id_2',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->attributes(['disabled'=>'disabled']) }}

                            <small class="text-muted">@lang('labels.products.form.helptext.alt_measure_unit_stockkeep_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('alt_measure_unit_stockkeep_unit_id_2') }}
                            </div>
                    </div>
                    <div class="form-group col-sm-2">
                        {{ html()->label(__('labels.products.form.label.lower_level_measurement_unit_qty'))->class('form-control-label')}}
                        {{ html()->input('number','lower_level_measurement_unit_qty_2')->class('form-control')->attributes(['min'=>'1', 'max'=>'1000000'])->attributes(['disabled'=>'disabled']) }}
                        <small class="text-muted">@lang('labels.products.form.helptext.lower_level_measurement_unit_qty')</small>
                          <div class="invalid-feedback">
                          {{ $errors->first('lower_level_measurement_unit_qty_2') }}
                          </div>
                    </div>
                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.lower_level_pack_hierarchy_measurement_unit_id'))->class('form-control-label') }}
                            {{ html()->select('lower_level_pack_hierarchy_measurement_unit_id_2',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->attributes(['disabled'=>'disabled']) }}

                            <small class="text-muted">@lang('labels.products.form.helptext.lower_level_pack_hierarchy_measurement_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('lower_level_pack_measurement_unit_id') }}
                            </div>
                    </div>

                    <div class="form-group col-sm-3">
                          {{ html()->label(__('labels.products.form.label.gtin_2'))->class('form-control-label')}}
                          {{ html()->text('gtin_2')->class('form-control')
                          ->attribute('maxlength', 13)
                          ->attributes(['disabled'=>'disabled']) }}
                          <small class="text-muted">@lang('labels.products.form.helptext.gtin_2')</small>
                          <div class="invalid-feedback">
                              @lang('validation.required', ['attribute' => __('labels.products.form.label.gtin_2')]).
                      </div>
                    </div>
                    <div class="col-sm-1">
                      
                    </div>

                  </div><!--1st row ends-->
                  <div class="row"><!--2nd row-->
                    
                    <div class="col-sm-4">
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_order_unit_2')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_order_unit_2','id'=>'ind_measurement_unit_is_order_unit_2','value'=>'1','disabled'=>'disabled'])
                            }}
                            <label class="custom-control-label" id="label_ind_measurement_unit_is_order_unit_2" for="ind_measurement_unit_is_order_unit_2">@lang('labels.products.form.label.ind_measurement_unit_is_order_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_order_unit_2"></small>
                        </div>
                    </div>
                    
                    <div class="col-sm-4">    
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_delivery_or_issue_unit_2')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_delivery_or_issue_unit_2','id'=>'ind_measurement_unit_is_delivery_or_issue_unit_2','value'=>'1','disabled'=>'disabled'])
                            }}
                            <label class="custom-control-label" id="label_measurement_unit_is_delivery_or_issue_unit_2" for="ind_measurement_unit_is_delivery_or_issue_unit_2">@lang('labels.products.form.label.ind_measurement_unit_is_delivery_or_issue_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_delivery_or_issue_unit_2"></small>
                        </div>
                    </div>

                  </div><!--2nd row ends-->
                  <div class="row">
                    <div class="col-sm-12">
                      <hr class="mb-4">                    
                    </div>
                  </div>
                </div>    
              </div><!--row id -->
              @endif

              @if($product->alt_measure_unit_stockkeep_unit_id_3 !== null)
              <div class="row" id="row_id_3"><!-- row id-->
                <div class="col-sm-12">
                  <div class="row"><!--1st row-->

                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.alt_measure_unit_stockkeep_unit_id'))->class('form-control-label') }}
                            {{ html()->select('alt_measure_unit_stockkeep_unit_id_3',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->attributes(['disabled'=>'disabled']) }}

                            <small class="text-muted">@lang('labels.products.form.helptext.alt_measure_unit_stockkeep_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('alt_measure_unit_stockkeep_unit_id_3') }}
                            </div>
                    </div>
                    <div class="form-group col-sm-2">
                        {{ html()->label(__('labels.products.form.label.lower_level_measurement_unit_qty'))->class('form-control-label')}}
                        {{ html()->input('number','lower_level_measurement_unit_qty_3')->class('form-control')
                          ->attributes(['min'=>'1', 'max'=>'1000000'])->attributes(['disabled'=>'disabled']) }}
                        <small class="text-muted">@lang('labels.products.form.helptext.lower_level_measurement_unit_qty')</small>
                          <div class="invalid-feedback">
                          {{ $errors->first('lower_level_measurement_unit_qty_3') }}
                          </div>
                    </div>
                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.lower_level_pack_hierarchy_measurement_unit_id'))->class('form-control-label') }}
                            {{ html()->select('lower_level_pack_hierarchy_measurement_unit_id_3',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->attributes(['disabled'=>'disabled']) }}

                            <small class="text-muted">@lang('labels.products.form.helptext.lower_level_pack_hierarchy_measurement_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('lower_level_pack_measurement_unit_id_3') }}
                            </div>
                    </div>

                    <div class="form-group col-sm-3">
                          {{ html()->label(__('labels.products.form.label.gtin_3'))->class('form-control-label')}}
                          {{ html()->text('gtin_3')->class('form-control')
                          ->attributes(['disabled'=>'disabled'])
                          ->required() }}
                          <small class="text-muted">@lang('labels.products.form.helptext.gtin_3')</small>
                          <div class="invalid-feedback">
                              @lang('validation.required', ['attribute' => __('labels.products.form.label.gtin_3')]).
                      </div>
                    </div>
                    <div class="col-sm-1">
                    </div>

                  </div><!--1st row ends-->
                  <div class="row"><!--2nd row-->
                    
                    <div class="col-sm-4">
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_order_unit_3')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_order_unit_3','id'=>'ind_measurement_unit_is_order_unit_3','value'=>'1','disabled'=>'disabled'])
                            }}
                            <label class="custom-control-label" id="label_ind_measurement_unit_is_order_unit_3" for="ind_measurement_unit_is_order_unit_3">@lang('labels.products.form.label.ind_measurement_unit_is_order_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_order_unit_3"></small>
                        </div>
                    </div>
                    <div class="col-sm-4">    
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_delivery_or_issue_unit_3')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_delivery_or_issue_unit_3','id'=>'ind_measurement_unit_is_delivery_or_issue_unit_3','value'=>'1','disabled'=>'disabled'])
                            }}
                            <label class="custom-control-label" id="label_measurement_unit_is_delivery_or_issue_unit_3" for="ind_measurement_unit_is_delivery_or_issue_unit_3">@lang('labels.products.form.label.ind_measurement_unit_is_delivery_or_issue_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_delivery_or_issue_unit_3"></small>
                        </div>
                    </div>
                  
                  </div><!--2nd row ends-->
                  <div class="row">
                    <div class="col-sm-12">
                      <hr class="mb-4">                    
                    </div>
                  </div>
                </div>    
              </div><!--row id-->
              @endif




              </div><!--card body ends-->
            </div><!--card ends-->
    </div><!--col-sm-12-->
    
    <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.auth.product.index'), __('buttons.general.goback')) }}
                    </div><!--col-->
                <div class="col text-right">
                @if($show_edit_button)
                    <a class="btn btn-success btn-sm pull-right" href="{{ route('admin.auth.product.edit', $product->id).'#packaging' }}" role="button">@lang('buttons.general.crud.edit')</a>
                @endif
                </div><!--row-->
                </div><!--row-->
    </div><!--card-footer-->

</div><!--col-->
{{ html()->closeModelForm() }}
