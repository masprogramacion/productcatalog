<?php

namespace App\Http\Requests\Frontend\User;

use Illuminate\Validation\Rule;
use App\Helpers\Frontend\Auth\Socialite;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

/**
 * Class UpdateProfileRequest.
 */
class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        switch ($request->input('person_type_id')){
            case '1':
            return [
                'cedula'    => 'required|max:191',
                'phone'    => 'required|max:50',
                'fax'    => 'sometimes|max:50',
                'payment_condition_days' => 'required|numeric',
                'payment_bonus_percentage' => 'required|numeric',
                'person_type_id' => 'required|numeric',
                'payment_type_id' => 'required|numeric',
                'delivery_time_days' => 'required|numeric',
    
            ];
            break;
            default:
                return [
                    'ruc'    => 'required|max:191',
                    'dv'    => 'required|max:10',
                    'phone'    => 'required|max:50',
                    'fax'    => 'sometimes|max:50',
                    'payment_condition_days' => 'required|numeric',
                    'payment_bonus_percentage' => 'required|numeric',
                    'person_type_id' => 'required|numeric',
                    'payment_type_id' => 'required|numeric',
                    'delivery_time_days' => 'required|numeric',
        
                ];
        }


    }


}
