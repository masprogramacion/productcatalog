<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Catalog\BankAccountType;
use App\Models\Catalog\Bank;
use App\Models\Catalog\PersonType;
use App\Models\Catalog\PaymentType;
use App\Models\Auth\User;

/**
 * Class AccountController.
 */
class AccountController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
       
        $user = auth()->user();
        $profile = User::find($user->id)->profile;
        //dd($profile);
        $bankAccount = User::find($user->id)->bankAccount;
        $banks = Bank::pluck('name','id');
        $bankAccountTypes = BankAccountType::pluck('name','id');
        $personTypes = PersonType::pluck('name','id');
        $paymentTypes = PaymentType::pluck('name','id');

        //dd($account);
        return view('frontend.user.account')
        ->withUser($user)
        ->withProfile($profile)
        ->withBankAccount($bankAccount)
        ->withBanks($banks)
        ->withBankAccountTypes($bankAccountTypes)
        ->withPersonTypes($personTypes)
        ->withPaymentTypes($paymentTypes);
    }
}
