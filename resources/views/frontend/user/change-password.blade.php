@extends('frontend.layouts.app2')

@section('content')
    <div class="row justify-content-center align-items-center mb-3">
        <div class="col col-sm-10 align-self-center">
            <div class="card">
                <div class="card-header">
                    <strong>
                        @lang('navs.frontend.user.account')
                    </strong>
                </div>

                <div class="card-body">

                
                    <div role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">

                            <li class="nav-item">
                                <a href="{{ route('frontend.user.account') }}" class="nav-link ">@lang('labels.frontend.user.info.update.profile')</a>
                            </li>

                            @if($logged_in_user->canChangePassword())
                                <li class="nav-item">
                                    <a href="#" class="nav-link active">@lang('navs.frontend.user.change_password')</a>
                                </li>
                            @endif
                        </ul>

                        <div class="tab-content">

                        @if($logged_in_user->canChangePassword())
                                <div role="tabpanel" class="tab-pane fade show active pt-3" id="password" aria-labelledby="password-tab">
                                    @include('frontend.user.account.tabs.change-password')
                                </div><!--tab panel change password-->
                            @endif

                        </div><!--tab content-->
                    </div><!--tab panel-->
                </div><!--card body-->
            </div><!-- card -->
        </div><!-- col-xs-12 -->
    </div><!-- row -->
@endsection