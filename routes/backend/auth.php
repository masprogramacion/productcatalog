<?php

use App\Http\Controllers\Backend\Auth\Role\RoleController;
use App\Http\Controllers\Backend\Auth\User\UserController;
use App\Http\Controllers\Backend\Auth\User\UserVendorController;
use App\Http\Controllers\Backend\Auth\User\UserAccessController;
use App\Http\Controllers\Backend\Auth\User\UserVendorAccessController;
use App\Http\Controllers\Backend\Auth\User\UserSocialController;
use App\Http\Controllers\Backend\Auth\User\UserStatusController;
use App\Http\Controllers\Backend\Auth\User\UserVendorStatusController;
use App\Http\Controllers\Backend\Auth\User\UserSessionController;
use App\Http\Controllers\Backend\Auth\User\UserVendorSessionController;
use App\Http\Controllers\Backend\Auth\User\UserPasswordController;
use App\Http\Controllers\Backend\Auth\User\UserVendorPasswordController;
use App\Http\Controllers\Backend\Auth\User\UserConfirmationController;
use App\Http\Controllers\Backend\Auth\User\UserVendorConfirmationController;
use App\Http\Controllers\Backend\Auth\Product\ProductController;
use App\Http\Controllers\Backend\Auth\Product\ProductStatusController;
use App\Http\Controllers\Backend\Auth\Excel\ExcelFileController;
use App\Http\Controllers\Backend\Auth\Catalog\CatalogController;
use App\Http\Controllers\Backend\Auth\Catalog\BankController;
use App\Http\Controllers\Backend\Auth\Department\DepartmentController;

/*
 * All route names are prefixed with 'admin.auth'.
 */
Route::group([
    'prefix'     => 'auth',
    'as'         => 'auth.',
    'namespace'  => 'Auth',
    'middleware' => 'role:'.config('access.users.admin_role'),
], function () {
    /*
     * User Management
     */
    Route::group(['namespace' => 'User'], function () {

        /*
         * User Status'
         */
        Route::get('user/deactivated', [UserStatusController::class, 'getDeactivated'])->name('user.deactivated');
        Route::get('user/deleted', [UserStatusController::class, 'getDeleted'])->name('user.deleted');

        /*
         * User (only for System Type) CRUD
         */
        Route::get('user', [UserController::class, 'index'])->name('user.index');
        Route::get('user/create', [UserController::class, 'create'])->name('user.create');
        Route::post('user', [UserController::class, 'store'])->name('user.store');




        /*
         * Specific User
         */
        Route::group(['prefix' => 'user/{user}'], function () {
            // User
            Route::get('/', [UserController::class, 'show'])->name('user.show');
            Route::get('edit', [UserController::class, 'edit'])->name('user.edit');
            Route::patch('/', [UserController::class, 'update'])->name('user.update');
            Route::delete('/', [UserController::class, 'destroy'])->name('user.destroy');

            // Account
            Route::get('account/confirm/resend', [UserConfirmationController::class, 'sendConfirmationEmail'])->name('user.account.confirm.resend');

            // Status
            Route::get('mark/{status}', [UserStatusController::class, 'mark'])->name('user.mark')->where(['status' => '[0,1]']);

            // Social
            Route::delete('social/{social}/unlink', [UserSocialController::class, 'unlink'])->name('user.social.unlink');

            // Confirmation
            Route::get('confirm', [UserConfirmationController::class, 'confirm'])->name('user.confirm');
            Route::get('unconfirm', [UserConfirmationController::class, 'unconfirm'])->name('user.unconfirm');

            // Password
            Route::get('password/change', [UserPasswordController::class, 'edit'])->name('user.change-password');
            Route::patch('password/change', [UserPasswordController::class, 'update'])->name('user.change-password.post');

            // Access
            Route::get('login-as', [UserAccessController::class, 'loginAs'])->name('user.login-as');

            // Session
            Route::get('clear-session', [UserSessionController::class, 'clearSession'])->name('user.clear-session');

            // Deleted
            Route::get('delete', [UserStatusController::class, 'delete'])->name('user.delete-permanently');
            Route::get('restore', [UserStatusController::class, 'restore'])->name('user.restore');
        });
    });

    /*
     * Role Management
     */
    Route::group(['namespace' => 'Role'], function () {
        Route::get('role', [RoleController::class, 'index'])->name('role.index');
        Route::get('role/create', [RoleController::class, 'create'])->name('role.create');
        Route::post('role', [RoleController::class, 'store'])->name('role.store');

        Route::group(['prefix' => 'role/{role}'], function () {
            Route::get('edit', [RoleController::class, 'edit'])->name('role.edit');
            Route::patch('/', [RoleController::class, 'update'])->name('role.update');
            Route::delete('/', [RoleController::class, 'destroy'])->name('role.destroy');
        });
    });



});


/*
 * All route names are prefixed with 'admin.auth'.
 */
Route::group([
    'prefix'     => 'auth',
    'as'         => 'auth.',
    'namespace'  => 'Auth',
    'middleware' => 'role:admintienda',
], function () {
    /*
     * Product Management
     */
    Route::group(['namespace' => 'Product'], function () {

        /*
        * Product Uncatalogued
        */
        //Route::get('product/uncatalogued', [ProductController::class, 'index'])->name('product.uncatalogued.index');
        Route::get('product/create/uncatalogued', [ProductController::class, 'create_uncatalogued'])->name('product.create.uncatalogued');
        Route::post('product/search/uncatalogued', [ProductController::class, 'search_uncatalogued'])->name('product.search.uncatalogued');
        Route::get('product/search/uncatalogued', [ProductController::class, 'search_uncatalogued'])->name('product.search.uncatalogued');
        Route::post('product/store/uncatalogued', [ProductController::class, 'store_uncatalogued'])->name('product.store.uncatalogued');
        Route::get('product/show/uncatalogued/{product}', [ProductController::class, 'show_uncatalogued'])->name('product.show.uncatalogued');
        Route::get('product/edit/uncatalogued/{product}', [ProductController::class, 'edit_uncatalogued'])->name('product.edit.uncatalogued');
        Route::patch('product/update/uncatalogued/{product}', [ProductController::class, 'update_uncatalogued'])->name('product.update.uncatalogued');

    });           

});



/*
 * All route names are prefixed with 'admin.auth'.
 */
Route::group([
    'prefix'     => 'auth',
    'as'         => 'auth.',
    'namespace'  => 'Auth',
    'middleware' => 'role:asistentecompra|administrador|planimetria|finanzas|gerentecategoria',
], function () {
    /*
     * Product Management
     */
    Route::group(['namespace' => 'Product'], function () {


        /*
        * Product Status'
        */
        Route::get('product/deleted', [ProductStatusController::class, 'getDeleted'])->name('product.deleted');
        Route::get('product/restore', [ProductStatusController::class, 'restore'])->name('product.restore');


        Route::get('product', [ProductController::class, 'index'])->name('product.index');
        Route::get('product/create/{product}/step2/', [ProductController::class, 'create_step2'])->name('product.create.step2');
        Route::get('product/create/{product}/step3/', [ProductController::class, 'create_step3'])->name('product.create.step3');
        Route::get('product/create/{product}/step4/', [ProductController::class, 'create_step4'])->name('product.create.step4');
        Route::patch('product/create/{product}/step2/', [ProductController::class, 'update_step2'])->name('product.update.step2');
        Route::patch('product/create/{product}/step3/', [ProductController::class, 'update_step3'])->name('product.update.step3');
        //Route::patch('product/create/{product}/step4/', [ProductController::class, 'update_step4'])->name('product.update.step4');
        Route::patch('product/update/{product}/general/', [ProductController::class, 'update_general'])->name('product.update.general');
        Route::patch('product/update/{product}/packaging/', [ProductController::class, 'update_packaging'])->name('product.update.packaging');
        Route::patch('product/update/{product}/images/', [ProductController::class, 'update_images'])->name('product.update.images');
        Route::patch('product/update/{product}/details/', [ProductController::class, 'update_details'])->name('product.update.details');
        Route::patch('product/update/{product}/status/update', [ProductController::class, 'status_update'])->name('product.update.status.update');



        Route::patch('product/update/{product}/status/confirmpayment', [ProductController::class, 'confirm_payment'])->name('product.update.status.confirmpayment');
        Route::get('product/create', [ProductController::class, 'create'])->name('product.create');
        Route::post('product', [ProductController::class, 'store'])->name('product.store');
        Route::get('product/category/{departmentid}', [ProductController::class, 'getCategory'])->name('product.department.category');
        Route::get('product/hierarchy/{categoryid}', [ProductController::class, 'getHierarchy'])->name('product.category.hierarchy');
        Route::post('product/search', [ProductController::class, 'search'])->name('product.search');
        Route::get('product/search', [ProductController::class, 'search'])->name('product.search');
        Route::get('product/message', [ProductController::class, 'view_messages'])->name('product.message.index');
        Route::get('product/message/{message}/show', [ProductController::class, 'show_message'])->name('product.message.show');
        /*
        * Specific Product
        */
        Route::group(['prefix' => 'product/{product}'], function () {
            // Product
            Route::get('/', [ProductController::class, 'show'])->name('product.show');
            Route::get('edit', [ProductController::class, 'edit'])->name('product.edit');
            Route::patch('/', [ProductController::class, 'update'])->name('product.update');
            Route::delete('/', [ProductController::class, 'destroy'])->name('product.destroy');
            Route::get('assign', [ProductController::class, 'assign'])->name('product.assign');
            Route::patch('/assign', [ProductController::class, 'update_assign'])->name('product.update.assign');
            //Route::patch('/', [ProductController::class, 'updateimages'])->name('product.updateimages');

        });


    });
});

/*
 * All route names are prefixed with 'admin.auth'.
 */
Route::group([
    'prefix'     => 'auth',
    'as'         => 'auth.',
    'namespace'  => 'Auth',
    'middleware' => 'role:asistentecompra|administrador',
], function () {

        /*
        * User Management (Vendor Type)
        */
        Route::group(['namespace' => 'User'], function () {

            /*
            * User (only for Vendor Type) Status'
            */
            Route::get('vendor/deactivated', [UserVendorStatusController::class, 'getDeactivated'])->name('vendor.deactivated');
            Route::get('vendor/deleted', [UserVendorStatusController::class, 'getDeleted'])->name('vendor.deleted');
            /*
            * User (only for Vendor Type) CRUD
            */
            Route::get('vendor', [UserVendorController::class, 'index'])->name('vendor.index');
            Route::get('vendor/create', [UserVendorController::class, 'create'])->name('vendor.create');
            Route::post('vendor', [UserVendorController::class, 'store'])->name('vendor.store');
            //Route::post('vendor', [UserVendorController::class, 'destroy'])->name('vendor.destroy');
            Route::get('vendor/search/{name}', [UserVendorController::class, 'getProvider'])->name('vendor.search.byname');


        /*
         * Specific User
         */
        Route::group(['prefix' => 'vendor/{user}'], function () {
            // User
             Route::get('/', [UserVendorController::class, 'show'])->name('vendor.show');
             Route::get('edit', [UserVendorController::class, 'edit'])->name('vendor.edit');
             Route::patch('/', [UserVendorController::class, 'update'])->name('vendor.update');
             Route::delete('/', [UserVendorController::class, 'destroy'])->name('vendor.destroy');

             // Account
             Route::get('account/confirm/resend', [UserVendorConfirmationController::class, 'sendConfirmationEmail'])->name('vendor.account.confirm.resend');

            // Status
             Route::get('mark/{status}', [UserVendorStatusController::class, 'mark'])->name('vendor.mark')->where(['status' => '[0,1]']);

            // Social
            //Route::delete('social/{social}/unlink', [UserSocialController::class, 'unlink'])->name('user.social.unlink');

            // Confirmation
            Route::get('confirm', [UserVendorConfirmationController::class, 'confirm'])->name('vendor.confirm');
            Route::get('unconfirm', [UserVendorConfirmationController::class, 'unconfirm'])->name('vendor.unconfirm');

            // Password
            Route::get('password/change', [UserVendorPasswordController::class, 'edit'])->name('vendor.change-password');
            Route::patch('password/change', [UserVendorPasswordController::class, 'update'])->name('vendor.change-password.post');

            // Access
            Route::get('login-as', [UserVendorAccessController::class, 'loginAs'])->name('vendor.login-as');

            // Session
            Route::get('clear-session', [UserVendorSessionController::class, 'clearSession'])->name('vendor.clear-session');

            // Deleted
            Route::get('delete', [UserVendorStatusController::class, 'delete'])->name('vendor.delete-permanently');
            Route::get('restore', [UserVendorStatusController::class, 'restore'])->name('vendor.restore');
        });


        });


});



/*
 * All route names are prefixed with 'admin.auth'.
 */
Route::group([
    'prefix'     => 'auth',
    'as'         => 'auth.',
    'namespace'  => 'Auth',
    'middleware' => 'role:datosmaestros|administrador',
], function () {

        /*
        * Winshuttle Management 
        */
        Route::group(['namespace' => 'Excel'], function () {

            /*
            * Excel - '
            */
            Route::get('excel/index', [ExcelFileController::class, 'index'])->name('excel.index');
            Route::get('excel/upload/index', [ExcelFileController::class, 'upload_index'])->name('excel.upload.index');
            Route::get('excel/download', [ExcelFileController::class, 'download'])->name('excel.download');
            Route::post('excel/getproducts', [ExcelFileController::class, 'getProducts'])->name('excel.products');
            Route::post('excel/upload/', [ExcelFileController::class, 'upload_excel'])->name('excel.upload');

        });


});




/*
 * All route names are prefixed with 'admin.auth'.
 */
Route::group([
    'prefix'     => 'auth',
    'as'         => 'auth.',
    'namespace'  => 'Auth',
    'middleware' => 'role:administrador',
], function () {

        /*
        * Catalog Management 
        */
        Route::group(['namespace' => 'Catalog'], function () {

            Route::get('catalog/index', [CatalogController::class, 'index'])->name('catalog.index');
            Route::get('catalog/edit/{table_name}', [CatalogController::class, 'edit'])->name('catalog.edit');
            
            // Deleted
            //Route::get('delete', [UserStatusController::class, 'delete'])->name('user.delete-permanently');
            //Route::get('restore', [UserStatusController::class, 'restore'])->name('user.restore');

            Route::get('catalog/bank/index', [BankController::class, 'index'])->name('catalog.bank.index');
            //Route::post('catalog/bank/update/{id}', [BankController::class, 'update'])->name('catalog.bank.update');
            //Route::post('catalog/bank/update/{bank}', [BankController::class, 'update'])->name('catalog.bank.update');
            //Route::delete('catalog/bank/destroy/{bank}', [BankController::class, 'destroy'])->name('catalog.bank.destroy');
            Route::post('catalog/bank/update/', [BankController::class, 'update'])->name('catalog.bank.update');
            Route::delete('catalog/bank/destroy/', [BankController::class, 'destroy'])->name('catalog.bank.destroy');
        });


        /*
        * Assign Department to Users
        */
        Route::group(['namespace' => 'Department'], function () {

            Route::get('department/index', [DepartmentController::class, 'index'])->name('department.index');
            Route::get('department/create', [DepartmentController::class, 'create'])->name('department.create');
            Route::get('department/edit/{user}', [DepartmentController::class, 'edit'])->name('department.edit');
            Route::patch('department/update/{user}', [DepartmentController::class, 'update'])->name('department.update');

            Route::get('department/index_assistant', [DepartmentController::class, 'index_assistant'])->name('department.index_assistant');
            

        });


});











