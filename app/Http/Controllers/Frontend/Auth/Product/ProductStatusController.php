<?php

namespace App\Http\Controllers\Frontend\Auth\Product;

use App\Models\Auth\Product;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\ProductRepository;
use App\Http\Requests\Frontend\Auth\Product\ManageProductRequest;

/**
 * Class ProductStatusController.
 */
class ProductStatusController extends Controller
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param ManageProductRequest $request
     *
     * @return mixed
     */
    public function getDeactivated(ManageProducRequest $request)
    {
        return view('frontend.auth.product.deactivated')
            ->withProducts($this->productRepository->getInactivePaginated(25, 'id', 'desc'));
    }

    /**
     * @param ManageProductRequest $request
     *
     * @return mixed
     */
    public function getDeleted(ManageProductRequest $request)
    {
        return view('frontend.auth.product.deleted')
            ->withProducts($this->productRepository->getDeletedPaginated(25, 'id', 'desc'));
    }

    /**
     * @param ManageProductRequest $request
     * @param Product              $product
     * @param                   $status
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function mark(ManageProductRequest $request, Product $product, $status)
    {
        $this->productRepository->mark($product, $status);

        return redirect()->route(
            $status == 1 ?
            'admin.auth.product.index' :
            'admin.auth.product.deactivated'
        )->withFlashSuccess(__('alerts.backend.product.updated'));
    }

    /**
     * @param ManageProductRequest $request
     * @param Product              $deletedProduct
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageProductRequest $request, Product $deletedProduct)
    {
        $this->productRepository->forceDelete($deletedProduct);

        return redirect()->route('frontend.auth.product.deleted')->withFlashSuccess(__('alerts.backend.products.deleted_permanently'));
    }

    /**
     * @param ManageProductRequest $request
     * @param Product              $deletedProduct
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageProductRequest $request, Product $deletedProduct)
    {
        $this->productRepository->restore($deletedProduct);

        return redirect()->route('frontend.auth.product.index')->withFlashSuccess(__('alerts.backend.products.restored'));
    }
}
