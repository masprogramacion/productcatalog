<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'        => 'Las credenciales no se han encontrado.',
    'general_error' => 'No tiene suficientes permisos..',
    'password_rules' => 'Su contraseña debe tener al menos 8 caracteres, debe tener 1 letra en mayúscula, 1 en minúscula y 1 número.',
    'password_used' => 'No puede usar una contraseña que Ud. usó anteriormente.',
    'socialite'     => [
        'unacceptable' => ':provider no es un tipo de autenticación válida.',
    ],
    'throttle' => 'Demasiados intentos de inicio de sesión. Vuelva a intentarlo en :seconds segundos.',
    'unknown'  => 'Se ha producido un error desconocido.',
];
