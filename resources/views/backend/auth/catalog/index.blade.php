@extends('backend.layouts.app')

@section('content')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.catalogs.management') }} <small class="text-muted">{{ __('labels.backend.access.catalogs.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">

            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.access.catalogs.table.name')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($catalogs as $catalog)
                            <tr>
                                <td>{{ $catalog->table_name }}</td>
                                <td><a href="{{route('admin.auth.catalog.edit', $catalog->id)}}" data-toggle="tooltip" data-placement="top" title="{{__('buttons.general.crud.edit')}}" class="btn btn-primary"><i class="fas fa-edit"></i></a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $catalogs->total() !!} {{ trans_choice('labels.backend.access.catalogs.table.total', $catalogs->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $catalogs->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->


@endsection