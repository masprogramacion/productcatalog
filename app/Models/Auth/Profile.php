<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\Traits\Relationship\ProfileRelationship;

class Profile extends Model
{
    use ProfileRelationship;


      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'cedula',
        'ruc',
        'dv',
        'phone',
        'fax',
        'payment_condition_days',
        'payment_bonus_percentage',
        'person_type_id',
        'payment_type_id',
        'delivery_time_days',
        'user_type_id'

    ];
}
