@extends('frontend.layouts.app2')

@section('title', __('labels.frontend.access.products.management') . ' | ' . __('labels.frontend.access.products.view'))

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.frontend.access.products.management')
                    <small class="text-muted">@lang('labels.frontend.access.products.view')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-expanded="true"><i class="fas fa-file-alt"></i> @lang('labels.frontend.access.products.tabs.titles.general')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#packaging" role="tab" aria-controls="packaging" aria-expanded="false"><i class="fas fa-box"></i> @lang('labels.frontend.access.products.tabs.titles.packaging')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#images" role="tab" aria-controls="images" aria-expanded="false"><i class="fas fa-camera-retro"></i> @lang('labels.frontend.access.products.tabs.titles.images')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#status" role="tab" aria-controls="status" aria-expanded="false"><i class="fas fa-check-circle"></i> @lang('labels.frontend.access.products.tabs.titles.status')</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show" id="general" role="tabpanel" aria-expanded="true">
                    @include('frontend.auth.product.show.tabs.general')
                    </div><!--tab-->
                    <div class="tab-pane" id="packaging" role="tabpanel" aria-expanded="true">
                    @include('frontend.auth.product.show.tabs.packaging')
                    </div><!--tab-->
                    <div class="tab-pane" id="images" role="tabpanel" aria-expanded="true">
                    @include('frontend.auth.product.show.tabs.images')
                    </div><!--tab-->
                    <div class="tab-pane" id="status" role="tabpanel" aria-expanded="true">
                    @include('frontend.auth.product.show.tabs.status')
                    </div><!--tab-->
                </div><!--tab-content-->
            </div><!--col-->
        </div><!--row-->



    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">

            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection
