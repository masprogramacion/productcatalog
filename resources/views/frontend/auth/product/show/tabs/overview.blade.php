<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Imagen</th>
                <td><img src="{{ $product->picture }}" class="img-responsive img-thumbnail" width="300px"  /></td>
            </tr>

            <tr>
                <th>@lang('labels.frontend.access.users.tabs.content.overview.name')</th>
                <td>{{ $product->name }}</td>
            </tr>

            <tr>
                <th>@lang('labels.frontend.access.users.tabs.content.overview.status')</th>
                <td>{!! $product->status_label !!}</td>
            </tr>

            <tr>
                <th>@lang('labels.general.description')</th>
                <td>{!! $product->description !!}</td>
            </tr>

            <tr>
                <th>Precio</th>
                <td>{{ $product->price }}</td>
            </tr>
        </table>
    </div>
    <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.auth.product.index'), __('buttons.general.goback')) }}
                    </div><!--col-->
                @if(!$product->assigned)     
                <div class="col text-right">
                    <a class="btn btn-success btn-sm pull-right" href="{{ route('admin.auth.product.edit', $product->id) }}" role="button">@lang('buttons.general.crud.edit')</a>
                </div><!--col-->
                @endif
                </div><!--row-->
    </div><!--card-footer-->
</div><!--table-responsive-->
