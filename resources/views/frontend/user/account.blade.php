@extends('frontend.layouts.app2')

@section('content')
    <div class="row justify-content-center align-items-center mb-3">
        <div class="col col-sm-10 align-self-center">
            <div class="card">
                <div class="card-header">
                    <strong>
                        @lang('navs.frontend.user.account')
                    </strong>
                </div>

                <div class="card-body">

                    <div role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">

                            <li class="nav-item">
                                <a href="#account" data-toggle="tab"  role="tab" aria-controls="account" aria-expanded="true" class="nav-link active">@lang('navs.frontend.user.info.general')</a>
                            </li>
                            <li class="nav-item">
                                <a href="#profile" data-toggle="tab" role="tab" aria-controls="packaging" aria-expanded="profile" class="nav-link" >@lang('navs.frontend.user.info.profile')</a>
                            </li>
                            <li class="nav-item">
                                <a href="#bank-account" data-toggle="tab"  role="tab" aria-controls="bank-account" aria-expanded="false" class="nav-link">@lang('navs.frontend.user.info.bank_account')</a>
                            </li>


                            @if($logged_in_user->canChangePassword())
                            <li class="nav-item">
                                <a href="#change-password" data-toggle="tab" role="tab" aria-controls="change-password" aria-expanded="false" class="nav-link">@lang('navs.frontend.user.info.change_password')</a>
                            </li>
                            @endif
                        </ul>

                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane show active pt-3" id="account" aria-labelledby="edit-tab">
                                @include('frontend.user.account.tabs.general')
                            </div><!--tab panel profile-->
                            <div role="tabpanel" class="tab-pane pt-3" id="profile" aria-labelledby="profile-tab">
                                @include('frontend.user.account.tabs.profile')
                            </div><!--tab panel profile-->
                            <div role="tabpanel" class="tab-pane pt-3" id="bank-account" aria-labelledby="bank-acount-tab">
                                @include('frontend.user.account.tabs.bank-account')
                            </div><!--tab panel profile-->
                            @if($logged_in_user->canChangePassword())
                            <div role="tabpanel" class="tab-pane pt-3" id="change-password" aria-labelledby="change-password-tab">
                                @include('frontend.user.account.tabs.change-password')
                            </div><!--tab panel profile-->
                            @endif

                        </div><!--tab content-->
                    </div><!--tab panel-->
                </div><!--card body-->
            </div><!-- card -->
        </div><!-- col-xs-12 -->
    </div><!-- row -->
@endsection
@push('after-scripts')
<script> 
$(document).ready(function(){
// read hash from page load and change tab
var hash = document.location.hash;
var prefix = "tab_";
if (hash) {
    $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
    setTimeout(() => {
      $(window).scrollTop(0);
    }, 400);
}

    @if($profile)
      
      @if($profile->person_type_id==1)
          $("#row_cedula").show();
          $("#row_ruc").hide();
      @else
          $("#row_cedula").hide();
          $("#row_ruc").show();
      @endif

    @endif

$('select[name=person_type_id]').change(function () {

        if ($(this).val() == '2') {
            $('#cedula').prop('required',false);
            $('#row_cedula').hide();
            $('#row_ruc').show();
            $('#ruc').prop('required',true);
            $('#dv').prop('required',true);

        } else {
            $('#ruc').prop('required',false);
            $('#dv').prop('required',false);
            $('#row_ruc').hide();
            $('#row_cedula').show();
            $('#cedula').prop('required',true);
        }
    });




});
</script>  
@endpush