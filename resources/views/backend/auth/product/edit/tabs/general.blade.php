{{ html()->modelForm($product, 'PATCH', route('admin.auth.product.update.general', $product->id))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-general-product','novalidate'=>'novalidate'])->open() }}
<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.name')</th>
                <td>
                    {{ html()->text('name')
                                            ->class('form-control')
                                            ->attribute('maxlength', 100)
                                            ->required()
                                            ->autofocus() }}
                    <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                    @lang('validation.required', ['attribute' => __('labels.products.form.label.product_name')])
                    </div>
                </td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.description')</th>
                <td>
                    {{ html()->textarea('description')
                                            ->class('form-control')
                                            ->attribute('maxlength', 255)
                                            ->required()}}
                    <div class="invalid-feedback">
                    @lang('validation.required', ['attribute' => __('labels.products.form.label.description')])
                    </div>
                </td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.price')</th>
                <td>
                    {{ html()->input('number','price')->class('form-control')->attributes(['step'=>'.01'])->required() }}
                    <div class="invalid-feedback">
                    {{ $errors->first('labels.products.form.helptext.price') }}
                    </div>
                </td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.suggested_price')</th>
                <td>
                    {{ html()->input('number','suggested_price')->class('form-control')->attributes(['step'=>'.01'])->required() }}
                    <div class="invalid-feedback">
                    {{ $errors->first('labels.products.form.helptext.suggested_price') }}
                    </div>
                </td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.min_remain_shelf_life')</th>
                <td>
                    {{ html()->input('number','min_remain_shelf_life')->class('form-control')->attributes(['min'=>'0', 'max'=>'365'])->required() }}
                    <div class="invalid-feedback">
                    @lang('validation.required', ['attribute' => __('labels.products.form.label.min_remain_shelf_life')]).
                    </div>
                </td>
            </tr>
            <!--
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.vendor_account_number')</th>
                <td>
                    {{ html()->text('vendor_account_number')
                                            ->class('form-control')
                                            ->attribute('maxlength', 50)
                                            ->required()}}
                    <div class="invalid-feedback">
                    {{ $errors->first('vendor_account_number') }}
                    @lang('validation.required', ['attribute' => __('labels.products.form.label.vendor_account_number')])
                    </div>
                </td>
            </tr>
            
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.vendor_account_number_id')</th>
                <td>
                    {{ html()->select('vendor_account_number_id',$vendor_accounts)
                                ->class('select2 custom-select d-block w-100')
                                ->value($product->vendor_account_number_id)
                                ->required() }}
                    <div class="invalid-feedback">
                    @lang('validation.required', ['attribute' => __('labels.products.form.label.vendor_account_number_id')]).
                    </div>
                </td>
            </tr>
            
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.ind_regular_vendor')</th>
                <td>
                 {{ html()->checkbox('ind_regular_vendor', $product->ind_regular_vendor, 1)}}
                </td>
            </tr>
            -->
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.brand_id')</th>
                <td>
                    {{ html()->select('brand_id',$brands)
                                ->class('select2 custom-select d-block w-100')
                                ->value($product->brand_id)
                                ->required() }}
                    <div class="invalid-feedback">
                    @lang('validation.required', ['attribute' => __('labels.products.form.label.brand_id')]).
                    </div>
                </td>
            </tr>
            
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.manufacturer_id')</th>
                <td>
                    {{ html()->select('manufacturer_id',$manufacturers)
                                ->class('select2 custom-select d-block w-100')
                                ->value($product->manufacturer_id)
                                ->required() }}
                    <div class="invalid-feedback">
                    @lang('validation.required', ['attribute' => __('labels.products.form.label.manufacturer_id')]).
                    </div>
                </td>
            </tr>
                     
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.include_discount')</th>
                <td>

                    <div class="row">
                        <div class="col-md-4">  
                            <div class="form-group">
                            @if($product->ind_include_discount)
                                {{ html()->label(html()->checkbox('ind_include_discount', $product->ind_include_discount, 1)->attributes(['data-toggle'=>'collapse', 'data-target'=>'.multi-collapse','aria-expanded'=>'true', 'aria-controls'=>'collapseOutter1 collapseOutter2']) . ' ' . __('labels.backend.access.products.tabs.content.overview.ind_include_discount'))->for('ind_include_discount') }}
                            @else
                                {{ html()->label(html()->checkbox('ind_include_discount', $product->ind_include_discount, 1)->attributes(['data-toggle'=>'collapse', 'data-target'=>'.multi-collapse','aria-expanded'=>'false', 'aria-controls'=>'collapseOutter1 collapseOutter2']) . ' ' . __('labels.backend.access.products.tabs.content.overview.ind_include_discount'))->for('ind_include_discount') }}
                            @endif
                            </div>
                        </div>
                        <div class="col-md-4" >
                            @if($product->ind_include_discount)  
                            <div id="collapseOutter1" class="collapse multi-collapse show">
                            @else
                            <div id="collapseOutter1" class="collapse multi-collapse">
                            @endif
                                <div class="input-group">
                                    {{ html()->input('number','discount_percentage')
                                    ->class('form-control')->attributes(['min'=>'1', 'max'=>'100'])
                                    ->value($product->discount_percentage) }}
                                    <div class="input-group-append">
                                        <span class="input-group-text">%</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <small class="text-muted">@lang('labels.products.form.helptext.discount_percentage')</small>
                                    <div class="invalid-feedback">
                                    @lang('validation.required', ['attribute' => __('labels.products.form.label.discount_percentage')]).
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div class="col-md-4">
                        </div>
                    </div>


                    
                    @if($product->ind_include_discount)  
                    <div class="row collapse multi-collapse show" id="collapseOutter2">
                    @else
                    <div class="row collapse multi-collapse" id="collapseOutter2">
                    @endif
                        <div class="col-md-4">  
                            <div class="form-group">
                            {{ html()->label(html()->checkbox('ind_limited_time_discount', $product->ind_limited_time_discount, 1)->attributes(['data-toggle'=>'collapse', 'data-target'=>'#collapseInner','aria-expanded'=>'true', 'aria-controls'=>'collapseInner']) . ' ' . __('labels.backend.access.products.tabs.content.overview.ind_limited_time_discount'))->for('ind_limited_time_discount') }}
                            </div>
                        </div>
                        
                        <div class="col-md-4"> 
                                @if($product->ind_limited_time_discount)
                                <div id="collapseInner" class="collapse show">
                                @else
                                <div id="collapseInner" class="collapse">
                                @endif
                                
                                    <div class="mb-3">
                                        {{ html()->label(__('labels.backend.access.products.tabs.content.overview.discount_start_date'))->class('form-control-label')}}
                                        {{ html()->input('text','discount_start_date')
                                        ->class('form-control')
                                        ->value($product->discount_start_date) }}
                                        <div class="invalid-feedback">
                                            @lang('validation.required', ['attribute' => __('labels.backend.access.products.tabs.content.overview.discount_start_date')]).
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        {{ html()->label(__('labels.backend.access.products.tabs.content.overview.discount_end_date'))->class('form-control-label')}}
                                        {{ html()->input('text','discount_end_date')
                                        ->class('form-control')
                                        ->value($product->discount_end_date) }}
                                        <div class="invalid-feedback">
                                            @lang('validation.required', ['attribute' => __('labels.backend.access.products.tabs.content.overview.discount_end_date')]).
                                        </div>
                                    </div>
                                </div>


                        </div>
                        <div class="col-md-4"></div>
                    </div>    
                </td>

            </tr>

            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.old_material_number')</th>
                <td>
                    {{ html()->text('old_material_number')
                                ->class('form-control')
                                ->attribute('maxlength', 100) }}
                    <div class="invalid-feedback">
                    @lang('validation.required', ['attribute' => __('labels.products.form.label.old_material_number')])
                    </div>
                </td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.vendor_material_number')</th>
                <td>
                    {{ html()->text('vendor_material_number')
                                ->class('form-control')
                                ->attribute('maxlength', 100) }}
                    <div class="invalid-feedback">
                    @lang('validation.required', ['attribute' => __('labels.products.form.label.vendor_material_number')])
                    </div>
                </td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.created_at')</th>
                <td>{{ $product->created_at }}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.updated_at')</th>
                <td>{{ $product->updated_at }}</td>
            </tr>
        </table>
    </div>

    <div class="card-footer clearfix">
                <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.product.index'), __('buttons.general.goback')) }}
                </div><!--col-->

                <div class="col text-right">
                    <button class="btn btn-success btn-sm pull-right" id="btnSubmitGeneral" type="button">@lang('buttons.general.crud.update')</button>
                </div><!--row-->
            </div><!--row-->
    </div><!--card-footer-->
</div><!--table-responsive-->
{{ html()->closeModelForm() }}