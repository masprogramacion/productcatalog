<?php

namespace App\Models\Catalog\Traits\Relationship;

use App\Models\Auth\User;

/**
 * Class DepartmentRelationship.
 */
trait DepartmentRelationship
{

    /**
     * @return mixed
     */
    public function users(){
        return $this->belongsToMany(User::class)->withTimestamps(); 
    }
}
