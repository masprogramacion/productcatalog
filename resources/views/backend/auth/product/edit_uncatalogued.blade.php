@extends('backend.layouts.app')

@section('title', __('labels.backend.access.products.management') . ' | ' . __('labels.backend.access.products.create'))

@section('content')


{{ html()->modelForm($product, 'PATCH', route('admin.auth.product.update.uncatalogued', $product))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-product','novalidate'=>'novalidate','enctype'=>'multipart/form-data'])->open() }}

<div class="container">

  <div class="py-1 text-center">
    <h2>EDITAR PRODUCTO NO CATALOGADO</h2>
    <!--<p class="lead"></p>-->
  </div>

  <div class="row">


    <div class="card bg-light mb-3">

    <div class="card-body"> 
        <hr class="mb-4">


        <div class="row">
          <div class="col-md-5 mb-3">
            {{ html()->label(__('labels.products.form.label.product_name'))->for('product_name')}}
            {{ html()->text('product_name')
                                    ->class('form-control')
                                    ->attribute('maxlength', 100)
                                    ->required()
                                    ->autofocus() }}
            <small class="text-muted">@lang('labels.products.form.helptext.product_name')</small>
            <div class="invalid-feedback">
            @lang('validation.required', ['attribute' => __('labels.products.form.label.product_name')])
            {{ $errors->first('name') }} <!--TODO add backend errors logic to show errors in each field-->
            </div>
          </div>
          <div class="col-md-7 mb-3">
            {{ html()->label(__('labels.products.form.label.description'))->for('description')}}
            {{ html()->textarea('description')
                                      ->class('form-control')
                                      ->attribute('maxlength', 40)
                                      ->required()}}
            <small class="text-muted">@lang('labels.products.form.helptext.description')</small>
            <div class="invalid-feedback">
            @lang('validation.required', ['attribute' => __('labels.products.form.label.description')])
            </div>
          </div>
        </div> <!--ends row-->

        <div class="row">
          
            <div class="col-md-4 mb-3">
              {{ html()->label('Marca')->class('form-control-label')->for('brand_id') }}
              {{ html()->select('brand_id',$brands)
                                      ->class('custom-select')
                                      ->required() }}
              <small class="text-muted">@lang('labels.products.form.helptext.brand_id')</small>
              <div class="invalid-feedback">
              @lang('validation.required', ['attribute' => __('labels.products.form.label.brand_id')]).
              </div>
            </div>

            <div class="col-md-4 mb-3">

                {{ html()->label(__('labels.products.form.label.store_id'))->class('form-control-label') }}
                {{ html()->select('store_id',$stores)
                            ->class('custom-select d-block w-100')
                            ->required() }}
                <small class="text-muted"></small>
                <div class="invalid-feedback">
                {{ $errors->first('store_id') }}
                </div>

            </div>

            <div class="col-md-4 mb-3">

            </div>

        </div>

        <div class="row">
          <div class="col-md-6 mb-3">
                          {{ html()->label(__('labels.products.form.label.department_id'))->class('form-control-label') }}
            {{ html()->select('department_id',$departments)
                          ->placeholder('')
                          ->required()
                          ->class('select2 custom-select') }}
            <small class="text-muted">@lang('labels.products.form.helptext.department_id')</small>
            <div class="invalid-feedback">
            @lang('validation.required', ['attribute' => __('labels.products.form.label.department_id')])
            {{ $errors->first('department_id') }}
            </div>                 
          </div><!--col-->
        </div>

        <div class="row">
          


            <div class="col-md-6 mb-3">
                {{ html()->label(__('CODIGO DE BARRA - GTIN (EAN/UPC)'))->class('form-control-label')->for('gtin_1') }}

                {{ html()->text('gtin_1')
                        ->class('form-control')
                        ->attribute('maxlength', 13)
                        ->required() }}

            </div>
            <div class="col-md-4 mb-3">

            </div>
            
            <div class="col-md-2 mb-3">

            </div>


        </div>


        <hr class="mb-4">

        <h4 class="mb-3">Fotos del Producto</h4>
        <div class="row"> <!--images-->
            <div class="col-sm-3">
                    <div class="card">
                              <img class="card-img-top" id="preview_image_front_1" name="preview_image_front_1"  src="{{ $product->imageFront }}" alt="@lang('labels.products.form.alt.select_front_image')">
                              <div class="card-body">
                                  <h5 class="card-title">Imagen Frontal</h5>
                                  <p class="card-text"></p>
                                  <div class="custom-file">
                                      {{ html()->file('image_front_1')
                                          ->class('custom-file-input')
                                              }}
                                      <label class="custom-file-label" for="image_front_1">Seleccione...</label>
                                  </div>
                              </div>
                    </div>
            </div>
            <div class="col-sm-3">        
                    <div class="card">
                            <img class="card-img-top" id="preview_image_back_1" name="preview_image_back_1" src="{{ $product->imageBack  }}" alt="@lang('labels.products.form.alt.select_back_image')">
                            <div class="card-body">
                                  <h5 class="card-title">Imagen Posterior</h5>
                                  <p class="card-text"></p>
                                  <div class="custom-file">
                                      {{ html()->file('image_back_1')
                                          ->class('custom-file-input')
                                              }}
                                      <label class="custom-file-label" for="image_back_1">Seleccione...</label>
                                  </div>
                            </div>
                    </div>
            </div>
        </div><!--images-->

      </div><!--ends card-body-->

      <div class="card-footer clearfix">     
        <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.dashboard'), __('buttons.general.cancel')) }}
                    </div>

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.update')) }}
                    </div>
      </div><!--ends card footer-->
         
        <!--<button class="btn btn-primary btn-lg btn-block" id="btnSubmit" type="button">Actualizar el Producto</button>-->

    </div>

  </div>
  </div><!--end row-->


</div><!--ends container--->
</form>


@endsection
@push('after-scripts')
<script> 

$(document).ready(function(){

  var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());



  //$(".select2").select2();
  $('[data-toggle="tooltip"]').tooltip();

  $('#collapseInner').on('show.bs.collapse', function( event ) {
    event.stopPropagation();
  })
  $('#collapseInner').on('hide.bs.collapse', function( event ) {
      event.stopPropagation();
  })

  $('#btnSubmit').on("click", validateForm);

  function readURL(input,previewId) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    
                    reader.onload = function (e) {
                        $(previewId).attr('src', e.target.result);

                    }
                    reader.readAsDataURL(input.files[0]);
                }
        }

        $("#image_front_1").change(function(){
           readURL(this, '#preview_image_front_1');
        });

        $("#image_back_1").change(function(){
           readURL(this, '#preview_image_back_1');
        });


});


function validateForm() {
   
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName("needs-validation");
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.classList.add("was-validated");
    });

    let product_name = document.querySelector("#product_name").checkValidity();
    let description = document.querySelector("#description").checkValidity();
    let brand_id = document.querySelector("#brand_id").checkValidity();
    let stored_id = document.querySelector("#store_id").checkValidity();
    let gtin_1 = document.querySelector("#gtin_1").checkValidity();
    let base_measurement_unit_id = document.querySelector("#base_measurement_unit_id").checkValidity();
    let image_front_1 = document.querySelector("#image_front_1").checkValidity();
    let image_back_1 = document.querySelector("#image_back_1").checkValidity();


    let no_errors  = product_name && description && brand_id && stored_id && gtin_1 && base_measurement_unit_id && image_front_1 && image_back_1;
    

    if (no_errors) {
      $('#form-update-product').submit();
      $(this).attr('disabled','disabled');
    }else {

          //var formElements = document.getElementById('form-create-product').querySelectorAll('textarea, input:not([type=hidden])');
          var errorElements = document.getElementById('form-create-product').querySelectorAll('input.form-control:invalid');
          if(errorElements.length > 0){
            //console.log(errorElements);
            //var scrollTop = $(window).scrollTop();
            var elementOffset = $(errorElements[0]).offset().top - 100;
            //console.log(elementOffset);
            //var currentElementOffset = (elementOffset - scrollTop);
            //console.log(currentElementOffset);
            $('html, body').animate({
                scrollTop: elementOffset
              }, 2000, function() {
                $(errorElements[0]).focus();
            });
          }


    }

  }

</script>  
@endpush