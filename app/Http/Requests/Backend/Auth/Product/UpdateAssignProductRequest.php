<?php

namespace App\Http\Requests\Backend\Auth\Product;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateAssignUserRequest.
 */
class UpdateAssignProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return $this->user()->isAdmin();
         //TODO this can be used by a regular user as well
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department_id'  => 'sometimes'
            //'vendor_material_number'  => 'max:50',
        ];
    }
}
