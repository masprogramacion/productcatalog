{{ html()->modelForm($profile, 'PATCH', route('frontend.user.info.update.profile'))->class('form-horizontal')->open() }}
<div class="row">

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>@lang('navs.frontend.user.info.profile')</strong>
                    <small>@lang('labels.general.general_information')</small>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="form-group col-sm-12">

                            {{ html()->label(__('labels.frontend.user.info.tabs.content.profile.person_type_id'))->for('person_type_id') }}
                            {{ html()->select('person_type_id',$personTypes)
                                ->class('form-control')
                                ->placeHolder('Seleccione un valor')
                                ->required()
                            }}
                        </div>
                    </div>

                    <div class="row" id="row_cedula">
                        <div class="form-group col-sm-12">
                                {{ html()->label(__('labels.frontend.user.info.tabs.content.profile.cedula'))->for('cedula') }}
                                {{ html()->text('cedula')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.frontend.cedula'))
                                    ->attribute('maxlength', 191) }}

                        </div>
                    </div>
                    <div class="row" id="row_ruc">
                        <div class="form-group col-sm-8">
                            {{ html()->label(__('labels.frontend.user.info.tabs.content.profile.ruc'))->for('ruc') }}
                            {{ html()->text('ruc')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.frontend.ruc'))
                            ->attribute('maxlength', 191) }}
                        </div>
                        <div class="form-group col-sm-4">
                            {{ html()->label(__('labels.frontend.user.info.tabs.content.profile.dv'))->for('dv') }}
                            {{ html()->text('dv')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.frontend.dv'))
                            ->attribute('maxlength', 191) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12">
                                {{ html()->label(__('labels.frontend.user.info.tabs.content.profile.phone'))->for('phone') }}
                                {{ html()->text('phone')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.frontend.phone'))
                                ->attribute('maxlength', 191) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12">

                            {{ html()->label(__('labels.frontend.user.info.tabs.content.profile.fax'))->for('fax') }}
                            {{ html()->text('fax')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.frontend.fax'))
                            ->attribute('maxlength', 191) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12">

                            {{ html()->label(__('labels.frontend.user.info.tabs.content.profile.payment_condition_days'))->for('payment_condition_days') }}
                            {{ html()->input('number','payment_condition_days')
                                ->class('form-control')
                                ->attributes(['min'=>'1', 'max'=>'365'])
                                ->required()  
                                ->placeholder(__('validation.attributes.frontend.payment_condition_days'))
                            }}
                            </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-sm-12">
                            {{ html()->label(__('labels.frontend.user.info.tabs.content.profile.payment_bonus_percentage'))->for('payment_bonus_percentage') }}
                            {{ html()->input('number','payment_bonus_percentage')->
                                class('form-control')
                                ->attributes(['min'=>'1', 'max'=>'100'])
                                ->required()  
                                ->placeholder(__('validation.attributes.frontend.payment_bonus_percentage'))
                            }}
                        </div>
                    </div>



                    <div class="row">
                        <div class="form-group col-sm-12">
                            {{ html()->label(__('labels.frontend.user.info.tabs.content.profile.payment_type_id'))->for('payment_type_id') }}
                            {{ html()->select('payment_type_id',$paymentTypes)
                                ->class('form-control')
                                ->placeHolder('Seleccione un valor')
                                ->required()
                            }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            {{ html()->label(__('labels.frontend.user.info.tabs.content.profile.delivery_time_days'))->for('delivery_time_days') }}
                            {{ html()->input('number','delivery_time_days')->
                                class('form-control')
                                ->attributes(['min'=>'1', 'max'=>'365'])
                                ->required()  
                                ->placeholder(__('validation.attributes.frontend.delivery_time_days'))
                            }}
                        </div>
                    </div>


                </div>
            </div> 
        </div>


</div>



    <div class="row">
        <div class="col">
            <div class="form-group mb-0 clearfix">
                {{ form_submit(__('labels.general.buttons.update')) }}
            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->
{{ html()->closeModelForm() }}

