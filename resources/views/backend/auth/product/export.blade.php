@extends('backend.layouts.app')

@push('after-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/af-2.3.3/b-1.5.6/r-2.2.2/sl-1.3.0/datatables.min.css"/>
<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
  
@endpush
@section('content')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.products.management') }} <small class="text-muted">{{ __('labels.backend.access.products.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.auth.product.includes.header-buttons-excel')
            </div><!--col-->
        </div><!--row-->


        <div class="row">
                <div class="col-md-12">
                    <h2 align="center">@lang('labels.general.verified_products')</h2>
					
					<table id="example" class="table table-hover table-striped">
						<thead>
							<tr>
                                <th><input name="select_all" value="1" type="checkbox"></th>
                                <th>@lang('labels.general.product_name')</th>
								<th>@lang('labels.general.description')</th>
								<th>@lang('labels.general.creation_date')</th>
                                <th>@lang('labels.general.downloaded')</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
                            <th></th>
                                <th>@lang('labels.general.product_name')</th>
								<th>@lang('labels.general.description')</th>
								<th>@lang('labels.general.creation_date')</th>
                                <th>@lang('labels.general.downloaded')</th>
							</tr>
						</tfoot>
					</table>
                </div>
        </div>

    </div><!--card-body-->
</div><!--card-->



@endsection


@push('after-scripts')


<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/af-2.3.3/b-1.5.6/r-2.2.2/sl-1.3.0/datatables.min.js"></script>
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>

<script>

//
// Updates "Select all" control in a data table
//
function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
}

$(document).ready(function (){

   // Array holding selected row IDs
   var rows_selected = [];

   var table = $('#example').DataTable( {
                'dom':'Bfrtip',
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url":"<?= route('admin.auth.excel.products') ?>",
					"dataType":"json",
					"type":"POST",
					"data":{"_token":"<?= csrf_token() ?>"}
				},
                'columnDefs': [{
                    'targets': 0,
                    'searchable': false,
                    'orderable': false,
                    'width': '1%',
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        return '<input type="checkbox">';
                    }
                    },
                    {
                    'targets': 4,
                    'searchable': false,
                    'orderable': false,
                    'width': '2%',
                    'className': 'dt-body-center',
                    'render': function (data, type, full, meta){
                        if(data===1){
                            return "<i class='fa fa-check'></i>";
                        }else {
                            return "";
                        }
                        
                    }
                    }
                    
                    
                    ],
                'buttons': [
                    {
                        text: 'Descargar Excel',
                        action: function ( e, dt, node, config ) {
                        var url_excel = '<?= route('admin.auth.excel.download') ?>';
                        var ids = rows_selected.join(',');
                        url_excel = url_excel + '?ids=' + ids;
                        //alert(url_excel);
                        window.open(url_excel, "_blank");
                            
                        }
                    }
                ],
				"columns":[
                    {"data":"id"},
					{"data":"name"},
					{"data":"description"},
					{"data":"created_at"},
                    {"data":"downloaded"}
				],
                "select": {
                    style:    'multi',
                },
                "order": [[ 1, 'asc' ]],
                'rowCallback': function(row, data, dataIndex){
                    // Get row ID
                    var rowId = data['id'];

                    // If row ID is in the list of selected row IDs
                    if($.inArray(rowId, rows_selected) !== -1){
                        $(row).find('input[type="checkbox"]').prop('checked', true);
                        $(row).addClass('selected');
                    }
                }
                
			});



   // Handle click on checkbox
   $('#example tbody').on('click', 'input[type="checkbox"]', function(e){
      var $row = $(this).closest('tr');

      // Get row data
      var data = table.row($row).data();

      // Get row ID
      var rowId = data['id'];

      //alert(rowId);
      // Determine whether row ID is in the list of selected row IDs
      var index = $.inArray(rowId, rows_selected);

      // If checkbox is checked and row ID is not in list of selected row IDs
      if(this.checked && index === -1){
         rows_selected.push(rowId);

      // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
      } else if (!this.checked && index !== -1){
         rows_selected.splice(index, 1);
      }

      if(this.checked){
         $row.addClass('selected');
      } else {
         $row.removeClass('selected');
      }

      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });

   // Handle click on table cells with checkboxes
   $('#example').on('click', 'tbody td, thead th:first-child', function(e){
      $(this).parent().find('input[type="checkbox"]').trigger('click');
   });

   // Handle click on "Select all" control
   $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
      if(this.checked){
         $('#example tbody input[type="checkbox"]:not(:checked)').trigger('click');
      } else {
         $('#example tbody input[type="checkbox"]:checked').trigger('click');
      }

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });

   // Handle table draw event
   table.on('draw', function(){
      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);
   });

});

		</script>
@endpush