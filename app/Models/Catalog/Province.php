<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    //
    public function country()
    {
        return $this->belongsTo('App\Models\Catalog\Country');
    }
}
