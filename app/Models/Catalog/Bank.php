<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
use App\Models\Catalog\Traits\Attribute\BankAttribute;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    use BankAttribute,
    SoftDeletes;
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'country_id'
    ];
    
    protected $dates = ['deleted_at'];
}
