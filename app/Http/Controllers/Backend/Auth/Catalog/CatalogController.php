<?php

namespace App\Http\Controllers\Backend\Auth\Catalog;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Auth\Catalog\ManageCatalogRequest;
use App\Models\Auth\Catalog;
use App\Models\Catalog\Bank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class CatalogController.
 */
class CatalogController extends Controller
{


     /**
     * @param ManageCatalogRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        //DB::enableQueryLog(); 
        //$totalData = Product::verified()->count();
        //dump($totalData);
        //dd(DB::getQueryLog()); // Show results of log\
        $catalogs = Catalog::where('enabled_for_edit',1)->paginate(10);
        return view('backend.auth.catalog.index')->withCatalogs($catalogs);
    }


    /**
     * @param ManageCatalogRequest $request
     *
     * @return mixed
     */
    public function edit(ManageCatalogRequest $request, $id)
    {
        
        $catalog = Catalog::find($id);
        //dd($catalog->table_name);
        // return view('backend.auth.product.create_step2')
        // ->with('measurement_units', $measurement_units)
        // ->with('packaging_measurement_units', $packaging_measurement_units)
        // ->withProduct($product);
        
        return redirect()->route($catalog->route);

        // if($catalog->table_name == 'banks'){
        //     $banks = Bank::all();
        //     return view('backend.auth.catalog.'.$catalog->table_name.'.index')
        //     ->with('banks',$banks);
        // }


    }


}