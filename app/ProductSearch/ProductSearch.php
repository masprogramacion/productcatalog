<?php

namespace App\ProductSearch;

use App\Models\Auth\Product;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Carbon\Carbon;

class ProductSearch
{
    
    public static function apply(Request $filters, $paged = 25, $orderBy = 'created_at', $sort = 'desc',$user_id=false):LengthAwarePaginator
    {
        
        
        $query = 
            static::applyDecoratorsFromRequest(
                $filters, (new Product)->newQuery()
            );
            
            if($user_id){
                $query->where('user_id','=',$user_id);
            }

            //dd($query->toSql());
            
            return $query->orderBy($orderBy, $sort)->paginate($paged);
            
        //return static::getResults($query);
    }
    
    private static function applyDecoratorsFromRequest(Request $request, Builder $query)
    {
        //dd($request->all());
        foreach ($request->all() as $filterName => $value) {

            $decorator = static::createFilterDecorator($filterName);

            if (static::isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);
            }

        }

        if (!is_null($request->start_date) && !is_null($request->end_date)) {
            $start = Carbon::parse($request->start_date)->startOfDay();  //2016-09-29 00:00:00.000000
            $end = Carbon::parse($request->end_date)->endOfDay(); //2016-09-29 23:59:59.000000
            $query->whereBetween('created_at', [$start, $end]);
        }else if (is_null($request->start_date) && !is_null($request->end_date)) {
            $end = Carbon::parse($request->end_date)->endOfDay(); //2016-09-29 23:59:59.000000
            $query->where('created_at', '<=', $end);
        }else if (!is_null($request->start_date) && is_null($request->end_date)) {
            $start = Carbon::parse($request->start_date)->startOfDay();  //2016-09-29 00:00:00.000000
            $query->where('created_at', '>=', $start);
        }

        // INTERNAL STATUS ARE MUST BE MAPPED TO ALLOW A PROVIDER MAKE A SEARCH BY STATUS
        if (!is_null($request->vendor_status_id) ) {

            switch ($request->vendor_status_id) {
                case 0:
                    $query->where('status_id', '=', 1);
                    break;
                case 1:
                    $query->whereIn('status_id', [2, 3, 4, 5, 6]);
                    break;
                case 2:
                    $query->whereIn('status_id', [ 7, 8, 9, 10]);
                    break;
                case 3:
                    $query->where('status_id', '=', 11);
                    break;
            }

        }



        //dd($request);
        return $query;
    }
    
    private static function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\Filters\\' . 
            str_replace(' ', '', 
                ucwords(str_replace('_', ' ', $name)));
    }
    
    private static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    private static function getResults(Builder $query)
    {
        return $query->get();
    }

}