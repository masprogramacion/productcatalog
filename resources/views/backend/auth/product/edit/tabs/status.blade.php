@php

$label_button = 'Aprobar el Producto';

@endphp

  @if($show_form)

  <div class="container">


    {{ html()->modelForm($product, 'PATCH', route('admin.auth.product.update.status.update', $product->id))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-status-product','novalidate'=>'novalidate'])->open() }}
    <input type="hidden"  id="flag_approved" name="flag_approved" value="1">
    <div class="table-responsive">
        <table class="table table-hover">

            <tr>
                    <th>@lang('labels.backend.access.products.tabs.content.status.comments')</th>
                    <td>
                        {{ html()->textarea('comments')
                                                ->class('form-control')
                                                ->attribute('maxlength', 255)
                                                ->required()}}
                        <div class="invalid-feedback">
                        @lang('validation.required', ['attribute' => __('labels.backend.access.products.tabs.content.status.comments')])
                        </div>
                        
                    </td>
            </tr>


    
    @if((auth()->user()->hasRole('planimetria') && ($product->status_id == 4)))

    <tr>
        <th>{{ html()->label(__('labels.products.form.label.store_id'))->class('form-control-label') }}</th>
        <td>


        <div class="col-md-6 mb-3">
          {{ html()->select('store_id_status',$stores)
                        ->class('select2m custom-select')
                        ->attributes(['multiple'=>'multiple', 'name'=>'store_id[]'])
                        ->required() }}
          {{ html()->label(html()->checkbox('checkBoxAllStatus', false, 1) . ' ' . __('labels.products.form.label.select_all'))->for('checkBoxAllStatus') }}  
  
          <small class="text-muted"></small>
          <div class="invalid-feedback">
          {{ $errors->first('store_id_status') }}
          </div>
          </div>
        </td>
    </tr>
    @endif

    @if(auth()->user()->hasRole('finanzas') && ($product->status_id == 5))
    
      @php
        $label_button  = __('buttons.backend.products.confirm_payment');
      @endphp


            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.status.payment_information')</th>
                <td>
                @lang('labels.backend.access.products.tabs.content.status.payment_verified_at')
                {{ html()->input('text','payment_verified_at')
                                        ->class('form-control')
                                        ->attributes(['placeholder'=>'Seleccione una fecha'])
                                        ->value($product->payment_verified_at)
                                        ->required() }}
                    <div class="invalid-feedback">
                    @lang('validation.required', ['attribute' => __('labels.backend.access.products.tabs.content.status.payment_verified_at')])
                    </div>
                </td>
                <td>
                @lang('labels.backend.access.products.tabs.content.status.sap_document')
                {{ html()->text('sap_document')
                                    ->class('form-control')
                                    ->placeholder('Documento de SAP')
                                    ->required()
                                    ->attribute('maxlength', 50)}}
                    <div class="invalid-feedback">
                    @lang('validation.required', ['attribute' => __('labels.backend.access.products.tabs.content.status.sap_document')])
                    </div>
                </td>
                <td></td>
            </tr>

    @endif
        <tr>

          <td>
           <button id="btnRejectProduct" class="btn btn-sm btn-danger" type="button"><i class="fa fa-thumbs-down"></i> Rechazar Producto</button>
          </td>
          <td>
            <button id="btnApproveProduct" class="btn btn-sm btn-success" type="button"><i class="fa fa-thumbs-up"></i> Aprobar Producto</button>
          </td>
        </tr>
    
    </table>
    </div><!-- ends table-responsive --> 
    {{ html()->form()->close() }}


  </div><!-- ends container --> 
  @endif





<div class="container mt-5 mb-5">
	<div class="row">
		<div class="col-md-10 offset-md-1">
			<h4>{{ __('labels.backend.access.products.tabs.content.status.history_transitions') }}</h4>

      <ul class="timeline">
      @foreach($workflow_statuses as $workflow_status)

        @if($workflow_status->status_id==2 || $workflow_status->status_id==3  || $workflow_status->status_id==4   || $workflow_status->status_id==5  )
        <li class="timeline-inverted">
          <div class="timeline-badge {{$workflow_status->color}}"><i class="fa {{$workflow_status->icon}}"></i></div>
        @elseif($workflow_status->status_id==7 || $workflow_status->status_id==8  || $workflow_status->status_id==9   || $workflow_status->status_id==10  )
        <li class="timeline">
          <div class="timeline-badge {{$workflow_status->color}}"><i class="fa {{$workflow_status->icon}}"></i></div>
        @elseif($workflow_status->status_id==11 )
        <li class="timeline-inverted">
          <div class="timeline-badge {{$workflow_status->color}}"><i class="fa {{$workflow_status->icon}}"></i></div>
        @else
        <li>
          <div class="timeline-badge"><i class="fa {{$workflow_status->icon}}"></i></div>
        @endif

          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">{{$workflow_status->current_status_description}}</h4>
              <p><small class="text-muted"><i class="fa {{$workflow_status->icon}}"></i> {{$workflow_status->updated_at}}</small></p>
            </div>
            <div class="timeline-body">
              <p>Usuario: {{$workflow_status->user->full_name}} <br/>Comentarios: {{$workflow_status->comments}}</p>
            </div>
          </div>
        </li>

      @endforeach
      </ul>
		</div>
	</div>
</div>



<div id="reject-modal" class="modal fade">
        <div class="modal-dialog">
  

                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Rechazar Producto</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Esta seguro que desea rechazar este producto  ?</p>
                        <div class="row">

                          <div class="col-sm-12">

                            <small class="text-muted"></small>
                            <div class="invalid-feedback">
                            </div>
                          </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-danger cancel" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
                        <button type="button" class="btn btn-sm btn-success ok  pull-left" data-loading-text="Loading&hellip;"><i class="fa fa-check"></i> Rechazar</button>

                    </div>
                </div>

        </div>
    </div>


    <div id="approve-modal" class="modal fade">
        <div class="modal-dialog">
 
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Aprobar Producto</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Esta seguro que desea aprobar este producto?</p>
                        <div class="row">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-danger cancel" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
                        <button type="button" class="btn btn-sm btn-success ok  pull-left" data-loading-text="Loading&hellip;"><i class="fa fa-check"></i> Aprobar</button>

                    </div>
                </div>

        </div>
    </div>
