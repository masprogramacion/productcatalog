<?php

namespace App\Http\Requests\Backend\Auth\Product;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateUserRequest.
 */
class SearchProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return $this->user()->isAdmin();
         //TODO this can be used by a regular user as well
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department_id' => 'max:191',
            'product_name' => 'max:255',
            'start_date' => 'sometimes|nullable|date | date_format:"Y-m-d"',
            'end_date' => 'sometimes|nullable|date | date_format:"Y-m-d"|after_or_equal:start_date'
        ];
    }
}
