<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('menus.backend.access.users.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.auth.vendor.index') }}">@lang('menus.backend.access.vendors.all')</a>
                <a class="dropdown-item" href="{{ route('admin.auth.vendor.create') }}">@lang('menus.backend.access.vendors.create')</a>
                <a class="dropdown-item" href="{{ route('admin.auth.vendor.deactivated') }}">@lang('menus.backend.access.vendors.deactivated')</a>
                <a class="dropdown-item" href="{{ route('admin.auth.vendor.deleted') }}">@lang('menus.backend.access.vendors.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
