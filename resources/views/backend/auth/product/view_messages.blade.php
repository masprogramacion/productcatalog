@extends('backend.layouts.app')

@section('content')


<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.products.messages.title') }} <small class="text-muted">{{ __('labels.backend.access.products.messages.messages_received') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ __('labels.backend.access.products.messages.status') }}</th>
                            <th>{{ __('labels.backend.access.products.messages.sender') }}</th>
                            <th>{{ __('labels.backend.access.products.messages.product_name') }}</th>
                            <th>{{ __('labels.backend.access.products.messages.subject') }}</th>
                            <th>{{ __('labels.backend.access.products.messages.received_at') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($messages as $message)
                            <tr>
                                <td>{!!$message->status_icon!!}</td>
                                <td>{{$message->sender->full_name}}</td>
                                <td>{{$message->product->name}}</td>
                                <td><b>{{$message->subject}}</b></td>
                                <td>{!!$message->status_badge!!}</td>
                                <td>{!!$message->show_button!!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $messages->total() !!} {{ trans_choice('labels.backend.access.products.table.total_messages', $messages->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $messages->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->


<!--
<div class="row">
<div class="toolbar">
<div class="btn-group">
<button class="btn btn-light" type="button">
<span class="fa fa-envelope"></span>
</button>
<button class="btn btn-light" type="button">
<span class="fa fa-star"></span>
</button>
<button class="btn btn-light" type="button">
<span class="fa fa-star-o"></span>
</button>
<button class="btn btn-light" type="button">
<span class="fa fa-bookmark-o"></span>
</button>
</div>
<div class="btn-group">
<button class="btn btn-light" type="button">
<span class="fa fa-mail-reply"></span>
</button>
<button class="btn btn-light" type="button">
<span class="fa fa-mail-reply-all"></span>
</button>
<button class="btn btn-light" type="button">
<span class="fa fa-mail-forward"></span>
</button>
</div>
<button class="btn btn-light" type="button">
<span class="fa fa-trash-o"></span>
</button>
<div class="btn-group">
<button class="btn btn-light dropdown-toggle" type="button" data-toggle="dropdown">
<span class="fa fa-tags"></span>
<span class="caret"></span>
</button>
<div class="dropdown-menu">
<a class="dropdown-item" href="#">add label
<span class="badge badge-danger"> Home</span>
</a>
<a class="dropdown-item" href="#">add label
<span class="badge badge-info"> Job</span>
</a>
<a class="dropdown-item" href="#">add label
<span class="badge badge-success"> Clients</span>
</a>
<a class="dropdown-item" href="#">add label
<span class="badge badge-warning"> News</span>
</a>
</div>
</div>
<div class="btn-group float-right">
<button class="btn btn-light" type="button">
<span class="fa fa-chevron-left"></span>
</button>
<button class="btn btn-light" type="button">
<span class="fa fa-chevron-right"></span>
</button>
</div>
</div>
<ul class="messages">
<li class="message unread">
<a href="#">
<div class="actions">
<span class="action">
<i class="fa fa-square-o"></i>
</span>
<span class="action">
<i class="fa fa-star-o"></i>
</span>
</div>
<div class="header">
<span class="from">Lukasz Holeczek</span>
<span class="date">
<span class="fa fa-paper-clip"></span> Today, 3:47 PM</span>
</div>
<div class="title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
</a>
</li>
<li class="message">
<a href="#">
<div class="actions">
<span class="action">
<i class="fa fa-square-o"></i>
</span>
<span class="action">
<i class="fa fa-star-o"></i>
</span>
</div>
<div class="header">
<span class="from">Lukasz Holeczek</span>
<span class="date">
<span class="fa fa-paper-clip"></span> Today, 3:47 PM</span>
</div>
<div class="title">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
</a>
</li>
<li class="message">
<a href="#">
<div class="actions">
<span class="action">
<i class="fa fa-square-o"></i>
</span>
<span class="action">
<i class="fa fa-star-o"></i>
</span>
</div>
<div class="header">
<span class="from">Lukasz Holeczek</span>
<span class="date">Today, 3:47 PM</span>
</div>
<div class="title">Lorem ipsum dolor sit amet.</div>
<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
</a>
</li>
<li class="message unread">
<a href="#">
<div class="actions">
<span class="action">
<i class="fa fa-square-o"></i>
</span>
<span class="action">
<i class="fa fa-star-o"></i>
</span>
</div>
<div class="header">
<span class="from">Lukasz Holeczek</span>
<span class="date">Today, 3:47 PM</span>
</div>
<div class="title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
</a>
</li>
<li class="message">
<a href="#">
<div class="actions">
<span class="action">
<i class="fa fa-square-o"></i>
</span>
<span class="action">
<i class="fa fa-star-o"></i>
</span>
</div>
<div class="header">
<span class="from">Lukasz Holeczek</span>
<span class="date">Today, 3:47 PM</span>
</div>
<div class="title">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
</a>
</li>
<li class="message">
<a href="#">
<div class="actions">
<span class="action">
<i class="fa fa-square-o"></i>
</span>
<span class="action">
<i class="fa fa-star-o"></i>
</span>
</div>
<div class="header">
<span class="from">Lukasz Holeczek</span>
<span class="date">Today, 3:47 PM</span>
</div>
<div class="title">Lorem ipsum dolor sit amet.</div>
<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
</a>
</li>
<li class="message unread">
<a href="#">
<div class="actions">
<span class="action">
<i class="fa fa-square-o"></i>
</span>
<span class="action">
<i class="fa fa-star-o"></i>
</span>
</div>
<div class="header">
<span class="from">Lukasz Holeczek</span>
<span class="date">Today, 3:47 PM</span>
</div>
<div class="title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</div>
<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
</a>
</li>
<li class="message">
<a href="#">
<div class="actions">
<span class="action">
<i class="fa fa-square-o"></i>
</span>
<span class="action">
<i class="fa fa-star-o"></i>
</span>
</div>
<div class="header">
<span class="from">Lukasz Holeczek</span>
<span class="date">Today, 3:47 PM</span>
</div>
<div class="title">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
</a>
</li>
<li class="message">
<a href="#">
<div class="actions">
<span class="action">
<i class="fa fa-square-o"></i>
</span>
<span class="action">
<i class="fa fa-star-o"></i>
</span>
</div>
<div class="header">
<span class="from">Lukasz Holeczek</span>
<span class="date">Today, 3:47 PM</span>
</div>
<div class="title">Lorem ipsum dolor sit amet.</div>
<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
</a>
</li>
</ul>
</div>
-->
      

@endsection
@push('after-scripts')
    
    <script>





    </script>
    
@endpush