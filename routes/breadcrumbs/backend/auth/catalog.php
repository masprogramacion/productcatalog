<?php

Breadcrumbs::for('admin.auth.catalog.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('labels.backend.access.catalogs.management'), route('admin.auth.catalog.index'));
});


Breadcrumbs::for('admin.auth.catalog.edit', function ($trail, $id) {
    $trail->parent('admin.auth.catalog.index');
    $trail->push(__('labels.backend.access.catalogs.edit'), route('admin.auth.catalog.edit', $id));
});

Breadcrumbs::for('admin.auth.catalog.bank.index', function ($trail) {
    $trail->parent('admin.auth.catalog.index');
    $trail->push(__('labels.backend.access.catalogs.banks.management'), route('admin.auth.catalog.bank.index'));
});



