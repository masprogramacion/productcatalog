<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\Product;
use App\Models\Auth\User;
use App\Models\Catalog\ProductStatus;

/**
 * Class WorkflowStatusRelationship.
 */

trait WorkflowStatusRelationship
{

    /**
     * @return mixed
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }


    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
     */
    public function productStatus()
    {
        return $this->belongsTo(ProductStatus::class);
    }



}
