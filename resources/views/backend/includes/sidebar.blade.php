<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/dashboard')) }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon icon-speedometer"></i> @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>

            <li class="nav-title">
                @lang('menus.backend.sidebar.system')
            </li>


            @if ($logged_in_user->isAdmin())
                <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/auth*')) }}" href="#">
                        <i class="nav-icon icon-user"></i> @lang('menus.backend.access.title')

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger"></span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.auth.user.index') }}">
                                @lang('labels.backend.access.users.management')

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger"></span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/vendor*')) }}" href="{{ route('admin.auth.vendor.index') }}">
                                @lang('labels.backend.access.vendors.management')

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger"></span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/role*')) }}" href="{{ route('admin.auth.role.index') }}">
                                @lang('labels.backend.access.roles.management')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/department*')) }}" href="{{ route('admin.auth.department.index') }}">
                                @lang('labels.backend.access.departments.management')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/department*')) }}" href="{{ route('admin.auth.department.index_assistant') }}">
                                @lang('labels.backend.access.departments.management_assistant')
                            </a>
                        </li>
                    </ul>
                </li>
            @endif

            @if ($logged_in_user->isAdmin() || auth()->user()->can('create uncatalogued products'))
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/product*')) }}" href="{{ route('admin.auth.product.create.uncatalogued') }}">
                <i class="nav-icon icon-basket"></i>
                    @lang('menus.backend.products.uncatalogued')

                    @if (! empty($products_pending_approval) && $products_pending_approval > 0)
                        <span class="badge badge-danger"></span>
                    @endif
                </a>
            </li>
            @endif

            @if ($logged_in_user->isAdmin() || auth()->user()->can('search products'))
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/product*')) }}" href="{{ route('admin.auth.product.index') }}">
                <i class="nav-icon icon-basket"></i>
                    @lang('menus.backend.products.main')

                    @if (! empty($products_pending_approval) && $products_pending_approval > 0)
                        <span class="badge badge-danger"></span>
                    @endif
                </a>
            </li>
            @endif

            @if ($logged_in_user->isAdmin() || auth()->user()->can('download_winshuttle_file'))
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/product*')) }}" href="{{ route('admin.auth.excel.index') }}">
                <i class="nav-icon icon-note"></i>
                    @lang('menus.backend.products.export-winshuttle')

                    @if (! empty($products_pending_upload) && $products_pending_upload > 0)
                        <span class="badge badge-danger"></span>
                    @endif
                </a>
            </li>
            @endif

            @if ($logged_in_user->isAdmin() || auth()->user()->can('view messages'))
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/product*')) }}" href="{{ route('admin.auth.product.message.index') }}">
                <i class="nav-icon icon-envelope"></i>
                    @lang('menus.backend.products.messages')

                    @if (  $logged_in_user->unread_messages_count > 0)
                        <span class="badge badge-danger">{{$logged_in_user->unread_messages_count}}</span>
                    @endif
                </a>
            </li>
            @endif
            <li class="divider"></li>


            @if ($logged_in_user->isAdmin() || auth()->user()->can('view vendors'))
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/vendor*')) }}" href="{{ route('admin.auth.vendor.index') }}">
                <i class="nav-icon icon-user"></i>
                    @lang('menus.backend.vendors.main')

                    @if ($vendors_pending_approval > 0)
                        <span class="badge badge-danger"></span>
                    @endif
                </a>
            </li>
            @endif


            @if ($logged_in_user->isAdmin())
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/log-viewer*')) }}" href="#">
                    <i class="nav-icon icon-list"></i> @lang('menus.backend.log-viewer.main')
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer')) }}" href="{{ route('log-viewer::dashboard') }}">
                            @lang('menus.backend.log-viewer.dashboard')
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer/logs*')) }}" href="{{ route('log-viewer::logs.list') }}">
                            @lang('menus.backend.log-viewer.logs')
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/phpinfo*')) }}" href="{{ route('laravelPhpInfo::phpinfo') }}">
                <i class="nav-icon icon-info"></i>
                    @lang('menus.backend.phpinfo.main')
                </a>
            </li>
            @endif

            @if ($logged_in_user->isAdmin())
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/catalog*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/catalog*')) }}" href="#">
                    <i class="nav-icon icon-list"></i> @lang('menus.backend.catalog.main')
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/catalog')) }}" href="{{ route('admin.auth.catalog.index') }}">
                            @lang('menus.backend.catalog.edit')
                        </a>
                    </li>
                </ul>
            </li>
            @endif


        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
