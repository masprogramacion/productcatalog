<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

/**
 * Class Handler.
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof UnauthorizedException) {
            return redirect()
                ->route(home_route())
                ->withFlashDanger(__('auth.general_error'));
        }

        //dd($exception);

        if ($exception instanceof \Illuminate\Http\Exceptions\PostTooLargeException) {
            // return Validator::make($request->all(), [
            //     'image_front_1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //     'image_back_1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            // ])->validate();
            //return redirect()->back()->withFlashDanger(['El archivo  debe tener ser de un maximo de 2MB']);
                    // card charge has failed
                    \Session::flash('msg', 'Changes Saved.' );
                    return redirect()->back()->with('error', 'Something went wrong.');
        }

        return parent::render($request, $exception);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param AuthenticationException  $exception
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {

        if($request->expectsJson()){
            return response()->json(['message' => $exception->getMessage()], 401);
        }
        
        $guard = array_get($exception->guards(), 0);

        switch ($guard){
            case 'admin':
                $login = 'frontend.auth.superadmin.login';
                break;
            default:
                $login = 'frontend.auth.login';
                break;
        }

        return redirect()->guest(route($login));
    }
}
