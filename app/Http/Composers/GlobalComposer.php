<?php

namespace App\Http\Composers;

use Illuminate\View\View;

/**
 * Class GlobalComposer.
 */
class GlobalComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     *
     * @return void
     */
    public function compose(View $view)
    {
        
        if (auth()->check()) { // checks for guard 'web'
            $view->with('logged_in_user', auth()->user());
        }elseif (auth('admin')->check()) {  
            $view->with('logged_in_user', auth('admin')->user());
        }

    }
}
