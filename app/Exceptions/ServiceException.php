<?php
 
namespace App\Exceptions;
 
use Exception;
 
class ServiceException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        /*
         * All 
         */
        //dd($this->message);
        return redirect()->route('admin.webservice_error')->withErrors($this->message);
    }
 

}