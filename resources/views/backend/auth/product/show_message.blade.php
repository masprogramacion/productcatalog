@extends('backend.layouts.app')

@section('content')


<div class="card shadow-none mt-3 border border-light">
               <div class="card-body">
                 <div class="media mb-3">
                   <img src="{{$message->sender->picture}}" class="rounded-circle mr-3 mail-img shadow" alt="media image"  width="100" height="100">
                     <div class="media-body">
                        <span class="media-meta float-right">{{ $message->created_at }}</span>
                        <h4 class="text-primary m-0">{{ $message->sender->full_name }}</h4>
                        <small class="text-muted">Enviado por : {{$message->sender->email}}</small>
                      </div>
                  </div> <!-- media -->

                  <p><b> </b><b>{{ $message->subject }}</b></p>
                  <p><b>Mensaje </b>{{ $message->body }}</p>
 
                  <hr>
                <!--
                  <h4> <i class="fa fa-paperclip mr-2"></i> Attachments <span>(3)</span> </h4>
                  <div class="row">
                      <div class="col-sm-4 col-md-3">
                          <a href="javascript:void();"> <img src="http://bootdey.com/img/Content/avatar/avatar6.png" alt="attachment" class="img-thumbnail"> </a>
                      </div>
                      <div class="col-sm-4 col-md-3">
                          <a href="javascript:void();"> <img src="http://bootdey.com/img/Content/avatar/avatar2.png" alt="attachment" class="img-thumbnail"> </a>
                      </div>
                      <div class="col-sm-4 col-md-3">
                          <a href="javascript:void();"> <img src="http://bootdey.com/img/Content/avatar/avatar3.png" alt="attachment" class="img-thumbnail"> </a>
                      </div>
                  </div>

                  <div class="media mt-3">
                      <a href="javascript:void();" class="media-left">
                          <img alt="" src="http://bootdey.com/img/Content/avatar/avatar1.png" width="50" height="50">
                      </a>
                      <div class="media-body">
                          <textarea class="wysihtml5 form-control" rows="9" placeholder="Reply here..."></textarea>
                      </div>
                  </div>
                  <div class="text-right">
                      <button type="button" class="btn btn-primary waves-effect waves-light mt-3"><i class="fa fa-send mr-1"></i> Send</button>
                  </div>
                  -->
              
              </div>
              <div class="card-footer clearfix">
                <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.product.message.index'), __('buttons.general.goback')) }}
                </div><!--col-->

                <div class="col text-right">

                </div><!--row-->
            </div><!--row-->
    </div><!--card-footer-->
            </div> <!-- card -->
      

@endsection
@push('after-scripts')
    
    <script>





    </script>
    
@endpush