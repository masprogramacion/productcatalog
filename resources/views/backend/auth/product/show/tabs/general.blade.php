<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.name')</th>
                <td>{{ $product->name }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.status')</th>
                <td>{!! $product->status_label !!}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.description')</th>
                <td>{!! $product->description !!}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.price')</th>
                <td>{{ $product->price }}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.suggested_price')</th>
                <td>{{ $product->suggested_price }}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.brand_id')</th>
                <td>
                @if($brand)
                {{ $brand->name }}
                @endif
                </td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.manufacturer_id')</th>
                <td>
                @if($manufacturer)
                {{ $manufacturer->name }}
                @endif
                </td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.min_remain_shelf_life')</th>
                <td>{{ $product->min_remain_shelf_life }}</td>
            </tr>
       
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.ind_include_discount')</th>
                @if($product->ind_include_discount)
                <td>@lang('labels.backend.access.products.tabs.content.overview.discount_percentage'): {{ $product->discount_percentage }}%</td>
                @else
                <td>@lang('labels.backend.access.products.tabs.content.overview.ind_include_discount_false')</td>
                @endif
            </tr>
            @if($product->ind_include_discount)
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.ind_limited_time_discount')</th>
                @if($product->ind_limited_time_discount)
                <td>@lang('labels.backend.access.products.tabs.content.overview.discount_start_date'): {{ $product->discount_start_date }} - @lang('labels.backend.access.products.tabs.content.overview.discount_end_date'):{{ $product->discount_end_date }}</td>
                @else
                <td>@lang('labels.backend.access.products.tabs.content.overview.ind_limited_time_discount_false')</td>
                @endif
            </tr>
            @endif
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.old_material_number')</th>
                <td>{{ $product->old_material_number }}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.vendor_material_number')</th>
                <td>{{ $product->vendor_material_number }}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.created_at')</th>
                <td>{{ $product->created_at }}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.updated_at')</th>
                <td>{{ $product->updated_at }}</td>
            </tr>
        </table>
    </div>
    <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.auth.product.index'), __('buttons.general.goback')) }}
                    </div><!--col-->
                <div class="col text-right">
                @if($show_edit_button)
                    <a class="btn btn-success btn-sm pull-right" href="{{ route('admin.auth.product.edit', $product->id) }}" role="button">@lang('buttons.general.crud.edit')</a>
                @endif
                </div><!--row-->
                </div><!--row-->
    </div><!--card-footer-->
</div><!--table-responsive-->
