<?php

namespace App\Http\Controllers\Frontend\Auth\Product;

use Illuminate\Support\Facades\Auth;
use App\Models\Auth\Product;
use App\Models\Auth\Message;
use App\Models\Catalog\Manufacturer;
use App\Models\Catalog\Brand;
use App\Models\Catalog\ProductStatus;
use App\Models\Catalog\MaterialType;
use App\Http\Controllers\Controller;
use App\Models\Catalog\PurchaseOrganization;
use App\Models\Catalog\VendorAccount;
use App\Models\Catalog\MaterialGroup;
use App\Models\Catalog\MeasurementUnit;
use App\Models\Catalog\PackagingMeasurementUnit;
use App\Models\Catalog\PurchaseGroup;
use App\Models\Catalog\GtinType;
use App\Models\Catalog\IvaCode;
use App\Models\Catalog\TaxCode;
use App\Models\Catalog\MaterialTaxType;
use App\Models\Auth\WorkflowStatus;

use App\Http\Requests\Frontend\Auth\Product\StoreProductRequest;
use App\Http\Requests\Frontend\Auth\Product\StoreStep2ProductRequest;
use App\Http\Requests\Frontend\Auth\Product\StoreStep3ProductRequest;
use App\Http\Requests\Frontend\Auth\Product\ManageProductRequest;
use App\Http\Requests\Frontend\Auth\Product\UpdateProductRequest;
use App\Http\Requests\Frontend\Auth\Product\SearchProductRequest;
use App\Http\Requests\Frontend\Auth\Product\UpdateImagesProductRequest;
use App\Http\Requests\Frontend\Auth\Product\UpdateGeneralProductRequest;
use App\Http\Requests\Frontend\Auth\Product\UpdatePackagingProductRequest;
use App\Repositories\Frontend\Auth\ProductRepository;
use App\Events\Backend\Auth\Product\ProductDeleted;
use App\Events\Backend\Auth\Product\ProductCreated;
use App\Events\Backend\Auth\Product\ProductUpdated;
use Illuminate\Support\Collection;
/**
 * Class ProductController.
 */
class ProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * ProductController constructor.
     *
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param ManageProductRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageProductRequest $request)
    {
        //$product_statuses = ProductStatus::pluck('name','id');
        $product_statuses = new Collection(['Creado', 'Pendiente de Revisión', 'Rechazado','Aprobado']);
        $products = $this->productRepository->getPaginated(config('xtra.paged.products'));
        //dd($products);
        if(auth()->user()->profile()->exists() && auth()->user()->bankAccount()->exists()) {

            return view('frontend.auth.product.index')
            ->with('product_statuses', $product_statuses)
            ->withProducts($products);

        }else {

            return redirect()->route('frontend.user.dashboard')->withFlashDanger(__('alerts.frontend.user.missing_information'));
        }

    }

    /**
     * @param ManageProductRequest    $request
     *
     * @return mixed
     */
    public function create(ManageProductRequest $request)
    {
        
        
        //$brands = $this->productRepository->getBrandLocal();
        //$brands = $this->productRepository->saveBrands();
        //$departments = $this->productRepository->saveDepartments();
        //dd();
        //$manufacturers = $this->productRepository->getManufacturers();
        $departments = $this->productRepository->getDepartment();
        $brands = Brand::pluck('name','id');


        //dd($manufacturers);
        $manufacturers = Manufacturer::pluck('name','id');
        //$product_statuses = ProductStatus::pluck('name','id');
       //$material_types = MaterialType::pluck('name','id');
        //$purchase_organizations = PurchaseOrganization::pluck('name','id');
        //$vendor_accounts = VendorAccount::pluck('name','id');
        //$material_groups = MaterialGroup::pluck('name','id');
        //$measurement_units = MeasurementUnit::pluck('short_text_description as name','id');
       // $purchase_groups = PurchaseGroup::pluck('name','id');
        //$gtin_types = GtinType::pluck('name','id');
        //$material_tax_types = MaterialTaxType::pluck('name','id');
        $iva_codes = IvaCode::pluck('name','id');
        $purchase_tax_codes = TaxCode::pluck('name','id');
        //dd($product_statuses);
        //$departments = $this->productRepository->getProductCategory('101019');
        
        //dd($departments);

        return view('frontend.auth.product.create')
        ->withManufacturers($manufacturers)
        ->with('departments',$departments)
        //->with('material_types',$material_types)
        //->with('purchase_organizations',$purchase_organizations)
        //->with('purchase_groups',$purchase_groups)
        ->withBrands($brands)
        //->with('vendor_accounts', $vendor_accounts)
        //->with('material_groups', $material_groups)
        //->with('measurement_units', $measurement_units)
        //->with('gtin_types', $gtin_types)
       // ->with('material_tax_types', $material_tax_types)
        ->with('purchase_tax_codes', $purchase_tax_codes)
        ->with('iva_codes', $iva_codes);
        //->with('product_statuses', $product_statuses);

    }



    /**
     * @param ManageProductRequest    $request
     * @param Product                 $product
     *
     * @return mixed
     */
    public function create_step2(ManageProductRequest $request, Product $product)
    {

        $sections_count = 1;
        if($product->alt_measure_unit_stockkeep_unit_id_2){
            $sections_count++;
        }
        if($product->alt_measure_unit_stockkeep_unit_id_3){
            $sections_count++;
        }

        $measurement_units = MeasurementUnit::pluck('short_text_description as name','id');
        $packaging_measurement_units = PackagingMeasurementUnit::pluck('short_text_description as name','id');
        $update_route = 'frontend.auth.product.update.step2';
        return view('frontend.auth.product.create_step2')
        ->with('measurement_units', $measurement_units)
        ->with('sections_count', $sections_count)
        ->with('update_route', $update_route)
        ->with('packaging_measurement_units', $packaging_measurement_units)
        ->withProduct($product);

    }


    /**
     * @param ManageProductRequest    $request
     * @param Product                 $product
     *
     * @return mixed
     */
    public function create_step3(ManageProductRequest $request, Product $product)
    {
       
        //dd($request);
        return view('frontend.auth.product.create_step3')
        ->withProduct($product);

    }    

     /**
     * @param String       $departmentId
     *
     * @return mixed
     */
    public function getCategory($departmentid)
    {
          $categories = $this->productRepository->getCategory($departmentid);
          return $categories;
    }


    /**
     * @param StoreProductRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreProductRequest $request)
    {
        $product = $this->productRepository->create($request->only(
            'product_name',
            'description',
            'price',
            'suggested_price',
            'brand_id',
            'manufacturer_id',
            'min_remain_shelf_life',
            'ind_include_discount',
            'discount_percentage',
            'ind_limited_time_discount',
            'discount_start_date',
            'discount_end_date',
            'old_material_number',
            'vendor_material_number',
            'purchase_tax_code_id',
            'iva_code_id',
            'department_id'
            
        ));

        //dd($request);

        return redirect()->route('frontend.auth.product.update.step2',$product)->withFlashSuccess(__('alerts.backend.products.created')); //TODO TRADUCIR
    }

    /**
     * @param StoreStep2ProductRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function update_step2(StoreStep2ProductRequest $request, Product $product)
    {
        
        //dd($param_image_front_1);

        $this->productRepository->update_step2($product, $request->only(
            'base_measurement_unit_id',
            'gtin_1',
            'lower_level_measurement_unit_qty_1',
            'lower_level_pack_hierarchy_measurement_unit_id_1',
            'ind_measurement_unit_is_order_unit_1',
            'ind_measurement_unit_is_delivery_or_issue_unit_1',
            'alt_measure_unit_stockkeep_unit_id_1',
            'gtin_2',
            'lower_level_measurement_unit_qty_2',
            'lower_level_pack_hierarchy_measurement_unit_id_2',
            'ind_measurement_unit_is_order_unit_2',
            'ind_measurement_unit_is_delivery_or_issue_unit_2',
            'alt_measure_unit_stockkeep_unit_id_2',
            'gtin_3',
            'lower_level_measurement_unit_qty_3',
            'lower_level_pack_hierarchy_measurement_unit_id_3',
            'ind_measurement_unit_is_order_unit_3',
            'ind_measurement_unit_is_delivery_or_issue_unit_3',
            'alt_measure_unit_stockkeep_unit_id_3',
            'length',
            'width',
            'height',
            'weight',
            'volume'
        )


        );
        
        //dd($request);

        return redirect()->route('frontend.auth.product.update.step3',$product)->withFlashSuccess(__('alerts.frontend.products.updated_now_upload_pictures')); 
    }



    /**
     * @param StoreStep3ProductRequest $request
     * @param Product $product
     * @return mixed
     * @throws \Throwable
     */
    public function update_step3(StoreStep3ProductRequest $request, Product $product)
    {
        
        //dd($request);
        $param_image_front_1 = $request->has('image_front_1') ? $request->file('image_front_1') : false;
        $param_image_back_1 = $request->has('image_back_1') ? $request->file('image_back_1') : false;


        //dd($param_image_front_1);

        $this->productRepository->update_step3($product, $request->only(
            'image_front_1',
            'image_back_1'
        ),
            $param_image_front_1,
            $param_image_back_1

        );
                
        //dd($request);

        return redirect()->route('frontend.auth.product.index',$product)->withFlashSuccess(__('alerts.frontend.products.sent_for_approval')); //TODO TRADUCIR
    }


    /**
     * @param UpdateGeneralProductRequest $request
     * @param Product $product
     * @return mixed
     * @throws \Throwable
     */
    public function update_general(UpdateGeneralProductRequest $request, Product $product)
    {
        
        $this->productRepository->update_general($product, $request->only(
            'name',
            'description',
            'price',
            'suggested_price',
            'min_remain_shelf_life',
            'department_id',
            'brand_id',
            'manufacturer_id',
            'ind_include_discount',
            'discount_percentage',
            'ind_limited_time_discount',
            'discount_start_date',
            'discount_end_date',
            'old_material_number',
            'vendor_material_number',
            //'material_tax_type_id',
            'purchase_tax_code_id',
            'iva_code_id'
        ));
        
        //dd($request);

        return redirect()->route('frontend.auth.product.edit',$product)->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }



    /**
     * @param UpdatePackagingProductRequest $request
     * @param Product $product
     * @return mixed
     * @throws \Throwable
     */
    public function update_packaging(UpdatePackagingProductRequest $request, Product $product)
    {
        
        //dd($request);
        $this->productRepository->update_packaging($product, $request->only(
            'base_measurement_unit_id',
            'gtin_1',
            'lower_level_measurement_unit_qty_1',
            'lower_level_pack_hierarchy_measurement_unit_id_1',
            'ind_measurement_unit_is_order_unit_1',
            'ind_measurement_unit_is_delivery_or_issue_unit_1',
            'alt_measure_unit_stockkeep_unit_id_1',
            'gtin_2',
            'lower_level_measurement_unit_qty_2',
            'lower_level_pack_hierarchy_measurement_unit_id_2',
            'ind_measurement_unit_is_order_unit_2',
            'ind_measurement_unit_is_delivery_or_issue_unit_2',
            'alt_measure_unit_stockkeep_unit_id_2',
            'gtin_3',
            'lower_level_measurement_unit_qty_3',
            'lower_level_pack_hierarchy_measurement_unit_id_3',
            'ind_measurement_unit_is_order_unit_3',
            'ind_measurement_unit_is_delivery_or_issue_unit_3',
            'alt_measure_unit_stockkeep_unit_id_3'
        ));
        
        //dd($request);

        return redirect()->route('frontend.auth.product.edit',$product)->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }

    /**
     * @param UpdateImagesProductRequest $request
     * @param Product $product
     * @return mixed
     * @throws \Throwable
     */
    public function update_images(UpdateImagesProductRequest $request, Product $product)
    {
        
        //dd($request);
        $param_image_front_1 = $request->has('image_front_1') ? $request->file('image_front_1') : false;
        $param_image_back_1 = $request->has('image_back_1') ? $request->file('image_back_1') : false;

        $this->productRepository->update_images($product, $request->only(
            'image_front_1',
            'image_back_1'
        ),
        $param_image_front_1,
        $param_image_back_1


        );
        
        //dd($request);

        return redirect()->route('frontend.auth.product.edit',$product)->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }




    /**
     * @param ManageProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     */
    public function show(ManageProductRequest $request, Product $product)
    {
        $sections_count = 1;
        if($product->alt_measure_unit_stockkeep_unit_id_2){
            $sections_count++;
        }
        if($product->alt_measure_unit_stockkeep_unit_id_3){
            $sections_count++;
        }

        $brand = Brand::where('id','=',$product->brand_id )->get()->first();
        //dd($brand);
        $manufacturer = Manufacturer::where('id','=',$product->manufacturer_id )->get()->first();
        //dd($manufacturers);
        //$brands = Brand::pluck('name','id');
        //$manufacturers = $this->productRepository->getManufacturers();
        $departments = $this->productRepository->getDepartment();
        $product_statuses = ProductStatus::pluck('name','id');
        $material_types = MaterialType::pluck('name','id');
        $purchase_organizations = PurchaseOrganization::pluck('name','id');
        $vendor_accounts = VendorAccount::pluck('name','id');
        $material_groups = MaterialGroup::pluck('name','id');
        $measurement_units = MeasurementUnit::pluck('short_text_description as name','id');
        $purchase_groups = PurchaseGroup::pluck('name','id');
        $gtin_types = GtinType::pluck('name','id');
        $material_tax_types = MaterialTaxType::pluck('name','id');
        $iva_codes = IvaCode::pluck('name','id');
        $tax_codes = TaxCode::pluck('name','id');
        $status_id_for_role = ProductStatus::pluck('name','id','role_id');
        $packaging_measurement_units = PackagingMeasurementUnit::pluck('short_text_description as name','id');
        $purchase_tax_codes = TaxCode::pluck('name','id');
        $workflow_statuses = WorkflowStatus::where('product_id', '=',$product->id)->whereIn('status_id',array(1,2,7,11))->orderBy('created_at', 'asc')->get();


        $loop_counter = 1;
        $elements_counter = count($workflow_statuses); 
        //dd($elements_counter);
        foreach ($workflow_statuses as $current_item) {
            


            if($loop_counter === $elements_counter) {

                $icon = 'fa-exclamation';
                $color = 'warning';
            }else {
                $icon = 'fa-check';
                $color = 'success';

            }

            if($current_item->status_id == 11) {
                $icon = 'fa-thumbs-up';
                $color = 'success';
            }

            if($current_item->status_id==7 || $current_item->status_id==8  || $current_item->status_id==9   || $current_item->status_id==10  ){
                $icon = 'fa-thumbs-down';
                $color = 'danger';
            }

            if($current_item->status_id == 1) {
                $icon = 'fa-cog';
                $color = 'danger';
            }

            $loop_counter = $loop_counter + 1;
            $current_item->color = $color;
            $current_item->icon = $icon;

        }

        $workflow_statuses_ordered = $workflow_statuses->reverse();



        return view('frontend.auth.product.show')
        ->with('material_types',$material_types)
        ->with('purchase_organizations',$purchase_organizations)
        ->with('purchase_groups',$purchase_groups)
        ->with('brand', $brand)
        ->with('manufacturer', $manufacturer)
        ->with('departments', $departments)
        ->with('vendor_accounts', $vendor_accounts)
        ->with('material_groups', $material_groups)
        ->with('measurement_units', $measurement_units)
        ->with('gtin_types', $gtin_types)
        ->with('material_tax_types', $material_tax_types)
        ->with('tax_codes', $tax_codes)
        ->with('iva_codes', $iva_codes)
        ->with('product_statuses', $product_statuses)
        ->with('status_id_for_role', $status_id_for_role)
        ->with('packaging_measurement_units', $packaging_measurement_units)
        ->with('measurement_units', $measurement_units)
        ->with('sections_count', $sections_count)
        ->with('purchase_tax_codes', $purchase_tax_codes)
        ->with('workflow_statuses', $workflow_statuses_ordered)
        ->with('product', $product);
    }

    /**
     * @param ManageProductRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     * @param Product                 $product
     *
     * @return mixed
     */
    public function edit(ManageProductRequest $request, Product $product)
    {
        $measurement_units = MeasurementUnit::pluck('short_text_description as name','id');
        $sections_count = 1;
       

        //$brands = Brand::pluck('name','id');
        $departments = $this->productRepository->getDepartment();
        $brands = $this->productRepository->getBrands();
        $manufacturers = $this->productRepository->getManufacturers();
        $product_statuses = ProductStatus::pluck('name','id');
        $material_types = MaterialType::pluck('name','id');
        $purchase_organizations = PurchaseOrganization::pluck('name','id');
        $vendor_accounts = VendorAccount::pluck('name','id');
        $material_groups = MaterialGroup::pluck('name','id');
        $measurement_units = MeasurementUnit::pluck('short_text_description as name','id');
        $purchase_groups = PurchaseGroup::pluck('name','id');
        $gtin_types = GtinType::pluck('name','id');
        $material_tax_types = MaterialTaxType::pluck('name','id');
        $iva_codes = IvaCode::pluck('name','id');
        $tax_codes = TaxCode::pluck('name','id');
        $status_id_for_role = ProductStatus::pluck('name','id','role_id');
        $packaging_measurement_units = PackagingMeasurementUnit::pluck('short_text_description as name','id');
        $purchase_tax_codes = TaxCode::pluck('name','id');
        $update_route = 'frontend.auth.product.update.packaging';
        $workflow_statuses = WorkflowStatus::where('product_id', '=',$product->id)->whereIn('status_id',array(1,2,7,11))->orderBy('created_at', 'desc')->get();

        $loop_counter = 1;
        $elements_counter = count($workflow_statuses); 
        //dd($elements_counter);
        foreach ($workflow_statuses as $current_item) {
            


            if($loop_counter === $elements_counter) {

                $icon = 'fa-exclamation';
                $color = 'warning';
            }else {
                $icon = 'fa-check';
                $color = 'success';

            }

            if($current_item->status_id == 11) {
                $icon = 'fa-thumbs-up';
                $color = 'success';
            }

            if($current_item->status_id==7 || $current_item->status_id==8  || $current_item->status_id==9   || $current_item->status_id==10  ){
                $icon = 'fa-thumbs-down';
                $color = 'danger';
            }

            if($current_item->status_id == 1) {
                $icon = 'fa-cog';
                $color = 'danger';
            }

            $loop_counter = $loop_counter + 1;
            $current_item->color = $color;
            $current_item->icon = $icon;

        }

        $workflow_statuses_ordered = $workflow_statuses->reverse();






        if($product->alt_measure_unit_stockkeep_unit_id_2){
            $sections_count++;
        }
        if($product->alt_measure_unit_stockkeep_unit_id_3){
            $sections_count++;
        }

        $show_update_status_form = false;

        if($product->status_id==7){
            $show_update_status_form=true;
        }



        return view('frontend.auth.product.edit')
        ->withManufacturers($manufacturers)
        ->with('material_types',$material_types)
        ->with('purchase_organizations',$purchase_organizations)
        ->with('purchase_groups',$purchase_groups)
        ->withBrands($brands)
        ->with('departments', $departments)
        ->with('vendor_accounts', $vendor_accounts)
        ->with('material_groups', $material_groups)
        ->with('measurement_units', $measurement_units)
        ->with('gtin_types', $gtin_types)
        ->with('material_tax_types', $material_tax_types)
        ->with('tax_codes', $tax_codes)
        ->with('iva_codes', $iva_codes)
        ->with('product_statuses', $product_statuses)
        ->with('status_id_for_role', $status_id_for_role)
        ->with('packaging_measurement_units', $packaging_measurement_units)
        ->with('measurement_units', $measurement_units)
        ->with('sections_count', $sections_count)
        ->with('purchase_tax_codes', $purchase_tax_codes)
        ->with('update_route', $update_route)
        ->with('show_update_status_form', $show_update_status_form)
        ->with('workflow_statuses', $workflow_statuses_ordered)
        ->withProduct($product);

    }

        /**
     * @param ManageProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     * @throws \Exception
     */
    public function resend(ManageProductRequest $request, Product $product)
    {
        
        //dd($request);
        $comments = null;

        if(isset($request->comments)){
            $comments= $request->comments;
        }

        $this->productRepository->update_status($product, true, $comments);

        event(new ProductUpdated($product));

        return redirect()->route('frontend.auth.product.edit', $product)->withFlashSuccess(__('alerts.backend.products.updated'));
    }


    /**
     * @param ManageProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageProductRequest $request, Product $product)
    {
        $this->productRepository->deleteById($product->id);

        event(new ProductDeleted($product));

        return redirect()->route('frontend.auth.product.deleted')->withFlashSuccess(__('alerts.backend.products.deleted'));
    }


     /**
     * @param SearchProductRequest $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function search(SearchProductRequest $request)
    {
        //$product_statuses = ProductStatus::pluck('name','id');
        $product_statuses = new Collection(['Creado', 'Pendiente de Revisión', 'Rechazado','Aprobado']);
        $request->flash();
        $request->request->add(['user_id' => Auth::user()->id]);
        return view('frontend.auth.product.index')
        ->with('product_statuses', $product_statuses)
        ->withProducts($this->productRepository->getFilteredPaginated($request, config('xtra.paged.products'), 'id', 'desc'));
    }

        /**
     * @param ManageProductRequest $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function view_messages(ManageProductRequest $request)
    {
         return view('frontend.auth.product.view_messages')
        ->withMessages($this->productRepository->getMessagesPaginated(config('xtra.paged.messages'), 'id', 'desc'));
    }


        /**
     * @param ManageProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     */
    public function show_message(ManageProductRequest $request, Message $message)
    {
        $message = $this->productRepository->update_message($message);
        return view('frontend.auth.product.show_message')
            ->withMessage($message);

    }

}
