<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Models\Auth\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\VendorRepository;
use App\Http\Requests\Backend\Auth\User\ManageUserVendorRequest;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;

/**
 * Class UserVendorConfirmationController.
 */
class UserVendorConfirmationController extends Controller
{
    /**
     * @var VendorRepository
     */
    protected $vendorRepository;

    /**
     * @param VendorRepository $vendorRepository
     */
    public function __construct(VendorRepository $vendorRepository)
    {
        $this->vendorRepository = $vendorRepository;
    }

    /**
     * @param ManageUserVendorRequest $request
     * @param User              $user
     *
     * @return mixed
     */
    public function sendConfirmationEmail(ManageUserVendorRequest $request, User $user)
    {
        // Shouldn't allow users to confirm their own accounts when the application is set to manual confirmation
        if (config('access.users.requires_approval')) {
            return redirect()->back()->withFlashDanger(__('alerts.backend.users.cant_resend_confirmation'));
        }

        if ($user->isConfirmed()) {
            return redirect()->back()->withFlashSuccess(__('exceptions.backend.access.users.already_confirmed'));
        }

        $user->notify(new UserNeedsConfirmation($user->confirmation_code,$user->email));

        return redirect()->back()->withFlashSuccess(__('alerts.backend.users.confirmation_email'));
    }

    /**
     * @param ManageUserVendorRequest $request
     * @param User              $user
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function confirm(ManageUserVendorRequest $request, User $user)
    {
        $this->vendorRepository->confirm($user);

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('alerts.backend.users.confirmed'));
    }

    /**
     * @param ManageUserVendorRequest $request
     * @param User              $user
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function unconfirm(ManageUserVendorRequest $request, User $user)
    {
        $this->vendorRepository->unconfirm($user);

        return redirect()->route('admin.auth.vendor.index')->withFlashSuccess(__('alerts.backend.users.unconfirmed'));
    }
}
