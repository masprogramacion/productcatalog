<?php

namespace App\Http\Controllers\Frontend\Auth;

//use App\Helpers\Auth\Auth;
use Auth;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Events\Frontend\Auth\AdminLoggedIn;
use App\Events\Frontend\Auth\AdminLoggedOut;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Repositories\Frontend\Auth\UserSessionRepository;

/**
 * Class LoginController.
 */
class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/admin'; //route to redirect to
    protected $guard = 'admin'; //guard id

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }


    /**
     * Where to redirect users after login.
     *
     * @return string
     */
    public function redirectPath()
    {
        //return route(home_route());
        //return "admin.dashboard";
        return "restricted.home";
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {

        return view('frontend.auth.superadmin-login');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return config('access.users.username');
    }


    public function login(Request $request){
        //return true;
        //validate the form data
        //dd($request);
        $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required|min:6'
        ]);
        //attempt to log the user in
        if (Auth::guard('admin')->attempt(['email'=> $request->email, 'password'=> $request->password],$request->remember)){
            //if successful, then redirect to their intended location
            //dd('t0:'. $request);
            return redirect()->intended(route('restricted.home'));
        }else {
            dd('t1:'. $request);
            //TODO VERIFY THIS
        }
        

        //if unsuccesful, then redirect back to the login with the form data
        return redirect()->back()->withInput($request->only('email'.'remember'));


    }


    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param         $user
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    protected function authenticated(Request $request, $user)
    {
        /*
         * Check to see if the users account is confirmed and active
         */

        //dd($user);
        if (! $user->isConfirmed()) {
            auth('admin')->logout();

        } elseif (! $user->isActive()) {
            auth('admin')->logout();
            throw new GeneralException(__('exceptions.frontend.auth.deactivated'));
        }

        event(new AdminLoggedIn($user));

        // If only allowed one session at a time
        // if (config('access.users.single_login')) {
        //     resolve(UserSessionRepository::class)->clearSessionExceptCurrent($user);
        // }
        dd($this->redirectPath());
        return redirect()->intended($this->redirectPath());
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {

        /*
         * Remove any session data from backend
         */
        //app()->make(Auth::class)->flushTempSession();
        //TODO verify if this method is needed

        /*
         * Fire event, Log out user, Redirect
         */
        event(new AdminLoggedOut($request->user()));

        /*
         * Laravel specific logic
         */
        $this->guard('admin')->logout();
        $request->session()->invalidate();

        return redirect()->route('frontend.index');
        //TODO REDIRECT TO SUPER ADMIN LOGIN
    }

    // /**
    //  * @return \Illuminate\Http\RedirectResponse
    //  */
    // public function logoutAs()
    // {
    //     // If for some reason route is getting hit without someone already logged in
    //     if (! auth()->user()) {
    //         return redirect()->route('frontend.auth.login');
    //     }

    //     // If admin id is set, relogin
    //     if (session()->has('admin_user_id') && session()->has('temp_user_id')) {
    //         // Save admin id
    //         $admin_id = session()->get('admin_user_id');

    //         app()->make(Auth::class)->flushTempSession();

    //         // Re-login admin
    //         auth()->loginUsingId((int) $admin_id);

    //         // Redirect to backend user page
    //         return redirect()->route('admin.auth.user.index');
    //     } else {
    //         app()->make(Auth::class)->flushTempSession();

    //         // Otherwise logout and redirect to login
    //         auth()->logout();

    //         return redirect()->route('frontend.auth.login');
    //     }
    // }
}
