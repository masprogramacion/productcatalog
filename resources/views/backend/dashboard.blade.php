@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')


@if(Auth::user()->can('assign products') || Auth::user()->isAdmin())
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.products.department_assignment') }} <small class="text-muted">{{ __('labels.backend.access.products.received') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">

            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.access.products.table.name')</th>
                            <th>Proveedor</th>
                            <th>Estatus</th>
                            <th>Fecha Creacion</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products_pending_asistente_compras as $product)
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->user->name }}</td>
                                <td>{!! $product->status_label !!}</td>
                                <td>{!! $product->created_at !!}</td>
                                <td>{!! $product->action_buttons_assign !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">

                    {!! $products_pending_asistente_compras->total() !!} {{ trans_choice('labels.backend.access.products.table.total', $products_pending_asistente_compras->total()) }}

                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                @if($products_pending_asistente_compras)
                    {!! $products_pending_asistente_compras->render() !!}
                @endif    
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endif


@if(Auth::user()->can('search products') || Auth::user()->isAdmin())
<div class="row">
    <div class="col-lg-12">
        <div class="card">

            <div class="card-header">
                    <i class="fa fa-search"></i> @lang('labels.backend.access.products.form.search.title')
                    <div class="card-header-actions">
                        <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true">
                        <i class="icon-arrow-up"></i>
                        </a>
                    </div>
            </div>

            <div class="card-body collapse show" id="collapseExample">
                
            {{ html()->form('POST', route('admin.auth.product.search'))->class('form-horizontal')->open() }}
            <div class="form-row">
                <div class="col-md-4 mb-3">
                {{ html()->label(__('Nombre'))->class('form-control-label')->for('product_name') }}
                {{ html()->text('product_name')
                        ->class('form-control')
                        ->attribute('maxlength', 100) }}
                </div>
                <div class="col-md-4 mb-3">
                {{ html()->label(__('labels.backend.access.products.form.search.status'))->class('form-control-label')->for('status_id') }}
                {{ html()->select('status_id', $product_statuses)
                            ->placeholder('Seleccione un Estatus')
                            ->class('form-control')}}
                </div>
                <div class="col-md-4 mb-3">
                </div>
            </div>

            <div class="form-row">

                <div class="form-group col-md-4">
                    {{ html()->label(__('labels.products.form.label.department_id'))->class('form-control-label') }}
                    {{ html()->select('department_id',$departments)
                                    ->placeholder('Seleccione un Departamento')
                                    ->class('select2 custom-select d-block w-100') }}
                    <small class="text-muted">@lang('labels.products.form.helptext.department_id')</small>
                    <div class="invalid-feedback">
                    {{ $errors->first('department_id') }}
                    </div>
                </div>
                <div class="form-group col-md-4">
                    {{ html()->label(__('labels.products.form.label.category_id'))->class('form-control-label') }}
                    {{ html()->select('category_id')
                                    ->class('select2 custom-select d-block w-100') }}
                    <small class="text-muted">@lang('labels.products.form.helptext.category_id')</small>
                    <div class="invalid-feedback">
                    {{ $errors->first('category_id') }}
                    </div>
                </div>
                <div class="form-group col-sm-4">
                    {{ html()->label(__('labels.products.form.label.hierarchy_id'))->class('form-control-label') }}
                    {{ html()->select('hierarchy_id')
                                    ->class('select2 custom-select d-block w-100') }}
                    <small class="text-muted">@lang('labels.products.form.helptext.hierarchy_id')</small>
                    <div class="invalid-feedback">
                    {{ $errors->first('hierarchy_id') }}
                    </div>
                </div>

            </div>


            <div class="form-row" >

                <div class="col-md-6 mb-3">
                {{ html()->label(__('labels.products.form.search.start_date'))->class('form-control-label')}}
                    <input name="start_date" id="start_date"  placeholder="Seleccione una fecha"  />
                </div>
                <div class="col-md-6 mb-3">
                {{ html()->label(__('labels.products.form.search.end_date'))->class('form-control-label')}}
                    <input name="end_date" id="end_date"  placeholder="Seleccione una fecha" />
                </div>

            </div>

            <button class="btn btn-primary" type="submit">Buscar</button>
            <button class="btn btn-secondary" type="button"  id="resetButton">Limpiar</button>
            {{ html()->form()->close() }}

            </div><!--card-body -->

        </div><!--card-->
    </div><!--col-->
</div><!--row-->
@endif


@if(Auth::user()->can('search uncatalogued products') || Auth::user()->isAdmin())


<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.products.management') }} <small class="text-muted">{{ __('labels.backend.access.products.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.auth.product.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.access.products.table.name')</th>
                            <th>Proveedor</th>
                            <th>Estatus</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products_uncatalogued as $product)
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->user->name }}</td>
                                <td>{!! $product->status_label !!}</td>
                                <td>{!! $product->show_button_uncatalogued !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">

                    {!! $products_uncatalogued->total() !!} {{ trans_choice('labels.backend.access.products.table.total', $products_uncatalogued->total()) }}

                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                @if($products_uncatalogued)
                    {!! $products_uncatalogued->appends(Request::capture()->except('page'))->render() !!}
                @endif
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->

@endif


@if(Auth::user()->can('view status board') || Auth::user()->isAdmin())
<div class="row">   

    <div class="container-fluid pt-3">
        <h3 class="font-weight-light">Tablero de Estatus</h3>
        <!--<div class="small">Ud puede arrastrar y soltar los productos</div>-->
        <div class="row flex-row flex-sm-nowrap py-3">
            <div class="col-sm-6 col-md-4 col-xl-3">
                <div class="card bg-light">
                    <div class="card-body">
                        <h6 class="card-title text-uppercase text-truncate py-2">COMPRAS</h6>
                        <div class="items border border-light">
                            
                                @if($products)
                                        @foreach($products as $product)
                                            <div class="card  shadow-sm bg-light" id="product_{{$product->id}}" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        {!! $product->statusBadge !!}
                                                        {{ $product->user->name }}
                                                    </div>
                                                    <p>
                                                    {{ $product->description }}
                                                    </p>
                                                    {!! $product->editStatusButton !!}
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>

                                        @endforeach
                                @endif
                            



                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-xl-3">
                <div class="card bg-light">
                    <div class="card-body">
                        <h6 class="card-title text-uppercase text-truncate py-2">GERENCIA DE CATEGORIAS</h6>
                        <div class="items border border-light">

                                @if($products_gerente_categoria)
                                        @foreach($products_gerente_categoria as $product)
                                            <div class="card  shadow-sm bg-light" id="product_{{$product->id}}" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        {!! $product->statusBadge !!}
                                                        {{ $product->user->name }}
                                                    </div>
                                                    <p>
                                                    {{ $product->description }}
                                                    </p>
                                                    {!! $product->editStatusButton !!}
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>

                                        @endforeach
                                @endif

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-xl-3">
                <div class="card bg-light">
                    <div class="card-body">
                        <h6 class="card-title text-uppercase text-truncate py-2">PLANIMETRIA</h6>
                        <div class="items border border-light">

                                @if($products_planimetria)
                                        @foreach($products_planimetria as $product)
                                            <div class="card  shadow-sm bg-light" id="product_{{$product->id}}" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        {!! $product->statusBadge !!}
                                                        {{ $product->user->name }}
                                                    </div>
                                                    <p>
                                                    {{ $product->description }}
                                                    </p>
                                                    {!! $product->editStatusButton !!}
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>

                                        @endforeach
                                @endif

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-xl-3">
                <div class="card bg-light">
                    <div class="card-body">
                        <h6 class="card-title text-uppercase text-truncate py-2">FINANZAS</h6>
                        <div class="items border border-light">
                        @if($products_finanzas)
                                        @foreach($products_finanzas as $product)
                                            <div class="card shadow-sm bg-light" id="product_{{$product->id}}" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        {!! $product->statusBadge !!}
                                                        {{ $product->user->name }}
                                                    </div>
                                                    <p>
                                                    {{ $product->description }}
                                                    </p>
                                                    {!! $product->editStatusButton !!}
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>

                                        @endforeach
                                @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


</div>
@endif





    
@endsection

@push('after-scripts')
    
<script>

const drag = (event) => {
  event.dataTransfer.setData("text/plain", event.target.id);
}

const allowDrop = (ev) => {
  ev.preventDefault();
  if (hasClass(ev.target,"dropzone")) {
    addClass(ev.target,"droppable");
  }
}

const clearDrop = (ev) => {
    removeClass(ev.target,"droppable");
}

const drop = (event) => {
  event.preventDefault();
  const data = event.dataTransfer.getData("text/plain");
  const element = document.querySelector(`#${data}`);
  try {
    // remove the spacer content from dropzone
    event.target.removeChild(event.target.firstChild);
    // add the draggable content
    event.target.appendChild(element);
    // remove the dropzone parent
    unwrap(event.target);
  } catch (error) {
    console.warn("can't move the item to the same place")
  }
  updateDropzones();
}

const updateDropzones = () => {
    /* after dropping, refresh the drop target areas
      so there is a dropzone after each item
      using jQuery here for simplicity */
    
    var dz = $('<div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>');
    
    // delete old dropzones
    $('.dropzone').remove();

    // insert new dropdzone after each item   
    dz.insertAfter('.card.draggable');
    
    // insert new dropzone in any empty swimlanes
    $(".items:not(:has(.card.draggable))").append(dz);
};

// helpers
function hasClass(target, className) {
    return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
}

function addClass(ele,cls) {
  if (!hasClass(ele,cls)) ele.className += " "+cls;
}

function removeClass(ele,cls) {
  if (hasClass(ele,cls)) {
    var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
    ele.className=ele.className.replace(reg,' ');
  }
}

function unwrap(node) {
    node.replaceWith(...node.childNodes);
}

$(document).ready(function(){

$('#start_date').datepicker({

format: '{{ config('app.date_format_js') }}' ,
uiLibrary: 'bootstrap4',
iconsLibrary: 'fortawesome',
maxDate: function () {
    return $('#end_date').val();
}
});
        
$('#end_date').datepicker({
format: '{{ config('app.date_format_js') }}',
uiLibrary: 'bootstrap4',
iconsLibrary: 'fortawesome',
minDate: function () {
    return $('#start_date').val();
} 
});
});


$(function () {

$('#resetButton').on('click', function() {

    //$(this).closest('form').get(0).reset();
    $('#product_name').val("");
    $('#status_id').prop('selectedIndex',-1);
    $('#start_date').val("");
    $('#end_date').val("");
    $('#department_id').prop('selectedIndex',0);
    $('#category_id').empty();
    $('#hierarchy_id').empty();
    $('#category_id').prop("disabled", true);
    $('#hierarchy_id').prop("disabled", true);

});


$('#department_id').on('change', function (e) {
    var department_id = e.target.value;
    category_id = false;
    categoryUpdate(department_id);
});
// Ajax Request for categories
function categoryUpdate(department_id) {
    $.get('{{ url('admin/auth/product/category') }}/' + department_id , function (data) {
        $('#category_id').empty();
        $('#category_id').append($("<option selected></option>").text('Seleccione una categoria'));
        $.each(data, function(key, value) {   
            $('#category_id')
                .append($("<option></option>")
                            .attr("value",key)
                            .text(value)); 
        });
        $('#category_id').prop("disabled", false);
        /*
        $.each(json, function(i, obj){
            $('#select').append($('<option>').text(obj.text).attr('value', obj.val));
        });

        $.each(data, function (index, categories) {
            $('#category').append($('<option>', {
                value: categories.id,
                text: categories.name
            }));
        });
        */

    });
}

$('#category_id').on('change', function (e) {
    var category_id = e.target.value;
    hierarchyUpdate(category_id);
});

// Ajax Request for hierarchies
function hierarchyUpdate(category_id) {
    $.get('{{ url('admin/auth/product/hierarchy') }}/' + category_id , function (data) {
        $('#hierarchy_id').empty();
        $('#hierarchy_id').append($("<option selected></option>").text('Seleccione una jerarquia'));
        $.each(data, function(key, value) {   
            $('#hierarchy_id')
                .append($("<option></option>")
                            .attr("value",key)
                            .text(value)); 
        });
        $('#hierarchy_id').prop("disabled", false);

    });
}



});


</script>
    
@endpush