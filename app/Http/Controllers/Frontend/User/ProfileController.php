<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Auth\UserRepository;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Http\Requests\Frontend\User\UpdateInfoRequest;
use App\Http\Requests\Frontend\User\UpdateBankAccountRequest;
use App\Models\Auth\File;

/**
 * Class ProfileController.
 */
class ProfileController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * ProfileController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UpdateProfileRequest $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function updateGeneral(UpdateInfoRequest $request)
    {
        $output = $this->userRepository->updateGeneral(
            $request->user()->id,
            $request->only('name','first_name', 'last_name', 'email','document'),
            $request->operations_document
        );

        // E-mail address was updated, user has to reconfirm
        if (is_array($output) && $output['email_changed']) {
            auth()->logout();

            return redirect()->route('frontend.auth.login')->withFlashInfo(__('strings.frontend.user.email_changed_notice'));
        }

        return redirect()->route('frontend.user.account')->withFlashSuccess(__('strings.frontend.user.profile_updated'));
    }

        /**
     * @param UpdateProfileRequest $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function updateProfile(UpdateProfileRequest $request)
    {
        $output = $this->userRepository->updateProfile(
            $request->user()->id,
            $request->only(
            'cedula',
            'ruc',
            'dv',
            'phone',
            'fax',
            'payment_condition_days',
            'payment_bonus_percentage',
            'person_type_id',
            'payment_type_id',
            'delivery_time_days'),
            false
        );

        return redirect()->route('frontend.user.account')->withFlashSuccess(__('strings.frontend.user.profile_updated'));
    }

     /**
     * @param UpdateBankAccountRequest $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function updateBankAccount(UpdateBankAccountRequest $request)
    {
        $output = $this->userRepository->updateBankAccount(
            $request->user()->id,
            $request->only(
            'bank_id',
            'account_number',
            'bank_account_type_id',
            'account_fullname',
            'noreturn_percentage'),
            false
        );

        return redirect()->route('frontend.user.account')->withFlashSuccess(__('strings.frontend.user.profile_updated'));
    }



     /**
     * @param String $uuid
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function download($uuid)
    {
        $document = File::where('uuid', $uuid)->firstOrFail();
        $pathToFile = storage_path('app/public/' . $document->url);
        $original_filename = $document->title;
        return response()->download($pathToFile,$original_filename);
    }



}