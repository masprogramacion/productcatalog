{{ html()->modelForm($product, 'PATCH', route('admin.auth.product.update.details', $product))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-details-product','novalidate'=>'novalidate'])->open() }}

<div class="col">

  <div class="col-sm-12">
  <hr class="mb-4">
  <h4 class="mb-3">@lang('labels.general.vendor_information')</h4>
  <div class="row">
        <div class="col-sm-6 mb-3">
            {{ html()->label(__('labels.products.form.label.vendor_account_number'))->for('vendor_account_number')}}
            {{ html()->text('vendor_account_number')
                                    ->class('form-control')
                                    ->attributes(['disabled'=>'disabled'])
                                    }}
            @if($product->vendor_account_number == null)                   
                <a href="#"><i class="fa fa-wrench faa-wrench animated"></i>&nbsp;Ud. debe llenar la cuenta del proveedor antes de poder aprobar o rechazar un producto </a>
            @endif
            <small class="text-muted">@lang('labels.products.form.helptext.vendor_account_number')</small>
            <div class="invalid-feedback">
            @lang('validation.required', ['attribute' => __('labels.products.form.label.vendor_account_number')])
            {{ $errors->first('vendor_account_number') }} <!--TODO add backend errors logic to show errors in each field-->
            </div>
        </div>
        <div class="col-sm-6 mb-3">
            <div class="custom-control custom-checkbox">
                              {{ html()->checkbox('ind_regular_vendor')
                              ->class('custom-control-input')
                              ->attributes(['name'=>'ind_regular_vendor','id'=>'ind_regular_vendor'])
                              }}
                              <label class="custom-control-label" label="label_ind_regular_vendor" for="ind_regular_vendor">@lang('labels.products.form.label.ind_regular_vendor')</label>
                              <small class="text-muted" id="text_muted_ind_regular_vendor"></small>
            </div>
        </div> 
  </div>
  <hr class="mb-4">
  <h4 class="mb-3">Ubicación del producto</h4>
  <div class="row">
        <div class="col-sm-6 mb-3">
            @if($product->store)

                  @foreach ($product->store as $singleStore)
                  <span class="badge badge-warning"> {{ $singleStore->name }}</span>
                  @endforeach
            @else
                  <h5 class="mb-3">No ha sido asignado</h4>
            @endif
        </div>
        <div class="col-sm-6 mb-3">

        </div> 
  </div>

  <hr class="mb-4">
  <h4 class="mb-3">Detalle del Material</h4>
  <div class="row">
    <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.material_type_id'))->class('form-control-label') }}
          {{ html()->select('material_type_id',$material_types)
                        ->class('custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->attributes(['disabled'=>'disabled']) }}
          <small class="text-muted">@lang('labels.products.form.helptext.material_type_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('material_type_id') }}
          </div>
    </div>
    <!--
    <div class="form-group col-sm-3">
    
          {{ html()->label(__('labels.products.form.label.material_group_id'))->class('form-control-label') }}
          {{ html()->select('material_group_id',$material_groups)
                        ->class('select2 custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.material_group_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('material_group_id') }}
          </div>
     
    </div>
    -->
    <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.purchase_tax_code_id'))->class('form-control-label') }}
          {{ html()->select('purchase_tax_code_id',$purchase_tax_codes)
                        ->class('custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->attributes(['disabled'=>'disabled']) }}
          <small class="text-muted">@lang('labels.products.form.helptext.purchase_tax_code_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('purchase_tax_code_id') }}
          </div>
    </div>
    <div class="form-group col-sm-3">
    
          {{ html()->label(__('labels.products.form.label.material_tax_type_id'))->class('form-control-label') }}
          {{ html()->select('material_tax_type_id',$material_tax_types)
                        ->class('select2 custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->attributes(['disabled'=>'disabled']) }}
          <small class="text-muted">@lang('labels.products.form.helptext.material_tax_type_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('material_tax_type_id') }}
          </div>

    </div>
  </div>

  <div class="row">
          <!--
          <div class="form-group col-sm-2">
          
            {{ html()->label(__('labels.products.form.label.sku'))->for('sku')}}
            {{ html()->text('sku')
                                    ->class('form-control')
                                    ->attribute('maxlength', 14)
                                    ->required()}}
            <small class="text-muted">@lang('labels.products.form.helptext.sku')</small>
            <div class="invalid-feedback">
            {{ $errors->first('sku') }}
            </div>
            
          </div>
          -->
        <div class="form-group col-sm-2">
          {{ html()->label(__('labels.products.form.label.price'))->for('price')}}
          {{ html()->input('number','price')->class('form-control')->attributes(['step'=>'.01'])->attributes(['disabled'=>'disabled']) }}
          <small class="text-muted"></small>
          <div class="invalid-feedback">
          {{ $errors->first('price') }}
          </div>
        </div>
        <div class="form-group col-sm-2">
          {{ html()->label(__('labels.products.form.label.suggested_price'))->for('suggested_price')}}
          {{ html()->text('suggested_price')
                                    ->class('form-control')
                                    ->attributes(['disabled'=>'disabled'])
                                    }}
          <small class="text-muted"></small>
          <div class="invalid-feedback">
          {{ $errors->first('suggested_price') }}
          </div>
        </div>

        <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.short_text_description'))->for('short_text_description')}}
          {{ html()->text('short_text_description')
                                    ->class('form-control')
                                    ->attribute('maxlength', 18)
                                    ->attributes(['disabled'=>'disabled'])}}
          <small class="text-muted">@lang('labels.products.form.helptext.short_text_description')</small>
          <div class="invalid-feedback">
          {{ $errors->first('short_text_description') }}
          </div>
        </div>

        <div class="form-group col-sm-3">

          {{ html()->label(__('labels.products.form.label.order_price_measurement_unit_id'))->class('form-control-label') }}
           {{ html()->select('order_price_measurement_unit_id',$packaging_measurement_units)
                                    ->class('select2 custom-select d-block w-100')
                                    ->attributes(['disabled'=>'disabled']) }}
          <small class="text-muted">@lang('labels.products.form.helptext.order_price_measurement_unit_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('order_price_measurement_unit_id') }}
          </div>

        </div>


  </div>

  <hr class="mb-4">
  <h4 class="mb-3">Clasificacion</h4>
  <div class="row">
    <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.purchase_organization_id'))->class('form-control-label') }}
          {{ html()->select('purchase_organization_id',$purchase_organizations)
                        ->class('custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->attributes(['disabled'=>'disabled']) }}
          <small class="text-muted">@lang('labels.products.form.helptext.purchase_organization_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('purchase_organization_id') }}
          </div>
    </div>
    <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.purchase_group_id'))->class('form-control-label') }}
          {{ html()->select('purchase_group_id',$purchase_groups)
                        ->class('custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->attributes(['disabled'=>'disabled']) }}
          <small class="text-muted">@lang('labels.products.form.helptext.purchase_group_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('purchase_group_id') }}
          </div>
    </div>

    <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.iva_code_id'))->class('form-control-label') }}
          {{ html()->select('iva_code_id',$iva_codes)
                        ->class('custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->attributes(['disabled'=>'disabled']) }}
          <small class="text-muted">@lang('labels.products.form.helptext.iva_code_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('iva_code_id') }}
          </div>
    </div>

  </div>

  <div class="row">

    <div class="form-group col-sm-4">
          {{ html()->label(__('labels.products.form.label.department_id'))->class('form-control-label') }}
          {{ html()->select('department_id',$departments)
                        ->placeholder('Seleccione un Departamento')
                        ->class('custom-select d-block w-100')
                        ->attributes(['disabled'=>'disabled']) }}
          <small class="text-muted">@lang('labels.products.form.helptext.department_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('department_id') }}
          </div>
    </div>
    <div class="form-group col-sm-4">
          {{ html()->label(__('labels.products.form.label.category_id'))->class('form-control-label') }}
          {{ html()->select('category_id',$categories)
            ->placeholder('Seleccione una Categoria')
                        ->class('custom-select d-block w-100')
                        ->attributes(['disabled'=>'disabled']) }}
          <small class="text-muted">@lang('labels.products.form.helptext.category_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('category_id') }}
          </div>
    </div>
    <div class="form-group col-sm-4">
          {{ html()->label(__('labels.products.form.label.hierarchy_id'))->class('form-control-label') }}
          {{ html()->select('hierarchy_id',$hierarchies)
            ->placeholder('Seleccione una Jerarquia')
            ->class('custom-select d-block w-100')
            ->attributes(['disabled'=>'disabled']) }}
          <small class="text-muted">@lang('labels.products.form.helptext.hierarchy_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('hierarchy_id') }}
          </div>
    </div>

  </div>

  <div class="row">

      <div class="form-group col-sm-4">
                      {{ html()->label(__('labels.products.form.label.available_deliverable_from'))->class('form-control-label')}}
                        {{ html()->input('text','available_deliverable_from')
                                        ->class('form-control')
                                        ->attributes(['placeholder'=>'Seleccione una fecha', 'disabled'=>'disabled'])
                                        ->value($product->available_deliverable_from) }}
                        <div class="invalid-feedback">
                        @lang('validation.required', ['attribute' => __('labels.products.form.label.available_deliverable_from')]).
                        </div>
      </div>
      <div class="form-group col-sm-4">
                      {{ html()->label(__('labels.products.form.label.available_deliverable_until'))->class('form-control-label')}}
                        {{ html()->input('text','available_deliverable_until')
                                        ->class('form-control')
                                        ->attributes(['placeholder'=>'Seleccione una fecha', 'disabled'=>'disabled'])
                                        ->value($product->available_deliverable_until) }}
                        <div class="invalid-feedback">
                        @lang('validation.required', ['attribute' => __('labels.products.form.label.available_deliverable_until')]).
                        </div>
        </div>


  </div>
  




</div>

    <div class="card-footer clearfix">
                <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.product.index'), __('buttons.general.goback')) }}
                </div><!--col-->

                <div class="col text-right">
                @if($show_edit_button)
                    <a class="btn btn-success btn-sm pull-right" href="{{ route('admin.auth.product.edit', $product->id) }}" role="button">@lang('buttons.general.crud.edit')</a>
                @endif
                </div><!--row-->
            </div><!--row-->
    </div><!--card-footer-->
</div><!--table-responsive-->
{{ html()->form()->close() }}