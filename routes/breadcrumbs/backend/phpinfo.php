<?php

Breadcrumbs::for('laravelPhpInfo::phpinfo', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('menus.backend.phpinfo.main'), url('admin/phpinfo'));
});
