<?php

Breadcrumbs::for('admin.auth.vendor.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('labels.backend.access.vendors.management'), route('admin.auth.vendor.index'));
});

Breadcrumbs::for('admin.auth.vendor.create', function ($trail) {
    $trail->parent('admin.auth.vendor.index');
    $trail->push(__('labels.backend.access.vendors.create'), route('admin.auth.vendor.create'));
});

Breadcrumbs::for('admin.auth.vendor.deactivated', function ($trail) {
    $trail->parent('admin.auth.vendor.index');
    $trail->push(__('labels.backend.access.vendors.deactivated'), route('admin.auth.vendor.deactivated'));
});

Breadcrumbs::for('admin.auth.vendor.show', function ($trail, $id) {
    $trail->parent('admin.auth.vendor.index');
    $trail->push(__('menus.backend.access.vendors.view'), route('admin.auth.vendor.show', $id));
});

Breadcrumbs::for('admin.auth.vendor.deleted', function ($trail) {
    $trail->parent('admin.auth.vendor.index');
    $trail->push(__('menus.backend.access.vendors.deleted'), route('admin.auth.vendor.deleted'));
});


Breadcrumbs::for('admin.auth.vendor.edit', function ($trail, $id) {
    $trail->parent('admin.auth.vendor.index');
    $trail->push(__('menus.backend.access.vendors.edit'), route('admin.auth.vendor.edit', $id));
});

Breadcrumbs::for('admin.auth.vendor.change-password', function ($trail, $id) {
    $trail->parent('admin.auth.vendor.index');
    $trail->push(__('menus.backend.access.vendors.change-password'), route('admin.auth.vendor.change-password', $id));
});

