<div class="container mt-5 mb-5">
	<div class="row">
		<div class="col-md-10 offset-md-1">
			<h4>{{ __('labels.frontend.access.products.tabs.content.status.history_transitions') }}</h4>

      <ul class="timeline">
      @foreach($workflow_statuses as $workflow_status)

        @if($workflow_status->status_id==2 || $workflow_status->status_id==3  || $workflow_status->status_id==4   || $workflow_status->status_id==5  )
        <li class="timeline-inverted">
          <div class="timeline-badge {{$workflow_status->color}}"><i class="fa {{$workflow_status->icon}}"></i></div>
        @elseif($workflow_status->status_id==7 || $workflow_status->status_id==8  || $workflow_status->status_id==9   || $workflow_status->status_id==10  )
        <li class="timeline">
          <div class="timeline-badge {{$workflow_status->color}}"><i class="fa {{$workflow_status->icon}}"></i></div>
        @elseif($workflow_status->status_id==11 )
        <li class="timeline-inverted">
          <div class="timeline-badge {{$workflow_status->color}}"><i class="fa {{$workflow_status->icon}}"></i></div>
        @else
        <li>
          <div class="timeline-badge"><i class="fa {{$workflow_status->icon}}"></i></div>
        @endif

          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">{{$workflow_status->current_status_description}}</h4>
              <p><small class="text-muted"><i class="fa {{$workflow_status->icon}}"></i> {{$workflow_status->updated_at}}</small></p>
            </div>
            <div class="timeline-body">
              <p>Usuario: {{$workflow_status->user->full_name}} <br/>Comentarios: {{$workflow_status->comments}}</p>
            </div>
          </div>
        </li>

      @endforeach
      </ul>
		</div>
	</div>
  <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('frontend.auth.product.index'), __('buttons.general.goback')) }}
                    </div><!--col-->
                </div><!--row-->
    </div><!--card-footer-->
</div>

