<?php

namespace App\Http\Requests\Backend\Auth\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\Auth\Product;

/**
 * Class StoreStep3ProductRequest.
 */
class StoreStep3ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //dd('here1');
        return true;
         //TODO this can be used by a regular user as well
        //return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [

            'image_front_1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_back_1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
}
