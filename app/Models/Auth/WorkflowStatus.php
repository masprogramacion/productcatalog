<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\Traits\Relationship\WorkflowStatusRelationship;

/**
 * Class WorkflowStatus.
 */
class WorkflowStatus extends Model
{
    use WorkflowStatusRelationship;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'workflow_statuses';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['comments', 'product_id', 'user_id', 'status_id', 'current_status_description'];

    protected $appends = ['color'. 'icon'];
}
