@extends('frontend.layouts.app2')

@section('content')


<div class="container-fluid">
  <ul class="list-unstyled multi-steps">
    <li>PASO 1</li>
    <li class="is-active">PASO 2</li>
    <li class="is-active">PASO 3</li>

  </ul>
</div>

<!--
<ul class="progress-indicator">
            <li class="completed">
                <span class="bubble"></span>
                <i class="fa fa-check-circle"></i>
                PASO 1
            </li>
            <li>
                <span class="bubble"></span>
                PASO 2
            </li>
            <li>
                <span class="bubble"></span>
                PASO 3
            </li>
            <li>
                <span class="bubble"></span>
                PASO 4
            </li>
</ul>
-->

<div class="container">
    <div class="py-1 text-center">
    <h2>PASO 2 DE 3</h2>
    <!--<p class="lead">Agregar la(s) Presentacion(es) y Fotos del Producto.</p>-->
    </div>
<!--
  <div class="row">
              <div class="col-sm-8">

              </div>
              <div class="col-sm-2">
                  Presione + para anadir otro campo, o - para eliminarlo
              </div>
              <div class="col-sm-2">
                  <div class="form-group">
                      <button class="btn btn-brand btn-success" type="button" style="margin-bottom: 4px" onclick="add_fields();"><i class="fa fa-plus"></i></button>
                  </div>    
              </div>
                  <div class="col-sm-1">
          <button class="btn btn-brand btn-success" type="button" style="margin-bottom: 4px" data-toggle="tooltip" data-placement="top" title="Presione + para anadir otra presentacion" onclick="add_fields();"><i class="fa fa-plus"></i></button>
    </div>
  </div>
  -->

  @include('includes.products.packaging')
  

</div><!--container-->



@endsection
@push('after-scripts')
<script> 
  //define counter
  var sectionsCount = {{ $sections_count }};
  $('[data-toggle="tooltip"]').tooltip();
</script>  
<script type="text/javascript" src="{{ URL::asset ('js/custom.js') }}"></script>
@endpush
