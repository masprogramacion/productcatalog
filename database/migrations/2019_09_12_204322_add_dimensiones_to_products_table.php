<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDimensionesToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->decimal('weight',  10, 2)->after('uncatalogued')->nullable();
            $table->integer('weight_mu_id')->after('weight')->nullable();
            $table->decimal('width',  10, 2)->after('weight_mu_id')->nullable();
            $table->decimal('height',  10, 2)->after('width')->nullable();
            $table->decimal('length',  10, 2)->after('height')->nullable();
            $table->integer('dimensions_mu_id')->after('length')->nullable();
            $table->decimal('volume',  10, 2)->after('dimensions_mu_id')->nullable();
            $table->integer('volume_mu_id')->after('volume')->nullable();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('weight');
            $table->dropColumn('weight_mu_id');
            $table->dropColumn('width');
            $table->dropColumn('height');
            $table->dropColumn('length');
            $table->dropColumn('dimensions_mu_id');
            $table->dropColumn('volume');
            $table->dropColumn('volume_mu_id');
        });
    }
}
