<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\User; 
use App\Models\Auth\Product; 
use App\Models\Auth\Traits\Attribute\MessageAttribute;

class Message extends Model
{

   
    use MessageAttribute;

    protected $fillable = ['body', 'subject', 'sent_to_id', 'sender_id','product_id'];

    // A message belongs to a sender
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    // A message also belongs to a receiver    
    public function receiver()
    {
        return $this->belongsTo(User::class, 'sent_to_id');
    }

    // A message also belongs to a product   
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }



}
