<?php

namespace App\Http\Requests\Backend\Auth\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Models\Auth\Product;
use Illuminate\Http\Request;

/**
 * Class StoreUncataloguedProductRequest.
 */
class StoreUncataloguedProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //dd('here1');
        return true;
         //TODO this can be used by a regular user as well
        //return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        Validator::extend('custom_gtin_conflict', function($attribute, $value, $parameters) 
        {
            $gtin_exists = 0;

            if (Product::where('gtin_1', '=', $value)->exists()) {
                $gtin_exists++;
            }

            return ($gtin_exists==0);

        }); 


        return [
            'product_name' => 'required|max:100',
            'description' => 'required|max:40',
            //'manufacturer_id' => 'required',
            'brand_id' => 'sometimes',
            'store_id' => 'required',
            'department_id'  => 'required',
            'gtin_1'  => 'required|max:13|custom_gtin_conflict',
            'image_front_1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_back_1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'custom_gtin_conflict' => 'Codigo de Barra GTIN (EAN/UPC) ya existe en el sistema',
        ];
    }
}
