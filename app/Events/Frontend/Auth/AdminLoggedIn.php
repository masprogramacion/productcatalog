<?php

namespace App\Events\Frontend\Auth;

use App\Models\Auth\Admin;
use Illuminate\Queue\SerializesModels;

/**
 * Class AdminLoggedIn.
 */
class AdminLoggedIn
{
    use SerializesModels;

    /**
     * @var
     */
    public $admin;

    /**
     * @param $admin
     */
    public function __construct(Admin $admin)
    {
        $this->admin = $admin;
    }
}
