<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\Relationship\ProductRelationship;
use App\Models\Auth\Traits\Method\ProductMethod;
use App\Models\Auth\Traits\Attribute\ProductAttribute;
use App\Models\Auth\Traits\Scope\ProductScope;
use Kyslik\ColumnSortable\Sortable;


class Product extends Model
{
    use SoftDeletes, 
    ProductRelationship,
    ProductMethod,
    ProductAttribute,
    ProductScope,
    Sortable;
    

    protected $primaryKey = 'id';
    
    protected $guarded = [];

    public $timestamps = true;
    
    protected $appends = ['product_name'];
    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    public $sortable = ['id', 'name',  'created_at', 'updated_at'];

}
