<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Models\Auth\Product;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = auth()->user();
        //$profile = Product::find($user->id);
        $products = Product::whereUserId($user->id)->get();

        $products_total =  $products->count();

        $products_approved = Product::whereUserId($user->id)->whereIn('status_id',array(11))->count();
        $products_pending = Product::whereUserId($user->id)->whereIn('status_id',array(2, 3, 4, 5, 8, 9, 10))->count();
        $products_rejected = Product::whereUserId($user->id)->where('status_id',7)->count();

        //$products_pending_analista_compras = Product::whereIn('status_id',array(2, 8, 9, 10))->orderBy('created_at', 'desc')->get();
        //$products_gerente_categoria = Product::where('status_id',3)->orderBy('created_at', 'desc')->get();
        //$products_planimetria = Product::where('status_id',4)->orderBy('created_at', 'desc')->get();
        //$products_finanzas = Product::where('status_id',5)->orderBy('created_at', 'desc')->get();
        //$products_approved = Product::where('status_id',6)->orderBy('created_at', 'desc')->get();

        //GET PRODUCTS WITH THE STATUS(ES) ASIGNED TO THE ROLE
        
        $profile_exists = auth()->user()->profile();
        $bankAccount_exists = auth()->user()->bankAccount();

        //dd($profile_exists);
        $general_information_complete = false;
         
        if (strlen($user->name)> 0 && $user->operationsDocument){
            $general_information_complete = true;
        }


        return view('frontend.user.dashboard')
        ->with('general_information_complete', $general_information_complete)
        ->with('products_total', $products_total)
        ->with('products_approved', $products_approved)
        ->with('products_pending', $products_pending)
        ->with('products_rejected', $products_rejected);
    }
}
