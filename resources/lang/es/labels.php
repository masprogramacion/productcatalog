<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
//á, é, í, ó, ú, ü, ñ, ¿, ¡
    'general' => [
        'all'       => 'Todos',
        'yes'       => 'Sí',
        'no'        => 'No',
        'copyright' => 'Copyright',
        'custom'    => 'Personalizado',
        'actions'   => 'Acciones',
        'active'    => 'Activo',
        'buttons'   => [
            'save'   => 'Guardar',
            'update' => 'Actualizar',
        ],
        'hide'              => 'Ocultar',
        'inactive'          => 'Inactivo',
        'none'              => 'Ninguno',
        'show'              => 'Mostrar',
        'toggle_navigation' => 'Abrir/Cerrar menú de navegación',
        'create_new'        => 'Crear Nuevo',
        'create_new_product'        => 'Crear Nuevo Producto',
        'create_new_vendor'        => 'Crear Nuevo Proveedor',
        'create_new_catalog'        => 'Crear Nuevo Catálogo',
        'toolbar_btn_groups' => 'Barra de menú con botones',
        'more'              => 'Más',
        'none'              => 'Ninguno',
        'select_option'     => 'Seleccione un valor',
        'description'     => 'Descripción',
        'status'     => 'Estatus',
        'measurement_unit'     => 'Unidad de Medida',
        'product_description'     => 'Producto - Descripción',
        'product_name'     => 'Nombre del Producto',
        'creation_date'     => 'Fecha de Creación',
        'downloaded'     => 'Descargado',
        'verified_products'     => 'Productos Verificados',
        'vendor_information'     => 'Información del Proveedor',
        'general_information'     => 'Información del Proveedor',
    ],


    /*TODO  MOVER ESTA SECCION DENTRO DE BACKEND MAS ABAJO DE ESTE FILE*/
    'products' => [

        'create' => [
            'step1'   => 'PASO 1',
            'step2'   => 'PASO 2',
            'step3'   => 'PASO 3',
            'step1of3'   => 'PASO 1 DE 3',
            'step2of3'   => 'PASO 2 DE 3',
            'step3of3'   => 'PASO 3 DE 3',
            'instructions_max_number_product_variations' => 'Instrucciones: Ud. puede añadir máximo 3 presentaciones de un mismo producto.',
            'alert_warning_must_complete_steps'   => 'USTED DEBE COMPLETAR LOS 3 PASOS PARA QUE EL PRODUCTO SEA ENVIADO PARA SU REVISIÓN.',
            'alert_success_product_submitted'   => 'LA INFORMACIÓN DEL PRODUCTO HA SIDO ENVIADA EXITOSAMENTE. USTED RECIBIRÁ NOTIFICACIONES SOBRE EL PROCESO DE REVISIÓN Y APROBACIÓN DE SU PRODUCTO VIA EMAIL.',
        ],

        'title' => [
            'measurement_unit'   => 'Unidad de Medida',
        ],
        'form'   => [
            'search' => [
                'start_date' => 'Fecha de Creación - Desde',
                'end_date' => 'Fecha de Creación - Hasta',
                'status' => 'Estatus',
                'product_name' => 'Nombre del producto',
            ],

            'label' => [
                'product_name'   => 'Nombre(*)',
                'description' => 'Descripción(*)',
                'gtin_1' => 'GTIN (EAN/UPC)(*)',
                'gtin_2' => 'GTIN (EAN/UPC)',
                'gtin_3' => 'GTIN (EAN/UPC)',
                'lower_level_measurement_unit_qty' => 'Cantidad(*)',
                'lower_level_pack_hierarchy_measurement_unit_id' => 'Unid. de Embalaje anterior(*)',
                'ind_measurement_unit_is_order_unit' => 'Es Unid.Medida para hacer pedido?',
                'base_measurement_unit_id' => 'Unid.Medida Mínima o Básica(*)',
                'alt_measure_unit_stockkeep_unit_id' => 'Unid.Medida de Almacenaje(*)',
                'ind_measurement_unit_is_delivery_or_issue_unit' => '¿Es Unid.Medida para hacer entrega?',
                'min_remain_shelf_life' => 'Caducidad(en días)(*)',
                'old_material_number' => 'Número de material antiguo',
                'vendor_material_number' => 'Número de material del proveedor',
                'price' => 'Precio(*)',
                'order_price_measurement_unit_id' => 'Unid.Medida del Pedido(*)',
                'vendor_account_number'=> 'Número de Cuenta del Proveedor',
                'vendor_account_number_id'=> 'Número de Cuenta del Proveedor',
                'brand_id' => 'Marca(*)',
                'material_tax_type_id' => 'Clasificación Fiscal Material(*)',
                'available_deliverable_from' => 'Suministrable desde(*)',
                'available_deliverable_until' => 'Suministrable hasta(*)',
                'ind_regular_vendor' => '¿Es Proveedor Regular?',
                'iva_code_id' => 'Indicador IVA(*)',
                'purchase_tax_code_id' => 'Identificador Impuesto(*)', 
                'material_type_id' => 'Tipo de Material(*)',
                'material_group_id' => 'Grupo de Material(*)',
                'purchase_organization_id' => 'Organización de Compras(*)',
                'purchase_group_id' => 'Grupo de Compras(*)',
                'provider_account_id' => 'Cuenta del Proveedor',
                'manufacturer_id' => 'Fabricante(*)',
                'sku' => 'SKU(*)',
                'gtin_type_id' => 'Tipo de GTIN (EAN/UPC): ZV/UC/HE',
                'store_id' => 'Sucursal(*)',
                'department_id' => 'Departamento(*)',
                'category_id' => 'Categoría(*)',
                'hierarchy_id' => 'Jerarquía(*)',
                'group_id' => 'Grupo',
                'product_status_id' => 'Estatus',
                'ind_include_discount'=>'¿Desea incluir descuento?',
                'ind_limited_time_discount'=>'¿Es por tiempo limitado?',
                'discount_start_date' => 'Fecha Inicio',
                'discount_end_date' => 'Fecha Fin',
                'discount_percentage' => 'Porcentaje de descuento',
                'suggested_price' => 'Precio sugerido(*)',
                'short_text_description' => 'Descripción corta(*)',
                'quantity'   => '#',
                'image_front_1'   => 'Imagen Frontal',
                'image_back_1'   => 'Imagen Posterior',
                'width'   => 'Ancho',
                'height'   => 'Alto',
                'length'   => 'Largo',
                'weight'   => 'Peso',
                'volume'   => 'Volumen',
                'dimensions'   => 'Dimensiones',
                'select_all'   => 'Seleccionar todo'
                
            ],

            'helptext' => [
                'product_name'   => 'Ej: Aceite Vegetal',
                'description' => 'Ej: Mezcla de Aceite Puro de Canola + Oliva',
                'gtin_1' => 'Debe incluir el dígito verificador',
                'gtin_2' => 'Debe incluir el dígito verificador',
                'gtin_3' => 'Debe incluir el dígito verificador',
                'lower_level_measurement_unit_qty' => '',
                'lower_level_pack_hierarchy_measurement_unit_id' => 'Ej. Para una caja (BOX) su unidad anterior seria botella (BOTTLE)',
                'ind_measurement_unit_is_order_unit' => 'se puede hacer un pedido usando:%UOM%?',
                'base_measurement_unit_id' => 'Ej. Unidad, Botella,Lata',
                'alt_measure_unit_stockkeep_unit_id' => 'Ej: Caja,Bolsa',
                'ind_measurement_unit_is_delivery_or_issue_unit' => 'se puede hacer una entrega usando:%UOM%?',
                'min_remain_shelf_life' => 'Ej: 60',
                'old_material_number' => 'Ej: 2001012333',
                'vendor_material_number' => 'Ej: IT12000012222333',
                'price' => 'Ej: 12.99',
                'order_price_measurement_unit_id' => 'Ej: $12.99 por <b>Kg.</b>',
                'vendor_account_number'=> '',
                'vendor_account_number_id'=> '',
                'brand_id' => '',
                'material_tax_type_id' => '',
                'available_deliverable_from' => 'Ej: 2019-02-15',
                'available_deliverable_until' => 'Ej: 2019-03-30',
                'ind_regular_vendor' => 'Ej: Ya existe en el sistema',
                'iva_code_id' => '',
                'purchase_tax_code_id' => '',
                'material_type_id' => '',
                'material_group_id' => '',
                'purchase_organization_id' => '',
                'purchase_group_id' => '',
                'provider_account_id' => '',
                'manufacturer_id' => '',
                'sku' => 'Ej: 1212323232222',
                'gtin_type_id' => 'Tipo:%GTIN_TYPE%',
                'store_id' => '',
                'department_id' => '',
                'category_id' => '',
                'hierarchy_id' => '',
                'group_id' => '',
                'product_status_id' => '',
                'ind_include_discount'=>'',
                'ind_limited_time_discount'=>'',
                'discount_start_date' => 'Ej: 2018-01-15',
                'discount_end_date' => 'Ej: 2019-07-31',
                'discount_percentage' => 'Entre 1% y 100%',
                'suggested_price' => 'Ej: 15.50',
                'short_text_description' => 'Descripción que aparecera a la cajera',
                'image_front_1'   => '',
                'image_back_1'   => 'IMPORTANTE:debe mostrarse el código de barra en la foto',
                'width'   => 'Ancho',
                'height'   => 'Alto',
                'length'   => 'Largo',
                'weight'   => 'kilogramos',
                'volume'   => 'litros'
            ],

            'alt' => [
                'select_front_image'   => 'Seleccione Imagen Frontal',
                'select_back_image'   => 'Seleccione Imagen Posterior',
            ],
        ],

    ],


    'backend' => [
        'access' => [

            'departments' => [
                'create'     => 'Crear Departamento',
                'edit'       => 'Modificar Departamento',
                'management' => 'Administración de Gerentes de Categorías',
                'management_category' => 'Administración de Categorías',
                'management_assistant' => 'Administración de Asistente de Compras',
                'assigned' => 'Asignación de Categorías',

                'table' => [
                    'assigned' => 'Asignados Actualmente',
                    'categories' => 'Categorías',
                    'catalog' => 'Catálogo',
                    'name' => 'Nombre',
                    'created_at'     => 'Fecha de creación',
                    'total'           => 'Todos los Departamentos',
                    'emails'           => 'Emails',
                ],
            ],

            'catalogs' => [
                'active'     => 'Listado de Catálogos',
                'create'     => 'Crear Catálogo',
                'edit'       => 'Modificar Catálogo',
                'management' => 'Administración de Catálogos',

                'table' => [
                    'name' => 'Nombre',
                    'created_at'     => 'Fecha de creación',
                    'total'           => 'Todos los Catálogos',
                ],

                'banks' => [
                    'management' => 'Administración de Bancos',

                    'table' => [
                        'name' => 'Nombre',
                        'created_at'     => 'Fecha de creación',
                        'total'           => 'Todos los Bancos',
                    ],


                ],


            ],

            'roles' => [
                'create'     => 'Crear Rol',
                'edit'       => 'Modificar Rol',
                'management' => 'Administración de Roles',

                'table' => [
                    'number_of_users' => 'Número de Usuarios',
                    'permissions'     => 'Permisos',
                    'role'            => 'Rol',
                    'sort'            => 'Orden',
                    'total'           => 'Todos los Roles',
                ],
            ],

            'users' => [
                'active'              => 'Usuarios activos',
                'all_permissions'     => 'Todos los Permisos',
                'change_password'     => 'Cambiar la contraseña',
                'change_password_for' => 'Cambiar la contraseña para :user',
                'create'              => 'Crear Usuario',
                'deactivated'         => 'Usuarios desactivados',
                'deleted'             => 'Usuarios eliminados',
                'edit'                => 'Modificar Usuario',
                'management'          => 'Administración de Usuarios',
                'no_permissions'      => 'Sin Permisos',
                'no_roles'            => 'No hay Roles disponibles.',
                'permissions'         => 'Permisos',

                'table' => [
                    'confirmed'         => 'Confirmado',
                    'created'           => 'Creado',
                    'email'             => 'Correo',
                    'id'                => 'ID',
                    'last_updated'      => 'Última modificación',
                    'name'              => 'Nombre',
                    'first_name'        => 'Nombre',
                    'last_name'         => 'Apellidos',
                    'no_deactivated'    => 'Ningún Usuario desactivado disponible',
                    'no_deleted'        => 'Ningún Usuario eliminado disponible',
                    'other_permissions' => 'Otros Permisos',
                    'permissions'       => 'Permisos',
                    'roles'             => 'Roles',
                    'social'            => 'Cuenta Social',
                    'total'             => 'Todos los Usuarios',
                    'abilities'         => 'Habilidades',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Resumen',
                        'history'  => 'Historia',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar'       => 'Avatar',
                            'confirmed'    => 'Confirmado',
                            'created_at'   => 'Creación',
                            'deleted_at'   => 'Eliminación',
                            'email'        => 'E-mail',
                            'last_login_at' => 'Último Login En',
                            'last_login_ip' => 'Último Login IP',
                            'last_updated' => 'Última Actualización',
                            'name'         => 'Nombre',
                            'first_name'   => 'Nombre',
                            'last_name'    => 'Apellidos',
                            'status'       => 'Estatus',
                            'timezone'     => 'Zona horaria',
                        ],
                    ],
                ],

                'view' => 'Ver Usuario',
            ],

            'vendors' => [
                'active'     => 'Proveedores activos',
                'change_password'     => 'Cambiar la contraseña',
                'change_password_for' => 'Cambiar la contraseña para :user',
                'deactivated'     => 'Proveedores desactivados',
                'deleted'     => 'Proveedores eliminados',
                'create'     => 'Crear Proveedor',
                'edit'       => 'Modificar Proveedor',
                'management' => 'Administración de Proveedores',
                'all_permissions' => 'Todos los Permisos',
                'view' => 'Ver Proveedor',
                'vendor_id' => 'Proveedor',
                'helptext_vendor_id' => 'Proveedor',
                

                'table' => [
                    'confirmed'         => 'Confirmado',
                    'created'           => 'Creado',
                    'email'             => 'Correo',
                    'id'                => 'ID',
                    'last_updated'      => 'Última modificación',
                    'name'              => 'Nombre',
                    'first_name'        => 'Nombre',
                    'last_name'         => 'Apellidos',
                    'no_deactivated'    => 'Ningún Proveedor desactivado disponible',
                    'no_deleted'        => 'Ningún Proveedor eliminado disponible',
                    'other_permissions' => 'Otros Permisos',
                    'permissions'       => 'Permisos',
                    'roles'             => 'Roles',
                    'social'            => 'Cuenta Social',
                    'total'             => 'Todos los Proveedores',
                    'abilities'         => 'Habilidades',
                ],
            ],

            'products' => [
                'active'              => 'Productos activos',
                'all_permissions'     => 'Todos los Permisos',
                'create'              => 'Crear Producto',
                'deactivated'         => 'Productos desactivados',
                'deleted'             => 'Productos eliminados',
                'edit'                => 'Modificar Producto',
                'management'          => 'Administración de Productos',
                'department_assignment'          => 'Asignación de Categoría',
                'received'          => 'Productos Recibidos',
                'search'              => 'Búsqueda de Productos',
                'no_permissions'      => 'Sin Permisos',
                'no_roles'            => 'No hay Roles disponibles.',
                'permissions'         => 'Permisos',
                'show'                => 'Ver Producto',
                'assign_product_division'    => 'Asignar Categoría al Producto',

                'messages'   => [
                    'title'=> 'Administración de Mensajes',
                    'messages_received'   => 'Mensajes recibidos',
                    'status' => 'Estatus',
                    'sender' => 'Enviado Por',
                    'product_name' => 'Nombre del Producto',
                    'subject' => 'Asunto',
                    'received_at' => 'Fecha Recibido',
                    'all_messages'        => 'Todos los Mensajes',

                ],  


                'form'   => [
                    'search' => [
                        'title'=> 'Buscar Productos',
                        'status' => 'Estatus en el flujo de aprobación interno',
                    ],
                ],


                'table' => [
                    'confirmed'         => 'Confirmado',
                    'created'           => 'Creado',
                    'email'             => 'Correo',
                    'id'                => 'ID',
                    'last_updated'      => 'Última modificación',
                    'name'              => 'Nombre',
                    'first_name'        => 'Nombre',
                    'last_name'         => 'Apellidos',
                    'no_deactivated'    => 'Ningún Producto desactivado disponible',
                    'no_deleted'        => 'Ningún Producto eliminado disponible',
                    'other_permissions' => 'Otros Permisos',
                    'permissions'       => 'Permisos',
                    'roles'             => 'Roles',
                    'social'            => 'Cuenta Social',
                    'total'             => 'Todos los Productos',
                    'total_messages'    => 'Todos los Mensajes',
                    'price'             => 'Precio',
                    'description'       => 'Descripción',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Resumen',
                        'general' => 'Generales',
                        'packaging'  => 'Embalaje',
                        'images' => 'Imagenes',
                        'details' => 'Detalle',
                        'status' => 'Estatus',
                    ],

                    'content' => [
                        'details' => [
                            'material_detail'    => 'Detalle del Material',
                        ],
                        'status' => [
                            'payment_information'    => 'Información de Pago',
                            'payment_verified_at'    => 'Pago recibido en',
                            'sap_document'    => 'Documento en SAP',
                            'comments'    => 'Comentarios',
                            'history_transitions'    => 'Histórico del flujo interno de aprobación',
                        ],
                        'overview' => [
                            'name'       => 'Nombre',
                            'description'    => 'Descripción',
                            'price'   => 'Precio',
                            'suggested_price'   => 'Precio Sugerido',
                            'brand_id'   => 'Marca',
                            'manufacturer_id'        => 'Fabricante',
                            'min_remain_shelf_life' => 'Caducidad (en días)',
                            'include_discount' => '¿Descuento?',
                            'ind_include_discount' => '¿Incluye Descuento?',
                            'ind_include_discount_false' => 'No Incluye Descuento',
                            'ind_limited_time_discount' => '¿Descuento tiene Fecha Limitada?',
                            'ind_limited_time_discount_false' => 'No tiene fecha límite',
                            'discount_start_date'=> 'Fecha Inicio del Descuento',
                            'discount_end_date'=> 'Fecha Final del Descuento',
                            'discount_percentage'=> 'Porcentaje de Descuento',
                            'old_material_number'=> 'Número de Material antiguo',
                            'vendor_material_number' => 'Número Material (en el sistema del proveedor)',
                            'created_at' => 'Fecha de Creación',
                            'updated_at' => 'Última Actualización',
                            'status'       => 'Estatus',
                            'ind_regular_vendor'=> '¿Es proveedor regular?',
                            'old_material_number' => 'Número de material antiguo',
                            'vendor_material_number' => 'Número de material del proveedor',
                            'vendor_account_number'=> 'Número de Cuenta del Proveedor',
                            'vendor_account_number_id'=> 'Número de Cuenta del Proveedor',
                            'material_tax_type_id' => 'Clasificación Fiscal Material',
                            'purchase_tax_code_id' => 'Identificador Impuesto', 
                            'iva_code_id' => 'Indicador IVA',

                        ],
                    ],
                ],
//á, é, í, ó, ú, ü, ñ, ¿, ¡

                'view' => 'Ver Producto',
            ],

        ],
    ],

    'frontend' => [

        'auth' => [
            'login_box_title'    => 'Iniciar Sesión',
            'admin-login_box_title'    => 'Admin - Iniciar Sesión',
            'login_button'       => 'Iniciar Sesión',
            'login_with'         => 'Iniciar Sesión mediante :social_media',
            'register_box_title' => 'Registrarse',
            'register_button'    => 'Registrarse',
            'remember_me'        => 'Recordarme',
        ],

        'contact' => [
            'box_title' => 'Contáctenos',
            'button' => 'Enviar información',
        ],

        'passwords' => [
            'expired_password_box_title'      => 'Tu contraseña a expirado.',
            'forgot_password'                 => 'Has olvidado la contraseña?',
            'reset_password_box_title'        => 'Reiniciar contraseña',
            'reset_password_button'           => 'Reiniciar contraseña',
            'update_password_button'          => 'Actualizar contraseña',
            'send_password_reset_link_button' => 'Enviar el correo de verificación',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Cambiar la contraseña',
            ],

            'info' => [
                'created_at'         => 'Creado el',
                'edit_information'   => 'Modificar la información',
                'last_updated'       => 'Última modificación',
                

                'tabs' => [

                    'content' => [
                        'general' => [
                            'name'    => 'Razon Social(*)',
                            'first_name'    => 'Nombre(*)',
                            'last_name'    => 'Apellido(*)',
                            'email'    => 'Email(*)',
                            'operations_document'    => 'Aviso de Operaciones(*)',
                            'current_document' => 'Documento Existente:',
                            'download' => 'Descargar',
                            'file_name' => 'Nombre del archivo:',
                        ],
                        'profile' => [
                            'cedula'    => 'Cédula(*)',
                            'ruc'    => 'RUC(*)',
                            'dv'    => 'DV(*)',
                            'phone'    => 'Teléfono(*)',
                            'fax'    => 'Fax',
                            'payment_condition_days' => 'Condición de Pago(*)',
                            'payment_bonus_percentage' => 'Porcentaje de bonificación por pronto Pago  (%)(*)',
                            'person_type_id' => 'Tipo de Personería(*)',
                            'payment_type_id' => 'Tipo de Pago(*)',
                            'delivery_time_days' => 'Tiempo de entrega(*)',
                        ],
                        'bank_account' => [
                            'bank_id'    => 'Banco(*)',
                            'account_number'    => 'Número de Cuenta(*)',
                            'bank_account_type_id'    => 'Tipo de Cuenta(*)',
                            'account_fullname'    => 'Nombre de la Cuenta(*)',
                            'noreturn_percentage'    => 'Porcentaje de no retorno (no return)(*)',

                            
                        ],
                        'password' => [
                            'comments'    => 'Comentarios',
                            'history_transitions'    => 'Histórico del flujo interno de aprobacion',
                        ],
                    ],

                ],
            ],
        ],

        'access' => [

            'products' => [
                
                'active'              => 'Productos activos',
                'all_permissions'     => 'Todos los Permisos',
                'create'              => 'Crear Producto',
                'deactivated'         => 'Productos desactivados',
                'deleted'             => 'Productos eliminados',
                'edit'                => 'Modificar Producto',
                'management'          => 'Administración de Productos',
                'search'              => 'Búsqueda de Productos',
                'no_permissions'      => 'Sin Permisos',
                'no_roles'            => 'No hay Roles disponibles.',
                'permissions'         => 'Permisos',
                'show'                => 'Ver Producto',
                'view' => 'Ver Producto',

                'messages'   => [
                    'title'=> 'Administración de Mensajes',
                    'messages_received'   => 'Mensajes recibidos',
                    'message_show'       => 'Mostrar Mensaje',
                    'status' => 'Estatus',
                    'sender' => 'Enviado Por',
                    'product_name' => 'Nombre del Producto',
                    'subject' => 'Asunto',
                    'received_at' => 'Fecha Recibido',
                    'all_messages'        => 'Todos los Mensajes',

                ], 

                'form'   => [
                    'search' => [
                        'title'=> 'Buscar Productos',
                        'status' => 'Estatus',
                    ],
                ],


                'table' => [
                    'confirmed'         => 'Confirmado',
                    'created'           => 'Creado',
                    'id'                => 'ID',
                    'last_updated'      => 'Última modificación',
                    'name'              => 'Nombre',
                    'no_deactivated'    => 'Ningún Producto desactivado disponible',
                    'no_deleted'        => 'Ningún Producto eliminado disponible',
                    'total'             => 'Todos los Productos',
                    'price'             => 'Precio',
                    'description'       => 'Descripción',
                    'total_messages'       => 'Mensajes Total',
                ],


                'tabs' => [
                    'titles' => [
                        'overview' => 'Resumen',
                        'general' => 'Generales',
                        'packaging'  => 'Embalaje',
                        'images' => 'Imagenes',
                        'details' => 'Detalle',
                        'status' => 'Estatus',
                    ],

                    'content' => [

                        'status' => [
                            'history_transitions' => 'Histórico del flujo de aprobación',
                        ],

                        'overview' => [
                            'name'       => 'Nombre',
                            'description'    => 'Descripción',
                            'price'   => 'Precio',
                            'suggested_price'   => 'Precio Sugerido',
                            'brand_id'   => 'Marca',
                            'manufacturer_id'        => 'Fabricante',
                            'min_remain_shelf_life' => 'Caducidad (en días)',
                            'include_discount' => 'Descuento?',
                            'ind_include_discount' => 'Incluye Descuento?',
                            'ind_include_discount_false' => 'No Incluye Descuento',
                            'ind_limited_time_discount' => 'Descuento tiene Fecha Limitada?',
                            'ind_limited_time_discount_false' => 'No tiene fecha límite',
                            'discount_start_date'=> 'Fecha Inicio del Descuento',
                            'discount_end_date'=> 'Fecha Final del Descuento',
                            'discount_percentage'=> 'Porcentaje de Descuento',
                            'old_material_number'=> 'Número de Material antiguo',
                            'vendor_material_number' => 'Número Material (en el sistema del proveedor)',
                            'created_at' => 'Fecha de Creación',
                            'updated_at' => 'Última Actualización',
                            'status'       => 'Estatus',
                            'ind_regular_vendor'=> 'Es proveedor regular?',
                            'old_material_number' => 'Número de material antiguo',
                            'vendor_material_number' => 'Número de material del proveedor',
                            'vendor_account_number_id'=> 'Número de Cuenta del Proveedor',
                            'material_tax_type_id' => 'Clasificación Fiscal Material',
                            'purchase_tax_code_id' => 'Identificador Impuesto', 
                            'iva_code_id' => 'Indicador IVA',

                        ],
                    ],
                ],
            ],
        ],

    ],
];
