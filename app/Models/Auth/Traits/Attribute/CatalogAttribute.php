<?php

namespace App\Models\Auth\Traits\Attribute;

use Illuminate\Support\Facades\Hash;

/**
 * Trait CatalogAttribute.
 */
trait CatalogAttribute
{

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {

        return '<a href="'.route('admin.auth.catalog.edit', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
    }


    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {

        return '
    	<div class="btn-group" role="group" aria-label="'.__('labels.backend.access.users.user_actions').'">
          '.$this->edit_button.'
		</div>';
    }


}
