<?php

namespace App\Repositories\Frontend\Auth;

use Illuminate\Support\Facades\Auth;
use App\Models\Auth\User;
use App\Models\Auth\Product;
use App\Models\Auth\Message;
use App\Models\Auth\WorkflowStatus;
use App\Models\Catalog\ProductStatus;
use App\Models\Catalog\Brand;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Exceptions\ServiceException;
use Illuminate\Http\UploadedFile;
use App\Repositories\BaseRepository;
use App\Events\Backend\Auth\Product\ProductCreated;
use App\Events\Backend\Auth\Product\ProductUpdated;
use App\Notifications\Backend\Auth\ProductStatusUpdated;
use Illuminate\Support\Facades\Log;
use App\ProductSearch\ProductSearch;
use Illuminate\Http\Request;

 use App\Events\Backend\Auth\Product\ProductRestored;
 use App\Events\Backend\Auth\Product\ProductPermanentlyDeleted;
// use App\Events\Backend\Auth\User\UserConfirmed;
// use App\Events\Backend\Auth\User\UserDeactivated;
// use App\Events\Backend\Auth\User\UserReactivated;
// use App\Events\Backend\Auth\User\UserUnconfirmed;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Collection;

use GuzzleHttp\Exception\RequestException;

/**
 * Class ProductRepository.
 */
class ProductRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Product::class;
    }


     /**
     * @param array $data
     *
     * @return Product
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Product
    {
        

        return DB::transaction(function () use ($data) {

          $ind_include_discount = null;
          if (array_key_exists('ind_include_discount', $data)) {
            $ind_include_discount =  $data['ind_include_discount'];
          }
          $discount_percentage = null;
          if (array_key_exists('discount_percentage', $data)) {
            $discount_percentage =  $data['discount_percentage'];
          }
          $ind_limited_time_discount = null;
          if (array_key_exists('ind_limited_time_discount', $data)) {
            $ind_limited_time_discount =  $data['ind_limited_time_discount'];
          }
          $discount_start_date = null;
          if (array_key_exists('discount_start_date', $data)) {
            $discount_start_date =  $data['discount_start_date'];
          }
          $discount_end_date = null;
          if (array_key_exists('discount_end_date', $data)) {
            $discount_end_date =  $data['discount_end_date'];
          }

            $product = parent::create([
                'department_id' => $data['department_id'],
                'name' => $data['product_name'],
                'price' => $data['price'],
                'suggested_price' => $data['suggested_price'],
                'description' => $data['description'],
                'brand_id' => $data['brand_id'],
                'manufacturer_id' => $data['manufacturer_id'],
                'min_remain_shelf_life' => $data['min_remain_shelf_life'],
                'ind_include_discount' => $ind_include_discount,
                'discount_percentage' => $discount_percentage,
                'ind_limited_time_discount' => $ind_limited_time_discount,
                'discount_start_date' => $discount_start_date,
                'discount_end_date' => $discount_end_date,
                'old_material_number' => $data['old_material_number'],
                'vendor_material_number' => $data['vendor_material_number'],
                //'material_tax_type_id' => $data['material_tax_type_id'],
                'purchase_tax_code_id' => $data['purchase_tax_code_id'],
                'iva_code_id' => $data['iva_code_id'],
                'status_id'=> 1,
                'user_id' => Auth::id(),
            ]);

            if ($product) {
                Log::debug('product created');
                
                $description = ProductStatus::find(1)->description;
                $status = WorkflowStatus::Create(
                  [
                      'comments' => 'Producto Creado',
                      'current_status_description'=> $description,
                      'product_id' => $product->id,
                      'status_id' => 1,
                      'user_id' => Auth::id()
                  ]
                );
                 

                //TODO create language entry
                //$product->setStatus('creado', 'estatus de creacion');

                //Send confirmation email if requested and account approval is off
                // if (isset($data['confirmation_email']) && $user->confirmed == 0 && ! config('access.users.requires_approval')) {
                //     $user->notify(new UserNeedsConfirmation($user->confirmation_code));
                // }
                //TODO HABILITAR EL ENVIO DE CONFIRMACION DE CORREO A QUE USUARIOS

                event(new ProductCreated($product));
                return $product;
            }else{
              Log::debug('error in product creation');
            }

            throw new GeneralException(__('exceptions.backend.access.users.create_error'));  //TODO anadir key y traducir
        });
    }



/**
     * @param Product  $product
     * @param array $data
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_step2(Product $product, array $data) : Product
    {


      $fn_update = function() use ($product,$data)
        {
 
            try
            {

                $product->base_measurement_unit_id =  $data['base_measurement_unit_id'];  
                $product->gtin_1 =  $data['gtin_1'];
                $product->lower_level_measurement_unit_qty_1 =  $data['lower_level_measurement_unit_qty_1'];
                $product->lower_level_pack_hierarchy_measurement_unit_id_1 =  $data['lower_level_pack_hierarchy_measurement_unit_id_1'];

                $ind_measurement_unit_is_order_unit_1 = null;
                $ind_measurement_unit_is_delivery_or_issue_unit_1 = null;
                $ind_measurement_unit_is_order_unit_2 = null;
                $ind_measurement_unit_is_delivery_or_issue_unit_2 = null;
                $ind_measurement_unit_is_order_unit_3 = null;
                $ind_measurement_unit_is_delivery_or_issue_unit_3 = null;              


               
                $product->alt_measure_unit_stockkeep_unit_id_1 =  $data['alt_measure_unit_stockkeep_unit_id_1'];

                $arr_gtin_type = get_gtin_type($data['gtin_1']);
                $product->gtin_type_id_1 = $arr_gtin_type['id'];

                if (array_key_exists('gtin_2', $data)) {
                  $product->gtin_2 =  $data['gtin_2'];
                  $arr_gtin_type = get_gtin_type($data['gtin_2']);
                  $product->gtin_type_id_2 = $arr_gtin_type['id'];
                }
                if (array_key_exists('lower_level_measurement_unit_qty_2', $data)) {
                  $product->lower_level_measurement_unit_qty_2 =  $data['lower_level_measurement_unit_qty_2'];
                }else {
                  $product->lower_level_measurement_unit_qty_2 = null;
                }
                if (array_key_exists('lower_level_pack_hierarchy_measurement_unit_id_2', $data)) {
                  $product->lower_level_pack_hierarchy_measurement_unit_id_2 =  $data['lower_level_pack_hierarchy_measurement_unit_id_2'];
                }else {
                  $product->lower_level_pack_hierarchy_measurement_unit_id_2 = null;
                }


                if (array_key_exists('ind_measurement_unit_is_order_unit_2', $data)) {
                 $product->ind_measurement_unit_is_order_unit_2 =  $data['ind_measurement_unit_is_order_unit_2'];
                }else {
                  $product->ind_measurement_unit_is_order_unit_2 =  null;
                }

                if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_2', $data)) {
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_2 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_2'];
                }else {
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_2 = null;
                }


                if (array_key_exists('alt_measure_unit_stockkeep_unit_id_2', $data)) {
                  $product->alt_measure_unit_stockkeep_unit_id_2 =  $data['alt_measure_unit_stockkeep_unit_id_2'];
                }else {
                  $product->alt_measure_unit_stockkeep_unit_id_2 = null;
                }

                if (array_key_exists('gtin_3', $data)) {
                  $product->gtin_3 =  $data['gtin_3'];
                  $arr_gtin_type = get_gtin_type($data['gtin_3']);
                  $product->gtin_type_id_3 = $arr_gtin_type['id'];
                }
                if (array_key_exists('lower_level_measurement_unit_qty_3', $data)) {
                  $product->lower_level_measurement_unit_qty_3 =  $data['lower_level_measurement_unit_qty_3'];
                }else {
                  $product->lower_level_measurement_unit_qty_3 = null;
                }

                if (array_key_exists('lower_level_pack_hierarchy_measurement_unit_id_3', $data)) {
                  $product->lower_level_pack_hierarchy_measurement_unit_id_3 =  $data['lower_level_pack_hierarchy_measurement_unit_id_3'];
                }else {
                  $product->lower_level_pack_hierarchy_measurement_unit_id_3 = null;
                }


                if (array_key_exists('alt_measure_unit_stockkeep_unit_id_3', $data)) {
                  $product->alt_measure_unit_stockkeep_unit_id_3 =  $data['alt_measure_unit_stockkeep_unit_id_3'];
                }else {
                  $product->alt_measure_unit_stockkeep_unit_id_3 = null;
                }

                if (array_key_exists('length', $data)) {
                  $product->length =  $data['length'];
                }
                
                if (array_key_exists('width', $data)) {
                  $product->width =  $data['width'];
                }

                if (array_key_exists('height', $data)) {
                  $product->height =  $data['height'];
                }

                if (array_key_exists('weight', $data)) {
                  $product->weight =  $data['weight'];
                }

                if (array_key_exists('volume', $data)) {
                  $product->volume =  $data['volume'];
                }
                

                if (array_key_exists('ind_measurement_unit_is_order_unit_1', $data)) {
                  $ind_measurement_unit_is_order_unit_1 =  $data['ind_measurement_unit_is_order_unit_1'];
                  $product->ind_measurement_unit_is_order_unit_1 = $ind_measurement_unit_is_order_unit_1;
                }
                if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_1', $data)) {
                  $ind_measurement_unit_is_delivery_or_issue_unit_1 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_1'];
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_1 = $ind_measurement_unit_is_delivery_or_issue_unit_1;
                }

                if (array_key_exists('ind_measurement_unit_is_order_unit_2', $data)) {
                  $ind_measurement_unit_is_order_unit_2 =  $data['ind_measurement_unit_is_order_unit_2'];
                  $product->ind_measurement_unit_is_order_unit_2 = $ind_measurement_unit_is_order_unit_2;
                }
                if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_2', $data)) {
                  $ind_measurement_unit_is_delivery_or_issue_unit_2 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_2'];
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_2 = $ind_measurement_unit_is_delivery_or_issue_unit_2;
                }

                if (array_key_exists('ind_measurement_unit_is_order_unit_3', $data)) {
                  $ind_measurement_unit_is_order_unit_3 =  $data['ind_measurement_unit_is_order_unit_3'];
                  $product->ind_measurement_unit_is_order_unit_3 = $ind_measurement_unit_is_order_unit_3;
                }
                if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_3', $data)) {
                  $ind_measurement_unit_is_delivery_or_issue_unit_3 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_3'];
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_3 = $ind_measurement_unit_is_delivery_or_issue_unit_3;
                }

               

                //VERIFICAR SI TODOS LOS INDICADORES DE PEDIDO Y ENTREGA SON NULL, ASIGNARLOS A LA UNIDAD MAS BASICA
                if(is_null($ind_measurement_unit_is_order_unit_2) && is_null($product->ind_measurement_unit_is_delivery_or_issue_unit_2) && 
                  is_null($ind_measurement_unit_is_order_unit_3) && is_null($product->ind_measurement_unit_is_delivery_or_issue_unit_3)){
                    
                    $product->ind_measurement_unit_is_order_unit_1 = 1;
                    $product->ind_measurement_unit_is_delivery_or_issue_unit_1 = 1;

                }




                if($product->update()) {

                  event(new ProductUpdated($product));
                  return $product;

                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }

            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }



     /**
     * @param Product  $product
     * @param array $data
     * @param bool|UploadedFile  $image_front_1
     * @param bool|UploadedFile  $image_back_1
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_step3(Product $product, array $data, 
    $image_front_1=false,
    $image_back_1=false,
    $image_front_2=false,
    $image_back_2=false,
    $image_front_3=false,
    $image_back_3=false
     
     ) : Product
    {

      $fn_update = function() use ($product,$data,$image_front_1,$image_back_1,$image_front_2,$image_back_2,$image_front_3,$image_back_3)
        {
 
            try
            {
              // 'image_front_1'
              // 'image_back_1'

              for($i = 1; $i <= 3; $i++) {
                // Upload image(s) if necessary
          
                  if (${"image_front_$i"}) {
                    $time_str = "_".time(); 
                    $imageName = $product->id.$time_str.'_front.'.${"image_front_$i"}->getClientOriginalExtension();
                    //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_front_$i"}->getClientOriginalExtension();
                    
                    if(${"image_front_$i"}->move(public_path('storage/products'), $imageName)){
                      $product->{"image_front_$i"} = 'products/' .$imageName;
                    }
                  }
                }
          
                for($i = 1; $i <= 3; $i++) {
                  // Upload image(s) if necessary
          
                    if (${"image_back_$i"}) {
                      $time_str = "_".time(); 
                      $imageName = $product->id.$time_str.'_back.'.${"image_back_$i"}->getClientOriginalExtension();
                      //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_back_$i"}->getClientOriginalExtension();
                      
                      if(${"image_back_$i"}->move(public_path('storage/products'), $imageName)){
                        $product->{"image_back_$i"} = 'products/' .$imageName;
                      }
                    }
                }

                if($product->update()) {
                  $this->update_status($product, true, "Producto enviado a asistente de compras para revisión");
                  event(new ProductUpdated($product));
                  return $product;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }
            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }

 /**
     * @param Product  $product
     * @param array $data
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_general(Product $product, array $data) : Product
    {


        return DB::transaction(function () use ($product, $data) {

          ///$ind_regular_vendor = null;
          $ind_include_discount = null;
          $discount_percentage = null;
          $ind_limited_time_discount = null;
          $discount_start_date = null;
          $discount_end_date = null;

          //if (array_key_exists('ind_regular_vendor', $data)) {
          //  $ind_regular_vendor =  $data['ind_regular_vendor'];
          //}

          if (array_key_exists('ind_include_discount', $data)) {
            $ind_include_discount =  $data['ind_include_discount'];

            
            if (array_key_exists('discount_percentage', $data)) {
              $discount_percentage =  $data['discount_percentage'];
            }


          
            if (array_key_exists('ind_limited_time_discount', $data)) {
              $ind_limited_time_discount =  $data['ind_limited_time_discount'];


            
              if (array_key_exists('discount_start_date', $data)) {
                $discount_start_date =  $data['discount_start_date'];
              }
              
              if (array_key_exists('discount_end_date', $data)) {
                $discount_end_date =  $data['discount_end_date'];
              }

            }

          }
          

            if ($product->update([
              'name' => $data['name'],
              'description' => $data['description'],
              'price' => $data['price'],
              'suggested_price' => $data['suggested_price'],
              'min_remain_shelf_life' => $data['min_remain_shelf_life'],
              'brand_id' => $data['brand_id'],
              'manufacturer_id' => $data['manufacturer_id'],
              'ind_include_discount' => $ind_include_discount,
              'discount_percentage' => $discount_percentage,
              'ind_limited_time_discount' => $ind_limited_time_discount,
              'discount_start_date' => $discount_start_date,
              'discount_end_date' => $discount_end_date,
              'old_material_number' => $data['old_material_number'],
              'vendor_material_number' => $data['vendor_material_number'],
              'department_id' => $data['department_id'],
              //'material_tax_type_id' => $data['material_tax_type_id'],
              'purchase_tax_code_id' => $data['purchase_tax_code_id'],
              'iva_code_id' => $data['iva_code_id'],

            ])) {
              
              
              
                event(new ProductUpdated($product));
                return $product;

            }

            throw new GeneralException(__('exceptions.backend.access.products.update_error')); //TODO anadir key y traducir
        }); 
    }


        /**
     * @param Product  $product
     * @param array $data
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_packaging(Product $product, array $data) : Product
    {


      $fn_update = function() use ($product,$data)
        {
 
            try
            {

                //dd($product);
                $product->base_measurement_unit_id =  $data['base_measurement_unit_id'];  
                $product->gtin_1 =  $data['gtin_1'];
                $product->lower_level_measurement_unit_qty_1 =  $data['lower_level_measurement_unit_qty_1'];
                $product->lower_level_pack_hierarchy_measurement_unit_id_1 =  $data['lower_level_pack_hierarchy_measurement_unit_id_1'];
                
                if (array_key_exists('ind_measurement_unit_is_order_unit_1', $data)) {
                  $product->ind_measurement_unit_is_order_unit_1 =  $data['ind_measurement_unit_is_order_unit_1'];
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_1 =  $data['ind_measurement_unit_is_order_unit_1'];
                }else {
                  $product->ind_measurement_unit_is_order_unit_1 = null;
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_1 = null;
                }
                
                // if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_1', $data)) {
                //   $product->ind_measurement_unit_is_delivery_or_issue_unit_1 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_1'];
                // }else {
                //   $product->ind_measurement_unit_is_delivery_or_issue_unit_1 = 0;
                // }
                
                $product->alt_measure_unit_stockkeep_unit_id_1 =  $data['alt_measure_unit_stockkeep_unit_id_1'];
                //$product->gtin_type_id_1 = get_gtin_type($data['gtin_1']); //TODO fix function to get gtin_type



                if (array_key_exists('gtin_2', $data)) {
                  $product->gtin_2 =  $data['gtin_2'];
                  //$product->gtin_type_id_2 = get_gtin_type($data['gtin_2']);
                }else {
                  if($product->gtin_2){
                    $product->gtin_2 = null;
                  }
                }

                if (array_key_exists('lower_level_measurement_unit_qty_2', $data)) {
                  $product->lower_level_measurement_unit_qty_2 =  $data['lower_level_measurement_unit_qty_2'];
                }else {
                    $product->lower_level_measurement_unit_qty_2 = null;
                }

                if (array_key_exists('lower_level_pack_hierarchy_measurement_unit_id_2', $data)) {
                  $product->lower_level_pack_hierarchy_measurement_unit_id_2 =  $data['lower_level_pack_hierarchy_measurement_unit_id_2'];
                }else {
                    $product->lower_level_pack_hierarchy_measurement_unit_id_2 = null;
                }

                if (array_key_exists('ind_measurement_unit_is_order_unit_2', $data)) {
                  $product->ind_measurement_unit_is_order_unit_2 =  $data['ind_measurement_unit_is_order_unit_2'];
                }else {
                      $product->ind_measurement_unit_is_order_unit_2 = null;
                }

                if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_2', $data)) {
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_2 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_2'];
                }else {
                      $product->ind_measurement_unit_is_delivery_or_issue_unit_2 = null;
                }

                if (array_key_exists('alt_measure_unit_stockkeep_unit_id_2', $data)) {
                  $product->alt_measure_unit_stockkeep_unit_id_2 =  $data['alt_measure_unit_stockkeep_unit_id_2'];
                }else {
                    $product->alt_measure_unit_stockkeep_unit_id_2 = null;
                }

                if (array_key_exists('gtin_3', $data)) {
                  $product->gtin_3 =  $data['gtin_3'];
                  //$product->gtin_type_id_3 = get_gtin_type($data['gtin_3']);
                }else {
                  if($product->gtin_3){
                    $product->gtin_3 = null;
                  }
                }




                if (array_key_exists('lower_level_measurement_unit_qty_3', $data)) {
                  $product->lower_level_measurement_unit_qty_3 =  $data['lower_level_measurement_unit_qty_3'];
                }else {
                    $product->lower_level_measurement_unit_qty_3 = null;
                }



                 if (array_key_exists('lower_level_pack_hierarchy_measurement_unit_id_3', $data)) {
                   $product->lower_level_pack_hierarchy_measurement_unit_id_3 =  $data['lower_level_pack_hierarchy_measurement_unit_id_3'];
                 }else {
                     $product->lower_level_pack_hierarchy_measurement_unit_id_3 = null;
                 }


                if (array_key_exists('ind_measurement_unit_is_order_unit_3', $data)) {
                  $product->ind_measurement_unit_is_order_unit_3 =  $data['ind_measurement_unit_is_order_unit_3'];
                }else {
                      $product->ind_measurement_unit_is_order_unit_3 = null;
                }

                if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_3', $data)) {
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_3 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_3'];
                }else {
                      $product->ind_measurement_unit_is_delivery_or_issue_unit_3 = null;
                }

                if (array_key_exists('alt_measure_unit_stockkeep_unit_id_3', $data)) {
                  $product->alt_measure_unit_stockkeep_unit_id_3 =  $data['alt_measure_unit_stockkeep_unit_id_3'];
                }else {
                    $product->alt_measure_unit_stockkeep_unit_id_3 = null;
                }


                if($product->update()) {
                  event(new ProductUpdated($product));
                  return $product;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }

            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }


     /**
     * @param Product  $product
     * @param array $data
     * @param bool|UploadedFile  $image_front_1
     * @param bool|UploadedFile  $image_back_1
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_images(Product $product, array $data,
      $image_front_1=false,
      $image_back_1=false
    ) : Product
    {


      //dd($image_front_1);
      for($i = 1; $i <= 1; $i++) {
      // Upload image(s) if necessary

        if (${"image_front_$i"}) {
          $time_str = "_".time(); 
          $imageName = $product->id.$time_str.'_front.'.${"image_front_$i"}->getClientOriginalExtension();
          //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_front_$i"}->getClientOriginalExtension();
          
          if(${"image_front_$i"}->move(public_path('storage/products'), $imageName)){
            $product->{"image_front_$i"} = 'products/' .$imageName;
          }
        }
      }

      for($i = 1; $i <= 1; $i++) {
        // Upload image(s) if necessary

          if (${"image_back_$i"}) {
            $time_str = "_".time(); 
            $imageName = $product->id.$time_str.'_back.'.${"image_back_$i"}->getClientOriginalExtension();
            //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_back_$i"}->getClientOriginalExtension();
            
            if(${"image_back_$i"}->move(public_path('storage/products'), $imageName)){
              $product->{"image_back_$i"} = 'products/' .$imageName;
            }
          }
      }

      //dd($product);
      $fn_update = function() use ($product,$data)
        {
 
            try
            {

                if($product->update()) {
                  event(new ProductUpdated($product));
                  return $product;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }

            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }

     /**
     * @param Message  $message
     * @return Message
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_message(Message $message) : Message
    {
     
      $fn_update = function() use ($message)
        {
 
            try
            {

                $message->read =  1;

                if($message->update()) {
                  //event(new ProductUpdated($product));
                  return $message;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }
            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;

    }



         /**
     * @param Product  $product
     * @param int $status_id
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */

    public function update_status(Product $product, bool $approved, string $comments="") : Product
    {

      
      //dd($product);
      if($product->status_id == 2){
        return null;
      }
      
      $old_status_id  = $product->status_id;

      $new_status_id = 2;

      $description = ProductStatus::find($new_status_id)->description;

      $fn_update = function() use ($product,$old_status_id, $new_status_id,$comments,$description)
        {
 
            try
            {

                $product->status_id =  $new_status_id;

                if($product->update()) {

                  if($old_status_id!=1){

                    $workflow_status = WorkflowStatus::where('product_id', '=',$product->id)->where('status_id', '=',7)->get()->sortByDesc('created_at')->first();
                    $user = User::find($workflow_status->user_id);
                    Auth::user()->sendMessageTo($product->id, $user->id, $description, $comments);
                    $user->notify(new ProductStatusUpdated(['product_name'=>$product->name, 'user_info'=>Auth::user()->email] ));

                  }



                  event(new ProductUpdated($product));

                  $status = WorkflowStatus::Create(
                    [
                        'comments' => $comments,
                        'current_status_description'=> $description,
                        'product_id' => $product->id,
                        'status_id' => $new_status_id,
                        'user_id' => Auth::id()
                    ]
                  );
                   
                  
                return $product;

                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }
            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }

    /**
     * @param int    $user_id
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getMessagesPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return Message::where('sent_to_id', Auth::user()->id)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            //->select('products.name', 'products.description', 'product_statuses.name as status_name','measurement_units.short_text_description')
            ->where('user_id', Auth::user()->id)
            //->leftJoin('measurement_units', 'products.order_price_measurement_unit_id', '=', 'measurement_units.id')
            //->leftJoin('product_statuses', 'products.status_id', '=', 'product_statuses.id')
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param SearchProductRequest $request
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getFilteredPaginated(Request $request, $paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        //dd($request);
     /* 
      return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    */
      //dd(ProductSearch::apply($request,$paged,$orderBy,$sort));

      return ProductSearch::apply($request,$paged,$orderBy,$sort,Auth::user()->id);

    }




    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

        /**
     * @param Product $product
     *
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Product $product) : Product
    {
        if (is_null($product->deleted_at)) {
            throw new GeneralException(__('exceptions.backend.products.delete_first'));
        }

        return DB::transaction(function () use ($product) {
            // Delete associated relationships
            //TODO WHAT RELATIONSHIPS DOES PRODUCT HAS???
            //$product->passwordHistories()->delete();
            //$product->providers()->delete();
            //$product->sessions()->delete();

            $product1 = Product::where('id', $product->id);
            dd($product1);

            if ($product->forceDelete()) {
                event(new ProductPermanentlyDeleted($product));

                return $product;
            }

            throw new GeneralException(__('exceptions.backend.products.delete_error'));
        });
    }


        /**
     * @param Product $product
     *
     * @return Product
     * @throws GeneralException
     */
    public function restore(Product $product) : Product
    {
        dd($product);
        if (is_null($product->deleted_at)) {
            throw new GeneralException(__('exceptions.backend.access.products.cant_restore')); //todo add entries in language files
        }

        if ($product->restore()) {
            event(new ProductRestored($product));

            return $product;
        }

        throw new GeneralException(__('exceptions.backend.access.products.restore_error'));
    }



     public function getManufacturers()
     {
 
      return Cache::remember('manufacturers', 1, function ()  {
 
       try {
 
           $url = env('MULE_MANUFACTURER_URL');
           $client = new \GuzzleHttp\Client(['headers' => [
             'channelID' => env('MULE_CHANNELID'),
             'user'      => env('MULE_USER'),
             'password'  => env('MULE_PASSWORD')]]
           );
 
           $response = $client->request('GET', $url);
 
           $apiResult = json_decode((string)$response->getBody());
           $manufacturers = $apiResult->manufacturerList;
           //dd($manufacturers);
           //$departmentsCollection = collect([]);
           $assocDataArray = array();
           $i = 1;
           foreach($manufacturers as $manufacturer){
               
               //$departmentsCollection->push([$department->departmentId=>$department->departmentName]);
               $assocDataArray[$i] = $manufacturer->ManufacturerName;
               $i++;
 
           }
           //dd('in.'.$departmentsCollection);
           //return  $departmentsCollection->toArray();
           return $assocDataArray;
 
           //return json_decode((string)$response->getBody());
 
       } catch (RequestException $e) {
           \Log::error('Error: '.$e);
           throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
       } 
 
 
 
   });
 
     }
 
     public function saveBrands(){

      $brands = $this->getBrands();
      //dd($brands);
       foreach ($brands as $key => $value) {
          
           $brandData = array('name' => $value, 'brand_id' => $key, 'user_id'=>1,'valid'=>1);
           Brand::create($brandData);

       }



  }

    public function getBrands()
    {

     return Cache::remember('brands', 1, function ()  {

      try {

          $url = env('MULE_BRAND_URL');
          //$url = env('MULE_MANUFACTURER_URL');
          //dd($url);

          $client = new \GuzzleHttp\Client(['headers' => [
            'channelID' => env('MULE_CHANNELID'),
            'user'      => env('MULE_USER'),
            'password'  => env('MULE_PASSWORD')]]
          );
     
          $response = $client->request('GET', $url);

          $apiResult = json_decode((string)$response->getBody());
          $brands = $apiResult->brandList;
          //dd($brands);
          //$departmentsCollection = collect([]);
          $assocDataArray = array();
          foreach($brands as $brand){
              
              //$departmentsCollection->push([$department->departmentId=>$department->departmentName]);
              $assocDataArray[$brand->brandId] = $brand->brandName;
          }
          //dd('in.'.$departmentsCollection);
          //return  $departmentsCollection->toArray();
          return $assocDataArray;

          //return json_decode((string)$response->getBody());

      } catch (RequestException $e) {
        \Log::error('Error: '.$e);
          throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
      } 

    });

    }




     public function getDepartment()
     {
         
         return Cache::remember('departments', 1, function ()  {
 
             try {
 
                 $url = env('MULE_DEPARTMENT_URL');
                 $client = new \GuzzleHttp\Client(['headers' => [
                   'channelID' => env('MULE_CHANNELID'),
                   'user'      => env('MULE_USER'),
                   'password'  => env('MULE_PASSWORD')]]
                 );
     
                 $response = $client->request('GET', $url);
     
                 $apiResult = json_decode((string)$response->getBody());
                 $departments = $apiResult->departmentList;
                 //$departmentsCollection = collect([]);
                 $assocDataArray = array();
                 foreach($departments as $department){
                     
                     //$departmentsCollection->push([$department->departmentId=>$department->departmentName]);
                     $assocDataArray[$department->departmentId] = $department->departmentName;
 
                 }
                 //dd('in.'.$departmentsCollection);
                 //return  $departmentsCollection->toArray();
                 return $assocDataArray;
     
                 //return json_decode((string)$response->getBody());
 
             } catch (RequestException $e) {
              \Log::error('Error: '.$e);
                 throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
             } 
 
 
 
         });
     }
 
 
     public function getCategory($departmentId)
     {
         return Cache::remember('department:' . $departmentId, 1, function () use ($departmentId) {
             
             try {
                 
                 //$response = $client->get('/not_found.xml')->send();
 
                 $client = new \GuzzleHttp\Client(['headers' => [
                   'channelID' => env('MULE_CHANNELID'),
                   'user'      => env('MULE_USER'),
                   'password'  => env('MULE_PASSWORD')]]
                 );
 
                 $url = env('MULE_CATEGORY_URL');
                 $response = $client->request('GET', $url, [
                     'query' => [
                         'departmentId' => $departmentId
                     ]
                 ]);
     
                 $apiResult = json_decode((string)$response->getBody());
                 $categories = $apiResult->categoryList;
                 //$departmentsCollection = collect([]);
                 $assocDataArray = array();
                 foreach($categories as $category){
                     
                     //$departmentsCollection->push([$department->departmentId=>$department->departmentName]);
                     $assocDataArray[$category->categoryId] = $category->categoryName;
 
                 }
                 //dd('in.'.$departmentsCollection);
                 //return  $departmentsCollection->toArray();
                 return $assocDataArray;
 
 
             } catch (RequestException $e) {
              \Log::error('Error: '.$e);
                 throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
             }    
 
         });
     }
 
 
     public function getHierarchy($categoryId)
     {
 
 
         return Cache::remember('hierarchy:' . $categoryId, 1, function () use ($categoryId) {
             
             try {
                 
                 //$response = $client->get('/not_found.xml')->send();
 
                 $client = new \GuzzleHttp\Client(['headers' => [
                   'channelID' => env('MULE_CHANNELID'),
                   'user'      => env('MULE_USER'),
                   'password'  => env('MULE_PASSWORD')]]
                 );
 
                 $url = env('MULE_HIERARCHY_URL');
                 $response = $client->request('GET', $url, [
                     'query' => [
                         'categoryId' => $categoryId
                     ]
                 ]);
                 //dd($response->getBody());
                 $apiResult = json_decode((string)$response->getBody());
                 //dd($apiResult);
                 $hierarchies = $apiResult->hierarchyList;
                 //dd($hierarchies);
                 //$departmentsCollection = collect([]);
                 $assocDataArray = array();
                 foreach($hierarchies as $hierarchy){
                     //dd($hierarchy);
                     //$departmentsCollection->push([$department->departmentId=>$department->departmentName]);
                     $assocDataArray[$hierarchy->hierarchyId] = $hierarchy->hierarchyName;
 
                 }
                 //dd('in.'.$departmentsCollection);
                 //return  $departmentsCollection->toArray();
                 return $assocDataArray;
 
                 //BUG 404 NOT FOUND 
                 //categoryId: 90902010
                 //\Log::info('User Logged In: '.$event->user->full_name);
             } catch (RequestException $e) {
              \Log::error('Error: '.$e);
                 throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
             }    
 
         });
     }


    //TODO IMPROVE EXCEPTION HANDLING
    public function getStores()
    {
        
        return Cache::remember('stores', 1, function ()  {

            try {

                $url = env('MULE_STORES_URL');
                $client = new \GuzzleHttp\Client(['headers' => [
                  'channelID' => env('MULE_CHANNELID'),
                  'user'      => env('MULE_USER'),
                  'password'  => env('MULE_PASSWORD')]]
                );
    
                $response = $client->request('GET', $url);
    
                $apiResult = json_decode((string)$response->getBody());
                $stores = $apiResult->storeList;

                $assocDataArray = array();
                foreach($stores as $store){

                    $assocDataArray[$store->storeNo] = $store->storeName;

                }

                return $assocDataArray;
    

            } catch (RequestException $e) {
              \Log::error('Error: '.$e);
                throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
            } 



        });
    }



}