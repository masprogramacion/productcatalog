<footer class="app-footer">
    <div>
        <strong>@lang('labels.general.copyright') &copy; {{ date('Y') }}</strong> @lang('strings.backend.general.all_rights_reserved')
    </div>

    <div class="ml-auto">Desarrollo por <a target="_blank" href="http://www.masprogramacion.com">MasProgramacion</a></div>
</footer>
