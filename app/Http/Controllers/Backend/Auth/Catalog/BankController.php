<?php

namespace App\Http\Controllers\Backend\Auth\Catalog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Catalog\Bank;
use App\Http\Requests\Backend\Auth\Catalog\ManageBankRequest;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //DB::enableQueryLog(); 
        //$totalData = Product::verified()->count();
        //dump($totalData);
        //dd(DB::getQueryLog()); // Show results of log\
        $banks = Bank::paginate(10);
        // foreach($banks as $bank){
        //    dump($bank->action_buttons);
        // }
        // dd($banks);
        return view('backend.auth.catalog.banks.index')
        ->with('banks', $banks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //\Log::info('Bank updated: '.$request);
        //dd($request);
        $action = $request['action'];

        switch ($action){
            case 'edit':
                $bank = Bank::find($request->input('id'));
                $bank->update([
                    'name' => $request['bank_name']
                ]);
                break;
                case 'delete':
                $bank = Bank::find($request->input('id'));
                $bank->delete();
                break;

            default:

                break;
        }



        //return redirect()->route('admin.auth.catalog.bank.index')->withFlashSuccess(__('alerts.backend.catalogs.updated'));
        return response()->json(['result'=>'updated']);

    }


    /**
     * Remove the specified resource from storage.
     * @param ManageUserRequest $request
     * @param Bank              $bank
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageBankRequest $request, Bank $bank)
    {
        
        //dd($bank);

        $bank->delete();

        //event(new RoleDeleted($role)); TODO check 

        return redirect()->route('admin.auth.catalog.bank.index')->withFlashSuccess(__('alerts.backend.catalogs.deleted'));
    }
}

