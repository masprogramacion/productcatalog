<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Models\Auth\User;
use App\Http\Controllers\Controller;
use App\Events\Backend\Auth\User\UserDeleted;
use App\Repositories\Backend\Auth\RoleRepository;
use App\Repositories\Backend\Auth\VendorRepository;
use App\Repositories\Backend\Auth\PermissionRepository;
use App\Http\Requests\Backend\Auth\User\StoreUserVendorRequest;
use App\Http\Requests\Backend\Auth\User\ManageUserVendorRequest;
use App\Http\Requests\Backend\Auth\User\UpdateUserVendorRequest;

/**
 * Class UserVendorController.
 */
class UserVendorController extends Controller
{
    /**
     * @var VendorRepository
     */
    protected $vendorRepository;

    /**
     * UserVendorController constructor.
     *
     * @param VendorRepository $vendorRepository
     */
    public function __construct(VendorRepository $vendorRepository)
    {
        $this->vendorRepository = $vendorRepository;
    }

    /**
     * @param ManageUserVendorRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageUserVendorRequest $request)
    {
        $users =  User::sortable()->where('user_type_id',1)->paginate(config('xtra.paged.users'));
        return view('backend.auth.vendor.index')
             ->withUsers($users);
            //->withUsers($this->vendorRepository->getActivePaginated(25, 'id', 'desc'));
    }

    /**
     * @param ManageUserVendorRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     *
     * @return mixed
     */
    public function create(ManageUserVendorRequest $request, RoleRepository $roleRepository, PermissionRepository $permissionRepository)
    {
        //dd($roleRepository->with('permissions')->get(['id', 'name']));
        return view('backend.auth.vendor.create')
            ->withVendors($this->vendorRepository->getVendors())
            ->withRoles($roleRepository->with('permissions')->get(['id', 'name']))
            ->withPermissions($permissionRepository->get(['id', 'name']));
    }

    /**
     * @param StoreUserVendorRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreUserVendorRequest $request)
    {
        //dd($request);
        $request['user_type_id']=1; //end_user
        $request['confirmed']=0; //force send email
        $this->vendorRepository->create($request->only(
            'first_name',
            'last_name',
            'email',
            'password',
            'active',
            'confirmed',
            'confirmation_email',
            'roles',
            'permissions',
            'user_type_id',
            'sap_account_number'
        ));

        return redirect()->route('admin.auth.vendor.index')->withFlashSuccess(__('alerts.backend.vendors.created'));
    }

    /**
     * @param ManageUserVendorRequest $request
     * @param User              $user
     *
     * @return mixed
     */
    public function show(ManageUserVendorRequest $request, User $user)
    {
        //dd($user);
        return view('backend.auth.vendor.show')
            ->withUser($user);
    }

    /**
     * @param ManageUserVendorRequest    $request
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     * @param User                 $user
     *
     * @return mixed
     */
    public function edit(ManageUserVendorRequest $request, RoleRepository $roleRepository, PermissionRepository $permissionRepository, User $user)
    {
        $sap_account_number = null;
        if($user->sapData && $user->sapData->sap_account_number){
            $sap_account_number =  $user->sapData->sap_account_number;
        }
        return view('backend.auth.vendor.edit')
            ->withUser($user)
            ->with('sap_account_number', $sap_account_number)
            ->withVendors($this->vendorRepository->getVendors())
            ->withRoles($roleRepository->get())
            ->withUserRoles($user->roles->pluck('name')->all())
            ->withPermissions($permissionRepository->get(['id', 'name']))
            ->withUserPermissions($user->permissions->pluck('name')->all());
    }

    /**
     * @param UpdateUserVendorRequest $request
     * @param User              $user
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateUserVendorRequest $request, User $user)
    {
        $this->vendorRepository->update($user, $request->only(
            'first_name',
            'last_name',
            'email',
            'roles',
            'permissions',
            'sap_account_number'
        ));

        return redirect()->route('admin.auth.vendor.index')->withFlashSuccess(__('alerts.backend.vendors.updated'));
    }

    /**
     * @param ManageUserVendorRequest $request
     * @param User              $user
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageUserVendorRequest $request, User $user)
    {
        $this->vendorRepository->deleteById($user->id);

        event(new UserDeleted($user));

        return redirect()->route('admin.auth.vendor.deleted')->withFlashSuccess(__('alerts.backend.vendors.deleted'));
    }
}
