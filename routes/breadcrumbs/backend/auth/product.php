<?php

Breadcrumbs::for('admin.auth.product.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('Area de Productos'), route('admin.auth.product.index'));
});

Breadcrumbs::for('admin.auth.product.create', function ($trail) {
    $trail->parent('admin.auth.product.index');
    $trail->push(__('Crear Producto'), route('admin.auth.product.create'));
});

Breadcrumbs::for('admin.auth.product.create.step2', function ($trail, $id) {
    $trail->parent('admin.auth.product.index');
    $trail->push(__('Crear Producto - Paso 2'), route('admin.auth.product.create.step2', $id));
});

Breadcrumbs::for('admin.auth.product.create.step3', function ($trail, $id) {
    $trail->parent('admin.auth.product.index');
    $trail->push(__('Crear Producto - Paso 3'), route('admin.auth.product.create.step3', $id));
});

Breadcrumbs::for('admin.auth.product.create.step4', function ($trail, $id) {
    $trail->parent('admin.auth.product.index');
    $trail->push(__('Crear Producto - Producto Enviado'), route('admin.auth.product.create.step4', $id));
});

Breadcrumbs::for('admin.auth.product.search', function ($trail) {
    $trail->parent('admin.auth.product.index');
    $trail->push(__('Buscar Producto'), route('admin.auth.product.search'));
});

Breadcrumbs::for('admin.auth.product.deleted', function ($trail) {
    $trail->parent('admin.auth.product.index');
    $trail->push(__('Borrar Producto'), route('admin.auth.product.deleted'));
});

Breadcrumbs::for('admin.auth.product.show', function ($trail, $id) {
    $trail->parent('admin.auth.product.index');
    $trail->push(__('Mostrar Producto'), route('admin.auth.product.show', $id));
});

Breadcrumbs::for('admin.auth.product.edit', function ($trail, $id) {
    $trail->parent('admin.auth.product.index');
    $trail->push(__('Editar Producto'), route('admin.auth.product.edit', $id));
});

Breadcrumbs::for('admin.auth.product.message.index', function ($trail) {
    $trail->parent('admin.auth.product.index');
    $trail->push(__('Ver Mensajes'), route('admin.auth.product.message.index'));
});

Breadcrumbs::for('admin.auth.product.message.show', function ($trail, $id) {
    $trail->parent('admin.auth.product.message.index');
    $trail->push(__('Leer Mensaje'), route('admin.auth.product.message.show', $id));
});

Breadcrumbs::for('admin.auth.excel.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('Exportar para Winshuttle'), route('admin.auth.excel.index'));
});

Breadcrumbs::for('admin.auth.product.assign', function ($trail, $id) {
    $trail->parent('admin.auth.product.index');
    $trail->push(__('Asignar Categoria'), route('admin.auth.product.assign', $id));
});


Breadcrumbs::for('admin.auth.product.create.uncatalogued', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('labels.frontend.access.products.create'), route('admin.auth.product.create.uncatalogued'));
});

Breadcrumbs::for('admin.auth.product.search.uncatalogued', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('Buscar Producto'), route('admin.auth.product.search.uncatalogued'));
});

Breadcrumbs::for('admin.auth.product.show.uncatalogued', function ($trail,$product) {
    $trail->parent('admin.dashboard');
    $trail->push(__('Buscar Producto'), route('admin.auth.product.show.uncatalogued',$product));
});

Breadcrumbs::for('admin.auth.product.edit.uncatalogued', function ($trail,$product) {
    $trail->parent('admin.dashboard');
    $trail->push(__('Editar Producto'), route('admin.auth.product.edit.uncatalogued',$product));
});

