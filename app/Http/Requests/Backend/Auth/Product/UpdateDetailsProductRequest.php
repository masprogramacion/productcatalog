<?php

namespace App\Http\Requests\Backend\Auth\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\Auth\Product;

/**
 * Class UpdateDetailsProductRequest.
 */
class UpdateDetailsProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return $this->user()->isAdmin();
         //TODO this can be used by a regular user as well
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        //dd($this->route('product')->id);
        Validator::extend('custom_sku_conflict', function($attribute, $value, $parameters) 
        {
            $sku_exists = 0;

            if (Product::where('sku', '=', $value)->where('id', '<>', $parameters[0])->exists()) {
                $sku_exists++;
            }


            return ($sku_exists==0);

        }); 


        return [

            //'material_tax_type_id',
            //'purchase_tax_code_id',
            //'iva_code_id',

            //'ind_regular_vendor',
            //'ind_measurement_unit_is_delivery_or_issue_unit',
            //'sku'   => 'required|numeric|custom_sku_conflict:' . $this->route('product')->id,
            //'vendor_account_number' => 'required|numeric',
            'ind_regular_vendor'  => 'sometimes|nullable',
            'price' => 'required|numeric',
            'material_type_id' => 'required',
            'order_price_measurement_unit_id' => 'required',
            //'material_group_id' => 'required',
            'purchase_organization_id' => 'required',
            'purchase_group_id' => 'required',
            'department_id' => 'required',
            'category_id' => 'required',
            'hierarchy_id' => 'required',
            'available_deliverable_from' => 'sometimes|date|nullable',
            'available_deliverable_until' => 'sometimes|date|nullable',
            'short_text_description'  => 'required|max:18'
        ];
    }

    public function messages()
    {
        return [
            'custom_sku_conflict' => 'Codigo SKU ya existe en el sistema',
        ];
    }



}
