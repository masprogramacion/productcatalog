<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;


class Brand extends Model
{

    public $timestamps = true;
    
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'brand_id', 'user_id', 'valid'];
}
