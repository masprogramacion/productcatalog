<?php

Breadcrumbs::for('frontend.auth.product.index', function ($trail) {
    $trail->parent('frontend.user.dashboard');
    $trail->push(__('labels.frontend.access.products.management'), route('frontend.auth.product.index'));
});

Breadcrumbs::for('frontend.auth.product.create.basic', function ($trail) {
    $trail->parent('frontend.user.dashboard');
    $trail->push(__('labels.frontend.access.products.create'), route('frontend.auth.product.create.basic'));
});


Breadcrumbs::for('frontend.auth.product.create', function ($trail) {
    $trail->parent('frontend.auth.product.index');
    $trail->push(__('labels.frontend.access.products.create'), route('frontend.auth.product.create'));
});

Breadcrumbs::for('frontend.auth.product.create.step2', function ($trail, $id) {
    $trail->parent('frontend.auth.product.index');
    $trail->push(__('Crear Producto - Paso 2'), route('frontend.auth.product.create.step2', $id));
});

Breadcrumbs::for('frontend.auth.product.create.step3', function ($trail, $id) {
    $trail->parent('frontend.auth.product.index');
    $trail->push(__('Crear Producto - Paso 3'), route('frontend.auth.product.create.step3', $id));
});

Breadcrumbs::for('frontend.auth.product.edit', function ($trail, $id) {
    $trail->parent('frontend.auth.product.index');
    $trail->push(__('labels.frontend.access.products.edit'), route('frontend.auth.product.edit', $id));
});

Breadcrumbs::for('frontend.auth.product.show', function ($trail, $id) {
    $trail->parent('frontend.auth.product.index');
    $trail->push(__('labels.frontend.access.products.show'), route('frontend.auth.product.show', $id));
});


Breadcrumbs::for('frontend.auth.product.deleted', function ($trail) {
    $trail->parent('frontend.auth.product.index');
    $trail->push(__('labels.frontend.access.products.deleted'), route('frontend.auth.product.deleted'));
});

Breadcrumbs::for('frontend.auth.product.search', function ($trail) {
    $trail->parent('frontend.auth.product.index');
    $trail->push(__('labels.frontend.access.products.search'), route('frontend.auth.product.search'));
});

Breadcrumbs::for('frontend.auth.product.message.index', function ($trail) {
    $trail->parent('frontend.auth.product.index');
    $trail->push(__('labels.frontend.access.products.messages.title'), route('frontend.auth.product.message.index'));
});

Breadcrumbs::for('frontend.auth.product.message.show', function ($trail, $id) {
    $trail->parent('frontend.auth.product.message.index');
    $trail->push(__('labels.frontend.access.products.message_show'), route('frontend.auth.product.message.show', $id));
});

