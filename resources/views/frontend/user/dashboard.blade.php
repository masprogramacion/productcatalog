@extends('frontend.layouts.app2')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')
    @if(!auth()->user()->profile()->exists() || !auth()->user()->bankAccount()->exists()) 
    <div class="row mb-4">
            <div class="col">
                <div class="alert alert-warning" role="alert">
                    <h4 class="alert-heading"><i class="fa fa-exclamation-circle"></i>Información Pendiente</h4>
                    <p><h5>¿Que información esta pendiente por completar?</h5>Usted debe completar la siguiente información para que se habilite la opción "Mis Productos" en donde podrá crear y administrar sus productos.</p>
                    <hr>
                    <ul>
                    @if(!$general_information_complete)
                    <li>Para completar su información general haga clic aquí <a href="{{ route('frontend.user.account').'#general'}}">GENERAL</a></li>
                    @endif                        
                    @if(!auth()->user()->profile()->exists())
                    <li>Para completar su información de su perfil haga clic aquí <a href="{{ route('frontend.user.account').'#profile'}}">PERFIL</a></li>
                    @endif
                    @if(!auth()->user()->bankAccount()->exists())
                    <li>Para completar su información de su cuenta bancaria haga clic aquí <a href="{{ route('frontend.user.account').'#bank-account'}}">CUENTA BANCARIA</a></li>
                    @endif
                    </ul>

                </div>
            </div>
    </div>
    @endif
 
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>
                        <i class="fas fa-tachometer-alt"></i> @lang('navs.frontend.dashboard')
                    </strong>
                </div><!--card-header-->

                <div class="card-body">

                        <div class="row">
                            <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-primary">
                            <div class="card-body pb-0">
                            <div class="btn-group float-right">
                            <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-settings"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(31px, 23px, 0px);">
                            <a class="dropdown-item" href="{{ route('frontend.auth.product.index') }}">Ver Detalles</a>
                            </div>
                            </div>
                            <div class="text-value">{{$products_total}}</div>
                            <div>Total de Productos</div>
                            </div>
                            <div class="chart-wrapper mt-3 mx-3" style="height:70px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                            <canvas class="chart chartjs-render-monitor" id="card-chart1" height="140" width="452" style="display: block; height: 70px; width: 226px;"></canvas>
                            </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-success">
                            <div class="card-body pb-0">
                            <div class="btn-group float-right">
                            <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-settings"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(31px, 23px, 0px);">
                            <a class="dropdown-item" href="{{ route('frontend.auth.product.search') }}?vendor_status_id=3">Ver Detalles</a>
                            </div>
                            </div>
                            <div class="text-value">{{$products_approved}}</div>
                            <div>Productos Aprobados</div>
                            </div>
                            <div class="chart-wrapper mt-3" style="height:70px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                            <canvas class="chart chartjs-render-monitor" id="card-chart3" height="140" width="516" style="display: block; height: 70px; width: 258px;"></canvas>
                            </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-warning">
                            <div class="card-body pb-0">
                            <div class="btn-group float-right">
                            <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-settings"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(31px, 23px, 0px);">
                            <a class="dropdown-item" href="{{ route('frontend.auth.product.search') }}?vendor_status_id=1">Ver Detalles</a>
                            </div>
                            </div>
                            <div class="text-value">{{$products_pending}}</div>
                            <div>Productos Pendiente de Revisión</div>
                            </div>
                            <div class="chart-wrapper mt-3" style="height:70px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                            <canvas class="chart chartjs-render-monitor" id="card-chart3" height="140" width="516" style="display: block; height: 70px; width: 258px;"></canvas>
                            </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-3">
                            <div class="card text-white bg-danger">
                            <div class="card-body pb-0">
                            <div class="btn-group float-right">
                            <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-settings"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ route('frontend.auth.product.search') }}?vendor_status_id=2">Ver Detalles</a>
                            </div>
                            </div>
                            <div class="text-value">{{$products_rejected}}</div>
                            <div>Productos Rechazados</div>
                            </div>
                            <div class="chart-wrapper mt-3 mx-3" style="height:70px;"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                            <canvas class="chart chartjs-render-monitor" id="card-chart4" height="140" width="452" style="display: block; height: 70px; width: 226px;"></canvas>
                            </div>
                            </div>
                        </div>

                        </div>

                </div> <!-- card-body -->
            </div><!-- card -->
        </div><!-- col -->
    </div><!-- row -->
@endsection

