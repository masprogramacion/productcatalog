@extends('frontend.layouts.app2')

@section('content')


<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.frontend.access.products.messages.title') }} <small class="text-muted">{{ __('labels.frontend.access.products.messages.messages_received') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ __('labels.frontend.access.products.messages.status') }}</th>
                            <th>{{ __('labels.frontend.access.products.messages.sender') }}</th>
                            <th>{{ __('labels.frontend.access.products.messages.product_name') }}</th>
                            <th>{{ __('labels.frontend.access.products.messages.subject') }}</th>
                            <th>{{ __('labels.frontend.access.products.messages.received_at') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($messages as $message)
                            <tr>
                                <td>{!!$message->status_icon!!}</td>
                                <td>{{$message->sender->full_name}}</td>
                                <td>{{$message->product->name}}</td>
                                <td><b>{{$message->subject}}</b></td>
                                <td>{!!$message->status_badge!!}</td>
                                <td>{!!$message->show_button!!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $messages->total() !!} {{ trans_choice('labels.frontend.access.products.table.total_messages', $messages->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $messages->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->


    

@endsection