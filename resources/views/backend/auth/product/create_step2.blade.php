@extends('backend.layouts.app')

@section('title', __('labels.backend.access.products.management') . ' | ' . __('labels.backend.access.products.create'))

@section('content')


<div class="container-fluid">
  <ul class="list-unstyled multi-steps">
    <li>PASO 1</li>
    <li class="is-active">PASO 2</li>
    <li class="is-active">PASO 3</li>

  </ul>
</div>

<div class="container">
    <div class="py-1 text-center">
    <h2>PASO 2 DE 3</h2>
    <!--<p class="lead">Agregar la(s) Presentacion(es) y Fotos del Producto.</p>-->
    </div>

  @include('includes.products.packaging')
  

</div><!--container-->



@endsection
@push('after-scripts')
<script> 
  //define counter
  var sectionsCount = {{ $sections_count }};
  $('[data-toggle="tooltip"]').tooltip();
</script>  
<script type="text/javascript" src="{{ URL::asset ('js/custom.js') }}"></script>
@endpush
