<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/dashboard')) }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon icon-speedometer"></i> @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>

            <li class="nav-title">
                @lang('menus.backend.sidebar.system')
            </li>


            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/product')) }}" href="{{ route('admin.auth.product.index') }}">
                    <i class="nav-icon icon-speedometer"></i>Inicio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/product')) }}" href="{{ route('admin.auth.product.index') }}">
                    <i class="nav-icon icon-speedometer"></i>Proveedores</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/product')) }}" href="{{ route('admin.auth.product.index') }}">
                    <i class="nav-icon icon-speedometer"></i>Productos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/product')) }}" href="{{ route('admin.auth.product.index') }}">
                    <i class="nav-icon icon-speedometer"></i>Sistema</a>
            </li>


            <li class="divider"></li>

        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
