{{ html()->modelForm($product, 'PATCH', route('admin.auth.product.update.details', $product))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-details-product','novalidate'=>'novalidate'])->open() }}

<div class="col">

  <div class="col-sm-12">
  <hr class="mb-4">
  <h4 class="mb-3">@lang('labels.general.vendor_information')</h4>
  <div class="row">
        <div class="col-sm-6 mb-3">
            {{ html()->label(__('labels.products.form.label.vendor_account_number'))->for('vendor_account_number')}}
            {{ html()->text('vendor_account_number', $sap_account_number)
                                    ->class('form-control')
                                    ->attribute('readonly', 'readonly')}}
        </div>
        <div class="col-sm-6 mb-3">
            <div class="custom-control custom-checkbox">
                              {{ html()->checkbox('ind_regular_vendor')
                              ->class('custom-control-input')
                              ->attributes(['name'=>'ind_regular_vendor','id'=>'ind_regular_vendor'])
                              }}
                              <label class="custom-control-label" label="label_ind_regular_vendor" for="ind_regular_vendor">@lang('labels.products.form.label.ind_regular_vendor')</label>
                              <small class="text-muted" id="text_muted_ind_regular_vendor"></small>
            </div>
        </div> 
  </div>
  <hr class="mb-4">
  <h4 class="mb-3">Ubicacion del producto</h4>
  <div class="row">
        <div class="col-sm-6 mb-3">
            @if($product->store)
                  {{ html()->label(__('labels.products.form.label.store_id'))->class('form-control-label') }}

                  @if(Auth::user()->hasRole('planimetria') && ($product->status_id == 5))
                        {{ html()->multiselect('store_id',$stores,$product->store->pluck('id')->toArray())
                                    ->class('select2m custom-select')
                                    ->attributes(['multiple'=>'multiple', 'name'=>'store_id[]'])
                                    ->required() }}
                                    
                        {{ html()->label(html()->checkbox('checkBoxAll', false, 1) . ' ' . __('labels.products.form.label.select_all'))->for('checkBoxAll') }}  
                  @else
                        @foreach ($product->store as $singleStore)
                        <span class="badge badge-warning"> {{ $singleStore->name }}</span>
                        @endforeach

                  @endif

         
                  <small class="text-muted">@lang('labels.products.form.helptext.store_id')</small>
                  <div class="invalid-feedback">
                  {{ $errors->first('store_id') }}
                  </div>
            @else
            <h5 class="mb-3">No ha sido asignado a ninguna sucursal</h4>
            @endif
        </div>
        <div class="col-sm-6 mb-3">

        </div> 
  </div>

  <hr class="mb-4">
  <h4 class="mb-3">Detalle del Material</h4>
  <div class="row">
    <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.material_type_id'))->class('form-control-label') }}
          {{ html()->select('material_type_id',$material_types)
                        ->class('custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.material_type_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('material_type_id') }}
          </div>
    </div>
    <!--
    <div class="form-group col-sm-3">
    
          {{ html()->label(__('labels.products.form.label.material_group_id'))->class('form-control-label') }}
          {{ html()->select('material_group_id',$material_groups)
                        ->class('select2 custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.material_group_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('material_group_id') }}
          </div>
     
    </div>
    -->
    <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.purchase_tax_code_id'))->class('form-control-label') }}
          {{ html()->select('purchase_tax_code_id',$purchase_tax_codes)
                        ->class('custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.purchase_tax_code_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('purchase_tax_code_id') }}
          </div>
    </div>
    <div class="form-group col-sm-3">
    
          {{ html()->label(__('labels.products.form.label.material_tax_type_id'))->class('form-control-label') }}
          {{ html()->select('material_tax_type_id',$material_tax_types)
                        ->class('custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.material_tax_type_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('material_tax_type_id') }}
          </div>
    </div>
  </div>

  <div class="row">
          <!--
          <div class="form-group col-sm-2">
          
            {{ html()->label(__('labels.products.form.label.sku'))->for('sku')}}
            {{ html()->text('sku')
                                    ->class('form-control')
                                    ->attribute('maxlength', 14)
                                    ->required()}}
            <small class="text-muted">@lang('labels.products.form.helptext.sku')</small>
            <div class="invalid-feedback">
            {{ $errors->first('sku') }}
            </div>
            
          </div>
          -->
        <div class="form-group col-sm-2">
          {{ html()->label(__('labels.products.form.label.price'))->for('price')}}
          {{ html()->input('number','price')->class('form-control')->attributes(['step'=>'.01'])->required() }}
          <small class="text-muted"></small>
          <div class="invalid-feedback">
          {{ $errors->first('price') }}
          </div>
        </div>
        <div class="form-group col-sm-2">
          {{ html()->label(__('labels.products.form.label.suggested_price'))->for('suggested_price')}}
          {{ html()->text('suggested_price')
                                    ->class('form-control')
                                    ->attribute('maxlength', 255)
                                    ->attribute('disabled','disabled')
                                    ->required()}}
          <small class="text-muted"></small>
          <div class="invalid-feedback">
          {{ $errors->first('suggested_price') }}
          </div>
        </div>

        <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.short_text_description'))->for('short_text_description')}}
          {{ html()->text('short_text_description')
                                    ->class('form-control')
                                    ->attribute('maxlength', 18)
                                    ->required()}}
          <small class="text-muted">@lang('labels.products.form.helptext.short_text_description')</small>
          <div class="invalid-feedback">
          {{ $errors->first('short_text_description') }}
          </div>
        </div>

        <div class="form-group col-sm-3">

          {{ html()->label(__('labels.products.form.label.order_price_measurement_unit_id'))->class('form-control-label') }}
           {{ html()->select('order_price_measurement_unit_id',$packaging_measurement_units)
                                    ->class('select2 custom-select d-block w-100')
                                    ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.order_price_measurement_unit_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('order_price_measurement_unit_id') }}
          </div>
          
        </div>


  </div>

  <hr class="mb-4">
  <h4 class="mb-3">Clasificacion</h4>
  <div class="row">
    <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.purchase_organization_id'))->class('form-control-label') }}
          {{ html()->select('purchase_organization_id',$purchase_organizations)
                        ->class('custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.purchase_organization_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('purchase_organization_id') }}
          </div>
    </div>
    <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.purchase_group_id'))->class('form-control-label') }}
          {{ html()->select('purchase_group_id',$purchase_groups)
                        ->class('custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.purchase_group_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('purchase_group_id') }}
          </div>
    </div>

    <div class="form-group col-sm-3">
          {{ html()->label(__('labels.products.form.label.iva_code_id'))->class('form-control-label') }}
          {{ html()->select('iva_code_id',$iva_codes)
                        ->class('custom-select d-block w-100')
                        ->placeholder('Seleccione una opción')
                        ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.iva_code_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('iva_code_id') }}
          </div>
    </div>

  </div>

  <div class="row">

    <div class="form-group col-sm-4">
          {{ html()->label(__('labels.products.form.label.department_id'))->class('form-control-label') }}
          {{ html()->select('department_id',$departments)
                        ->placeholder('Seleccione un Departamento')
                        ->class('custom-select d-block w-100')
                        ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.department_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('department_id') }}
          </div>
    </div>
    <div class="form-group col-sm-4">
          {{ html()->label(__('labels.products.form.label.category_id'))->class('form-control-label') }}
          {{ html()->select('category_id',$categories)
            ->placeholder('Seleccione una Categoria')
                        ->class('custom-select d-block w-100')
                        ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.category_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('category_id') }}
          </div>
    </div>
    <div class="form-group col-sm-4">
          {{ html()->label(__('labels.products.form.label.hierarchy_id'))->class('form-control-label') }}
          {{ html()->select('hierarchy_id',$hierarchies)
            ->placeholder('Seleccione una Jerarquia')
                        ->class('custom-select d-block w-100')
                        ->required() }}
          <small class="text-muted">@lang('labels.products.form.helptext.hierarchy_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('hierarchy_id') }}
          </div>
    </div>

  </div>

  <div class="row">

      <div class="form-group col-sm-4">
                      {{ html()->label(__('labels.products.form.label.available_deliverable_from'))->class('form-control-label')}}
                        {{ html()->input('text','available_deliverable_from')
                                        ->class('form-control')
                                        ->attributes(['placeholder'=>'Seleccione una fecha'])
                                        ->value($product->available_deliverable_from) }}
                        <div class="invalid-feedback">
                        @lang('validation.required', ['attribute' => __('labels.products.form.label.available_deliverable_from')]).
                        </div>
      </div>
      <div class="form-group col-sm-4">
                      {{ html()->label(__('labels.products.form.label.available_deliverable_until'))->class('form-control-label')}}
                        {{ html()->input('text','available_deliverable_until')
                                        ->class('form-control')
                                        ->attributes(['placeholder'=>'Seleccione una fecha'])
                                        ->value($product->available_deliverable_until) }}
                        <div class="invalid-feedback">
                        @lang('validation.required', ['attribute' => __('labels.products.form.label.available_deliverable_until')]).
                        </div>
        </div>


  </div>
  




</div>

    <div class="card-footer clearfix">
                <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.product.index'), __('buttons.general.goback')) }}
                </div><!--col-->

                <div class="col text-right">
                    <button class="btn btn-success btn-sm pull-right" id="btnSubmitDetails" type="button">@lang('buttons.general.crud.update')</button>
                </div><!--row-->
            </div><!--row-->
    </div><!--card-footer-->
</div><!--table-responsive-->
{{ html()->form()->close() }}