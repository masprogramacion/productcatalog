<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\Attribute\CatalogAttribute;

class Catalog extends Model
{
    use SoftDeletes,
    CatalogAttribute;
    

    protected $primaryKey = 'id';
    
    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'table_name'
    ];
    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
    


}
