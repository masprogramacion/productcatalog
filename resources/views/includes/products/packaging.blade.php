{{ html()->modelForm($product, 'PATCH', route($update_route, $product))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-packaging-product','novalidate'=>'novalidate'])->open() }}
<input type="hidden" name="base_measurement_unit_id" id="base_measurement_unit_id">
<div class="col">

<hr class="mb-4">
  <h4 class="mb-3">Embalaje</h4>
    <div class="col-sm-12">
    <div class="card bg-light mb-3" >
              <div class="card-header"><h3>@lang('labels.products.title.measurement_unit')</h3>
                <div class="row">
                <div class="col-sm-4">
                            <label class="form-control-label">Escoja una Configuración</label>
                            <select class="custom-select d-block w-100" name="product_material_preset_id" id="product_material_preset_id">
                            <option value="" selected disabled>Por favor escoja una opción</option>
                            <option value="0">Unidad</option>
                            <option value="1">Unidad/Display/Caja</option>
                            <option value="2">Unidad/Display</option>
                            <option value="3">Unidad/Caja</option>
                            <option value="4">Kg</option>
                            <option value="5">Kg/Caja</option>
                            <option value="6">Metro</option>
                            <option value="7">Metro/Rollo</option>
                            </select>
                            <small class="text-muted"></small>
                        <div class="invalid-feedback">
                        
                        </div>
                  </div>
                  <div class="col-sm-2"><button id="btn_reset" name="btn_reset" type="button" class="btn btn-sm btn-info"><i class="icon-reload"></i> Limpiar</button></div>
                  <div class="col-sm-6"></div>
                </div>
              </div>
              <div class="card-body"  id="sections">

              <div class="row" id="row_to_clone"><!--multiple fields row-->
                <div class="col-sm-12">
                  <div class="row"><!--1st row-->

                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.alt_measure_unit_stockkeep_unit_id'))->class('form-control-label') }}
                            {{ html()->select('alt_measure_unit_stockkeep_unit_id_1',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->required() }}

                            <small class="text-muted">@lang('labels.products.form.helptext.alt_measure_unit_stockkeep_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('alt_measure_unit_stockkeep_unit_id_1') }}
                            </div>
                    </div>
                    <div class="form-group col-sm-2">
                        {{ html()->label(__('labels.products.form.label.lower_level_measurement_unit_qty'))->class('form-control-label')}}
                        {{ html()->input('number','lower_level_measurement_unit_qty_1')->class('form-control')->attributes(['min'=>'1', 'max'=>'1000000'])->required() }}
                        <small class="text-muted">@lang('labels.products.form.helptext.lower_level_measurement_unit_qty')</small>
                          <div class="invalid-feedback">
                          {{ $errors->first('lower_level_measurement_unit_qty_1') }}
                          </div>
                    </div>
                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.lower_level_pack_hierarchy_measurement_unit_id'))->class('form-control-label') }}
                            {{ html()->select('lower_level_pack_hierarchy_measurement_unit_id_1',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->required() }}

                            <small class="text-muted">@lang('labels.products.form.helptext.lower_level_pack_hierarchy_measurement_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('lower_level_pack_measurement_unit_id') }}
                            </div>
                    </div>

                    <div class="form-group col-sm-3">
                          {{ html()->label(__('labels.products.form.label.gtin_1'))->class('form-control-label')}}
                          {{ html()->text('gtin_1')->class('form-control')
                          ->attribute('maxlength', 13)
                          ->required() }}
                          <small class="text-muted">@lang('labels.products.form.helptext.gtin_1')</small>
                          <div class="invalid-feedback">
                              @lang('validation.required', ['attribute' => __('labels.products.form.label.gti_1')]).
                      </div>
                    </div>
                    <div class="col-sm-1">
                      <div class="hdnButton d-none">
                        <button class="btn btn-brand btn-danger" type="button" style="margin-bottom: 4px" data-toggle="tooltip" data-placement="top" title="Presione - para eliminar esta presentacion"><i class="fa fa-minus"></i></button>
                      </div>
                    </div>

                  </div><!--1st row ends-->
                  @if($product->lower_level_pack_hierarchy_measurement_unit_id_2 == null)
                  <div class="row" id='ind_measurement_unit_is_order_unit_1_div'><!--2nd row-->
                    <div class="col-sm-4">
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_order_unit_1')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_order_unit_1','id'=>'ind_measurement_unit_is_order_unit_1','value'=>'1'])
                            }}
                            <label class="custom-control-label" id="label_ind_measurement_unit_is_order_unit_1" for="ind_measurement_unit_is_order_unit_1">@lang('labels.products.form.label.ind_measurement_unit_is_order_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_order_unit_1"></small>
                        </div>
                    </div>

                    <div class="col-sm-4">    
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_delivery_or_issue_unit_1')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_delivery_or_issue_unit_1','id'=>'ind_measurement_unit_is_delivery_or_issue_unit_1','value'=>'1'])
                            }}
                            <label class="custom-control-label" id="label_measurement_unit_is_delivery_or_issue_unit_1" for="ind_measurement_unit_is_delivery_or_issue_unit_1">@lang('labels.products.form.label.ind_measurement_unit_is_delivery_or_issue_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_delivery_or_issue_unit_1"></small>
                        </div>
                    </div>

                  </div><!--2nd row ends-->
                  @endif
                  <div class="row">
                    <div class="col-sm-12">
                      <hr class="mb-4">                    
                    </div>
                  </div>
                </div>    
              </div><!--multiple fields row-->

              @if($product->lower_level_pack_hierarchy_measurement_unit_id_2 !== null)
              <div class="row" id="row_id_2"><!--row id -->
                <div class="col-sm-12">
                  <div class="row"><!--1st row-->

                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.alt_measure_unit_stockkeep_unit_id'))->class('form-control-label') }}
                            {{ html()->select('alt_measure_unit_stockkeep_unit_id_2',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->required() }}

                            <small class="text-muted">@lang('labels.products.form.helptext.alt_measure_unit_stockkeep_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('alt_measure_unit_stockkeep_unit_id_2') }}
                            </div>
                    </div>
                    <div class="form-group col-sm-2">
                        {{ html()->label(__('labels.products.form.label.lower_level_measurement_unit_qty'))->class('form-control-label')}}
                        {{ html()->input('number','lower_level_measurement_unit_qty_2')->class('form-control')->attributes(['min'=>'1', 'max'=>'1000000'])->required() }}
                        <small class="text-muted">@lang('labels.products.form.helptext.lower_level_measurement_unit_qty')</small>
                          <div class="invalid-feedback">
                          {{ $errors->first('lower_level_measurement_unit_qty_2') }}
                          </div>
                    </div>
                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.lower_level_pack_hierarchy_measurement_unit_id'))->class('form-control-label') }}
                            {{ html()->select('lower_level_pack_hierarchy_measurement_unit_id_2',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->required() }}

                            <small class="text-muted">@lang('labels.products.form.helptext.lower_level_pack_hierarchy_measurement_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('lower_level_pack_measurement_unit_id') }}
                            </div>
                    </div>

                    <div class="form-group col-sm-3">
                          {{ html()->label(__('labels.products.form.label.gtin_2'))->class('form-control-label')}}
                          {{ html()->text('gtin_2')->class('form-control')
                          ->attribute('maxlength', 13)
                           }}
                          <small class="text-muted">@lang('labels.products.form.helptext.gtin_2')</small>
                          <div class="invalid-feedback">
                              @lang('validation.required', ['attribute' => __('labels.products.form.label.gtin_2')]).
                      </div>
                    </div>
                    <div class="col-sm-1">
                      <div class="hdnButton d-none">
                        <button class="btn btn-brand btn-danger" type="button" style="margin-bottom: 4px" data-toggle="tooltip" data-placement="top" title="Presione - para eliminar esta presentacion" onclick="return removeSection()"><i class="fa fa-minus"></i></button>
                      </div>
                    </div>

                  </div><!--1st row ends-->
                  @if($product->lower_level_pack_hierarchy_measurement_unit_id_3 == null)
                  <div class="row"><!--2nd row-->
                  
                    <div class="col-sm-4">
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_order_unit_2')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_order_unit_2','id'=>'ind_measurement_unit_is_order_unit_2','value'=>'1'])
                            }}
                            <label class="custom-control-label" id="label_ind_measurement_unit_is_order_unit_2" for="ind_measurement_unit_is_order_unit_2">@lang('labels.products.form.label.ind_measurement_unit_is_order_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_order_unit_2"></small>
                        </div>
                    </div>
                    <div class="col-sm-4">    
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_delivery_or_issue_unit_2')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_delivery_or_issue_unit_2','id'=>'ind_measurement_unit_is_delivery_or_issue_unit_2','value'=>'1'])
                            }}
                            <label class="custom-control-label" id="label_measurement_unit_is_delivery_or_issue_unit_2" for="ind_measurement_unit_is_delivery_or_issue_unit_2">@lang('labels.products.form.label.ind_measurement_unit_is_delivery_or_issue_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_delivery_or_issue_unit_2"></small>
                        </div>
                    </div>
                    
                  </div><!--2nd row ends-->
                  @endif
                  <div class="row">
                    <div class="col-sm-12">
                      <hr class="mb-4">                    
                    </div>
                  </div>
                </div>    
              </div><!--row id -->
              @endif

              @if($product->lower_level_pack_hierarchy_measurement_unit_id_3 !== null)
              <div class="row" id="row_id_3"><!-- row id-->
                <div class="col-sm-12">
                  <div class="row"><!--1st row-->

                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.alt_measure_unit_stockkeep_unit_id'))->class('form-control-label') }}
                            {{ html()->select('alt_measure_unit_stockkeep_unit_id_3',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->required() }}

                            <small class="text-muted">@lang('labels.products.form.helptext.alt_measure_unit_stockkeep_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('alt_measure_unit_stockkeep_unit_id_3') }}
                            </div>
                    </div>
                    <div class="form-group col-sm-2">
                        {{ html()->label(__('labels.products.form.label.lower_level_measurement_unit_qty'))->class('form-control-label')}}
                        {{ html()->input('number','lower_level_measurement_unit_qty_3')->class('form-control')->attributes(['min'=>'1', 'max'=>'1000000'])->required() }}
                        <small class="text-muted">@lang('labels.products.form.helptext.lower_level_measurement_unit_qty')</small>
                          <div class="invalid-feedback">
                          {{ $errors->first('lower_level_measurement_unit_qty_3') }}
                          </div>
                    </div>
                    <div class="col-sm-3">
                            {{ html()->label(__('labels.products.form.label.lower_level_pack_hierarchy_measurement_unit_id'))->class('form-control-label') }}
                            {{ html()->select('lower_level_pack_hierarchy_measurement_unit_id_3',$packaging_measurement_units)
                                          ->class('select2 custom-select d-block w-100')
                                          ->required() }}

                            <small class="text-muted">@lang('labels.products.form.helptext.lower_level_pack_hierarchy_measurement_unit_id')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('lower_level_pack_measurement_unit_id_3') }}
                            </div>
                    </div>

                    <div class="form-group col-sm-3">
                          {{ html()->label(__('labels.products.form.label.gtin_3'))->class('form-control-label')}}
                          {{ html()->text('gtin_3')->class('form-control')
                          ->attribute('maxlength', 13)
                           }}
                          <small class="text-muted">@lang('labels.products.form.helptext.gtin_3')</small>
                          <div class="invalid-feedback">
                              @lang('validation.required', ['attribute' => __('labels.products.form.label.gtin_3')]).
                      </div>
                    </div>
                    <div class="col-sm-1">
                      <div class="hdnButton d-none">
                        <button class="btn btn-brand btn-danger" type="button" style="margin-bottom: 4px" data-toggle="tooltip" data-placement="top" title="Presione - para eliminar esta presentacion" onclick="return removeSection()"><i class="fa fa-minus"></i></button>
                      </div>
                    </div>

                  </div><!--1st row ends-->
                  <div class="row"><!--2nd row-->
                  
                    <div class="col-sm-4">
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_order_unit_3')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_order_unit_3','id'=>'ind_measurement_unit_is_order_unit_3','value'=>'1'])
                            }}
                            <label class="custom-control-label" id="label_ind_measurement_unit_is_order_unit_3" for="ind_measurement_unit_is_order_unit_3">@lang('labels.products.form.label.ind_measurement_unit_is_order_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_order_unit_3"></small>
                        </div>
                    </div>
                    <div class="col-sm-4">    
                        <div class="custom-control custom-checkbox">
                            {{ html()->checkbox('ind_measurement_unit_is_delivery_or_issue_unit_3')
                            ->class('custom-control-input')
                            ->attributes(['name'=>'ind_measurement_unit_is_delivery_or_issue_unit_3','id'=>'ind_measurement_unit_is_delivery_or_issue_unit_3','value'=>'1'])
                            }}
                            <label class="custom-control-label" id="label_measurement_unit_is_delivery_or_issue_unit_3" for="ind_measurement_unit_is_delivery_or_issue_unit_3">@lang('labels.products.form.label.ind_measurement_unit_is_delivery_or_issue_unit')</label>
                            <small class="text-muted" id="text_muted_ind_measurement_unit_is_delivery_or_issue_unit_3"></small>
                        </div>
                    </div>
                  
                  </div><!--2nd row ends-->
                  <div class="row">
                    <div class="col-sm-12">
                      <hr class="mb-4">                    
                    </div>
                  </div>
                </div>    
              </div><!--row id-->
              @endif



              <div class="row mb-3">
                <div class="col-sm-12">
                    <div class="row"><!--1st row-->
                      <div class="col-xs-1 ml-lg-2">
                          {{ html()->label(__('labels.products.form.label.dimensions'))->class('col-form-label')}}
                      </div>
                      <div class="col-xs-2">
                          {{ html()->input('number','length')->class('form-control')->attributes(['min'=>'1', 'max'=>'1000000'])->placeholder(__('labels.products.form.label.length')) }}
                          <small class="text-muted">@lang('labels.products.form.helptext.length')</small>
                      </div>
                      <div class="col-xs-3">
                      {{ html()->input('number','width')->class('form-control')->attributes(['min'=>'1', 'max'=>'1000000'])->placeholder(__('labels.products.form.label.width')) }}
                      <small class="text-muted">@lang('labels.products.form.helptext.width')</small>
                      </div>
                      <div class="col-xs-4">
                      {{ html()->input('number','height')->class('form-control')->attributes(['min'=>'1', 'max'=>'1000000'])->placeholder(__('labels.products.form.label.height')) }}
                      <small class="text-muted">@lang('labels.products.form.helptext.height')</small>

                      </div>
                      <div class="col-xs-1">
                          <label for="exampleInputName2" class="col-form-label">(cm)</label>
                      </div>
                    </div>
                </div>
              </div>


              <div class="row mb-3"><!--weight /volume-->
                <div class="col-sm-12">
                  <div class="row"><!--1st row-->
                    <div class="col-sm-4">
                          {{ html()->label(__('labels.products.form.label.weight'))->class('form-control-label')}}
                          {{ html()->input('number','weight')->class('form-control')->attributes(['min'=>'1', 'max'=>'1000000']) }}
                          <small class="text-muted">@lang('labels.products.form.helptext.weight')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('weight') }}
                            </div>
                    </div>
                    <div class="col-sm-4">
                          {{ html()->label(__('labels.products.form.label.volume'))->class('form-control-label')}}
                          {{ html()->input('number','volume')->class('form-control')->attributes(['min'=>'1', 'max'=>'1000000']) }}
                          <small class="text-muted">@lang('labels.products.form.helptext.volume')</small>
                            <div class="invalid-feedback">
                            {{ $errors->first('volume') }}
                            </div>
                    </div>
                    <div class="col-sm-4">
  
                    </div>
                  </div>
                </div>
              </div>

              </div><!--card body ends-->
              <div class="card-footer clearfix">
                <div class="row">

                @if(strpos($update_route,'packaging') !== false)
                <div class="col">
                    {{ form_cancel(route('frontend.auth.product.index'), __('buttons.general.goback')) }}
                </div><!--col-->

                <div class="col text-right">
                    <button class="btn btn-success btn-sm pull-right" id="btnSubmitPackaging" type="button">@lang('buttons.general.crud.update')</button>
                </div><!--col-->
                @endif
                @if(strpos($update_route,'step2') !== false)
                    <button class="btn btn-primary btn-lg btn-block" id="btnSubmitPackaging" type="button">Actualizar el producto y seguir al siguiente paso</button>
                @endif

            </div><!--row-->
    </div><!--card-footer-->
            </div><!--card ends-->
    </div><!--col-sm-12-->
    


</div><!--col-->
{{ html()->closeModelForm() }}

<hr class="mb-4">

