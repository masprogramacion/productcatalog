<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\User\DashboardController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.user'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires','role:proveedor|administrador']], function () {

    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', [AccountController::class, 'index'])->name('account');

        /*
         * User Information Specific
         */
        Route::patch('info/update/general', [ProfileController::class, 'updateGeneral'])->name('info.update.general');
        Route::patch('info/update/profile', [ProfileController::class, 'updateProfile'])->name('info.update.profile');
        Route::patch('info/update/bank_account', [ProfileController::class, 'updateBankAccount'])->name('info.update.bank_account');
        Route::get('info/documents/download/{uuid}', [ProfileController::class, 'download'])->name('info.documents.download');


    });
});
