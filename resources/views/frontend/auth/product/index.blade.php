@extends('frontend.layouts.app2')

@section('content')

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<i class="fa fa-search"></i> @lang('labels.frontend.access.products.form.search.title')
<div class="card-header-actions">
<a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true">
<i class="fas fa-angle-down rotate-icon"></i>
</a>
</div>
</div>

<div class="card-body collapse show" id="collapseExample">

       
{{ html()->form('POST', route('frontend.auth.product.search'))->class('form-horizontal')->open() }}
   <div class="form-row">
    <div class="col-md-4 mb-3">
      {{ html()->label(__('labels.products.form.search.product_name'))->class('form-control-label')->for('product_name') }}
      {{ html()->text('product_name')
               ->class('form-control')
               ->attribute('maxlength', 100) }}
    </div>
    <div class="col-md-4 mb-3">
      {{ html()->label(__('labels.products.form.search.status'))->class('form-control-label')}}
      {{ html()->select('vendor_status_id',$product_statuses)
                ->placeholder('Seleccione un Estatus')
                ->class('form-control')}}
    </div>
    <div class="col-md-4 mb-3">
    </div>
  </div>


  <div class="form-row" >

    <div class="col-md-6 mb-3">
    {{ html()->label(__('labels.products.form.search.start_date'))->class('form-control-label')}}
        <input name="start_date" id="start_date"  placeholder="Seleccione una fecha"  />
    </div>
    <div class="col-md-6 mb-3">
    {{ html()->label(__('labels.products.form.search.end_date'))->class('form-control-label')}}
        <input name="end_date" id="end_date"  placeholder="Seleccione una fecha" />
    </div>

 </div>

  <button class="btn btn-primary" type="submit">Buscar</button>
  <button class="btn btn-secondary" type="button"  id="resetButton">Limpiar</button>
  {{ html()->form()->close() }}
        
        

</div>
</div>
</div>

</div>



<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.frontend.access.products.management') }} <small class="text-muted">{{ __('labels.frontend.access.products.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('frontend.auth.product.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('labels.frontend.access.products.table.name')</th>
                            <th>@lang('labels.general.status')</th>
                            <th>@lang('labels.general.description')</th>
                            <th>@lang('labels.general.creation_date')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>{!! $product->status_label !!}</td>
                                <td>{{ $product->description }}</td>
                                <td>{{ $product->created_at }}</td>
                                <td>{!! $product->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $products->total() !!} {{ trans_choice('labels.frontend.access.products.table.total', $products->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                {!! $products->appends(Request::capture()->except('page'))->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->

@endsection
@push('after-scripts')
    



<script>

$(document).ready(function(){

    $('#start_date').datepicker({
    format: '{{ config('app.date_format_js') }}' ,
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fortawesome',
    maxDate: function () {
        return $('#end_date').val();
    }
    });
            
    $('#end_date').datepicker({
    format: '{{ config('app.date_format_js') }}',
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fortawesome',
    minDate: function () {
        return $('#start_date').val();
    } 
    });

    $('#resetButton').on('click', function() {
        $(this).closest('form').get(0).reset();
        $(this).closest('form').find("input[type=text], textarea").val("");
    });


});



</script>

    
@endpush