<?php

namespace App\Repositories\Backend\Auth;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\Auth\Product;
use App\Models\Auth\Brand;
use App\Models\Auth\User;
use App\Models\Auth\Message;
use App\Models\Auth\WorkflowStatus;
use App\Models\Catalog\ProductStatus;
use App\Models\Catalog\Department;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Exceptions\ServiceException;
use App\Repositories\BaseRepository;
use App\Events\Backend\Auth\Product\ProductCreated;
use App\Events\Backend\Auth\Product\ProductUpdated;
use Illuminate\Support\Facades\Log;
use App\ProductSearch\ProductSearch;
use Illuminate\Http\Request;
use App\Notifications\Backend\Auth\ProductStatusUpdated;
use App\Notifications\Backend\Auth\ProductStatusApproved;
use App\Notifications\Backend\Auth\ProductStatusRejected;
use App\Events\Backend\Auth\Product\ProductRestored;
 //TODO  check if this events are needed to be tracked
 //use App\Events\Backend\Auth\User\UserConfirmed;
// use App\Events\Backend\Auth\User\UserDeactivated;
// use App\Events\Backend\Auth\User\UserReactivated;
// use App\Events\Backend\Auth\User\UserUnconfirmed;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Collection;

use GuzzleHttp\Exception\RequestException;

/**
 * Class ProductRepository.
 */
class ProductRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Product::class;
    }


     /**
     * @param array $data
     *
     * @return Product
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Product
    {
        

        return DB::transaction(function () use ($data) {

          $ind_include_discount = null;
          if (array_key_exists('ind_include_discount', $data)) {
            $ind_include_discount =  $data['ind_include_discount'];
          }
          $discount_percentage = null;
          if (array_key_exists('discount_percentage', $data)) {
            $discount_percentage =  $data['discount_percentage'];
          }
          $ind_limited_time_discount = null;
          if (array_key_exists('ind_limited_time_discount', $data)) {
            $ind_limited_time_discount =  $data['ind_limited_time_discount'];
          }
          $discount_start_date = null;
          if (array_key_exists('discount_start_date', $data)) {
            $discount_start_date =  $data['discount_start_date'];
          }
          $discount_end_date = null;
          if (array_key_exists('discount_end_date', $data)) {
            $discount_end_date =  $data['discount_end_date'];
          }

            $product = parent::create([
                'department_id' => $data['department_id'],
                'name' => $data['product_name'],
                'price' => $data['price'],
                'suggested_price' => $data['suggested_price'],
                'description' => $data['description'],
                'brand_id' => $data['brand_id'],
                'manufacturer_id' => $data['manufacturer_id'],
                'min_remain_shelf_life' => $data['min_remain_shelf_life'],
                'ind_include_discount' => $ind_include_discount,
                'discount_percentage' => $discount_percentage,
                'ind_limited_time_discount' => $ind_limited_time_discount,
                'discount_start_date' => $discount_start_date,
                'discount_end_date' => $discount_end_date,
                'old_material_number' => $data['old_material_number'],
                'vendor_material_number' => $data['vendor_material_number'],
                //'material_tax_type_id' => $data['material_tax_type_id'],
                'purchase_tax_code_id' => $data['purchase_tax_code_id'],
                'iva_code_id' => $data['iva_code_id'],
                'status_id'=> 1,
                'user_id' => Auth::id(),
            ]);

            if ($product) {
                //Log::debug('product created');
                // TODO GET PRODUCT STATUS FROM STATUS CATALOG


                //TODO create language entry
                //$product->setStatus('creado', 'estatus de creacion');

                //$user = $this->getById($id);

                $status = WorkflowStatus::Create(
                    [
                        'comments' => 'Producto Creado',
                        'current_status_description'=> 'Producto Creado',
                        'product_id' => $product->id,
                        'user_id' => Auth::id(),
                        'status_id' => 1
                    ]
                );
        

                event(new ProductCreated($product));

                return $product;
            }else{
              Log::error('error in product creation');
            }

            throw new GeneralException(__('exceptions.backend.access.users.create_error'));  //TODO anadir key y traducir
        });
    }


     /**
     * @param Product  $product
     * @param array $data
     * @param bool|UploadedFile  $image
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Product $product, array $data, $image=false) : Product
    {

      //dd($image);
      // Upload profile image if necessary
      if ($image) {
        $time_str = "_".time();
        $imageName = $product->id.$time_str.'.'.$image->getClientOriginalExtension();
        $imageThumbName = $product->id.$time_str.'_thmb.'.$image->getClientOriginalExtension();
        
        if($image->move(public_path('storage/products'), $imageName)){
          $product->image_original = 'products/' .$imageName;
          $product->image_thumb = 'products/'.$imageThumbName;

        }

      } 


        return DB::transaction(function () use ($product, $data) {

          $available_deliverable_from = null;
          $available_deliverable_until = null;

          if (array_key_exists('available_deliverable_from', $data)) {
            $available_deliverable_from =  $data['available_deliverable_from'];
          }

          if (array_key_exists('available_deliverable_until', $data)) {
            $available_deliverable_until =  $data['available_deliverable_until'];
          }

            if ($product->update([
              'name' => $data['product_name'],
              'description' => $data['description'],
              'gtin_1' => $data['gtin_1'],
              'lower_level_measurement_unit_qty_1' => $data['lower_level_measurement_unit_qty_1'],
              'lower_level_pack_hierarchy_measurement_unit_id_1' => $data['lower_level_pack_hierarchy_measurement_unit_id_1'],
              'ind_measurement_unit_is_order_unit_1' => $data['ind_measurement_unit_is_order_unit_1'],
              'gtin_2' => $data['gtin_2'],
              'lower_level_measurement_unit_qty_2' => $data['lower_level_measurement_unit_qty_2'],
              'lower_level_pack_hierarchy_measurement_unit_id_2' => $data['lower_level_pack_hierarchy_measurement_unit_id_2'],
              'gtin_3' => $data['gtin_3'],
              'lower_level_measurement_unit_qty_3' => $data['lower_level_measurement_unit_qty_3'],
              'lower_level_pack_hierarchy_measurement_unit_id_2' => $data['lower_level_pack_hierarchy_measurement_unit_id_3'],
              'min_remain_shelf_life' => $data['min_remain_shelf_life'],
              'old_material_number' => $data['old_material_number'],
              'vendor_material_number' => $data['vendor_material_number'],
              'price' => $data['price'],
              'order_price_measurement_unit_id' => $data['order_price_measurement_unit_id'],
              'brand_id' => $data['brand_id'],
              //'material_group_id' => $data['material_group_id'],
              //'material_tax_type_id' => $data['material_tax_type_id'],
              'available_deliverable_from' => $available_deliverable_from,
              'available_deliverable_until' => $available_deliverable_until ,
              'status_id' => $data['status_id']

            ])) {
              
              
              
                event(new ProductUpdated($product));
                return $product;

            }

            throw new GeneralException(__('exceptions.backend.access.users.update_error')); //TODO anadir key y traducir
        }); 
    }


 /**
     * @param Product  $product
     * @param array $data
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_general(Product $product, array $data) : Product
    {


        return DB::transaction(function () use ($product, $data) {

          $ind_regular_vendor = null;
          $ind_include_discount = null;
          $discount_percentage = null;
          $ind_limited_time_discount = null;
          $discount_start_date = null;
          $discount_end_date = null;

          if (array_key_exists('ind_regular_vendor', $data)) {
            $ind_regular_vendor =  $data['ind_regular_vendor'];
          }

          if (array_key_exists('ind_include_discount', $data)) {
            $ind_include_discount =  $data['ind_include_discount'];

            
            if (array_key_exists('discount_percentage', $data)) {
              $discount_percentage =  $data['discount_percentage'];
            }


          
            if (array_key_exists('ind_limited_time_discount', $data)) {
              $ind_limited_time_discount =  $data['ind_limited_time_discount'];


            
              if (array_key_exists('discount_start_date', $data)) {
                $discount_start_date =  $data['discount_start_date'];
              }
              
              if (array_key_exists('discount_end_date', $data)) {
                $discount_end_date =  $data['discount_end_date'];
              }

            }

          }
          

            if ($product->update([
              'name' => $data['name'],
              'description' => $data['description'],
              'price' => $data['price'],
              'suggested_price' => $data['suggested_price'],
              'min_remain_shelf_life' => $data['min_remain_shelf_life'],
              //'vendor_account_number_id' => $data['vendor_account_number_id'],
              'brand_id' => $data['brand_id'],
              'manufacturer_id' => $data['manufacturer_id'],
              'ind_include_discount' => $ind_include_discount,
              'discount_percentage' => $discount_percentage,
              'ind_limited_time_discount' => $ind_limited_time_discount,
              'discount_start_date' => $discount_start_date,
              'discount_end_date' => $discount_end_date,
              'old_material_number' => $data['old_material_number'],
              'vendor_material_number' => $data['vendor_material_number'],

            ])) {
              
                event(new ProductUpdated($product));
                return $product;

            }

            throw new GeneralException(__('exceptions.backend.access.products.update_error')); //TODO anadir key y traducir
        }); 
    }



         /**
     * @param Product  $product
     * @param array $data
     * @param bool|UploadedFile  $image_front_1
     * @param bool|UploadedFile  $image_back_1
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function store_uncatalogued(array $data,
      $image_front_1=false,
      $image_back_1=false
    ) : Product
    {



      return DB::transaction(function () use ($data,$image_front_1,$image_back_1) {

          $product = parent::create([
            'name' => $data['product_name'],
            'description' => $data['description'],
            'brand_id' => $data['brand_id'],
            'department_id' => $data['department_id'],
            'store_id' => $data['store_id'],
            'gtin_1' => $data['gtin_1'],
            'status_id'=> 1,
            'uncatalogued'=> 1,
            'user_id' => Auth::id()
          ]);



          if($product) {
                event(new ProductCreated($product));

                // Upload image(s) if necessary

                if ($image_front_1) {
                  $time_str = "_".time(); 
                  $imageName = $product->id.$time_str.'_front.'.$image_front_1->getClientOriginalExtension();
                  //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_front_$i"}->getClientOriginalExtension();
                  
                  if($image_front_1->move(public_path('storage/products'), $imageName)){
                    $product->image_front_1 = 'products/' .$imageName;
                  }
                }
              
                if ($image_back_1) {
                  $time_str = "_".time(); 
                  $imageName = $product->id.$time_str.'_back.'.$image_back_1->getClientOriginalExtension();
                  //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_front_$i"}->getClientOriginalExtension();
                  
                  if($image_back_1->move(public_path('storage/products'), $imageName)){
                    $product->image_back_1 = 'products/' .$imageName;
                  }
                }
                $product->update();


            return $product;
          }else {
            Log::error('error in product creation');
            throw new GeneralException(__('exceptions.backend.access.products.create_error'));
          }

          throw new GeneralException(__('exceptions.backend.access.users.create_error'));  //TODO anadir key y traducir
      });




    }



    /**
     * @param Product  $product
     * @param array $data
     * @param bool|UploadedFile  $image_front_1
     * @param bool|UploadedFile  $image_back_1
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_uncatalogued(Product $product, array $data,
      $image_front_1=false,
      $image_back_1=false
    ) : Product
    {


      $fn_update = function() use ($product,$data,$image_front_1,$image_back_1)
        {
 
            try
            {

                $product->name = $data['product_name'];
                $product->description = $data['description'];
                $product->brand_id = $data['brand_id'];
                $product->store_id = $data['store_id'];
                $product->gtin_1 = $data['gtin_1'];
                $product->department_id = $data['department_id'];

                //$product->status_id = $data['status_id'];


                if($product->update()) {

                  event(new ProductUpdated($product));

                      // Upload image(s) if necessary

                      if ($image_front_1) {
                        $time_str = "_".time(); 
                        $imageName = $product->id.$time_str.'_front.'.$image_front_1->getClientOriginalExtension();
                        //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_front_$i"}->getClientOriginalExtension();
                        
                        if($image_front_1->move(public_path('storage/products'), $imageName)){
                          $product->image_front_1 = 'products/' .$imageName;
                        }
                      }
                    
                      if ($image_back_1) {
                        $time_str = "_".time(); 
                        $imageName = $product->id.$time_str.'_back.'.$image_back_1->getClientOriginalExtension();
                        //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_front_$i"}->getClientOriginalExtension();
                        
                        if($image_back_1->move(public_path('storage/products'), $imageName)){
                          $product->image_back_1 = 'products/' .$imageName;
                        }
                      }
    

                  return $product;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.create_error'));
                }

            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.create_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }






     /**
     * @param Product  $product
     * @param array $data
     * @param bool|UploadedFile  $image_front_1
     * @param bool|UploadedFile  $image_back_1
     * @param bool|UploadedFile  $image_front_2
     * @param bool|UploadedFile  $image_back_2
     * @param bool|UploadedFile  $image_front_3
     * @param bool|UploadedFile  $image_back_3
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_step2(Product $product, array $data,
      $image_front_1=false,
      $image_back_1=false,
      $image_front_2=false,
      $image_back_2=false,
      $image_front_3=false,
      $image_back_3=false
    ) : Product
    {


      // 'image_front_1'
      // 'image_back_1'
      // 'image_front_2'
      // 'image_back_2'
      // 'image_front_3'
      // 'image_back_3'

      for($i = 1; $i <= 3; $i++) {
      // Upload image(s) if necessary

        if (${"image_front_$i"}) {
          $time_str = "_".time(); 
          $imageName = $product->id.$time_str.'_front.'.${"image_front_$i"}->getClientOriginalExtension();
          //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_front_$i"}->getClientOriginalExtension();
          
          if(${"image_front_$i"}->move(public_path('storage/products'), $imageName)){
            $product->{"image_front_$i"} = 'products/' .$imageName;
          }
        }
      }

      for($i = 1; $i <= 3; $i++) {
        // Upload image(s) if necessary

          if (${"image_back_$i"}) {
            $time_str = "_".time(); 
            $imageName = $product->id.$time_str.'_back.'.${"image_back_$i"}->getClientOriginalExtension();
            //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_back_$i"}->getClientOriginalExtension();
            
            if(${"image_back_$i"}->move(public_path('storage/products'), $imageName)){
              $product->{"image_back_$i"} = 'products/' .$imageName;
            }
          }
      }


      $fn_update = function() use ($product,$data)
        {
 
            try
            {

                $product->base_measurement_unit_id =  $data['base_measurement_unit_id'];  
                $product->gtin_1 =  $data['gtin_1'];
                $arr_gtin_type = get_gtin_type($data['gtin_1']);
                $product->gtin_type_id_1 = $arr_gtin_type['id'];


                //$product->lower_level_measurement_unit_qty_1 =  $data['lower_level_measurement_unit_qty_1'];
                $product->lower_level_pack_hierarchy_measurement_unit_id_1 =  $data['lower_level_pack_hierarchy_measurement_unit_id_1'];
                
                if (array_key_exists('ind_measurement_unit_is_order_unit_1', $data)) {
                  $product->ind_measurement_unit_is_order_unit_1 =  $data['ind_measurement_unit_is_order_unit_1'];
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_1 = $data['ind_measurement_unit_is_order_unit_1'];
                }
                
                //if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_1', $data)) {
                  //$product->ind_measurement_unit_is_delivery_or_issue_unit_1 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_1'];
                //}
                
                $product->alt_measure_unit_stockkeep_unit_id_1 =  $data['alt_measure_unit_stockkeep_unit_id_1'];

                if (array_key_exists('gtin_2', $data)) {
                  $product->gtin_2 =  $data['gtin_2'];
                  $arr_gtin_type = get_gtin_type($data['gtin_2']);
                  $product->gtin_type_id_2 = $arr_gtin_type['id'];
                }
                if (array_key_exists('lower_level_measurement_unit_qty_2', $data)) {
                  $product->lower_level_measurement_unit_qty_2 =  $data['lower_level_measurement_unit_qty_2'];
                }else {
                  $product->lower_level_measurement_unit_qty_2 =  null;
                }


                if (array_key_exists('lower_level_pack_hierarchy_measurement_unit_id_2', $data)) {
                  $product->lower_level_pack_hierarchy_measurement_unit_id_2 =  $data['lower_level_pack_hierarchy_measurement_unit_id_2'];
                }else {
                  $product->lower_level_pack_hierarchy_measurement_unit_id_2 =  null;
                }



                if (array_key_exists('ind_measurement_unit_is_order_unit_2', $data)) {
                  $product->ind_measurement_unit_is_order_unit_2 =  $data['ind_measurement_unit_is_order_unit_2'];
                }else {
                  $product->ind_measurement_unit_is_order_unit_2 =  null;
                }
                if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_2', $data)) {
                 $product->ind_measurement_unit_is_delivery_or_issue_unit_2 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_2'];
                }else {
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_2 =  null;
                }

                if (array_key_exists('alt_measure_unit_stockkeep_unit_id_2', $data)) {
                  $product->alt_measure_unit_stockkeep_unit_id_2 =  $data['alt_measure_unit_stockkeep_unit_id_2'];
                }else {
                  $product->alt_measure_unit_stockkeep_unit_id_2 =  null;
                }
                

                if (array_key_exists('gtin_3', $data)) {
                  $product->gtin_3 =  $data['gtin_3'];
                  $arr_gtin_type = get_gtin_type($data['gtin_3']);
                  $product->gtin_type_id_3 = $arr_gtin_type['id'];
                }
                if (array_key_exists('lower_level_measurement_unit_qty_3', $data)) {
                  $product->lower_level_measurement_unit_qty_3 =  $data['lower_level_measurement_unit_qty_3'];
                }else {
                  $product->lower_level_measurement_unit_qty_3 =  null;
                }


                if (array_key_exists('lower_level_pack_hierarchy_measurement_unit_id_3', $data)) {
                  $product->lower_level_pack_hierarchy_measurement_unit_id_3 =  $data['lower_level_pack_hierarchy_measurement_unit_id_3'];
                }else {
                  $product->lower_level_pack_hierarchy_measurement_unit_id_3 =  null;
                }
                if (array_key_exists('ind_measurement_unit_is_order_unit_3', $data)) {
                  $product->ind_measurement_unit_is_order_unit_3 =  $data['ind_measurement_unit_is_order_unit_3'];
                }else {
                  $product->ind_measurement_unit_is_order_unit_3 =  null;
                }
                if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_3', $data)) {
                 $product->ind_measurement_unit_is_delivery_or_issue_unit_3 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_3'];
                }else {
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_3 =  null;
                }

                if (array_key_exists('alt_measure_unit_stockkeep_unit_id_3', $data)) {
                  $product->alt_measure_unit_stockkeep_unit_id_3 =  $data['alt_measure_unit_stockkeep_unit_id_3'];
                }else {
                  $product->alt_measure_unit_stockkeep_unit_id_3 = null;
                }


                if($product->update()) {
                  event(new ProductUpdated($product));
                  return $product;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }

            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }
    



         /**
     * @param Product  $product
     * @param array $data
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_packaging(Product $product, array $data) : Product
    {


      $fn_update = function() use ($product,$data)
        {
 
            try
            {

                //dd($product);
                //dd($data);
                $product->base_measurement_unit_id =  $data['base_measurement_unit_id'];  
                $product->gtin_1 =  $data['gtin_1'];
                $product->lower_level_measurement_unit_qty_1 =  $data['lower_level_measurement_unit_qty_1'];
                //$product->lower_level_pack_hierarchy_measurement_unit_id_1 =  $data['lower_level_pack_hierarchy_measurement_unit_id_1'];
                
                if (array_key_exists('ind_measurement_unit_is_order_unit_1', $data)) {
                  $product->ind_measurement_unit_is_order_unit_1 =  $data['ind_measurement_unit_is_order_unit_1'];
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_1 = $data['ind_measurement_unit_is_order_unit_1'];
                }else {
                  $product->ind_measurement_unit_is_order_unit_1 = 0;
                  $product->ind_measurement_unit_is_delivery_or_issue_unit_1 = 0;
                }
                
                // if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_1', $data)) {
                //   $product->ind_measurement_unit_is_delivery_or_issue_unit_1 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_1'];
                // }else {
                //   $product->ind_measurement_unit_is_delivery_or_issue_unit_1 = 0;
                // }
                
                $product->alt_measure_unit_stockkeep_unit_id_1 =  $data['alt_measure_unit_stockkeep_unit_id_1'];
                $arr_gtin_type = get_gtin_type($data['gtin_1']);
                $product->gtin_type_id_1 = $arr_gtin_type['id'];



                if (array_key_exists('gtin_2', $data)) {
                  $product->gtin_2 =  $data['gtin_2'];
                  $arr_gtin_type = get_gtin_type($data['gtin_2']);
                  $product->gtin_type_id_2 = $arr_gtin_type['id'];
                }else {
                  if($product->gtin_2){
                    $product->gtin_2 = null;
                  }
                }

                if (array_key_exists('lower_level_measurement_unit_qty_2', $data)) {
                  $product->lower_level_measurement_unit_qty_2 =  $data['lower_level_measurement_unit_qty_2'];
                }else {
                  if($product->lower_level_measurement_unit_qty_2){
                    $product->lower_level_measurement_unit_qty_2 = null;
                  }
                }


                // if (array_key_exists('lower_level_pack_hierarchy_measurement_unit_id_2', $data)) {
                //   $product->lower_level_pack_hierarchy_measurement_unit_id_2 =  $data['lower_level_pack_hierarchy_measurement_unit_id_2'];
                // }else {
                //   if($product->lower_level_pack_hierarchy_measurement_unit_id_2){
                //     $product->lower_level_pack_hierarchy_measurement_unit_id_2 = null;
                //   }
                // }


                if (array_key_exists('ind_measurement_unit_is_order_unit_2', $data)) {
                  $product->ind_measurement_unit_is_order_unit_2 =  $data['ind_measurement_unit_is_order_unit_2'];
                  $product->ind_measurement_unit_is_order_unit_1 = null;
                  $product->ind_measurement_unit_is_order_unit_3 = null;

                }else {
                  
                    
                    $product->ind_measurement_unit_is_order_unit_2 = null;

                    // $product->ind_measurement_unit_is_order_unit_2 = 0;
                    // if($product->ind_measurement_unit_is_order_unit_2){
                    //   $product->ind_measurement_unit_is_order_unit_2 = null;
                    // }
                }

                // if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_2', $data)) {
                //   $product->ind_measurement_unit_is_delivery_or_issue_unit_2 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_2'];
                // }else {
                //   $product->ind_measurement_unit_is_delivery_or_issue_unit_2 = 0;
                //     if($product->ind_measurement_unit_is_delivery_or_issue_unit_2){
                //       $product->ind_measurement_unit_is_delivery_or_issue_unit_2 = null;
                //     }
                // }

                if (array_key_exists('alt_measure_unit_stockkeep_unit_id_2', $data)) {
                  $product->alt_measure_unit_stockkeep_unit_id_2 =  $data['alt_measure_unit_stockkeep_unit_id_2'];
                }else {
                  if($product->alt_measure_unit_stockkeep_unit_id_2){
                    $product->alt_measure_unit_stockkeep_unit_id_2 = null;
                  }
                }

                if (array_key_exists('gtin_3', $data)) {
                  $product->gtin_3 =  $data['gtin_3'];
                  $arr_gtin_type = get_gtin_type($data['gtin_3']);
                  $product->gtin_type_id_3 = $arr_gtin_type['id'];
                }else {
                  if($product->gtin_3){
                    $product->gtin_3 = null;
                  }
                }




                if (array_key_exists('lower_level_measurement_unit_qty_3', $data)) {
                  $product->lower_level_measurement_unit_qty_3 =  $data['lower_level_measurement_unit_qty_3'];
                }else {
                  if($product->lower_level_measurement_unit_qty_3){
                    $product->lower_level_measurement_unit_qty_3 = null;
                  }
                }



                 if (array_key_exists('lower_level_pack_hierarchy_measurement_unit_id_3', $data)) {
                   $product->lower_level_pack_hierarchy_measurement_unit_id_3 =  $data['lower_level_pack_hierarchy_measurement_unit_id_3'];
                 }else {
                   if($product->lower_level_pack_hierarchy_measurement_unit_id_3){
                     $product->lower_level_pack_hierarchy_measurement_unit_id_3 = null;
                   }
                 }



                 if (array_key_exists('ind_measurement_unit_is_order_unit_3', $data)) {
                  $product->ind_measurement_unit_is_order_unit_3 =  $data['ind_measurement_unit_is_order_unit_2'];
                  $product->ind_measurement_unit_is_order_unit_1 = null;
                  $product->ind_measurement_unit_is_order_unit_2 = null;

                }else {
                   
                    $product->ind_measurement_unit_is_order_unit_3 = null;


                }

                // if (array_key_exists('ind_measurement_unit_is_delivery_or_issue_unit_3', $data)) {
                //   $product->ind_measurement_unit_is_delivery_or_issue_unit_3 =  $data['ind_measurement_unit_is_delivery_or_issue_unit_3'];
                // }else {
                //   $product->ind_measurement_unit_is_delivery_or_issue_unit_3 = 0;
                //     if($product->ind_measurement_unit_is_delivery_or_issue_unit_3){
                //       $product->ind_measurement_unit_is_delivery_or_issue_unit_3 = null;
                //     }
                // }

                if (array_key_exists('alt_measure_unit_stockkeep_unit_id_3', $data)) {
                  $product->alt_measure_unit_stockkeep_unit_id_3 =  $data['alt_measure_unit_stockkeep_unit_id_3'];
                }else {
                  if($product->alt_measure_unit_stockkeep_unit_id_3){
                    $product->alt_measure_unit_stockkeep_unit_id_3 = null;
                  }
                }

                if (array_key_exists('length', $data)) {
                  $product->length =  $data['length'];
                }
                
                if (array_key_exists('width', $data)) {
                  $product->width =  $data['width'];
                }

                if (array_key_exists('height', $data)) {
                  $product->height =  $data['height'];
                }

                if (array_key_exists('weight', $data)) {
                  $product->weight =  $data['weight'];
                }

                if (array_key_exists('volume', $data)) {
                  $product->volume =  $data['volume'];
                }


                if($product->update()) {
                  event(new ProductUpdated($product));
                  return $product;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }

            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }


     /**
     * @param Product  $product
     * @param array $data
     * @param bool|UploadedFile  $image_front_1
     * @param bool|UploadedFile  $image_back_1
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_images(Product $product, array $data,
      $image_front_1=false,
      $image_back_1=false
    ) : Product
    {


      //dd($image_front_1);
      for($i = 1; $i <= 1; $i++) {
      // Upload image(s) if necessary

        if (${"image_front_$i"}) {
          $time_str = "_".time(); 
          $imageName = $product->id.$time_str.'_front.'.${"image_front_$i"}->getClientOriginalExtension();
          //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_front_$i"}->getClientOriginalExtension();
          
          if(${"image_front_$i"}->move(public_path('storage/products'), $imageName)){
            $product->{"image_front_$i"} = 'products/' .$imageName;
          }
        }
      }

      for($i = 1; $i <= 1; $i++) {
        // Upload image(s) if necessary

          if (${"image_back_$i"}) {
            $time_str = "_".time(); 
            $imageName = $product->id.$time_str.'_back.'.${"image_back_$i"}->getClientOriginalExtension();
            //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_back_$i"}->getClientOriginalExtension();
            
            if(${"image_back_$i"}->move(public_path('storage/products'), $imageName)){
              $product->{"image_back_$i"} = 'products/' .$imageName;
            }
          }
      }

      //dd($product);
      $fn_update = function() use ($product,$data)
        {
 
            try
            {

                if($product->update()) {
                  event(new ProductUpdated($product));
                  return $product;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }

            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }

     /**
     * @param Product  $product
     * @param array $data
     * @param bool|UploadedFile  $image_front_1
     * @param bool|UploadedFile  $image_back_1
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_step3(Product $product, array $data, 
    $image_front_1=false,
    $image_back_1=false,
    $image_front_2=false,
    $image_back_2=false,
    $image_front_3=false,
    $image_back_3=false
     
     ) : Product
    {

      $fn_update = function() use ($product,$data,$image_front_1,$image_back_1,$image_front_2,$image_back_2,$image_front_3,$image_back_3)
        {
 
            try
            {
              // 'image_front_1'
              // 'image_back_1'

              for($i = 1; $i <= 3; $i++) {
                // Upload image(s) if necessary
          
                  if (${"image_front_$i"}) {
                    $time_str = "_".time(); 
                    $imageName = $product->id.$time_str.'_front.'.${"image_front_$i"}->getClientOriginalExtension();
                    //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_front_$i"}->getClientOriginalExtension();
                    
                    if(${"image_front_$i"}->move(public_path('storage/products'), $imageName)){
                      $product->{"image_front_$i"} = 'products/' .$imageName;
                    }
                  }
                }
          
                for($i = 1; $i <= 3; $i++) {
                  // Upload image(s) if necessary
          
                    if (${"image_back_$i"}) {
                      $time_str = "_".time(); 
                      $imageName = $product->id.$time_str.'_back.'.${"image_back_$i"}->getClientOriginalExtension();
                      //$imageThumbName = $product->id.$time_str.'_thmb.'.${"image_back_$i"}->getClientOriginalExtension();
                      
                      if(${"image_back_$i"}->move(public_path('storage/products'), $imageName)){
                        $product->{"image_back_$i"} = 'products/' .$imageName;
                      }
                    }
                }

                if($product->update()) {
                  $this->update_status($product, true, "Producto enviado a asistente de compras para revisión");
                  event(new ProductUpdated($product));
                  return $product;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }
            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }
    

     /**
     * @param Product  $product
     * @param array $data
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_assign(Product $product, array $data ) : Product
    {


      $fn_update = function() use ($product,$data)
        {
 
            try
            {

                if (array_key_exists("department_id",$data))
                {
                  $product->department_id =  $data['department_id'];
                }

                $product->product_assignment_verified_at = Carbon::now()->toDateTimeString();
                $product->product_assignment_verified_by =  Auth::user()->id;

                if($product->update()) {

                  event(new ProductUpdated($product));
                  return $product;
                  
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }
            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }



     /**
     * @param Product  $product
     * @param array $data
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_details(Product $product, array $data) : Product
    {
     

      //dd($data);
      $fn_update = function() use ($product,$data)
        {
 
            try
            {

              $available_deliverable_from = null;
              $available_deliverable_until = null;
    
              if (array_key_exists('available_deliverable_from', $data)) {
                $available_deliverable_from =  $data['available_deliverable_from'];
              }
    
              if (array_key_exists('available_deliverable_until', $data)) {
                $available_deliverable_until =  $data['available_deliverable_until'];
              }

              $ind_regular_vendor = null;
              if (array_key_exists('ind_regular_vendor', $data)) {
                $ind_regular_vendor =  $data['ind_regular_vendor'];
                $product->ind_regular_vendor =  $ind_regular_vendor;
              }

              $store_id = null;
              if (array_key_exists('store_id', $data)) {
                $store_id =  $data['store_id'];
                //$product->store =  $store_id;
                $product->store()->sync((array)$store_id);
              }


                //$product->vendor_account_number =  $data['vendor_account_number'];
                $product->price =  $data['price'];
                //$product->sku =  $data['sku'];
                $product->material_type_id =  $data['material_type_id'];
                //$product->material_group_id =  $data['material_group_id'];
                $product->purchase_organization_id =  $data['purchase_organization_id'];
                $product->purchase_group_id =  $data['purchase_group_id'];
                $product->material_tax_type_id =  $data['material_tax_type_id'];
                $product->purchase_tax_code_id =  $data['purchase_tax_code_id'];
                $product->iva_code_id =  $data['iva_code_id'];
                $product->order_price_measurement_unit_id =  $data['order_price_measurement_unit_id'];
                $product->department_id =  $data['department_id'];
                $product->category_id =  $data['category_id'];
                $product->hierarchy_id =  $data['hierarchy_id'];
                $product->available_deliverable_from =  $available_deliverable_from;
                $product->available_deliverable_until =  $available_deliverable_until;
                $product->short_text_description =  $data['short_text_description'];
                //$product->ind_regular_vendor =  $data['ind_regular_vendor'];



                if($product->update()) {
                  event(new ProductUpdated($product));
                  return $product;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }
            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;

    }
    

         /**
     * @param Product  $product
     * @param array $data
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function confirm_payment(Product $product, array $data) : Product
    {
     
      $fn_update = function() use ($product,$data)
        {
 
            try
            {

                $product->sap_document =  $data['sap_document'];
                $product->payment_verified_at =  $data['payment_verified_at'];
                //$product->ind_regular_vendor =  $data['ind_regular_vendor'];

                if($product->update()) {
                  event(new ProductUpdated($product));
                  //TODO LOG WHO UPDATED
                  return $product;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }
            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;

    }



     /**
     * @param Product  $product
     * @param int $status_id
     * @return Product
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_status(Product $product, bool $approved, string $comments="") : Product
    {

      $new_status_id = $product->status_id;

      //dump($approved);
      //dump($product);
      //dd();
      if($approved){

          switch ($product->status_id){
            case 1:
            case 2:
            case 3:
            case 4:
                $new_status_id++;
                break;
            case 5:              
            case 6:
                $new_status_id=11;
                break;    
            case 7:
                $new_status_id=2;
                break;              
            case 8:
            case 9:
            case 10:
                $new_status_id=3;
                break;
        }


      }else {
        //REJECTED
        if (auth()->user()->hasRole('asistentecompra') && ($product->status_id == 2 ||  $product->status_id == 8 ||  $product->status_id == 9 ||  $product->status_id == 10)){
           $new_status_id = 7;
        }
        if (auth()->user()->hasRole('gerentecategoria') && ($product->status_id == 3)){
          $new_status_id = 10;
        }
        if (auth()->user()->hasRole('planimetria') && ($product->status_id  == 4)){
          $new_status_id = 8;
        }
        if(auth()->user()->hasRole('finanzas') && ($product->status_id  == 5)){
          $new_status_id = 9;
        }

      }


      $description = ProductStatus::find($new_status_id)->description;

      $fn_update = function() use ($product,$new_status_id,$comments,$description)
        {
 
            try
            {

                $product->status_id =  $new_status_id;

                if($product->update()) {

                if ($new_status_id==3){
                    $users = User::role('gerentecategoria')->get();
                    foreach($users as $user){
                       //$workflow_status = WorkflowStatus::where('product_id', '=',$product->id)->get()->sortByDesc('created_at')->first();

                       $departments = $user->departments->pluck('department_id');

                       for ($i = 0; $i < count( $departments); ++$i) {
                        if($departments[$i] == $product->department_id){

                          Auth::user()->sendMessageTo($product->id,$user->id, $description, $comments);
                          $user->notify(new ProductStatusUpdated(['product_name'=>$product->name, 'user_info'=>Auth::user()->email] ));

                        }
                       }
                       
                    }
                 }
                 
                 if ($new_status_id==4){
                    $users = User::role('planimetria')->get();
                    foreach($users as $user){
                      Auth::user()->sendMessageTo($product->id, $user->id, $description, $comments);
                      $user->notify(new ProductStatusApproved(['product_name'=>$product->name,'product_gtin'=>$product->gtin_1]));
                    }
                 }

                 if ($new_status_id==5){
                    $users = User::role('finanzas')->get();
                    foreach($users as $user){
                      Auth::user()->sendMessageTo($product->id, $user->id, $description, $comments);
                      $user->notify(new ProductStatusApproved(['product_name'=>$product->name,'product_gtin'=>$product->gtin_1]));
                    }
                 }
         
                 if($new_status_id==6){
                    $vendor = User::find($product->user_id);
                    Auth::user()->sendMessageTo($product->id, $vendor->id, $description, $comments);
                    $vendor->notify(new ProductStatusApproved(['product_name'=>$product->name,'product_gtin'=>$product->gtin_1]));
                 }


                 if($new_status_id==7){
                    $vendor = User::find($product->user_id);
                    Auth::user()->sendMessageTo($product->id, $vendor->id, $description, $comments);
                    $vendor->notify(new ProductStatusRejected(['product_name'=>$product->name]));
                }

                if($new_status_id==8 || $new_status_id==9 || $new_status_id==10){
                  $workflow_status = WorkflowStatus::where('product_id', '=',$product->id)->where('status_id', '=',3)->get()->sortByDesc('created_at')->first();
                  Auth::user()->sendMessageTo($product->id, $workflow_status->user_id, $description, $comments);

                  //$vendor->notify(new ProductStatusRejected(['product_name'=>$product->name]));
                }

                event(new ProductUpdated($product));


                $status = WorkflowStatus::Create(
                  [
                      'comments' => $comments,
                      'current_status_description'=> $description,
                      'product_id' => $product->id,
                      'status_id' => $new_status_id,
                      'user_id' => Auth::id()
                  ]
                );
                 



                  return $product;

                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }
            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;


    }


     /**
     * @param Message  $message
     * @return Message
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update_message(Message $message) : Message
    {
     
      $fn_update = function() use ($message)
        {
 
            try
            {

                $message->read =  1;

                if($message->update()) {
                  //event(new ProductUpdated($product));
                  return $message;
                }else {
                  throw new GeneralException(__('exceptions.backend.access.products.update_error'));
                }
            }
            catch(Exception $e)
            {
              throw new GeneralException(__('exceptions.backend.access.products.update_error'));
            }
        };
 
        $return = DB::transaction($fn_update);
        return $return;

    }
    

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param SearchProductRequest $request
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getFilteredPaginated(Request $request, $paged = 25, $orderBy = 'created_at', $sort = 'desc',$current_user=false) : LengthAwarePaginator
    {
        
     /* 
      return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    */
      //dd(ProductSearch::apply($request,$paged,$orderBy,$sort));
      return ProductSearch::apply($request,$paged,$orderBy,$sort,$current_user);

    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getMessagesPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        
      return Message::where('sent_to_id', Auth::user()->id)
      ->orderBy($orderBy, $sort)
      ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }



    /**
     * @param Product $product
     *
     * @return Product
     * @throws GeneralException
     */
    public function restore(Product $product) : Product
    {
        if (is_null($product->deleted_at)) {
            throw new GeneralException(__('exceptions.backend.access.products.cant_restore')); //todo add entries in language files
        }

        if ($product->restore()) {
            event(new ProductRestored($product));

            return $product;
        }

        throw new GeneralException(__('exceptions.backend.access.products.restore_error'));
    }


    
    public function getManufacturers()
    {


     return Cache::remember('manufacturers', 1, function ()  {

      try {

          $url = env('MULE_MANUFACTURER_URL');
          $client = new \GuzzleHttp\Client(['headers' => [
            'channelID' => env('MULE_CHANNELID'),
            'user'      => env('MULE_USER'),
            'password'  => env('MULE_PASSWORD')]]
          );

          $response = $client->request('GET', $url);

          $apiResult = json_decode((string)$response->getBody());
          $manufacturers = $apiResult->manufacturerList;
          //dd($manufacturers);
          $assocDataArray = array();
          $i = 1;
          foreach($manufacturers as $manufacturer){
              
              $assocDataArray[$i] = $manufacturer->ManufacturerName;
              $i++;

          }

          return $assocDataArray;

          //return json_decode((string)$response->getBody());

      } catch (RequestException $e) {
          \Log::error('Error: '.$e);
          throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
      } 



    });

    }

   
    public function getBrands()
    {
     //dd('here');
     return Cache::remember('brands', 1, function ()  {

      try {

          $url = env('MULE_BRAND_URL');
          $client = new \GuzzleHttp\Client();
          //dd($client);
          $response = $client->request('GET', $url);

          $apiResult = json_decode((string)$response->getBody());
          $brands = $apiResult->brandList;
          //dd($brands);

          $assocDataArray = array();
          foreach($brands as $brand){
              

              $assocDataArray[$brand->brandId] = $brand->brandName;
          }

          return $assocDataArray;

          //return json_decode((string)$response->getBody());

      } catch (RequestException $e) {
          //dd($e);
          \Log::error('Error: '.$e);
          throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
      } 

    });

    }



    public function getStores()
    {
        
        return Cache::remember('stores', 1, function ()  {

            try {

                $url = env('MULE_STORES_URL');
                $client = new \GuzzleHttp\Client(['headers' => [
                  'channelID' => env('MULE_CHANNELID'),
                  'user'      => env('MULE_USER'),
                  'password'  => env('MULE_PASSWORD')]]
                );
    
                $response = $client->request('GET', $url);
    
                $apiResult = json_decode((string)$response->getBody());
                $stores = $apiResult->storeList;

                $assocDataArray = array();
                foreach($stores as $store){

                    $assocDataArray[$store->storeNo] = $store->storeName;

                }

                return $assocDataArray;
    

            } catch (RequestException $e) {
              \Log::error('Error: '.$e);
                throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
            } 



        });
    }





    public function getDepartment()
    {
        
        return Cache::remember('departments', 1, function ()  {

            try {

                $url = env('MULE_DEPARTMENT_URL');
                $client = new \GuzzleHttp\Client(['headers' => [
                  'channelID' => env('MULE_CHANNELID'),
                  'user'      => env('MULE_USER'),
                  'password'  => env('MULE_PASSWORD')]]
                );
    
                $response = $client->request('GET', $url);
    
                $apiResult = json_decode((string)$response->getBody());
                $departments = $apiResult->departmentList;
                //$departmentsCollection = collect([]);
                $assocDataArray = array();
                foreach($departments as $department){
                    
                    //$departmentsCollection->push([$department->departmentId=>$department->departmentName]);
                    $assocDataArray[$department->departmentId] = $department->departmentName;

                }
                //dd('in.'.$departmentsCollection);
                //return  $departmentsCollection->toArray();
                return $assocDataArray;
    
                //return json_decode((string)$response->getBody());

            } catch (RequestException $e) {
              \Log::error('Error: '.$e);
                throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
            } 



        });
    }


    public function getCategory($departmentId)
    {
        return Cache::remember('department:' . $departmentId, 1, function () use ($departmentId) {
            
            try {
                
                //$response = $client->get('/not_found.xml')->send();

                $client = new \GuzzleHttp\Client(['headers' => [
                  'channelID' => env('MULE_CHANNELID'),
                  'user'      => env('MULE_USER'),
                  'password'  => env('MULE_PASSWORD')]]
                );

                $url = env('MULE_CATEGORY_URL');
                $response = $client->request('GET', $url, [
                    'query' => [
                        'departmentId' => $departmentId
                    ]
                ]);
    
                $apiResult = json_decode((string)$response->getBody());
                $categories = $apiResult->categoryList;

                $assocDataArray = array();
                foreach($categories as $category){

                    $assocDataArray[$category->categoryId] = $category->categoryName;

                }

                return $assocDataArray;


            } catch (RequestException $e) {
              \Log::error('Error: '.$e);
                throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
            }    

        });
    }


    public function getHierarchy($categoryId)
    {


        return Cache::remember('hierarchy:' . $categoryId, 1, function () use ($categoryId) {
            
            try {
                
                //$response = $client->get('/not_found.xml')->send();

                $client = new \GuzzleHttp\Client(['headers' => [
                  'channelID' => env('MULE_CHANNELID'),
                  'user'      => env('MULE_USER'),
                  'password'  => env('MULE_PASSWORD')]]
                );

                $url = env('MULE_HIERARCHY_URL');
                $response = $client->request('GET', $url, [
                    'query' => [
                        'categoryId' => $categoryId
                    ]
                ]);
                //dd($response->getBody());
                $apiResult = json_decode((string)$response->getBody());
                //dd($apiResult);
                $hierarchies = $apiResult->hierarchyList;
                //dd($hierarchies);
                //$departmentsCollection = collect([]);
                $assocDataArray = array();
                foreach($hierarchies as $hierarchy){
                    //dd($hierarchy);
                    //$departmentsCollection->push([$department->departmentId=>$department->departmentName]);
                    $assocDataArray[$hierarchy->hierarchyId] = $hierarchy->hierarchyName;

                }
                //dd('in.'.$departmentsCollection);
                //return  $departmentsCollection->toArray();
                return $assocDataArray;

                //BUG 404 NOT FOUND 
                //categoryId: 90902010
                //\Log::info('User Logged In: '.$event->user->full_name);
            } catch (RequestException $e) {
                \Log::error('Error: '.$e);
                throw new ServiceException(__('exceptions.backend.access.products.data_access_error'));
            }    

        });
    }



    public function saveManufacturers(){


        //dd($manufacturers);
        // foreach ($manufacturers as $manufacturer) {
            
        //     $userData = array('name' => $manufacturer, 'information' => '', 'country_id'=>165);
        //     Manufacturer::create($userData);

        // }



    }

    public function saveBrands(){

      $brands = $this->getBrands();
      //dd($brands);

      
       foreach ($brands as $brand) {
          
           $brandData = array('name' => $brand->name, 'brand_id' => $brand->brand_id, 'user_id'=>1,'valid'=>1, 'created_at' => Carbon::now());
           Brand::create($brandData);

       }

  }


  public function saveDepartments(){

     $departments = $this->getDepartment();

     foreach ($departments as $key => $value) {
        
         $departmentData = array('department_id' => $key, 'department_name' => $value);
         Department::create($departmentData);

     }

}


    

















    






}