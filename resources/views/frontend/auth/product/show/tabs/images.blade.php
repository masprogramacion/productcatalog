
<div class="row"> <!--images-->

<div class="col-sm-3">
        <div class="card">
                    <img class="card-img-top" id="preview_image_front_1" name="preview_image_front_1"  src="{{ $product->imageFront }}" alt="@lang('labels.products.form.alt.select_front_image')">
                    <div class="card-body">
                        <h5 class="card-title">Imagen Frontal</h5>
                        <p class="card-text"></p>
                     </div>
        </div>
</div>
<div class="col-sm-3">        
        <div class="card">
                <img class="card-img-top" id="preview_image_back_1" name="preview_image_back_1" src="{{ $product->imageBack  }}" alt="@lang('labels.products.form.alt.select_back_image')">
                <div class="card-body">
                        <h5 class="card-title">Imagen Posterior</h5>
                </div>
        </div>
</div>

</div><!--images-->
<div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('frontend.auth.product.index'), __('buttons.general.goback')) }}
                    </div><!--col-->
                @if(!$product->assigned)    
                <div class="col text-right">
                    <a class="btn btn-success btn-sm pull-right" href="{{ route('frontend.auth.product.edit', $product->id).'#images' }}" role="button">@lang('buttons.general.crud.edit')</a>
                </div><!--col-->
                @endif
                </div><!--row-->
</div><!--card-footer-->
