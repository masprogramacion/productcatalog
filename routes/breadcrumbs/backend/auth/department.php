<?php

Breadcrumbs::for('admin.auth.department.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('labels.backend.access.departments.management'), route('admin.auth.department.index'));
});

Breadcrumbs::for('admin.auth.department.edit', function ($trail,$id) {
    $trail->parent('admin.auth.department.index');
    $trail->push(__('labels.backend.access.departments.edit'), route('admin.auth.department.edit',$id));
});

Breadcrumbs::for('admin.auth.department.index_assistant', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('labels.backend.access.departments.management_assistant'), route('admin.auth.department.index_assistant'));
});

