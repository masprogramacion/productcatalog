<?php

namespace App\Events\Frontend\Auth;

use App\Models\Auth\Admin;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserLoggedOut.
 */
class AdminLoggedOut
{
    use SerializesModels;

    /**
     * @var
     */
    public $admin;

    /**
     * @param $user
     */
    public function __construct(Admin $admin)
    {
        $this->admin = $admin;
    }
}
