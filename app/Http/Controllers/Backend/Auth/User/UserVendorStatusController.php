<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Models\Auth\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\VendorRepository;
use App\Http\Requests\Backend\Auth\User\ManageUserVendorRequest;

/**
 * Class UserVendorStatusController.
 */
class UserVendorStatusController extends Controller
{
    /**
     * @var VendorRepository
     */
    protected $vendorRepository;

    /**
     * @param VendorRepository $vendorRepository
     */
    public function __construct(VendorRepository $vendorRepository)
    {
        $this->vendorRepository = $vendorRepository;
    }

    /**
     * @param ManageUserVendorRequest $request
     *
     * @return mixed
     */
    public function getDeactivated(ManageUserVendorRequest $request)
    {
        return view('backend.auth.vendor.deactivated')
            ->withUsers($this->vendorRepository->getInactivePaginated(25, 'id', 'desc'));
    }

    /**
     * @param ManageUserVendorRequest $request
     *
     * @return mixed
     */
    public function getDeleted(ManageUserVendorRequest $request)
    {
        return view('backend.auth.vendor.deleted')
            ->withUsers($this->vendorRepository->getDeletedPaginated(25, 'id', 'desc'));
    }

    /**
     * @param ManageUserVendorRequest $request
     * @param User              $user
     * @param                   $status
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function mark(ManageUserVendorRequest $request, User $user, $status)
    {
        $this->vendorRepository->mark($user, $status);

        return redirect()->route(
            $status == 1 ?
            'admin.auth.vendor.index' :
            'admin.auth.vendor.deactivated'
        )->withFlashSuccess(__('alerts.backend.users.updated'));
    }

    /**
     * @param ManageUserVendorRequest $request
     * @param User              $deletedUser
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageUserVendorRequest $request, User $deletedUser)
    {
        $this->vendorRepository->forceDelete($deletedUser);

        return redirect()->route('admin.auth.vendor.deleted')->withFlashSuccess(__('alerts.backend.users.deleted_permanently'));
    }

    /**
     * @param ManageUserVendorRequest $request
     * @param User              $deletedUser
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageUserVendorRequest $request, User $deletedUser)
    {
        $this->vendorRepository->restore($deletedUser);

        return redirect()->route('admin.auth.vendor.index')->withFlashSuccess(__('alerts.backend.users.restored'));
    }
}
