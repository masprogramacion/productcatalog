<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductAssignmentToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            // 1. Create new column
            // You probably want to make the new column nullable
            $table->integer('product_assignment_verified_by')->unsigned()->nullable()->after('description');
            $table->timestamp('product_assignment_verified_at')->nullable()->after('description');

            // 2. Create foreign key constraints
            $table->foreign('product_assignment_verified_by')->references('id')->on('users')->onDelete('SET NULL');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {

            // 1. Drop foreign key constraints
            $table->dropForeign(['product_assignment_verified_by']);

            // 2. Drop the column
            $table->dropColumn('product_assignment_verified_by');
            $table->dropColumn('product_assignment_verified_at');

        });
    }
}
