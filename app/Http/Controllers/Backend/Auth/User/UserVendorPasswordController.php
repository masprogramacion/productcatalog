<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Models\Auth\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\VendorRepository;
use App\Http\Requests\Backend\Auth\User\ManageUserVendorRequest;
use App\Http\Requests\Backend\Auth\User\UpdateUserVendorPasswordRequest;

/**
 * Class UserVendorPasswordController.
 */
class UserVendorPasswordController extends Controller
{
    /**
     * @var VendorRepository
     */
    protected $userRepository;

    /**
     * @param VendorRepository $vendorRepository
     */
    public function __construct(VendorRepository $vendorRepository)
    {
        $this->vendorRepository = $vendorRepository;
    }

    /**
     * @param ManageUserVendorRequest $request
     * @param User              $user
     *
     * @return mixed
     */
    public function edit(ManageUserVendorRequest $request, User $user)
    {
        return view('backend.auth.vendor.change-password')
            ->withUser($user);
    }

    /**
     * @param UpdateUserVendorPasswordRequest $request
     * @param User                      $user
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function update(UpdateUserVendorPasswordRequest $request, User $user)
    {
        $this->vendorRepository->updatePassword($user, $request->only('password'));

        return redirect()->route('admin.auth.vendor.index')->withFlashSuccess(__('alerts.backend.users.updated_password'));
    }
}
