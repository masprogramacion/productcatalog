<?php

namespace App\Http\Controllers\Backend\Auth\Department;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Auth\Department\ManageDepartmentRequest;
use App\Http\Requests\Backend\Auth\Department\UpdateDepartmentRequest;
use App\Models\Catalog\Department;
use App\Models\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\Backend\Auth\UserRepository;

/**
 * Class DepartmentController.
 */
class DepartmentController extends Controller
{

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

     /**
     * @param ManageDepartmentRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        //DB::enableQueryLog(); 
        //$totalData = Product::verified()->count();
        //dump($totalData);
        //dd(DB::getQueryLog()); // Show results of log\
        $department_managers = User::role('gerentecategoria')->where('active',1)->paginate();
        return view('backend.auth.department.index')
        ->with('department_managers', $department_managers);
    }


    /**
     * @param ManageDepartmentRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index_assistant(Request $request)
    {

        //DB::enableQueryLog(); 
        //$totalData = Product::verified()->count();
        //dump($totalData);
        //dd(DB::getQueryLog()); // Show results of log\
        $department_managers = User::role('asistentecompra')->where('active',1)->paginate();
        return view('backend.auth.department.index')
        ->with('department_managers', $department_managers);
    }


    /**
     * @param ManageDepartmentRequest $request
     *
     * @return mixed
     */
    public function create(ManageDepartmentRequest $request)
    {
        return view('backend.auth.department.create');
    }

    /**
     * @param ManageDepartmentRequest $request
     *
     * @return mixed
     */
    public function edit(Request $request, User $user)
    {
        
        $departments = Department::all();
        $user_departments = $user->departments()->get();
        //dump($departments);
        //dump($user_departments);
        ///dd();
        return view('backend.auth.department.edit')
        ->with('user', $user)
        ->with('departments', $departments)
        ->with('user_departments', $user_departments);
    }

    /**
     * @param UpdateDepartmentRequest $request
     * @param User              $user
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateDepartmentRequest $request, User $user)
    {
        $this->userRepository->updateDepartments($user, $request->only(
            'departments'

        ));

        if($user->hasrole('asistentecompra')){
            return redirect()->route('admin.auth.department.index_assistant')->withFlashSuccess(__('alerts.backend.users.updated'));
        }else {
            return redirect()->route('admin.auth.department.index')->withFlashSuccess(__('alerts.backend.users.updated'));
        }
        
    }


}