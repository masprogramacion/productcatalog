<?php

Breadcrumbs::for('frontend.index', function ($trail) {
    $trail->push(__('Home'), route('frontend.index'));
});

Breadcrumbs::for('frontend.auth.login', function ($trail) {
    $trail->push(__('Home'), route('frontend.auth.login'));
});


Breadcrumbs::for('frontend.user.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('frontend.user.dashboard'));
});

Breadcrumbs::for('frontend.user.account', function ($trail) {
    $trail->push(__('Mi Cuenta'), route('frontend.user.account'));
});

Breadcrumbs::for('frontend.auth.password.index', function ($trail) {
    $trail->push(__('Cambiar Contrasena'), route('frontend.auth.password.index'));
});

require __DIR__.'/auth.php';

