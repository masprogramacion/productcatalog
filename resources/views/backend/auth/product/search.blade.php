@extends('backend.layouts.app')

@section('content')

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<i class="fa fa-search"></i>@lang('labels.backend.access.products.form.search.title')
<div class="card-header-actions">
<a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true">
<i class="icon-arrow-up"></i>
</a>

</div>
</div>

<div class="card-body collapse show" id="collapseExample">

       
   {{ html()->form('POST', route('admin.auth.product.search'))->class('form-horizontal')->open() }}
   <div class="form-row">
    <div class="col-md-4 mb-3">
      {{ html()->label(__('Nombre'))->class('form-control-label')->for('product_name') }}
      {{ html()->text('product_name')
               ->class('form-control')
               ->attribute('maxlength', 100) }}
    </div>
    <div class="col-md-4 mb-3">
      {{ html()->label(__('labels.backend.access.products.form.search.status'))->class('form-control-label')->for('status_id') }}
      {{ html()->select('status_id',$product_statuses)
                ->placeholder('Seleccione un Estatus')
                ->class('form-control')}}
    </div>
    <div class="col-md-4 mb-3">
    </div>
  </div>

  <div class="form-row">

    <div class="form-group col-md-4">
          {{ html()->label(__('labels.products.form.label.department_id'))->class('form-control-label') }}
          {{ html()->select('department_id',$departments)
                        ->placeholder('Seleccione un Departamento')
                        ->class('select2 custom-select d-block w-100') }}
          <small class="text-muted">@lang('labels.products.form.helptext.department_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('department_id') }}
          </div>
    </div>
    <div class="form-group col-md-4">
          {{ html()->label(__('labels.products.form.label.category_id'))->class('form-control-label') }}
          {{ html()->select('category_id')
                        ->class('select2 custom-select d-block w-100') }}
          <small class="text-muted">@lang('labels.products.form.helptext.category_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('category_id') }}
          </div>
    </div>
    <div class="form-group col-sm-4">
          {{ html()->label(__('labels.products.form.label.hierarchy_id'))->class('form-control-label') }}
          {{ html()->select('hierarchy_id')
                        ->class('select2 custom-select d-block w-100') }}
          <small class="text-muted">@lang('labels.products.form.helptext.hierarchy_id')</small>
          <div class="invalid-feedback">
          {{ $errors->first('hierarchy_id') }}
          </div>
    </div>

  </div>


  <div class="form-row" >

    <div class="col-md-6 mb-3">
    {{ html()->label(__('labels.products.form.search.start_date'))->class('form-control-label')}}
        <input name="start_date" id="start_date"  placeholder="Seleccione una fecha"  />
    </div>
    <div class="col-md-6 mb-3">
    {{ html()->label(__('labels.products.form.search.end_date'))->class('form-control-label')}}
        <input name="end_date" id="end_date"  placeholder="Seleccione una fecha" />
    </div>

 </div>

  <button class="btn btn-primary" type="submit">Buscar</button>
  <button class="btn btn-secondary" type="button"  id="resetButton">Limpiar</button>
  {{ html()->form()->close() }}
        
</div>
</div>
</div>

</div>



<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.products.management') }} <small class="text-muted">{{ __('labels.backend.access.products.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.auth.product.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.access.products.table.name')</th>
                            <th>Proveedor</th>
                            <th>Estatus</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->user->name }}</td>
                                <td>{!! $product->status_label !!}</td>
                                <td>{!! $product->show_button !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $products->total() !!} {{ trans_choice('labels.backend.access.products.table.total', $products->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $products->appends(Request::capture()->except('page'))->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->


@endsection
@push('after-scripts')
    
    <script>

        $(document).ready(function(){

            $('#start_date').datepicker({

            format: '{{ config('app.date_format_js') }}' ,
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fortawesome',
            maxDate: function () {
                return $('#end_date').val();
            }
            });
                    
            $('#end_date').datepicker({
            format: '{{ config('app.date_format_js') }}',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fortawesome',
            minDate: function () {
                return $('#start_date').val();
            } 
            });
        });


        $(function () {

            $('#resetButton').on('click', function() {
  
                //$(this).closest('form').get(0).reset();
                $('#product_name').val("");
                $('#status_id').prop('selectedIndex',-1);
                $('#start_date').val("");
                $('#end_date').val("");
                $('#department_id').prop('selectedIndex',0);
                $('#category_id').empty();
                $('#hierarchy_id').empty();
                $('#category_id').prop("disabled", true);
                $('#hierarchy_id').prop("disabled", true);

            });


            $('#department_id').on('change', function (e) {
                var department_id = e.target.value;
                category_id = false;
                categoryUpdate(department_id);
            });
            // Ajax Request for categories
            function categoryUpdate(department_id) {
                $.get('{{ url('admin/auth/product/category') }}/' + department_id , function (data) {
                    $('#category_id').empty();
                    $('#category_id').append($("<option selected></option>").text('Seleccione una categoria'));
                    $.each(data, function(key, value) {   
                        $('#category_id')
                            .append($("<option></option>")
                                        .attr("value",key)
                                        .text(value)); 
                    });
                    $('#category_id').prop("disabled", false);
                    /*
                    $.each(json, function(i, obj){
                        $('#select').append($('<option>').text(obj.text).attr('value', obj.val));
                    });

                    $.each(data, function (index, categories) {
                        $('#category').append($('<option>', {
                            value: categories.id,
                            text: categories.name
                        }));
                    });
                    */

                });
            }

            $('#category_id').on('change', function (e) {
                var category_id = e.target.value;
                hierarchyUpdate(category_id);
            });

            // Ajax Request for hierarchies
            function hierarchyUpdate(category_id) {
                $.get('{{ url('admin/auth/product/hierarchy') }}/' + category_id , function (data) {
                    $('#hierarchy_id').empty();
                    $('#hierarchy_id').append($("<option selected></option>").text('Seleccione una jerarquia'));
                    $.each(data, function(key, value) {   
                        $('#hierarchy_id')
                            .append($("<option></option>")
                                        .attr("value",key)
                                        .text(value)); 
                    });
                    $('#hierarchy_id').prop("disabled", false);

                });
            }



        });






    </script>
    
@endpush