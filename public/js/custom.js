
        //define template



      $("#btnSubmitPackaging").click(function(e) {
      
        //var $clicked = $(e.target);
        //console.log($clicked);
        //console.log($clicked.parents('form').attr('id'));
        var form = $(this).closest('form')[0];
        //console.log(form);
        //var form2 = $(this).closest('form');
        //console.log(form2);
      
        validatePackagingForm(form);
      
      
      });

      $("#btn_reset").click(function () {
        window.location.reload();
      });

      function removeSection(){

          var id = 'row_id_'+ sectionsCount;
          
          var elem = document.getElementById(id);
          //alert(elem);
          elem.parentNode.removeChild(elem);
          sectionsCount--;

          if(sectionsCount==2){
            //console.log($('#row_id_2').find('.hdnButton'));
            $('#row_id_2').find('.hdnButton').removeClass("d-none");
          }
      }



      //add new section
      function add_fields() {

          if(sectionsCount == 3) return false;
          //increment
          sectionsCount++;
          
          //var section = template.clone();
          //var template = $('#row_to_clone').clone();
          var section =  $('#row_to_clone').clone();
          // = "row_id_" + sectionsCount;
          section.attr("id", "row_id_" + sectionsCount);
          if(sectionsCount===2){
            //section.find('.card-header h3').text(sectionsCount + '- Embalaje (Ej:Caja)');
          }
          if(sectionsCount===3){
            //section.find('.card-header h3').text(sectionsCount + '- Embalaje (Ej:Pallet)');
          }

          //loop through each input
          section.find(':input').each(function(){
              //set id to store the updated section number
              var name_id = this.name.substring(0,this.name.length-2);
              var new_name_id =  name_id + '_' + sectionsCount;
              //update id
              
              this.id = new_name_id;
              this.name = new_name_id;
              //console.log(this.name);
              if (this.type == 'text' || this.type == 'number') {
                  $(this).val(" ");
                  $(this).prop('disabled', false);
                  if(this.name.substring(0,this.name.length-2) === 'gtin'){
                    $(this).removeAttr('required');
                  }

              }

              if (this.type == 'checkbox') {
                  
               
                $(this).siblings("label").each(function( index ) {
                    console.log( index + ": " + $(this).text() );
                    var curr_val= $(this).attr('for').substring(0,$(this).attr('for').length-2);
                    var new_val =  curr_val + '_' + sectionsCount;
                    $(this).attr('for',new_val);
                    $(this).attr('id',new_val);

                  
                });
                $(this).siblings("small").each(function( index ) {
                    var base_id = this.id.substring(0,this.id.length-2);
                    var new_id =  base_id + '_' + sectionsCount;
                    this.id = new_id;
             

                });
              }

              if (this.type == 'button') {
                   this.onclick = removeSection;
              }

          }).end();

          
          $(section).find('.hdnButton').removeClass("d-none");

          if(sectionsCount==3){
            //console.log($('#row_id_2').find('.hdnButton'));
            $('#row_id_2').find('.hdnButton').addClass("d-none");
          }


          //$(section).find('#ind_measurement_unit_is_order_unit_1_div').remove();

          //inject new section
          $("#sections").append(section);
          //$("#sections").find('select').each(function(){
          //    $(this).select2();
          //});

          return false;
      }



function clearFields(){

  for (var i = 3; i > 1; i--) {
    if($('#row_id_'+i).length){
         $('#row_id_'+i).remove();
    }
  }

  $('#alt_measure_unit_stockkeep_unit_id_1').prop('disabled', false);
  $('#lower_level_pack_hierarchy_measurement_unit_id_1').prop('disabled', false);

  $("#alt_measure_unit_stockkeep_unit_id_1 > option:first").attr("value", 0);
  $("#lower_level_pack_hierarchy_measurement_unit_id_1 > option:first").attr("value", 0);

  sectionsCount=1;
  //$('#product_material_preset_id').val(''); 

}


$("#product_material_preset_id").change(function () {
    clearFields();
    var preset = this.value;
    //alert(preset);
    if(preset==0){


        $('#alt_measure_unit_stockkeep_unit_id_1').val('215'); //UNIDAD
        $('#lower_level_pack_hierarchy_measurement_unit_id_1').val('215'); //UNIDAD
        $('#lower_level_measurement_unit_qty_1').val('1'); //UNIDAD
        

        $('#alt_measure_unit_stockkeep_unit_id_1').prop('disabled', true);
        $('#lower_level_pack_hierarchy_measurement_unit_id_1').prop('disabled', true);
        $('#lower_level_measurement_unit_qty_1').prop('disabled', true);


      }else if(preset==1){

      add_fields();
      add_fields();

      $('#alt_measure_unit_stockkeep_unit_id_1').val('215'); //UNIDAD
      $('#alt_measure_unit_stockkeep_unit_id_2').val('98'); //DISPLAY (CAJA)
      $('#alt_measure_unit_stockkeep_unit_id_3').val('39'); //CAJA (CJ)

      $('#lower_level_pack_hierarchy_measurement_unit_id_1').val('215'); //UNIDAD
      $('#lower_level_pack_hierarchy_measurement_unit_id_2').val('215'); //UNIDAD
      $('#lower_level_pack_hierarchy_measurement_unit_id_3').val('98'); //UNIDAD

      $('#alt_measure_unit_stockkeep_unit_id_1').prop('disabled', true);
      $('#alt_measure_unit_stockkeep_unit_id_2').prop('disabled', true);
      $('#alt_measure_unit_stockkeep_unit_id_3').prop('disabled', true);

      $('#lower_level_measurement_unit_qty_1').val('1'); //UNIDAD
      $('#lower_level_measurement_unit_qty_1').prop('disabled', true);

      $('#lower_level_pack_hierarchy_measurement_unit_id_1').prop('disabled', true);
      $('#lower_level_pack_hierarchy_measurement_unit_id_2').prop('disabled', true);
      $('#lower_level_pack_hierarchy_measurement_unit_id_3').prop('disabled', true);

      $('#ind_measurement_unit_is_order_unit_3').prop('checked', true);
      $('#ind_measurement_unit_is_delivery_or_issue_unit_3').prop('checked', true);
      

    }else if(preset==2){

      add_fields();

      

      $('#alt_measure_unit_stockkeep_unit_id_1').val('215'); //UNIDAD
      $('#alt_measure_unit_stockkeep_unit_id_2').val('98'); //DISPLAY (CAJA)

      $('#lower_level_pack_hierarchy_measurement_unit_id_1').val('215'); //UNIDAD
      $('#lower_level_pack_hierarchy_measurement_unit_id_2').val('215'); //UNIDAD

      $('#alt_measure_unit_stockkeep_unit_id_1').prop('disabled', true);
      $('#alt_measure_unit_stockkeep_unit_id_2').prop('disabled', true);

      $('#lower_level_pack_hierarchy_measurement_unit_id_1').prop('disabled', true);
      $('#lower_level_pack_hierarchy_measurement_unit_id_2').prop('disabled', true);

      $('#ind_measurement_unit_is_order_unit_2').prop('checked', true);
      $('#ind_measurement_unit_is_delivery_or_issue_unit_2').prop('checked', true);

    }else if(preset==3){

      add_fields();

      $('#alt_measure_unit_stockkeep_unit_id_1').val('215'); //UNIDAD
      $('#alt_measure_unit_stockkeep_unit_id_2').val('39'); //CAJA (CJ)

      $('#lower_level_pack_hierarchy_measurement_unit_id_1').val('215'); //UNIDAD
      $('#lower_level_pack_hierarchy_measurement_unit_id_2').val('215'); //UNIDAD

      $('#alt_measure_unit_stockkeep_unit_id_1').prop('disabled', true);
      $('#alt_measure_unit_stockkeep_unit_id_2').prop('disabled', true);

      $('#lower_level_pack_hierarchy_measurement_unit_id_1').prop('disabled', true);
      $('#lower_level_pack_hierarchy_measurement_unit_id_2').prop('disabled', true);

      $('#ind_measurement_unit_is_order_unit_2').prop('checked', true);
      $('#ind_measurement_unit_is_delivery_or_issue_unit_2').prop('checked', true);

    }else if(preset==4){


      $('#alt_measure_unit_stockkeep_unit_id_1').val('90'); //KG
      $('#lower_level_pack_hierarchy_measurement_unit_id_1').val('90'); //KG
      $('#lower_level_measurement_unit_qty_1').val('1'); //KG
        

      $('#alt_measure_unit_stockkeep_unit_id_1').prop('disabled', true);
      $('#lower_level_pack_hierarchy_measurement_unit_id_1').prop('disabled', true);
      $('#lower_level_measurement_unit_qty_1').prop('disabled', true);



    }else if(preset==5){

      add_fields();

      $('#alt_measure_unit_stockkeep_unit_id_1').val('90'); //KG
      $('#alt_measure_unit_stockkeep_unit_id_2').val('39'); //CAJA (CJ)

      $('#lower_level_pack_hierarchy_measurement_unit_id_1').val('90'); //KG
      $('#lower_level_pack_hierarchy_measurement_unit_id_2').val('90'); //KG

      $('#alt_measure_unit_stockkeep_unit_id_1').prop('disabled', true);
      $('#alt_measure_unit_stockkeep_unit_id_2').prop('disabled', true);

      $('#lower_level_pack_hierarchy_measurement_unit_id_1').prop('disabled', true);
      $('#lower_level_pack_hierarchy_measurement_unit_id_2').prop('disabled', true);

      
      $('#ind_measurement_unit_is_order_unit_2').prop('checked', true);
      $('#ind_measurement_unit_is_delivery_or_issue_unit_2').prop('checked', true);

    }else if(preset==6){

      $('#alt_measure_unit_stockkeep_unit_id_1').val('124'); //METRO
      $('#lower_level_pack_hierarchy_measurement_unit_id_1').val('124'); //METRO
      $('#lower_level_measurement_unit_qty_1').val('1'); //METRO
        

      $('#alt_measure_unit_stockkeep_unit_id_1').prop('disabled', true);
      $('#lower_level_pack_hierarchy_measurement_unit_id_1').prop('disabled', true);
      $('#lower_level_measurement_unit_qty_1').prop('disabled', true);



    }else if(preset==7){

      add_fields();

      $('#alt_measure_unit_stockkeep_unit_id_1').val('124'); //METRO
      $('#alt_measure_unit_stockkeep_unit_id_2').val('210'); //ROLLOS

      $('#lower_level_pack_hierarchy_measurement_unit_id_1').val('124'); //METRO
      $('#lower_level_pack_hierarchy_measurement_unit_id_2').val('124'); //METRO

      $('#alt_measure_unit_stockkeep_unit_id_1').prop('disabled', true);
      $('#alt_measure_unit_stockkeep_unit_id_2').prop('disabled', true);

      $('#lower_level_pack_hierarchy_measurement_unit_id_1').prop('disabled', true);
      $('#lower_level_pack_hierarchy_measurement_unit_id_2').prop('disabled', true);

      
      $('#ind_measurement_unit_is_order_unit_2').prop('checked', true);
      $('#ind_measurement_unit_is_delivery_or_issue_unit_2').prop('checked', true);

    }


    //var firstDropVal = $('#pick').val();
});


function validatePackagingForm(form_obj) {

  //set base unit
  var base_unit = $('#alt_measure_unit_stockkeep_unit_id_1').val();
  $('#base_measurement_unit_id').attr('value', base_unit);

  form_obj.classList.add("was-validated");


  let alt_measure_unit_stockkeep_unit_id_1 = document.querySelector("#alt_measure_unit_stockkeep_unit_id_1").checkValidity();
  let lower_level_measurement_unit_qty_1 = document.querySelector("#lower_level_measurement_unit_qty_1").checkValidity();
  //let lower_level_pack_hierarchy_measurement_unit_id_1 = document.querySelector("#lower_level_pack_hierarchy_measurement_unit_id_1").checkValidity();
  let gtin_1 = document.querySelector("#gtin_1").checkValidity();

  //if there is a discount, validate value
  let no_p1_errors = true;
  no_p1_errors = ( base_measurement_unit_id && alt_measure_unit_stockkeep_unit_id_1 && lower_level_measurement_unit_qty_1  && gtin_1);

  let no_p2_errors = true;
  if(document.querySelector("#gtin_2")){
    let alt_measure_unit_stockkeep_unit_id_2 = document.querySelector("#alt_measure_unit_stockkeep_unit_id_2").checkValidity();
    let lower_level_measurement_unit_qty_2 = document.querySelector("#lower_level_measurement_unit_qty_2").checkValidity();
    //let lower_level_pack_hierarchy_measurement_unit_id_2 = document.querySelector("#lower_level_pack_hierarchy_measurement_unit_id_2").checkValidity();
    let gtin_2 = document.querySelector("#gtin_2").checkValidity();
    no_p2_errors = ( alt_measure_unit_stockkeep_unit_id_2 && lower_level_measurement_unit_qty_2  && gtin_2);
  }




  let no_p3_errors = true;
  if(document.querySelector("#gtin_3")){
    let alt_measure_unit_stockkeep_unit_id_3 = document.querySelector("#alt_measure_unit_stockkeep_unit_id_3").checkValidity();
    let lower_level_measurement_unit_qty_3 = document.querySelector("#lower_level_measurement_unit_qty_3").checkValidity();
    //let lower_level_pack_hierarchy_measurement_unit_id_3 = document.querySelector("#lower_level_pack_hierarchy_measurement_unit_id_3").checkValidity();
    let gtin_3 = document.querySelector("#gtin_3").checkValidity();
    no_p3_errors = (alt_measure_unit_stockkeep_unit_id_3 && lower_level_measurement_unit_qty_3 &&  gtin_3);
  }


    if (no_p1_errors && no_p2_errors && no_p3_errors) {


        $(form_obj).find('input,select,textarea').each(function(i){ 
          $(this).prop('disabled', false);
          //console.log($(this));
        });


        //$('#form-update-product').submit();
        form_obj.submit();


    }else {


      //var formElements = document.getElementById('form-create-product').querySelectorAll('textarea, input:not([type=hidden])');
      var errorElements = document.getElementById(form_obj.id).querySelectorAll('input.form-control:invalid');

      if(errorElements.length > 0){
        //console.log(errorElements);
        //var scrollTop = $(window).scrollTop();
        var elementOffset = $(errorElements[0]).offset().top - 100;
        //console.log(elementOffset);
        //var currentElementOffset = (elementOffset - scrollTop);
        //console.log(currentElementOffset);
        $('html, body').animate({
            scrollTop: elementOffset
          }, 2000, function() {
            $(errorElements[0]).focus();
        });
      }

    }
}