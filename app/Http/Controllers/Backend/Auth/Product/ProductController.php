<?php

namespace App\Http\Controllers\Backend\Auth\Product;
use DB;
use App\Models\Auth\Product;
use App\Models\Auth\User;
use App\Models\Auth\Message;
use App\Models\Catalog\Manufacturer;
use App\Models\Catalog\Brand;
use App\Models\Catalog\ProductStatus;
use App\Models\Auth\WorkflowStatus;
use App\Models\Catalog\MaterialType;
use App\Http\Controllers\Controller;
use App\Models\Catalog\PurchaseOrganization;
use App\Models\Catalog\VendorAccount;
use App\Models\Catalog\MaterialGroup;
use App\Models\Catalog\MeasurementUnit;
use App\Models\Catalog\PackagingMeasurementUnit;
use App\Models\Catalog\PurchaseGroup;
use App\Models\Catalog\GtinType;
use App\Models\Catalog\IvaCode;
use App\Models\Catalog\Department;
use App\Models\Catalog\Store;

use App\Models\Catalog\TaxCode;
use App\Models\Catalog\MaterialTaxType;

use App\Http\Requests\Backend\Auth\Product\StoreProductRequest;
use App\Http\Requests\Backend\Auth\Product\StoreStep2ProductRequest;
use App\Http\Requests\Backend\Auth\Product\StoreStep3ProductRequest;
use App\Http\Requests\Backend\Auth\Product\StoreUncataloguedProductRequest;
use App\Http\Requests\Backend\Auth\Product\UpdateUncataloguedProductRequest;
use App\Http\Requests\Backend\Auth\Product\ManageProductRequest;
use App\Http\Requests\Backend\Auth\Product\UpdateProductRequest;
use App\Http\Requests\Backend\Auth\Product\UpdateAssignProductRequest;
use App\Http\Requests\Backend\Auth\Product\SearchProductRequest;
use App\Http\Requests\Backend\Auth\Product\UpdateGeneralProductRequest;
use App\Http\Requests\Backend\Auth\Product\UpdatePackagingProductRequest;
use App\Http\Requests\Backend\Auth\Product\UpdateImagesProductRequest;
use App\Http\Requests\Backend\Auth\Product\UpdateDetailsProductRequest;
use App\Repositories\Backend\Auth\ProductRepository;

/**
 * Class ProductController.
 */
class ProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * ProductController constructor.
     *
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param ManageProductRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageProductRequest $request)
    {

        //$manufacturers = $this->productRepository->getManufacturers();
        $products = Product::sortable()->paginate(config('xtra.paged.products'));
        return view('backend.auth.product.index')
            ->withProducts($products);
            //->withProducts($this->productRepository->getPaginated(config('xtra.paged.products'), 'id', 'desc'));
    }

    
    /**
     * @param SearchProductRequest $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function search_uncatalogued(SearchProductRequest $request)
    {
        $departments = $this->productRepository->getDepartment();
        $product_statuses = ProductStatus::pluck('description as name','id');
        $request->flash();

        return view('backend.auth.product.search_uncatalogued')
        ->withDepartments($departments)
        ->with('product_statuses', $product_statuses)
        ->withProducts($this->productRepository->getFilteredPaginated($request, config('xtra.paged.products'), 'id', 'desc'));
    }


    /**
     * @param ManageProductRequest    $request
     *
     * @return mixed
     */
    public function create_uncatalogued(ManageProductRequest $request)
    {
        
        
        //$brands = $this->productRepository->getBrandLocal();
        //$brands = $this->productRepository->saveBrands();
        //$departments = $this->productRepository->saveDepartments();
        //dd();
        //$manufacturers = $this->productRepository->getManufacturers();
        $brands = Brand::pluck('name','id');


        //dd($manufacturers);
        $manufacturers = Manufacturer::pluck('name','id');
        $product_statuses = ProductStatus::pluck('name','id');

        $gtin_types = GtinType::pluck('name','id');
        //$stores = $this->productRepository->getStores();
        //$stores = $this->productRepository->getLocalStores();
        $stores = Store::pluck('name','id');

        //dd($product_statuses);
        $departments = $this->productRepository->getDepartment();
        
        //dd($departments);

        return view('backend.auth.product.create_uncatalogued')
        ->withManufacturers($manufacturers)
        ->withBrands($brands)
        ->with('departments', $departments)
        ->with('gtin_types', $gtin_types)
        ->with('stores', $stores);

    }


        /**
     * @param StoreUncataloguedProductRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store_uncatalogued(StoreUncataloguedProductRequest $request)
    {
        
        //dd($request);
        $param_image_front_1 = $request->has('image_front_1') ? $request->file('image_front_1') : false;
        $param_image_back_1 = $request->has('image_back_1') ? $request->file('image_back_1') : false;

        //dd($param_image_front_1);

        $product = $this->productRepository->store_uncatalogued($request->only(
            'product_name',
            'description',
            'status_id',
            'brand_id',
            'department_id',
            'store_id',
            'base_measurement_unit_id',
            'gtin_1',
            'image_front_1',
            'image_back_1'
        ),
        $param_image_front_1,
        $param_image_back_1

        );
        
        //dd($request);

        return redirect()->route('admin.auth.product.create.uncatalogued',$product)->withFlashSuccess(__('alerts.backend.products.created')); //TODO TRADUCIR
    }



        /**
     * @param ManageProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     */
    public function show_uncatalogued(ManageProductRequest $request, Product $product)
    {
  
  
        $manufacturers = Manufacturer::pluck('name','id');
        $product_statuses = ProductStatus::pluck('name','id');
        $packaging_measurement_units = PackagingMeasurementUnit::pluck('short_text_description as name','id');
        //$stores = $this->productRepository->getStores();
        //$stores = $this->productRepository->getLocalStores();
        $stores = Store::pluck('name','id');
        $brand = Brand::where('id','=',$product->brand_id )->get()->first();
        //dd($brand);
        $department = Department::where('department_id','=',$product->department_id )->get()->first();
        //dd($manufacturer);
        //$departments = $this->productRepository->getProductCategory('101019');
        
        //dd($department);

        return view('backend.auth.product.show_uncatalogued')
        ->withDepartment($department)
        ->withBrand($brand)
        ->withProduct($product)  
        ->with('packaging_measurement_units', $packaging_measurement_units)
        ->with('stores', $stores);



    }


            /**
     * @param ManageProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     */
    public function edit_uncatalogued(ManageProductRequest $request, Product $product)
    {

        //dd($manufacturers);
        $manufacturers = Manufacturer::pluck('name','id');
        $product_statuses = ProductStatus::pluck('name','id');
        //$stores = $this->productRepository->getStores();
        //$stores = $this->productRepository->getLocalStores();
        $stores = Store::pluck('name','id');
        $brands = Brand::pluck('name','id');
        //dd($manufacturers);
        $manufacturers = Manufacturer::pluck('name','id');
        $departments = $this->productRepository->getDepartment();
        //dd($manufacturer);
        //$departments = $this->productRepository->getProductCategory('101019');
        
        //dd($departments);

        return view('backend.auth.product.edit_uncatalogued')
        ->withManufacturers($manufacturers)
        ->withBrands($brands)
        ->withProduct($product)  
        ->with('departments', $departments)
        ->with('stores', $stores);



    }


    /**
     * @param StoreUncataloguedProductRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function update_uncatalogued(UpdateUncataloguedProductRequest $request, Product $product)
    {
        
        //dd($request);
        $param_image_front_1 = $request->has('image_front_1') ? $request->file('image_front_1') : false;
        $param_image_back_1 = $request->has('image_back_1') ? $request->file('image_back_1') : false;

        //dd($param_image_front_1);

        $this->productRepository->update_uncatalogued($product, $request->only(
            'product_name',
            'description',
            'status_id',
            'brand_id',
            'store_id',
            'department_id',
            'gtin_1',
            'image_front_1',
            'image_back_1'
        ),
        $param_image_front_1,
        $param_image_back_1

        );
        
        //dd($request);

        return redirect()->route('admin.auth.product.create.uncatalogued')->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }


    /**
     * @param ManageProductRequest    $request
     *
     * @return mixed
     */
    public function create(ManageProductRequest $request)
    {
        
        //dd('here');
        //$departments = $this->productRepository->retrieveNwsData();

        //$brands = $this->productRepository->getBrandLocal();
        $brands = $this->productRepository->getBrands();
        //$manufacturers = $this->productRepository->getManufacturers();
        //$brands = Brand::pluck('name','id');
        //$departments = $this->productRepository->saveDepartments();
        $departments = $this->productRepository->getDepartment();
        //dd();

        //dd($manufacturers);
        $manufacturers = Manufacturer::pluck('name','id');

        // foreach ($manufacturers as $manufacturer) {
            
        //     $userData = array('name' => $manufacturer, 'information' => '', 'country_id'=>165);
        //     Manufacturer::create($userData);

        // }


        $product_statuses = ProductStatus::pluck('name','id');
        $material_types = MaterialType::pluck('name','id');
        $purchase_organizations = PurchaseOrganization::pluck('name','id');
        $vendor_accounts = VendorAccount::pluck('name','id');
        $material_groups = MaterialGroup::pluck('name','id');
        $measurement_units = MeasurementUnit::pluck('short_text_description as name','id');
        $purchase_groups = PurchaseGroup::pluck('name','id');
        $gtin_types = GtinType::pluck('name','id');
        $material_tax_types = MaterialTaxType::pluck('name','id');
        $purchase_tax_codes = TaxCode::pluck('name','id');
        $iva_codes = IvaCode::pluck('name','id');
        //dd($product_statuses);
        //$departments = $this->productRepository->getProductCategory('101019');
        
        //dd($departments);

        return view('backend.auth.product.create')
        ->withManufacturers($manufacturers)
        ->with('material_types',$material_types)
        ->with('purchase_organizations',$purchase_organizations)
        ->with('purchase_groups',$purchase_groups)
        ->withBrands($brands)
        ->with('departments', $departments)
        ->with('vendor_accounts', $vendor_accounts)
        ->with('material_groups', $material_groups)
        ->with('measurement_units', $measurement_units)
        ->with('gtin_types', $gtin_types)
        ->with('material_tax_types', $material_tax_types)
        ->with('purchase_tax_codes', $purchase_tax_codes)
        ->with('iva_codes', $iva_codes)
        ->with('product_statuses', $product_statuses);
    }



    /**
     * @param ManageProductRequest    $request
     * @param Product                 $product
     *
     * @return mixed
     */
    public function create_step2(ManageProductRequest $request, Product $product)
    {

        $sections_count = 1;
        if($product->alt_measure_unit_stockkeep_unit_id_2){
            $sections_count++;
        }
        if($product->alt_measure_unit_stockkeep_unit_id_3){
            $sections_count++;
        }

        
        $measurement_units = MeasurementUnit::pluck('short_text_description as name','id');
        $packaging_measurement_units = PackagingMeasurementUnit::pluck('short_text_description as name','id');
        $update_route = 'admin.auth.product.update.step2';

        return view('backend.auth.product.create_step2')
        ->with('measurement_units', $measurement_units)
        ->with('sections_count', $sections_count)
        ->with('update_route', $update_route)
        ->with('packaging_measurement_units', $packaging_measurement_units)
        ->withProduct($product);

    }


    /**
     * @param ManageProductRequest    $request
     * @param Product                 $product
     *
     * @return mixed
     */
    public function create_step3(ManageProductRequest $request, Product $product)
    {

        //($status_id_for_role);
        $material_types = MaterialType::pluck('name','id');
        $material_groups = MaterialGroup::pluck('name','id');
        $purchase_organizations = PurchaseOrganization::pluck('name','id');
        $purchase_groups = PurchaseGroup::pluck('name','id');
        $departments = $this->productRepository->getDepartment();
        $measurement_units = MeasurementUnit::pluck('short_text_description as name','id');
        $material_tax_types = MaterialTaxType::pluck('name','id');

        return view('backend.auth.product.create_step3')
        ->with('material_types',$material_types)
        ->with('material_groups', $material_groups)
        ->with('purchase_organizations',$purchase_organizations)
        ->with('purchase_groups',$purchase_groups)
        ->with('departments',$departments)
        ->with('measurement_units', $measurement_units)
        ->with('material_tax_types', $material_tax_types)
        // ->withBrands($brands)
        // ->with('vendor_accounts', $vendor_accounts)
        // ->with('measurement_units', $measurement_units)
        // ->with('gtin_types', $gtin_types)
        // ->with('product_statuses', $product_statuses)
        // ->with('status_id_for_role', $status_id_for_role)

        ->withProduct($product);

    }

    /**
     * @param ManageProductRequest    $request
     * @param Product                 $product
     *
     * @return mixed
     */
    public function create_step4(ManageProductRequest $request, Product $product)
    {
       
        return view('backend.auth.product.create_step4')
        ->withProduct($product);

    }


     /**
     * @param String       $departmentId
     *
     * @return mixed
     */
    public function getCategory($departmentid)
    {
          $categories = $this->productRepository->getCategory($departmentid);
          return $categories;
    }

     /**
     * @param String       $categoryId
     *
     * @return mixed
     */
    public function getHierarchy($categoryId)
    {
          $hierarchies = $this->productRepository->getHierarchy($categoryId);
          return $hierarchies;
    }


    /**
     * @param StoreProductRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreProductRequest $request)
    {
        $product = $this->productRepository->create($request->only(
            'department_id',
            'product_name',
            'price',
            'suggested_price',
            'description',
            'brand_id',
            'manufacturer_id',
            'min_remain_shelf_life',
            'ind_include_discount',
            'discount_percentage',
            'ind_limited_time_discount',
            'discount_start_date',
            'discount_end_date',
            'old_material_number',
            'vendor_material_number',
            //'material_tax_type_id',
            'purchase_tax_code_id',
            'iva_code_id'
            
        ));

        //dd($request);

        return redirect()->route('admin.auth.product.create.step2',$product)->withFlashSuccess(__('alerts.backend.products.created')); //TODO TRADUCIR
    }

    /**
     * @param StoreStep2ProductRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function update_step2(StoreStep2ProductRequest $request, Product $product)
    {
        
        $this->productRepository->update_step2($product, $request->only(
            'base_measurement_unit_id',
            'gtin_1',
            'lower_level_measurement_unit_qty_1',
            'lower_level_pack_hierarchy_measurement_unit_id_1',
            'ind_measurement_unit_is_order_unit_1',
            'ind_measurement_unit_is_delivery_or_issue_unit_1',
            'alt_measure_unit_stockkeep_unit_id_1',
            'gtin_2',
            'lower_level_measurement_unit_qty_2',
            'lower_level_pack_hierarchy_measurement_unit_id_2',
            'ind_measurement_unit_is_order_unit_2',
            'ind_measurement_unit_is_delivery_or_issue_unit_2',
            'alt_measure_unit_stockkeep_unit_id_2',
            'gtin_3',
            'lower_level_measurement_unit_qty_3',
            'lower_level_pack_hierarchy_measurement_unit_id_3',
            'ind_measurement_unit_is_order_unit_3',
            'ind_measurement_unit_is_delivery_or_issue_unit_3',
            'alt_measure_unit_stockkeep_unit_id_3',
            'length',
            'width',
            'height',
            'weight',
            'volume'
        )
        );

        return redirect()->route('admin.auth.product.create.step3',$product)->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }



    /**
     * @param StoreStep3ProductRequest $request
     * @param Product $product
     * @return mixed
     * @throws \Throwable
     */
    public function update_step3(StoreStep3ProductRequest $request, Product $product)
    {
        

        //dd($request);
        $param_image_front_1 = $request->has('image_front_1') ? $request->file('image_front_1') : false;
        $param_image_back_1 = $request->has('image_back_1') ? $request->file('image_back_1') : false;


        //dd($param_image_front_1);

        $this->productRepository->update_step3($product, $request->only(
            'image_front_1',
            'image_back_1'
        ),
            $param_image_front_1,
            $param_image_back_1

        );
                
        return redirect()->route('admin.auth.product.index',$product)->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }


    /**
     * @param UpdateGeneralProductRequest $request
     * @param Product $product
     * @return mixed
     * @throws \Throwable
     */
    public function update_general(UpdateGeneralProductRequest $request, Product $product)
    {
        
        $this->productRepository->update_general($product, $request->only(
            'name',
            'description',
            'price',
            'suggested_price',
            'min_remain_shelf_life',
            'brand_id',
            'manufacturer_id',
            'ind_include_discount',
            'discount_percentage',
            'ind_limited_time_discount',
            'discount_start_date',
            'discount_end_date',
            'old_material_number',
            'vendor_material_number'
        ));
        
        //dd($request);

        return redirect()->route('admin.auth.product.edit',$product)->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }



    /**
     * @param UpdatePackagingProductRequest $request
     * @param Product $product
     * @return mixed
     * @throws \Throwable
     */
    public function update_packaging(UpdatePackagingProductRequest $request, Product $product)
    {
        
        //dd($request);
        $this->productRepository->update_packaging($product, $request->only(
            'base_measurement_unit_id',
            'gtin_1',
            'lower_level_measurement_unit_qty_1',
            'lower_level_pack_hierarchy_measurement_unit_id_1',
            'ind_measurement_unit_is_order_unit_1',
            'ind_measurement_unit_is_delivery_or_issue_unit_1',
            'alt_measure_unit_stockkeep_unit_id_1',
            'gtin_2',
            'lower_level_measurement_unit_qty_2',
            'lower_level_pack_hierarchy_measurement_unit_id_2',
            'ind_measurement_unit_is_order_unit_2',
            'ind_measurement_unit_is_delivery_or_issue_unit_2',
            'alt_measure_unit_stockkeep_unit_id_2',
            'gtin_3',
            'lower_level_measurement_unit_qty_3',
            'lower_level_pack_hierarchy_measurement_unit_id_3',
            'ind_measurement_unit_is_order_unit_3',
            'ind_measurement_unit_is_delivery_or_issue_unit_3',
            'alt_measure_unit_stockkeep_unit_id_3',
            'length',
            'width',
            'height',
            'weight',
            'volume'

        ));
        
        //dd($request);

        return redirect()->route('admin.auth.product.edit',$product)->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }

    /**
     * @param UpdateImagesProductRequest $request
     * @param Product $product
     * @return mixed
     * @throws \Throwable
     */
    public function update_images(UpdateImagesProductRequest $request, Product $product)
    {
        
        //dd($request);
        $param_image_front_1 = $request->has('image_front_1') ? $request->file('image_front_1') : false;
        $param_image_back_1 = $request->has('image_back_1') ? $request->file('image_back_1') : false;

        $this->productRepository->update_images($product, $request->only(
            'image_front_1',
            'image_back_1'
        ),
        $param_image_front_1,
        $param_image_back_1


        );
        
        //dd($request);

        return redirect()->route('admin.auth.product.edit',$product)->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }


    /**
     * @param UpdateDetailsProductRequest $request
     * @param Product $product
     * @return mixed
     * @throws \Throwable
     */
    public function update_details(UpdateDetailsProductRequest $request, Product $product)
    {
        //dd($request);
        $this->productRepository->update_details($product, $request->only(
            'ind_regular_vendor',
            'vendor_account_number',
            //'sku',
            'price',
            'material_type_id',
            //'material_group_id',
            'purchase_organization_id',
            'purchase_group_id',
            'material_tax_type_id',
            'purchase_tax_code_id',
            'iva_code_id',
            'order_price_measurement_unit_id',
            'department_id',
            'category_id',
            'hierarchy_id',
            'available_deliverable_from',
            'available_deliverable_until',
            'store_id',
            'ind_measurement_unit_is_delivery_or_issue_unit',
            'short_text_description'

        ));
        
        //dd($request);

        return redirect()->route('admin.auth.product.edit',$product)->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }


    /**
     * @param ManageProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     */
    public function show(ManageProductRequest $request, Product $product)
    {
        //$brand = Brand::find($product->brand_id);
        //$manufacturer = Manufacturer::find($product->manufacturer_id);

     
        $sections_count = 1;
        if($product->alt_measure_unit_stockkeep_unit_id_2){
            $sections_count++;
        }
        if($product->alt_measure_unit_stockkeep_unit_id_3){
            $sections_count++;
        }


        $show_edit_button = false;

        
        if (auth()->user()->hasRole('asistentecompra') && ($product->status_id == 2 ||  $product->status_id == 8 || $product->status_id == 9 ||  $product->status_id == 10))
        {
                $show_edit_button  = true;
        }

        //PENDIENTE DE REVISION POR GERENTE DE CATEGORIA 
        if(auth()->user()->hasRole('gerentecategoria') && ($product->status_id == 3)){
            $show_edit_button = true;
        }

        //PENDIENTE DE REVISION POR PLANIMETRIA
        if(auth()->user()->hasRole('planimetria') && ($product->status_id  == 4)){
            $show_edit_button = true;
        }

        //PENDIENTE DE REVISION POR FINANZAS
        if(auth()->user()->hasRole('finanzas') && ($product->status_id  == 5))
        {
            $show_edit_button = true; 
        }
        
        //$brands = Brand::pluck('name','id');
        //$manufacturers = Manufacturer::pluck('name','id');
        //dd($product->brand_id);
        //$manufacturers = $this->productRepository->getManufacturers();

        $brand = Brand::where('id','=',$product->brand_id )->get()->first();
        //dd($brand);
        $manufacturer = Manufacturer::where('id','=',$product->manufacturer_id )->get()->first();
        //dd($manufacturers);

        $workflow_statuses = WorkflowStatus::where('product_id', '=',$product->id)->orderBy('created_at', 'asc')->get();
        //dd($workflow_statuses);


        $loop_counter = 1;
        $elements_counter = count($workflow_statuses); 
        //dd($elements_counter);
        foreach ($workflow_statuses as $current_item) {
            


            if($loop_counter === $elements_counter) {

                $icon = 'fa-exclamation';
                $color = 'warning';
            }else {
                $icon = 'fa-check';
                $color = 'success';

            }

            if($current_item->status_id == 11) {
                $icon = 'fa-thumbs-up';
                $color = 'success';
            }

            if($current_item->status_id==7 || $current_item->status_id==8  || $current_item->status_id==9   || $current_item->status_id==10  ){
                $icon = 'fa-thumbs-down';
                $color = 'danger';
            }

            if($current_item->status_id == 1) {
                $icon = 'fa-cog';
                $color = 'danger';
            }

            $loop_counter = $loop_counter + 1;
            $current_item->color = $color;
            $current_item->icon = $icon;

        }

        $workflow_statuses_ordered = $workflow_statuses->reverse();

        $product_statuses = ProductStatus::pluck('name','id');
        $material_types = MaterialType::pluck('name','id');
        $purchase_organizations = PurchaseOrganization::pluck('name','id');
        $vendor_accounts = VendorAccount::pluck('name','id');
        $material_groups = MaterialGroup::pluck('name','id');
        $measurement_units = MeasurementUnit::pluck('short_text_description as name','id');
        $purchase_groups = PurchaseGroup::pluck('name','id');
        $gtin_types = GtinType::pluck('name','id');
        $material_tax_types = MaterialTaxType::pluck('name','id');
        $iva_codes = IvaCode::pluck('name','id');
        $tax_codes = TaxCode::pluck('name','id');
        $status_id_for_role = ProductStatus::pluck('name','id','role_id');
        $packaging_measurement_units = PackagingMeasurementUnit::pluck('short_text_description as name','id');
        $purchase_tax_codes = TaxCode::pluck('name','id');
        $departments = $this->productRepository->getDepartment();
 
        $department_id =0;
        $category_id =0;
        $categories = [];
        $hierarchies = [];

        if($product->department_id){
            $department_id = $product->department_id;
            $categories = $this->productRepository->getCategory($department_id);
        }

        if($product->category_id){
            $category_id=$product->category_id;
            $hierarchies = $this->productRepository->getHierarchy($category_id);
        }
        return view('backend.auth.product.show')
        ->with('material_types',$material_types)
        ->with('purchase_organizations',$purchase_organizations)
        ->with('purchase_groups',$purchase_groups)
        ->with('vendor_accounts', $vendor_accounts)
        ->with('material_groups', $material_groups)
        ->with('gtin_types', $gtin_types)
        ->with('material_tax_types', $material_tax_types)
        ->with('tax_codes', $tax_codes)
        ->with('iva_codes', $iva_codes)
        ->with('product_statuses', $product_statuses)
        ->with('status_id_for_role', $status_id_for_role)
        ->with('packaging_measurement_units', $packaging_measurement_units)
        ->with('measurement_units', $measurement_units)
        ->with('sections_count', $sections_count)
        ->with('purchase_tax_codes', $purchase_tax_codes)
        ->with('brand', $brand)
        ->with('manufacturer', $manufacturer)
        ->with('workflow_statuses', $workflow_statuses_ordered)
        ->with('departments', $departments)
        ->with('categories', $categories)
        ->with('hierarchies', $hierarchies)
        ->with('show_edit_button', $show_edit_button)
        ->with('product', $product);

        
        // $result = DB::table('products')->select('products.*', 'brands.name as brand_name', 'manufacturers.name as manufacturer_name', 
        // 'material_types.name as material_type_name', 'packaging_measurement_units.short_text_description as base_measurement_unit_name')
        // ->leftJoin('brands', 'products.brand_id', '=', 'brands.id')
        // ->leftJoin('manufacturers', 'products.manufacturer_id', '=', 'manufacturers.id')
        // ->leftJoin('material_types', 'products.material_type_id', '=', 'material_types.id')
        // ->leftJoin('packaging_measurement_units', 'products.base_measurement_unit_id', '=', 'packaging_measurement_units.id')
        // ->where('products.id', '=', $product->id)
        // ->first();
        // //dd($product);

        // return view('backend.auth.product.show')
        //     ->withProduct($result);
        //     //->withBrand($brand)
        //     //->withManufacturer($manufacturer);
    }

    /**
     * @param ManageProductRequest    $request
     * @param Product                 $product
     *
     * @return mixed
     */
    public function edit(ManageProductRequest $request, Product $product)
    {
        $sections_count = 1;
        $departments = $this->productRepository->getDepartment();
        $department_id =0;
        $category_id =0;
        $categories = [];
        $hierarchies = [];
        if($product->department_id){
            $department_id = $product->department_id;
            $categories = $this->productRepository->getCategory($department_id);
        }

        if($product->category_id){
            $category_id=$product->category_id;
            $hierarchies = $this->productRepository->getHierarchy($category_id);
        }
        
        $workflow_statuses = WorkflowStatus::where('product_id', '=',$product->id)->orderBy('created_at', 'asc')->get();


        $loop_counter = 1;
        $elements_counter = count($workflow_statuses); 
        //dd($elements_counter);
        foreach ($workflow_statuses as $current_item) {
            


            if($loop_counter === $elements_counter) {

                $icon = 'fa-exclamation';
                $color = 'warning';
            }else {
                $icon = 'fa-check';
                $color = 'success';

            }

            if($current_item->status_id == 11) {
                $icon = 'fa-thumbs-up';
                $color = 'success';
            }

            if($current_item->status_id==7 || $current_item->status_id==8  || $current_item->status_id==9   || $current_item->status_id==10  ){
                $icon = 'fa-thumbs-down';
                $color = 'danger';
            }

            if($current_item->status_id == 1) {
                $icon = 'fa-cog';
                $color = 'danger';
            }

            $loop_counter = $loop_counter + 1;
            $current_item->color = $color;
            $current_item->icon = $icon;

        }

        $workflow_statuses_ordered = $workflow_statuses->reverse();
        
        $brands = Brand::pluck('name','id');
        $manufacturers = $this->productRepository->getManufacturers();
        //$stores = $this->productRepository->getStores();
        //$stores = $this->productRepository->getLocalStores();
        $stores = Store::pluck('name','id');
        $product_statuses = ProductStatus::pluck('name','id');
        $material_types = MaterialType::pluck('name','id');
        $purchase_organizations = PurchaseOrganization::pluck('name','id');
        $vendor_accounts = VendorAccount::pluck('name','id');
        $material_groups = MaterialGroup::pluck('name','id');
        $measurement_units = MeasurementUnit::pluck('short_text_description as name','id');
        $purchase_groups = PurchaseGroup::pluck('name','id');
        $gtin_types = GtinType::pluck('name','id');
        $material_tax_types = MaterialTaxType::pluck('name','id');
        $iva_codes = IvaCode::pluck('name','id');
        $tax_codes = TaxCode::pluck('name','id');
        $status_id_for_role = ProductStatus::pluck('name','id','role_id');
        $packaging_measurement_units = PackagingMeasurementUnit::pluck('short_text_description as name','id');
        $purchase_tax_codes = TaxCode::pluck('name','id');
        
        if($product->alt_measure_unit_stockkeep_unit_id_2){
            $sections_count++;
        }
        if($product->alt_measure_unit_stockkeep_unit_id_3){
            $sections_count++;
        }

        $show_form = false;
        //dd($product->department_id );
        //show/hide approve form per role and current status

        if (auth()->user()->hasRole('asistentecompra') && ($product->status_id == 2 ||  $product->status_id == 8 || $product->status_id == 9 ||  $product->status_id == 10))
        {
            if(!is_null($product->department_id) &&  !is_null($product->category_id) && !is_null($product->hierarchy_id)){
                $show_form = true;
            }
        }

        //PENDIENTE DE REVISION POR GERENTE DE CATEGORIA 
        if(auth()->user()->hasRole('gerentecategoria') && ($product->status_id == 3)){
            $show_form = true;
        }

        //PENDIENTE DE REVISION POR PLANIMETRIA
        if(auth()->user()->hasRole('planimetria') && ($product->status_id  == 4)){
            $show_form = true;
        }

        //PENDIENTE DE REVISION POR FINANZAS
        if(auth()->user()->hasRole('finanzas') && ($product->status_id  == 5))
        {
            $show_form = true; 
        }


        $update_route = 'admin.auth.product.update.packaging';
        //($status_id_for_role);
        
        $sap_account_number = null;
        $user = User::find($product->user_id);
        if($user && $user->sapData && $user->sapData->sap_account_number)
        {
            $sap_account_number = $user->sapData->sap_account_number;
        }

        return view('backend.auth.product.edit')
        ->withManufacturers($manufacturers)
        ->with('material_types',$material_types)
        ->with('stores',$stores)
        ->with('purchase_organizations',$purchase_organizations)
        ->with('purchase_groups',$purchase_groups)
        ->withBrands($brands)
        ->with('vendor_accounts', $vendor_accounts)
        ->with('material_groups', $material_groups)
        ->with('measurement_units', $measurement_units)
        ->with('gtin_types', $gtin_types)
        ->with('material_tax_types', $material_tax_types)
        ->with('tax_codes', $tax_codes)
        ->with('iva_codes', $iva_codes)
        ->with('product_statuses', $product_statuses)
        ->with('status_id_for_role', $status_id_for_role)
        ->with('packaging_measurement_units', $packaging_measurement_units)
        ->with('sections_count', $sections_count)
        ->with('update_route', $update_route)
        ->with('purchase_tax_codes', $purchase_tax_codes)
        ->with('departments', $departments)
        ->with('categories', $categories)
        ->with('hierarchies', $hierarchies)
        ->with('workflow_statuses', $workflow_statuses_ordered)
        ->with('show_form',$show_form)
        ->with('sap_account_number', $sap_account_number)
        ->withProduct($product);

    }






    /**
     * @param UpdateProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        //dd($request);
        $this->productRepository->update($product, $request->only(
            'product_name',
            'description',
            'gtin_1',
            'lower_level_measurement_unit_qty_1',
            'lower_level_pack_hierarchy_measurement_unit_id_1',
            'ind_measurement_unit_is_order_unit_1',
            'ind_measurement_unit_is_delivery_or_issue_unit_1',
            'alt_measure_unit_stockkeep_unit_1',
            'image_front_1',
            'image_back_1',
            'gtin_type_id_1',
            'gtin_2',
            'lower_level_measurement_unit_qty_2',
            'lower_level_pack_hierarchy_measurement_unit_id_2',
            'ind_measurement_unit_is_order_unit_2',
            'ind_measurement_unit_is_delivery_or_issue_unit_2',
            'alt_measure_unit_stockkeep_unit_2',
            'image_front_2',
            'image_back_2',
            'gtin_type_id_2',
            'gtin_3',
            'lower_level_measurement_unit_qty_3',
            'lower_level_pack_hierarchy_measurement_unit_id_3',
            'ind_measurement_unit_is_delivery_or_issue_unit_3',
            'ind_measurement_unit_is_order_unit_3',
            'alt_measure_unit_stockkeep_unit_3',
            'image_front_3',
            'image_back_3',
            'gtin_type_id_3',
            'min_remain_shelf_life',
            'old_material_number',
            'vendor_material_number',
            'price',
            'base_measurement_unit_id',
            'brand_id',
            //'material_group_id',
            //'material_tax_type_id',
            'available_deliverable_from',
            'available_deliverable_until',
            'status_id'
      

//                `brand_id`,
//                 `material_type_id`,
//                  `material_group_id`,
//                   `price`, 
//                   `sku`,
//  `ind_gtin_type_automatic`,
//   `lower_level_pack_measurement_unit_id_1`,
//          `ind_measurement_unit_is_delivery_or_issue_unit`,
//           `min_remain_shelf_life`,
//            `old_material_number`,
//             `available_deliverable_from`,
//             `available_deliverable_until`,
//             `vendor_material_number`,
//             `ind_regular_vendor`,
//             `purchasing_group_id`,
//             `iva_code_id`,
//             `order_price_measurement_unit_id`,
//             `purchase_tax_code_id`,
//             `material_tax_code`
//             `language_id`,
//             `manufacturer_id`,
//             `department_id`,
//             `category_id`,
//             `user_id`,
//             `status_id`,
//             `material_tax_type_id`





        
        ),
            $request->has('image_original') ? $request->file('image_original') : false
        );

        return redirect()->route('admin.auth.product.index')->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }


   /**
     * @param UpdateProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update_assign(UpdateAssignProductRequest $request, Product $product)
    {
        //dd($request);
        $this->productRepository->update_assign($product, $request->only(
            'department_id'
        ));

        return redirect()->route('admin.dashboard')->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }



    /**
     * @param ManageProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function assign(ManageProductRequest $request, Product $product)
    {
        $departments = $this->productRepository->getDepartment();
        return view('backend.auth.product.assign')
        ->withDepartments($departments)
        ->withProduct($product);
    }



        /**
     * @param ManageProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     * @throws \Exception
     */
    public function status_update(ManageProductRequest $request, Product $product)
    {


        //dd($request);
        $comments = null;

        if(isset($request->approved)){
            $approved= $request->approved;
        }

        if(isset($request->comments)){
            $comments= $request->comments;
        }

        if($request->flag_approved==='1'){


            //dd($request->store_id);
            if(isset($request->store_id)){
                //$product->store_id = $request->store_id;
                $product->store()->sync((array)$request->store_id);
            }
    
            if(isset($request->payment_verified_at)){
                $product->payment_verified_at = $request->payment_verified_at;
            }
    
            if(isset($request->sap_document)){
                $product->sap_document = $request->sap_document;
            }
    
            $this->productRepository->update_status($product,true,$comments);
            
            //event(new ProductUpdated($product));

        }else{



            $this->productRepository->update_status($product, false, $comments);
            
            //event(new ProductUpdated($product));
    
        }


        return redirect()->route('admin.auth.product.edit',$product)->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }





        /**
     * @param ManageProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     * @throws \Exception
     */
    public function confirm_payment(ManageProductRequest $request, Product $product)
    {
        $this->productRepository->update_status($product,true);
        $this->productRepository->confirm_payment($product, $request->only(
            'payment_verified_at',
            'sap_document'));
        //event(new ProductUpdated($product));

        return redirect()->route('admin.auth.product.edit',$product)->withFlashSuccess(__('alerts.backend.products.updated')); //TODO TRADUCIR
    }


    /**
     * @param SearchProductRequest $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function search(SearchProductRequest $request)
    {
        $departments = $this->productRepository->getDepartment();
        $product_statuses = ProductStatus::pluck('description as name','id');
        $request->flash();

        return view('backend.auth.product.search')
        ->withDepartments($departments)
        ->with('product_statuses', $product_statuses)
        ->withProducts($this->productRepository->getFilteredPaginated($request, config('products.paged'), 'id', 'desc'));
    }





        /**
     * @param ManageProductRequest $request
     *
     * @return mixed
     * @throws \Exception
     */
    public function view_messages(ManageProductRequest $request)
    {
         return view('backend.auth.product.view_messages')
        ->withMessages($this->productRepository->getMessagesPaginated(config('xtra.paged.messages'), 'id', 'desc'));
    }



        /**
     * @param ManageProductRequest $request
     * @param Product              $product
     *
     * @return mixed
     */
    public function show_message(ManageProductRequest $request, Message $message)
    {
        $message = $this->productRepository->update_message($message);
        return view('backend.auth.product.show_message')
            ->withMessage($message);

    }

}
