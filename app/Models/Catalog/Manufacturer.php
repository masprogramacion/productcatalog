<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;


class Manufacturer extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'information', 'country_id',
    ];

}
