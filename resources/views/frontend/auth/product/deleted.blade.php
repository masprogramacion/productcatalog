@extends('frontend.layouts.app2')

@section('title', __('labels.frontend.access.users.management') . ' | ' . __('labels.frontend.access.users.edit'))

@section('breadcrumb-links')
    @include('frontend.auth.product.includes.breadcrumb-links')
@endsection

@section('content')


<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.frontend.access.products.management') }} <small class="text-muted">{{ __('labels.frontend.access.products.deleted') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('frontend.auth.product.includes.header-buttons')
            </div>
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('labels.frontend.access.products.table.name')</th>
                            <th>@lang('labels.general.status')</th>
                            <th>@lang('labels.general.description')</th>
                            <th>@lang('labels.general.measurement_unit')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>{!! $product->status_label !!}</td>
                                <td>{{ $product->description }}</td>
                                <td>{{ $product->short_text_description }}</td>
                                <td>{!! $product->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $products->total() !!} {{ trans_choice('labels.frontend.access.products.table.total', $products->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $products->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->

@endsection
