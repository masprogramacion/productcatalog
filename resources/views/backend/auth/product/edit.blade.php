@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.view'))

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('labels.backend.access.products.management')
                    <small class="text-muted">@lang('labels.backend.access.products.view')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-expanded="true"><i class="fas fa-file-alt"></i> @lang('labels.backend.access.products.tabs.titles.general')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#packaging" role="tab" aria-controls="packaging" aria-expanded="false"><i class="fas fa-box"></i> @lang('labels.backend.access.products.tabs.titles.packaging')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#images" role="tab" aria-controls="images" aria-expanded="false"><i class="fas fa-camera-retro"></i> @lang('labels.backend.access.products.tabs.titles.images')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-expanded="false"><i class="fas fa-edit"></i> @lang('labels.backend.access.products.tabs.titles.details')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#status" role="tab" aria-controls="status" aria-expanded="false"><i class="fas fa-check-circle"></i> @lang('labels.backend.access.products.tabs.titles.status')</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show" id="general" role="tabpanel" aria-expanded="true">
                    @include('backend.auth.product.edit.tabs.general')
                    </div><!--tab-->
                    <div class="tab-pane" id="packaging" role="tabpanel" aria-expanded="true">
                    @include('backend.auth.product.edit.tabs.packaging')
                    </div><!--tab-->
                    <div class="tab-pane" id="images" role="tabpanel" aria-expanded="true">
                    @include('backend.auth.product.edit.tabs.images')
                    </div><!--tab-->
                    <div class="tab-pane" id="details" role="tabpanel" aria-expanded="true">
                    @include('backend.auth.product.edit.tabs.details')
                    </div><!--tab-->
                    <div class="tab-pane" id="status" role="tabpanel" aria-expanded="true">
                    @include('backend.auth.product.edit.tabs.status')
                    </div><!--tab-->
                </div><!--tab-content-->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">

            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection
@push('after-scripts')
<script> 

//define counter
var sectionsCount = {{ $sections_count }};


$(document).ready(function(){

  $('.select2').select2({ width: '100%' });
  $('.select2m').select2({ 
    width: '100%',
    multiple: true });

  $("#checkBoxAll").click(function(){
      if($("#checkBoxAll").is(':checked') ){ //select all
        $("#store_id").find('option').prop("selected",true);
        $("#store_id").trigger('change');
      } else { //deselect all
        $("#store_id").find('option').prop("selected",false);
        $("#store_id").trigger('change');
      }
  });


  $("#checkBoxAllStatus").click(function(){
      if($("#checkBoxAllStatus").is(':checked') ){ //select all
        $("#store_id_status").find('option').prop("selected",true);
        $("#store_id_status").trigger('change');
      } else { //deselect all
        $("#store_id_status").find('option').prop("selected",false);
        $("#store_id_status").trigger('change');
      }
  });



  var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

  $('#payment_verified_at').datepicker({
    format: '{{ config('app.date_format_js') }}',
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fortawesome',
    maxDate: today,
  });


  $('#discount_start_date').datepicker({

            format: '{{ config('app.date_format_js') }}' ,
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fortawesome',
            minDate: today,
            maxDate: function () {
                return $('#discount_end_date').val();
            }
  });

  $('#discount_end_date').datepicker({
    format: '{{ config('app.date_format_js') }}',
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fortawesome',
            minDate: function () {
                return $('#discount_start_date').val();
            } 
  });

    $('#btnSubmitGeneral').on("click", validateGeneralForm);
    $('#btnSubmitImages').on("click", validateImagesForm); 
    $('#btnSubmitDetails').on("click", validateDetailsForm);   
    $('#btnApproveProduct').on("click", validateStatusForm); 
    $('#btnRejectProduct').on("click", validateStatusForm);  


    $('#ind_include_discount').on('click', function() {
        var checked=$(this).prop('checked');
        if (checked) {
            $('#discount_percentage').attr('required','required');
        }else {
            $('#discount_percentage').removeAttr('required');
            
        }

    });

    $('#ind_limited_time_discount').on('click', function() {
        var checked=$(this).prop('checked');
        if (checked) {
            $('#discount_start_date').attr('required','required');
        }else {
            $('#discount_start_date').removeAttr('required');
            
        }

        if (checked) {
            $('#discount_end_date').attr('required','required');
        }else {
            $('#discount_end_date').removeAttr('required');
        }
    });

    function readURL(input,previewId) {
    if (input.files && input.files[0]) {

        var a=(input.files[0].size);

        if(a/(1024*1024) > 2){ // make it in MB so divide by 1024*1024
            alert('La imagen debe ser menor a 2 MB');
            return false;
        }


        var reader = new FileReader();
        
        reader.onload = function (e) {
            $(previewId).attr('src', e.target.result);

        }
        reader.readAsDataURL(input.files[0]);
    }
  }

        $("#image_front_1").change(function(){
           readURL(this, '#preview_image_front_1');
        });

        $("#image_back_1").change(function(){
           readURL(this, '#preview_image_back_1');
        });

$('#available_deliverable_from').datepicker({

          format: '{{ config('app.date_format_js') }}' ,
          uiLibrary: 'bootstrap4',
          iconsLibrary: 'fontawesome',
          minDate: today,
          maxDate: function () {
              return $('#available_deliverable_until').val();
          }
        });
$('#available_deliverable_until').datepicker({
  format: '{{ config('app.date_format_js') }}',
  uiLibrary: 'bootstrap4',
  iconsLibrary: 'fontawesome',
          minDate: function () {
              return $('#available_deliverable_from').val();
          } 
});


            function resetSelect() {
              $('#category_id').empty();
              $('#hierarchy_id').empty();
              $('#category_id').prop("disabled", true);
              $('#hierarchy_id').prop("disabled", true);

            }

            $('#department_id').on('change', function (e) {
                resetSelect();
                var department_id = e.target.value;
                categoryUpdate(department_id);
            });
            // Ajax Request for categories
            function categoryUpdate(department_id) {
                $.get('{{ url('admin/auth/product/category') }}/' + department_id , function (data) {
                    $('#category_id').empty();
                    $.each(data, function(key, value) {   
                        $('#category_id')
                            .append($("<option></option>")
                                        .attr("value",key)
                                        .text(value)); 
                    });
                    $('#category_id').val('');
                    $('#category_id').prop("disabled", false);

                });
            }

            $('#category_id').on('change', function (e) {
                $('#hierarchy_id').empty();
                $('#hierarchy_id').prop("disabled", true);
                var category_id = e.target.value;
                hierarchyUpdate(category_id);
            });

            // Ajax Request for hierarchies
            function hierarchyUpdate(category_id) {
                $.get('{{ url('admin/auth/product/hierarchy') }}/' + category_id , function (data) {
                    $('#hierarchy_id').empty();
                    $.each(data, function(key, value) {   
                        $('#hierarchy_id')
                            .append($("<option></option>")
                                        .attr("value",key)
                                        .text(value)); 
                    });
                    $('#hierarchy_id').val('');
                    $('#hierarchy_id').prop("disabled", false);

                })
                .done(function() {
                    //alert( "second success" );
                })
                .fail(function() {
                    //alert( "error" );
                })
                .always(function() {
                    //alert( "finished" );
                });
            }


    function validateGeneralForm() {
   
   var form = document.getElementById("form-update-general-product");
   form.classList.add("was-validated");


   let product_name = document.querySelector("#name").checkValidity();
   let description = document.querySelector("#description").checkValidity();
   let price = document.querySelector("#price").checkValidity();
   let suggested_price = document.querySelector("#suggested_price").checkValidity();
   let min_remain_shelf_life = document.querySelector("#min_remain_shelf_life").checkValidity();
   let old_material_number = document.querySelector("#old_material_number").checkValidity();
   let vendor_material_number = document.querySelector("#vendor_material_number").checkValidity();

   let no_errors  = product_name && description && price && suggested_price && min_remain_shelf_life && old_material_number && vendor_material_number;
   
   //if there is a discount, validate value
   let no_discount_errors = true;
   if($('#ind_include_discount').is(':checked')){
       no_discount_errors = document.querySelector("#discount_percentage").checkValidity();
   }
   //if there is a limited discount, validate start and end date
   let no_date_errors = true;
   if($('#ind_limited_time_discount').is(':checked')){
     let discount_start_date = document.querySelector("#discount_start_date").checkValidity();
     let discount_end_date = document.querySelector("#discount_end_date").checkValidity();
     if(discount_start_date && discount_end_date){
       let tmp_discount_start_date = document.querySelector("#discount_start_date");
       //alert(tmp_discount_start_date);
     }else {
       no_date_errors = false;
     }
   }


   if (no_errors && no_discount_errors && no_date_errors) {
     $('#form-update-general-product').submit();
     $(this).attr('disabled','disabled');
   }else {

         //var formElements = document.getElementById('form-create-product').querySelectorAll('textarea, input:not([type=hidden])');
         var errorElements = document.getElementById('form-update-general-product').querySelectorAll('input.form-control:invalid');
         if(errorElements.length > 0){
           //console.log(errorElements);
           //var scrollTop = $(window).scrollTop();
           var elementOffset = $(errorElements[0]).offset().top - 100;
           //console.log(elementOffset);
           //var currentElementOffset = (elementOffset - scrollTop);
           //console.log(currentElementOffset);
           $('html, body').animate({
               scrollTop: elementOffset
             }, 2000, function() {
               $(errorElements[0]).focus();
           });
         }


   }
 }





function validateImagesForm() {

//if there is a discount, validate value
let no_errors = true;
let image_front_1 = document.querySelector('#image_front_1').checkValidity();
let image_back_1 = document.querySelector('#image_back_1').checkValidity();

no_errors = ( image_front_1 && image_back_1 );
    if (no_errors) {
        //console.log('valid');
        $('#form-update-images-product').submit();
        $(this).attr('disabled','disabled');
    }
}


function validateStatusForm() {


    var confirm = true;
    
    var button_id = $(this).prop('id') ;
    //alert(button_id);
    if(button_id === 'btnRejectProduct'){
        confirm = false;
    }

    var form = document.getElementById("form-update-status-product");
    form.classList.add("was-validated");
    //if there is a discount, validate value
    let no_errors = true;
    let comments = document.querySelector('#comments').checkValidity();
    //alert(comments);


    let stores = true;
    let payment_verified_at = true;
    let sap_document = true;

    let exist_stores = document.querySelector('#stores');
    let exists_payment_verified_at  = document.querySelector('#payment_verified_at');
    let exists_sap_document = document.querySelector('#sap_document');

    if(exist_stores){
        stores = document.querySelector('#comments').checkValidity()
    }

    if(exists_payment_verified_at){
        payment_verified_at = document.querySelector('#payment_verified_at').checkValidity()
    }

    if(exists_sap_document){
        sap_document = document.querySelector('#sap_document').checkValidity()
    }

    no_errors = (comments && stores && payment_verified_at && sap_document);

    //what button was clicked??

    //alert(no_errors);
    if (no_errors) {
        //console.log('valid');
        //$('#form-update-status-product').submit();
        if(confirm){
            $("#approve-modal").modal("show");
        }else{
            $("#reject-modal").modal("show");
        }
    }

}




function validateDetailsForm() {

    var form = document.getElementById("form-update-details-product");
    form.classList.add("was-validated");


    //let sku = document.querySelector('#sku').checkValidity();
    let price = document.querySelector('#price').checkValidity();
    let material_type_id = document.querySelector('#material_type_id').checkValidity();
    //let material_group_id = document.querySelector('#material_group_id').checkValidity();
    let purchase_organization_id = document.querySelector('#purchase_organization_id').checkValidity();
    let purchase_group_id = document.querySelector('#purchase_group_id').checkValidity();
    //let material_tax_type_id = document.querySelector('#material_tax_type_id').checkValidity();
    let purchase_tax_code_id = document.querySelector('#purchase_tax_code_id').checkValidity();
    let iva_code_id = document.querySelector('#iva_code_id').checkValidity();
    let department_id = document.querySelector('#department_id').checkValidity();
    let category_id = document.querySelector('#category_id').checkValidity();
    let hierarchy_id = document.querySelector('#hierarchy_id').checkValidity();
    let order_price_measurement_unit_id = document.querySelector('#order_price_measurement_unit_id').checkValidity();
    
    //let available_deliverable_from = document.querySelector('#available_deliverable_from').checkValidity();
    //let available_deliverable_until = document.querySelector('#available_deliverable_until').checkValidity();
    let short_text_description = document.querySelector('#short_text_description').checkValidity();


    //if there is a discount, validate value
    let no_errors = true;
    no_errors = (price && material_type_id  && purchase_organization_id && purchase_tax_code_id && iva_code_id
    && department_id && category_id &&  hierarchy_id && short_text_description && order_price_measurement_unit_id);


    if (no_errors) {
        //console.log('valid');
        $('#form-update-details-product').submit();
        $(this).attr('disabled','disabled');
    }else {


        //var formElements = document.getElementById('form-create-product').querySelectorAll('textarea, input:not([type=hidden])');
        var errorElements = document.getElementById('form-update-details-product').querySelectorAll('input.form-control:invalid');

        if(errorElements.length > 0){
        //console.log(errorElements);
        //var scrollTop = $(window).scrollTop();
        var elementOffset = $(errorElements[0]).offset().top - 100;
        //console.log(elementOffset);
        //var currentElementOffset = (elementOffset - scrollTop);
        //console.log(currentElementOffset);
        $('html, body').animate({
            scrollTop: elementOffset
            }, 2000, function() {
            $(errorElements[0]).focus();
        });
        }

    }
}

    $('#approve-modal button.ok').off().on('click', function() {
        // close window
        $('#approve_modal').modal('hide');
        $('#flag_approved').val('1');
        $('#form-update-status-product').submit();
        $(this).attr('disabled','disabled');

    });

    $('#approve-modal button.cancel').off().on('click', function() {
        // close window
        $('#approve_modal').modal('hide');

    });

    $('#reject-modal button.ok').off().on('click', function() {
        // close window
        $('#reject_modal').modal('hide');
        $('#flag_approved').val('0');
        $('#form-update-status-product').submit();
        $(this).attr('disabled','disabled');

    });

     $('#reject-modal button.cancel').off().on('click', function() {
        // close window
        $('#modal-custom-confirmation').modal('hide');
        // callback

    });




});




</script>
<script type="text/javascript" src="{{ URL::asset ('js/custom.js') }}"></script>  
@endpush
