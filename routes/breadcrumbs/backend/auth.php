<?php

require __DIR__.'/auth/user.php';
require __DIR__.'/auth/role.php';
require __DIR__.'/auth/product.php';
require __DIR__.'/auth/vendor.php';
require __DIR__.'/auth/catalog.php';
require __DIR__.'/auth/department.php';
