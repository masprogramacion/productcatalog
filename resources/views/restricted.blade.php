@extends('backend.layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">admin Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in as an admin

                   <a class="dropdown-item" href="{{ route('frontend.auth.superadmin.logout') }}">
                         <i class="fas fa-lock"></i> @lang('navs.general.logout')
                   </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<i class="fa fa-edit"></i>Form Elements
<div class="card-header-actions">
<a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true">
<i class="icon-arrow-up"></i>
</a>
</div>
</div>
<div class="card-body collapse show" id="collapseExample">
<form class="form-horizontal">
<div class="form-group">
<label class="col-form-label" for="prependedInput">Prepended text</label>
<div class="controls">
<div class="input-prepend input-group">
<div class="input-group-prepend">
<span class="input-group-text">@</span>
</div>
<input class="form-control" id="prependedInput" size="16" type="text">
</div>
<p class="help-block">Here's some help text</p>
</div>
</div>
<div class="form-group">
<label class="col-form-label" for="appendedInput">Appended text</label>
<div class="controls">
<div class="input-group">
<input class="form-control" id="appendedInput" size="16" type="text">
<div class="input-group-append">
<span class="input-group-text">.00</span>
</div>
</div>
<span class="help-block">Here's more help text</span>
</div>
</div>
<div class="form-group">
<label class="col-form-label" for="appendedPrependedInput">Append and prepend</label>
<div class="controls">
<div class="input-prepend input-group">
<div class="input-group-prepend">
<span class="input-group-text">$</span>
</div>
<input class="form-control" id="appendedPrependedInput" size="16" type="text">
<div class="input-group-append">
<span class="input-group-text">.00</span>
</div>
</div>
</div>
</div>
<div class="form-group">
<label class="col-form-label" for="appendedInputButton">Append with button</label>
<div class="controls">
<div class="input-group">
<input class="form-control" id="appendedInputButton" size="16" type="text">
<span class="input-group-append">
<button class="btn btn-secondary" type="button">Go!</button>
</span>
</div>
</div>
</div>
<div class="form-group">
<label class="col-form-label" for="appendedInputButtons">Two-button append</label>
<div class="controls">
<div class="input-group">
<input class="form-control" id="appendedInputButtons" size="16" type="text">
<span class="input-group-append">
<button class="btn btn-secondary" type="button">Search</button>
<button class="btn btn-secondary" type="button">Options</button>
</span>
</div>
</div>
</div>
<div class="form-actions">
<button class="btn btn-primary" type="submit">Save changes</button>
<button class="btn btn-secondary" type="button">Cancel</button>
</div>
</form>
</div>
</div>
</div>

</div>



<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<i class="fa fa-align-justify"></i> Combined All Table</div>
<div class="card-body">
<table class="table table-responsive-sm table-bordered table-striped table-sm">
<thead>
<tr>
<th>Username</th>
<th>Date registered</th>
<th>Role</th>
<th>Status</th>
</tr>
</thead>
<tbody>
<tr>
<td>Vishnu Serghei</td>
<td>2012/01/01</td>
<td>Member</td>
<td>
<span class="badge badge-success">Active</span>
</td>
</tr>
<tr>
<td>Zbyněk Phoibos</td>
<td>2012/02/01</td>
<td>Staff</td>
<td>
<span class="badge badge-danger">Banned</span>
</td>
</tr>
<tr>
<td>Einar Randall</td>
<td>2012/02/01</td>
<td>Admin</td>
<td>
<span class="badge badge-secondary">Inactive</span>
</td>
</tr>
<tr>
<td>Félix Troels</td>
<td>2012/03/01</td>
<td>Member</td>
<td>
<span class="badge badge-warning">Pending</span>
</td>
</tr>
<tr>
<td>Aulus Agmundr</td>
<td>2012/01/21</td>
<td>Staff</td>
<td>
<span class="badge badge-success">Active</span>
</td>
</tr>
</tbody>
</table>
<nav>
<ul class="pagination">
<li class="page-item">
<a class="page-link" href="#">Prev</a>
</li>
<li class="page-item active">
<a class="page-link" href="#">1</a>
</li>
<li class="page-item">
<a class="page-link" href="#">2</a>
</li>
<li class="page-item">
<a class="page-link" href="#">3</a>
</li>
<li class="page-item">
<a class="page-link" href="#">4</a>
</li>
<li class="page-item">
<a class="page-link" href="#">Next</a>
</li>
</ul>
</nav>
</div>
</div>
</div>

</div>



</div>

@endsection