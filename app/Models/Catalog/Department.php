<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;
use App\Models\Catalog\Traits\Relationship\DepartmentRelationship;

class Department extends Model
{
    use DepartmentRelationship;
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'department_id',
        'department_name'
    ];



}
