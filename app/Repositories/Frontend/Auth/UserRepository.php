<?php

namespace App\Repositories\Frontend\Auth;

use Carbon\Carbon;
use App\Models\Auth\User;
use App\Models\Auth\File;
use App\Models\Auth\Profile;
use App\Models\Auth\BankAccount;
use Illuminate\Http\UploadedFile;
use App\Models\Auth\SocialAccount;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Events\Frontend\Auth\UserConfirmed;
use App\Events\Frontend\Auth\UserProviderRegistered;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;

/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * @param $token
     *
     * @return bool|\Illuminate\Database\Eloquent\Model
     */
    public function findByPasswordResetToken($token)
    {
        foreach (DB::table(config('auth.passwords.users.table'))->get() as $row) {
            if (password_verify($token, $row->token)) {
                return $this->getByColumn($row->email, 'email');
            }
        }

        return false;
    }

    /**
     * @param $uuid
     *
     * @return mixed
     * @throws GeneralException
     */
    public function findByUuid($uuid)
    {
        $user = $this->model
            ->uuid($uuid)
            ->first();

        if ($user instanceof $this->model) {
            return $user;
        }

        throw new GeneralException(__('exceptions.backend.access.users.not_found'));
    }

    /**
     * @param $code
     *
     * @return mixed
     * @throws GeneralException
     */
    public function findByConfirmationCode($code)
    {
        $user = $this->model
            ->where('confirmation_code', $code)
            ->first();

        if ($user instanceof $this->model) {
            return $user;
        }

        throw new GeneralException(__('exceptions.backend.access.users.not_found'));
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model|mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $user = parent::create([
                'first_name'        => $data['first_name'],
                'last_name'         => $data['last_name'],
                'email'             => $data['email'],
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'active'            => 1,
                'password'          => $data['password'],
                                    // If users require approval or needs to confirm email
                'confirmed'         => config('access.users.requires_approval') || config('access.users.confirm_email') ? 0 : 1,
            ]);

            if ($user) {
                /*
                 * Add the default site role to the new user
                 */
                $user->assignRole(config('access.users.default_role'));
            }

            /*
             * If users have to confirm their email and this is not a social account,
             * and the account does not require admin approval
             * send the confirmation email
             *
             * If this is a social account they are confirmed through the social provider by default
             */
            if (config('access.users.confirm_email')) {
                // Pretty much only if account approval is off, confirm email is on, and this isn't a social account.
                $user->notify(new UserNeedsConfirmation($user->confirmation_code, $user->email));
            }

            /*
             * Return the user object
             */
            return $user;
        });
    }

    /**
     * @param       $id
     * @param array $input
     * @param bool|UploadedFile  $document
     *
     * @return array|bool
     * @throws GeneralException
     */
    public function updateGeneral($id, array $input, $document = false)
    {
        $user = $this->getById($id);
        $user->first_name = $input['first_name'];
        $user->last_name = $input['last_name'];
        $user->name = $input['name'];
        //dd($user);

        // $profile = Profile::updateOrCreate(
        //     [
        //         'user_id' => $user->id
        //     ],
        //     [
        //         'cedula' => $input['cedula'],
        //         'user_id' => $user->id,
        //         'user_type_id'=> $user->user_type_id,
        //         'ruc' => $input['ruc'],
        //         'dv' => $input['dv'],
        //         'phone' => $input['phone'],
        //         'fax' => $input['fax'],
        //         'payment_condition_days' => $input['payment_condition_days'],
        //         'payment_bonus_percentage' => $input['payment_bonus_percentage'],
        //         'person_type_id' => $input['person_type_id'],
        //         'payment_type_id' => $input['payment_type_id'],
        //         'delivery_time_days' => $input['delivery_time_days']

        //     ]
        // );

        // $bankAccount = BankAccount::updateOrCreate(
        //     [
        //         'user_id' => $user->id
        //     ],
        //     [
        //         'bank_id' => $input['bank_id'],
        //         'account_number' => $input['account_number'],
        //         'bank_account_type_id' => $input['bank_account_type_id'],
        //         'account_fullname' => $input['account_fullname'],
        //         'noreturn_percentage' => $input['noreturn_percentage']
        //     ]
        // );




        // // Upload profile image if necessary
        // if ($image) {
        //     $user->avatar_location = $image->store('/avatars', 'public');
        // } else {
        //     // No image being passed
        //     if ($input['avatar_type'] == 'storage') {
        //         // If there is no existing image
        //         if (! strlen(auth()->user()->avatar_location)) {
        //             throw new GeneralException('You must supply a profile image.');
        //         }
        //     } else {
        //         // If there is a current image, and they are not using it anymore, get rid of it
        //         if (strlen(auth()->user()->avatar_location)) {
        //             Storage::disk('public')->delete(auth()->user()->avatar_location);
        //         }

        //         $user->avatar_location = null;
        //     }
        // }




        

        // Upload document

        if($document) {

            $time_str = "_".time(); 
            $documentName = $user->id.$time_str.'_aviso_oper.'.$document->getClientOriginalExtension();
            $documentOrigName = $document->getClientOriginalName();
            $url = $document->move(public_path('storage/documents'), $documentName,$documentOrigName);
            if($url){

                $file = File::updateOrCreate(
                    [
                        'user_id' => $user->id
                    ],
                    [
                        'title' => $documentOrigName,
                        'url' => 'documents/'.$documentName
                    ]
                );

            }
            
        }




        if ($user->canChangeEmail()) {
            //Address is not current address so they need to reconfirm
            if ($user->email != $input['email']) {
                //Emails have to be unique
                if ($this->getByColumn($input['email'], 'email')) {
                    throw new GeneralException(__('exceptions.frontend.auth.email_taken'));
                }

                // Force the user to re-verify his email address if config is set
                if (config('access.users.confirm_email')) {
                    $user->confirmation_code = md5(uniqid(mt_rand(), true));
                    $user->confirmed = 0;
                    $user->notify(new UserNeedsConfirmation($user->confirmation_code, $user->email));
                }
                $user->email = $input['email'];
                $updated = $user->save();

                // Send the new confirmation e-mail

                return [
                    'success' => $updated,
                    'email_changed' => true,
                ];
            }
        }

        return $user->save();
    }

    /**
     * @param       $id
     * @param array $input
     *
     * @return array|bool
     * @throws GeneralException
     */
    public function updateProfile($id, array $input)
    {

        $user = $this->getById($id);

        $profile = Profile::updateOrCreate(
            [
                'user_id' => $user->id
            ],
            [
                'cedula' => $input['cedula'],
                'user_id' => $user->id,
                'user_type_id'=> $user->user_type_id,
                'ruc' => $input['ruc'],
                'dv' => $input['dv'],
                'phone' => $input['phone'],
                'fax' => $input['fax'],
                'payment_condition_days' => $input['payment_condition_days'],
                'payment_bonus_percentage' => $input['payment_bonus_percentage'],
                'person_type_id' => $input['person_type_id'],
                'payment_type_id' => $input['payment_type_id'],
                'delivery_time_days' => $input['delivery_time_days']

            ]
        );

        return $user->save();
    }


    /**
     * @param       $id
     * @param array $input
     *
     * @return array|bool
     * @throws GeneralException
     */
    public function updateBankAccount($id, array $input)
    {
        $user = $this->getById($id);
        
        $bankAccount = BankAccount::updateOrCreate(
            [
                'user_id' => $user->id
            ],
            [
                'bank_id' => $input['bank_id'],
                'account_number' => $input['account_number'],
                'bank_account_type_id' => $input['bank_account_type_id'],
                'account_fullname' => $input['account_fullname'],
                'noreturn_percentage' => $input['noreturn_percentage']
            ]
        );


        return $user->save();
    }




    /**
     * @param      $input
     * @param bool $expired
     *
     * @return bool
     * @throws GeneralException
     */
    public function updatePassword($input, $expired = false)
    {
        $user = $this->getById(auth()->id());

        if (Hash::check($input['old_password'], $user->password)) {
            if ($expired) {
                $user->password_changed_at = Carbon::now()->toDateTimeString();
            }

            return $user->update(['password' => $input['password']]);
        }

        throw new GeneralException(__('exceptions.frontend.auth.password.change_mismatch'));
    }

    /**
     * @param $code
     *
     * @return bool
     * @throws GeneralException
     */
    public function confirm($code)
    {
        $user = $this->findByConfirmationCode($code);

        if ($user->confirmed == 1) {
            throw new GeneralException(__('exceptions.frontend.auth.confirmation.already_confirmed'));
        }

        if ($user->confirmation_code == $code) {
            $user->confirmed = 1;

            event(new UserConfirmed($user));

            return $user->save();
        }

        throw new GeneralException(__('exceptions.frontend.auth.confirmation.mismatch'));
    }

    /**
     * @param $data
     * @param $provider
     *
     * @return mixed
     * @throws GeneralException
     */
    public function findOrCreateProvider($data, $provider)
    {
        // User email may not provided.
        $user_email = $data->email ?: "{$data->id}@{$provider}.com";

        // Check to see if there is a user with this email first.
        $user = $this->getByColumn($user_email, 'email');

        /*
         * If the user does not exist create them
         * The true flag indicate that it is a social account
         * Which triggers the script to use some default values in the create method
         */
        if (! $user) {
            // Registration is not enabled
            if (! config('access.registration')) {
                throw new GeneralException(__('exceptions.frontend.auth.registration_disabled'));
            }

            // Get users first name and last name from their full name
            $nameParts = $this->getNameParts($data->getName());

            $user = parent::create([
                'first_name'  => $nameParts['first_name'],
                'last_name'  => $nameParts['last_name'],
                'email' => $user_email,
                'active' => 1,
                'confirmed' => 1,
                'password' => null,
                'avatar_type' => $provider,
            ]);

            event(new UserProviderRegistered($user));
        }

        // See if the user has logged in with this social account before
        if (! $user->hasProvider($provider)) {
            // Gather the provider data for saving and associate it with the user
            $user->socialaccounts()->save(new SocialAccount([
                'provider'    => $provider,
                'provider_id' => $data->id,
                'token'       => $data->token,
                'avatar'      => $data->avatar,
            ]));
        } else {
            // Update the users information, token and avatar can be updated.
            $user->socialaccounts()->update([
                'token'       => $data->token,
                'avatar'      => $data->avatar,
            ]);

            $user->avatar_type = $provider;
            $user->update();
        }

        // Return the user object
        return $user;
    }

    /**
     * @param $fullName
     *
     * @return array
     */
    protected function getNameParts($fullName)
    {
        $parts = array_values(array_filter(explode(' ', $fullName)));
        $size = count($parts);
        $result = [];

        if (empty($parts)) {
            $result['first_name'] = null;
            $result['last_name'] = null;
        }

        if (! empty($parts) && $size == 1) {
            $result['first_name'] = $parts[0];
            $result['last_name'] = null;
        }

        if (! empty($parts) && $size >= 2) {
            $result['first_name'] = $parts[0];
            $result['last_name'] = $parts[1];
        }

        return $result;
    }
}
