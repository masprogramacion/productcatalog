{{ html()->modelForm($bankAccount, 'PATCH', route('frontend.user.info.update.bank_account'))->class('form-horizontal')->open() }}
<div class="row">

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>Cuenta Bancaria</strong>
                    <small>Información</small>
                </div>

                <div class="card-body">

                    <div class="form-group">
                        {{ html()->label(__('labels.frontend.user.info.tabs.content.bank_account.bank_id'))->for('bank_id') }}
                        {{ html()->select('bank_id',$banks)
                        ->class('form-control')
                        ->placeHolder('Seleccione un valor')}}
                    </div>

                    <div class="form-group">
                        {{ html()->label(__('labels.frontend.user.info.tabs.content.bank_account.account_number'))->for('account_number') }}
                        {{ html()->text('account_number')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.frontend.account_number'))
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>


                    <div class="form-group">
                        {{ html()->label(__('labels.frontend.user.info.tabs.content.bank_account.bank_account_type_id'))->for('bank_account_type_id') }}
                        {{ html()->select('bank_account_type_id',$bankAccountTypes)
                            ->class('form-control')
                            ->placeHolder('Seleccione un valor')}}
                    </div>

                    <div class="form-group">
                        {{ html()->label(__('labels.frontend.user.info.tabs.content.bank_account.account_fullname'))->for('account_fullname') }}
                        {{ html()->text('account_fullname')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.frontend.account_fullname'))
                        ->attribute('maxlength', 191)
                        ->required() }}
                    </div>

                    <div class="form-group">
                        {{ html()->label(__('labels.frontend.user.info.tabs.content.bank_account.noreturn_percentage'))->for('noreturn_percentage') }}
                        {{ html()->input('number','noreturn_percentage')
                            ->class('form-control')
                                ->attributes(['min'=>'1', 'max'=>'100'])
                                ->required()  
                                ->placeholder(__('validation.attributes.frontend.noreturn_percentage'))
                            }}
                    </div>

                </div>
            </div>
        </div>

</div>



    <div class="row">
        <div class="col">
            <div class="form-group mb-0 clearfix">
                {{ form_submit(__('labels.general.buttons.update')) }}
            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->
{{ html()->closeModelForm() }}