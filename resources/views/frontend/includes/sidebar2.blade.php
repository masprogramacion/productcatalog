<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.frontend.sidebar.general')
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('dashboard')) }}" href="{{ route('frontend.user.dashboard') }}">
                    <i class="nav-icon icon-speedometer"></i> @lang('menus.frontend.sidebar.dashboard')
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('product')) }}" href="{{ route('frontend.auth.product.index') }}">
                    <i class="nav-icon icon-basket"></i>@lang('menus.frontend.sidebar.products')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('product*')) }}" href="{{ route('frontend.auth.product.message.index') }}">
                <i class="nav-icon icon-envelope"></i>
                    @lang('menus.frontend.products.messages')

                    @if (  $logged_in_user->unread_messages_count > 0)
                        <span class="badge badge-danger">{{$logged_in_user->unread_messages_count}}</span>
                    @endif
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('account')) }}" href="{{ route('frontend.user.account') }}">
                    <i class="nav-icon icon-user"></i>@lang('menus.frontend.sidebar.account')</a>
            </li>

            <li class="divider"></li>

        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
