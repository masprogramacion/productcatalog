@extends('backend.layouts.app')

@section('title', __('labels.backend.access.products.management') . ' | ' . __('labels.backend.access.products.create'))

@section('content')


<div class="container-fluid">
  <ul class="list-unstyled multi-steps">
    <li class="is-active">@lang('labels.products.create.step1')</li>
    <li class="is-active">@lang('labels.products.create.step2')</li>
    <li class="is-active">@lang('labels.products.create.step3')</li>
  </ul>
</div>

<div class="container">
  <div class="py-1 text-center">
    <h2>@lang('labels.products.create.step1of3')</h2>
    <p class="lead"></p>
    <div class="alert alert-warning" role="alert">@lang('labels.products.create.alert_warning_must_complete_steps')</div>
  </div>

  <div class="row">
    <div class="col-md-4 order-md-2 mb-4">
    </div>

    <div class="col-md-12 mb-4">
     
        {{ html()->form('POST', route('admin.auth.product.store'))->class('form-horizontal needs-validation')->attributes(['id'=>'form-create-product','novalidate'=>'novalidate'])->open() }}
        <hr class="mb-4">
        <h4 class="mb-3">@lang('labels.general.product_description')</h4>
        <div class="row">
          <div class="col-md-6 mb-3">
            {{ html()->label(__('labels.products.form.label.product_name'))->for('product_name')}}
            {{ html()->text('product_name')
                                    ->class('form-control')
                                    ->attribute('maxlength', 100)
                                    ->required()
                                    ->autofocus() }}
            <small class="text-muted">@lang('labels.products.form.helptext.product_name')</small>
            <div class="invalid-feedback">
            @lang('validation.required', ['attribute' => __('labels.products.form.label.product_name')])
            {{ $errors->first('name') }} <!--TODO add backend errors logic to show errors in each field-->
            </div>
          </div>
          <div class="col-md-6 mb-3">
            {{ html()->label(__('labels.products.form.label.description'))->for('description')}}
            {{ html()->textarea('description')
                                      ->class('form-control')
                                      ->attribute('maxlength', 40)
                                      ->required()}}
            <small class="text-muted">@lang('labels.products.form.helptext.description')</small>
            <div class="invalid-feedback">
            {{ $errors->first('description') }}
            @lang('validation.required', ['attribute' => __('labels.products.form.label.description')])
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-md-6 mb-3">
          {{ html()->label(__('labels.products.form.label.brand_id'))->class('form-control-label')->for('brand_id') }}
            {{ html()->select('brand_id',$brands)
                                    ->placeholder('')
                                    ->class('select2 custom-select d-block w-100')
                                    ->required() }}
            <small class="text-muted">@lang('labels.products.form.helptext.brand_id')</small>
            <div class="invalid-feedback">
            @lang('validation.required', ['attribute' => __('labels.products.form.label.brand_id')])
            {{ $errors->first('brand_id') }}
            </div>
          </div>


        </div>

        <div class="row">

          <div class="col-md-6 mb-3">
          {{ html()->label(__('labels.products.form.label.manufacturer_id'))->class('form-control-label')->for('manufacturer_id') }}
            {{ html()->select('manufacturer_id',$manufacturers)
                                    ->placeholder('')
                                    ->class('select2 custom-select d-block w-100')
                                    ->required() }}
            <small class="text-muted">@lang('labels.products.form.helptext.manufacturer_id')</small>
            <div class="invalid-feedback">
            @lang('validation.required', ['attribute' => __('labels.products.form.label.manufacturer_id')])
            {{ $errors->first('manufacturer_id') }}
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-md-6 mb-3">
                          {{ html()->label(__('labels.products.form.label.department_id'))->class('form-control-label') }}
            {{ html()->select('department_id',$departments)
                          ->placeholder('')
                          ->required()
                          ->class('select2 custom-select') }}
            <small class="text-muted">@lang('labels.products.form.helptext.department_id')</small>
            <div class="invalid-feedback">
            @lang('validation.required', ['attribute' => __('labels.products.form.label.department_id')])
            {{ $errors->first('department_id') }}
            </div>                 
          </div><!--col-->
        </div>
        
        <hr class="mb-4">
        <div class="row">

            <div class="col-md-4 mb-3">
              {{ html()->label(__('labels.products.form.label.price'))->for('price')}}
              {{ html()->input('number','price')->class('form-control')->attributes(['step'=>'.01','min'=>'0', 'max'=>'1000000'])->required() }}
              <small class="text-muted">@lang('labels.products.form.helptext.price')</small>
              <div class="invalid-feedback">
              @lang('validation.required', ['attribute' => __('labels.products.form.label.price')])
              {{ $errors->first('price') }}
              </div>
            </div>
            <div class="col-md-4 mb-3">
              {{ html()->label(__('labels.products.form.label.suggested_price'))->for('suggested_price')}}
              {{ html()->input('number','suggested_price')->class('form-control')->attributes(['step'=>'.01','min'=>'0', 'max'=>'1000000'])->required() }}
              <small class="text-muted">@lang('labels.products.form.helptext.suggested_price')</small>
              <div class="invalid-feedback">
              @lang('validation.required', ['attribute' => __('labels.products.form.label.suggested_price')])
              {{ $errors->first('suggested_price') }}
              </div>
            </div>
            <div class="col-md-4 mb-3">
            {{ html()->label(__('labels.products.form.label.min_remain_shelf_life'))->class('form-control-label')}}
                  {{ html()->input('number','min_remain_shelf_life')->class('form-control')->attributes(['min'=>'0', 'max'=>'365'])->required() }}
                  <small class="text-muted">@lang('labels.products.form.helptext.min_remain_shelf_life')</small>
              <div class="invalid-feedback">
              @lang('validation.required', ['attribute' => __('labels.products.form.label.min_remain_shelf_life')])
              {{ $errors->first('min_remain_shelf_life') }}
              </div>
            </div>
        </div>

        <div class="row">
          <div class="col-md-12 mb-3">

            <div class="row">
                <div class="col-md-3 mb-3">  
                  <div class="custom-control custom-checkbox mb-3">
                      <input class="custom-control-input" type="checkbox" value="1" name="ind_include_discount" id="ind_include_discount" data-toggle="collapse" data-target="#collapseExample">
                      <label class="custom-control-label" for="ind_include_discount">
                        @lang('labels.products.form.label.ind_include_discount')
                      </label>
                  </div>
                </div>
                <div class="col-md-4  mb-3 collapse" id="collapseExample">
                  <div class="input-group">
                    {{ html()->label(__('labels.products.form.label.discount_percentage'))->class('form-control-label')->for('discount_percentage') }}
                    {{ html()->input('number','discount_percentage')->class('form-control')->attributes(['min'=>'1', 'max'=>'100']) }}
                    <div class="input-group-append">
                      <span class="input-group-text">%</span>
                    </div>
                    <small class="text-muted">@lang('labels.products.form.helptext.discount_percentage')</small>
                    <div class="invalid-feedback" style="width: 100%;">
                    @lang('validation.required', ['attribute' => __('labels.products.form.label.discount_percentage')]).
                    </div>
                  </div>
                </div>
                <div class="col-md-5 mb-3 collapse" id="collapseExample">  
                  <div class="custom-control custom-checkbox mb-3">
                      <input class="custom-control-input" type="checkbox" value="1" name="ind_limited_time_discount"  id="ind_limited_time_discount" data-toggle="collapse" data-target="#collapseInner">
                      <label class="custom-control-label" for="ind_limited_time_discount">
                        @lang('labels.products.form.label.ind_limited_time_discount')
                      </label>
                  </div>
                  <div class="row" >
                    <div id="collapseInner" class="collapse">
                        <div class="mb-3">
                        {{ html()->label(__('labels.products.form.label.discount_start_date'))->class('form-control-label')}}
                          <input name="discount_start_date" id="discount_start_date"  placeholder="Seleccione una fecha"  />
                          <div class="invalid-feedback">
                          @lang('validation.required', ['attribute' => __('labels.products.form.label.discount_start_date')]).
                          </div>
                        </div>
                        <div class="mb-3">
                        {{ html()->label(__('labels.products.form.label.discount_end_date'))->class('form-control-label')}}
                          <input name="discount_end_date" id="discount_end_date"  placeholder="Seleccione una fecha" />
                          <div class="invalid-feedback">
                          @lang('validation.required', ['attribute' => __('labels.products.form.label.discount_end_date')]).
                          </div>
                        </div>
                    </div>
                  </div>

                </div>


            </div>


          </div>

        </div>

        <div class="row">
            <div class="col-md-6 mb-3">
                {{ html()->label(__('labels.products.form.label.purchase_tax_code_id'))->class('form-control-label') }}
                {{ html()->select('purchase_tax_code_id',$purchase_tax_codes)
                              ->class('custom-select')
                              ->placeholder('Seleccione una opción')
                              ->required() }}
                  <small class="text-muted">@lang('labels.products.form.helptext.purchase_tax_code_id')</small>
                  <div class="invalid-feedback">
                  @lang('validation.required', ['attribute' => __('labels.products.form.label.purchase_tax_code_id')])
                  {{ $errors->first('purchase_tax_code_id') }}
                  </div>
            </div>

            <div class="col-md-6 mb-3">
              {{ html()->label(__('labels.products.form.label.iva_code_id'))->class('form-control-label') }}
              {{ html()->select('iva_code_id',$iva_codes)
                            ->class('custom-select')
                            ->placeholder('Seleccione una opción')
                            ->required() }}
              <small class="text-muted">@lang('labels.products.form.helptext.iva_code_id')</small>
              <div class="invalid-feedback">
              @lang('validation.required', ['attribute' => __('labels.products.form.label.iva_code_id')])
              {{ $errors->first('iva_code_id') }}
              </div>
            </div>

        </div>
        
        <hr class="mb-4">

        <h4 class="mb-3">Información en el Sistema del Proveedor</h4>


        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="old_material_number">@lang('labels.products.form.label.old_material_number')<span class="text-muted">(Opcional)</span></label>
            {{ html()->text('old_material_number')
                                    ->class('form-control')
                                    ->attribute('maxlength', 100) }}
            <small class="text-muted">@lang('labels.products.form.helptext.old_material_number')</small>
            <div class="invalid-feedback"></div>            
          </div>
          <div class="col-md-6 mb-3">
            <label for="vendor_material_number">@lang('labels.products.form.label.vendor_material_number')<span class="text-muted">(Opcional)</span></label>
            {{ html()->text('vendor_material_number')
                                    ->class('form-control')
                                    ->attribute('maxlength', 100) }}
            <small class="text-muted">@lang('labels.products.form.helptext.vendor_material_number')</small>
            <div class="invalid-feedback"></div>            
          </div>

        </div>

        <hr class="mb-4">
        <!--
        <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.auth.product.index'), __('buttons.general.cancel')) }}
                    </div>

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div>
         </div>
         -->
        <button class="btn btn-primary btn-lg btn-block" id="btnSubmit" type="button">Crear el Producto y seguir al siguiente paso</button>
      </form>
    </div>
  </div>


</div>



@endsection
@push('after-scripts')
 <script> 

$(document).ready(function(){

  var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

  $('#discount_start_date').datepicker({

            format: '{{ config('app.date_format_js') }}' ,
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fortawesome',
            minDate: today,
            maxDate: function () {
                return $('#discount_end_date').val();
            }
          });
  $('#discount_end_date').datepicker({
    format: '{{ config('app.date_format_js') }}',
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fortawesome',
            minDate: function () {
                return $('#discount_start_date').val();
            } 
  });


  $(".select2").select2();
  $('[data-toggle="tooltip"]').tooltip();

  $('#collapseInner').on('show.bs.collapse', function( event ) {
    event.stopPropagation();
  })
  $('#collapseInner').on('hide.bs.collapse', function( event ) {
      event.stopPropagation();
  })

  $('#btnSubmit').on("click", validateForm);

  $('#ind_include_discount').change(function() {

    if ($('#discount_percentage').attr('required')) {
        $('#discount_percentage').removeAttr('required');
    } 

    else {
        $('#discount_percentage').attr('required','required');
    }

    });


});


function validateForm() {
   
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName("needs-validation");
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.classList.add("was-validated");
    });

    let product_name = document.querySelector("#product_name").checkValidity();
    let description = document.querySelector("#description").checkValidity();
    let price = document.querySelector("#price").checkValidity();
    let suggested_price = document.querySelector("#suggested_price").checkValidity();
    let min_remain_shelf_life = document.querySelector("#min_remain_shelf_life").checkValidity();
    let old_material_number = document.querySelector("#old_material_number").checkValidity();
    let vendor_material_number = document.querySelector("#vendor_material_number").checkValidity();
    //let material_tax_type_id = document.querySelector('#material_tax_type_id').checkValidity();
    let purchase_tax_code_id = document.querySelector('#purchase_tax_code_id').checkValidity();
    let iva_code_id = document.querySelector('#iva_code_id').checkValidity();

    let no_errors  = product_name && description && price && suggested_price && min_remain_shelf_life && old_material_number && vendor_material_number && purchase_tax_code_id && iva_code_id;
    
    //if there is a discount, validate value
    let no_discount_errors = true;
    if($('#ind_include_discount').is(':checked')){
        no_discount_errors = document.querySelector("#discount_percentage").checkValidity();
    }
    //if there is a limited discount, validate start and end date
    let no_date_errors = true;
    if($('#ind_limited_time_discount').is(':checked')){
      let discount_start_date = document.querySelector("#discount_start_date").checkValidity();
      let discount_end_date = document.querySelector("#discount_end_date").checkValidity();
      if(discount_start_date && discount_end_date){
        let tmp_discount_start_date = document.querySelector("#discount_start_date");
        //alert(tmp_discount_start_date);
      }else {
        no_date_errors = false;
      }
    }


    if (no_errors && no_discount_errors && no_date_errors) {
      $('#form-create-product').submit();
      $(this).attr('disabled','disabled');
    }else {

          //var formElements = document.getElementById('form-create-product').querySelectorAll('textarea, input:not([type=hidden])');
          var errorElements = document.getElementById('form-create-product').querySelectorAll('input.form-control:invalid');
          if(errorElements.length > 0){
            //console.log(errorElements);
            //var scrollTop = $(window).scrollTop();
            var elementOffset = $(errorElements[0]).offset().top - 100;
            //console.log(elementOffset);
            //var currentElementOffset = (elementOffset - scrollTop);
            //console.log(currentElementOffset);
            $('html, body').animate({
                scrollTop: elementOffset
              }, 2000, function() {
                $(errorElements[0]).focus();
            });
          }


    }
  }

</script>  
@endpush