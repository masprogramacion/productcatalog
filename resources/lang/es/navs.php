<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'   => 'Inicio',
        'logout' => 'Cerrar Sesión',
    ],

    'frontend' => [
        'contact'   => 'Contacto',
        'dashboard' => 'Principal',
        'login'     => 'Iniciar Sesión',
        'macros'    => 'Macros',
        'register'  => 'Registrarse',

        'user' => [
            'account'         => 'Mi Cuenta',
            'administration'  => 'Administración',

            'my_information'  => 'Mi Cuenta',
            'settings'         => 'Configuraciones',
            'updates'         => 'Actualizaciones',
            'messages'         => 'Mensajes',


            'info' => [
                'general' => 'General',
                'profile' => 'Perfil',
                'bank_account'  => 'Cuenta Bancaria',
                'change_password' => 'Cambiar la contraseña',
            ],
        ],

        'product' => [
            'information'  => 'Información',
            'images'         => 'Imagenes',
        ],
    ],



    'backend' => [
        'contact'   => 'Contacto',
        'dashboard' => 'Principal',
        'login'     => 'Iniciar Sesión',
        'macros'    => 'Macros',
        'register'  => 'Registrarse',

        'user' => [
            'account'         => 'Mi Cuenta',
            'administration'  => 'Administración',
            'change_password' => 'Cambiar la contraseña',
            'my_information'  => 'Mi Cuenta',
            'profile'         => 'Perfil',
            'settings'         => 'Configuraciones',
            'updates'         => 'Actualizaciones',
            'messages'         => 'Mensajes',

        ],

        'product' => [
            'information'  => 'Información',
            'images'         => 'Imagenes',
        ],
    ],
];
