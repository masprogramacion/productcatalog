<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Uuid;

class File extends Model
{
  
    use Uuid;
    
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'url',
        'uuid'
    ];
}
