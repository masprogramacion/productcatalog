<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Twilio\Twiml;

class MuleService
{

    //TODO IMPLEMENT SERVICE INSTEAD OF WEB SERVICE CALLS FROM PRODUCT REPOSITORY
    protected function getProductDepartment()
    {
        
        return Cache::remember('departments', 60, function ()  {

            try {
                $url = 'http://generalutilities-sys-api.cloudhub.io/generalutilities-sys-api/productCategoryTree/getDepartment';

                $client = new \GuzzleHttp\Client();
    
                $response = $client->request('GET', $url);
    
                return json_decode((string)$response->getBody());

            }catch (Guzzle\Http\Exception\BadResponseException $e) {


            }  

        });
    }


    

    protected function getProductCategory($departmentId)
    {
        return Cache::remember('department:' . $departmentId, 1440, function () use ($departmentId) {
            
            try {

                $response = $client->get('/not_found.xml')->send();

                $client = new \GuzzleHttp\Client();
                //$url = 'http://generalutilities-sys-api.cloudhub.io/generalutilities-sys-api/productCategoryTree/getCategory?departmentId=101019';
                $url = 'http://generalutilities-sys-api.cloudhub.io/generalutilities-sys-api/productCategoryTree/getCategory';
                $response = $client->request('GET', $url, [
                    'query' => [
                        'departmentId' => $departmentId
                    ]
                ]);
    
                $json = json_decode((string)$response->getBody());
    
                return $json->postalCodes[0];


            } catch (Guzzle\Http\Exception\BadResponseException $e) {


            }    

        });
    }











    


}