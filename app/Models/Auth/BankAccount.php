<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use App\Models\Auth\Traits\Relationship\BankAccountRelationship;

class BankAccount extends Model
{
    use BankAccountRelationship;


      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bank_id',
        'user_id',
        'account_number',
        'bank_account_type_id',
        'account_fullname',
        'noreturn_percentage'
    ];



}
