<?php

namespace App\Http\Requests\Frontend\User;

use Illuminate\Validation\Rule;
use App\Helpers\Frontend\Auth\Socialite;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateBankAccountRequest.
 */
class UpdateBankAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_id'    => 'required|numeric',
            'account_number'    => 'required|max:191',
            'bank_account_type_id'    => 'required|numeric',
            'account_fullname' => 'required|max:191',
            'noreturn_percentage' => 'required|numeric',
        ];
    }


}
