<?php

namespace App\Models\Auth\Traits\Attribute;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

/**
 * Trait ProductAttribute.
 */
trait ProductAttribute
{
    /**
     * @return string
     */
    public function getProductNameAttribute($value)
    {
        return $this->attributes['name'];
    }

    /**
     * @return string
     */
    public function getAssignedAttribute()
    {
        //dd($this->status_id);
        return ($this->status_id > 2 && $this->status_id != 7);
    }


    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        
        $link ='';
        
        if (auth()->check()) { 
            $user = Auth::user();
            if(Auth::user()->hasRole('proveedor')) {

                switch ($this->status_id) {
                    case 1:
                    return "<span class='badge badge-warning'>".__('Creado').'</span>';
        
                    case 6:
                    case 11:
                    case 12:
                    case 13:
                    return "<span class='badge badge-success'>".__('Aprobado').'</span>';
        
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    return "<span class='badge badge-danger'>".__('Rechazado').'</span>';
        
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    return "<span class='badge badge-warning'>".__('Pendiente de Revisión').'</span>';
                }


            }else
            {

                switch ($this->status_id) {
                    case 1:
                    return "<span class='badge badge-warning'>".__('Creado').'</span>';
        

                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    return "<span class='badge badge-danger'>".__('Rechazado').'</span>';
        
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    return "<span class='badge badge-warning'>".__('Pendiente de Revisión').'</span>';


                    case 11:
                    case 12:
                    case 13:
                    return "<span class='badge badge-success'>".__('Aprobado').'</span>';
                }


            }
        }
        return $link;






    }




    /**
     * @return string
     */
    public function getStatusBadgeAttribute()
    {
        $created = new Carbon($this->created_at);
        $now = Carbon::now();
        $difference = ($created->diff($now)->days < 1) ? '1 dia' : $created->diffForHumans($now);
        

        $badge ='';
        
            switch (true) {
                case $created->diff($now)->days <= 7:
                    $badge = "<span class='badge badge-success float-right'>".$created->diff($now)->days.' dias </span>';
                    break;
                case $created->diff($now)->days <= 15:
                    $badge = "<span class='badge badge-warning float-right'>".$created->diff($now)->days.' dias </span>';
                    break;
                default:
                    $badge = "<span class='badge badge-danger float-right'>".$created->diff($now)->days.' dias </span>';
                    break;
            }


        return $badge;

        
    }

    /**
     * @return string
     */
    public function getConfirmedLabelAttribute()
    {
        if ($this->isConfirmed()) {
            if ($this->id != 1 && $this->id != auth()->id()) {
                return '<a href="'.route(
                    'admin.auth.user.unconfirm',
                        $this
                ).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.backend.access.users.unconfirm').'" name="confirm_item"><span class="badge badge-success" style="cursor:pointer">'.__('labels.general.yes').'</span></a>';
            } else {
                return '<span class="badge badge-success">'.__('labels.general.yes').'</span>';
            }
        }

        return '<a href="'.route('admin.auth.user.confirm', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.backend.access.users.confirm').'" name="confirm_item"><span class="badge badge-danger" style="cursor:pointer">'.__('labels.general.no').'</span></a>';
    }


    /**
     * @return string
     */
    public function getPermissionsLabelAttribute()
    {
        $permissions = $this->getDirectPermissions()->toArray();

        if (\count($permissions)) {
            return implode(', ', array_map(function ($item) {
                return ucwords($item['name']);
            }, $permissions));
        }

        return 'N/A';
    }


    /**
     * @return mixed
     */
    public function getImageFrontAttribute()
    {
        if(strlen($this->image_front_1)){
            return url('storage/'.$this->image_front_1);
        }else {
            return asset('img/products/camera_icon.png');
        }
        
    }
    /**
     * @return mixed
     */
    public function getImageBackAttribute()
    {

        if(strlen($this->image_front_1)){
            return url('storage/'.$this->image_back_1);
        }else {
            return asset('img/products/camera_icon.png');
        }
    }




    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        $link ='';
        
        if (auth()->check()) { 
            $user = Auth::user();
            if(Auth::user()->hasRole('proveedor')) {
                return '<a href="'.route('frontend.auth.product.show',$this->id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-info"><i class="fas fa-eye"></i></a>';
            }else
            {
                return '<a href="'.route('admin.auth.product.show', $this->id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-primary"><i class="fas fa-eye"></i></a>';
            }
        }
        return $link;

        
    }


    /**
     * @return string
     */
    public function getShowButtonUncataloguedAttribute()
    {

        return '<a href="'.route('admin.auth.product.show.uncatalogued', $this->id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-primary"><i class="fas fa-eye"></i></a>';

    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        $link ='';
        //return $link;
        //TODO ANIMATE ICONS TO DRAW ATTENTION TO PENDING ACTIONS FROM USERS
        if (auth()->check()) { 
            $user = Auth::user();
            if(Auth::user()->hasRole('proveedor')) {
                
                if($this->status_id == 1){
                    return '<a href="'. route('frontend.auth.product.create.step2',  $this->id). '" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
                }else {
                    if(!$this->assigned){
                        return '<a href="'.route('frontend.auth.product.edit', $this->id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
                    }
                }

    
            }else
            {


                if(auth()->user()->hasRole(config('access.users.admin_role'))){
                    return '<a href="'.route('admin.auth.product.edit', $this->id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
                }else {
                    return '';
                }
                
            }
        }
        return $link;

        
    }


        /**
     * @return string
     */
    public function getAssignButtonAttribute()
    {
        $link ='';

        if (auth()->check()) { 

            if(Auth::user()->hasRole('proveedor')) {
                $link = '';
            }else {
                return '<a href="'.route('admin.auth.product.assign', $this->id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
            }

        }
        return $link;

        
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        return '<a href="'.route('frontend.auth.product.destroy', $this).'"
			 data-method="delete"
			 data-trans-button-cancel="'.__('buttons.general.cancel').'"
			 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
			 data-trans-title="'.__('strings.backend.general.are_you_sure').'"
			 class="btn btn-danger"><i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'"></i></a> ';
    }


    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        if ($this->id != auth()->id()) {
            switch ($this->active) {
                case 0:
                    return '<a href="'.route('admin.auth.user.mark', [
                            $this,
                            1,
                        ]).'" class="dropdown-item">'.__('buttons.backend.access.users.activate').'</a> ';

                case 1:
                    return '<a href="'.route('admin.auth.user.mark', [
                            $this,
                            0,
                        ]).'" class="dropdown-item">'.__('buttons.backend.access.users.deactivate').'</a> ';

                default:
                    return '';
            }
        }

        return '';
    }


    /**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="'.route('frontend.auth.product.delete-permanently', $this).'" name="confirm_item" class="btn btn-danger"><i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="'.__('buttons.frontend.products.delete_permanently').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="'.route('frontend.auth.product.restore', $this).'" name="confirm_item" class="btn btn-info"><i class="fas fa-sync" data-toggle="tooltip" data-placement="top" title="'.__('buttons.frontend.products.restore_product').'"></i></a> ';
    }


    /**
     * @return string
     */
    public function getRejectButtonAttribute()
    {
        return '<a href="'.route('admin.auth.product.update.reject', $this).'" name="confirm_item" class="btn btn-danger"><i class="fas fa-exclamation-triangle" data-toggle="tooltip" data-placement="top" title="'.__('buttons.backend.products.reject').'"></i> '.__('buttons.backend.products.reject').'</a> ';
    }

    /**
     * @return string
     */
    public function getApproveButtonAttribute()
    {
        return '<a href="'.route('admin.auth.product.update.approve', $this).'" name="confirm_item" class="btn btn-info"><i class="fas fa-check-square" data-toggle="tooltip" data-placement="top" title="'.__('buttons.backend.products.approve').'"></i> '.__('buttons.backend.products.approve').'</a> ';
    }


    /**
     * @return string
     */
    public function getEditStatusButtonAttribute()
    {
        $link = "";
        if(
         //CREADOS O RECHAZADOS POR GERENTE/PLANIMETRIA O FINANZAS   
        (auth()->user()->hasRole('asistentecompra') && ($this->status_id == 2 ||  $this->status_id == 8 ||  $this->status_id == 9 ||  $this->status_id == 10)) ||
        //PENDIENTE DE REVISION POR GERENTE DE CATEGORIA
        (auth()->user()->hasRole('gerentecategoria') && ($this->status_id == 3)) ||
        //PENDIENTE DE REVISION POR PLANIMETRIA
        (auth()->user()->hasRole('planimetria') && ($this->status_id  == 4)) ||
        //PENDIENTE DE REVISION POR FINANZAS
        (auth()->user()->hasRole('finanzas') && ($this->status_id  == 5))
        ){
            $link = '<a href="'. route('admin.auth.product.edit', $this). '" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
        }else {
            $link = '<a href="'. route('admin.auth.product.show', $this). '" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-primary"><i class="fas fa-eye"></i></a>';
        }

        return $link;
    
    }

    




    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        



         if ($this->trashed()) {
            return '
				<div class="btn-group" role="group" aria-label="'.__('labels.backend.access.users.user_actions').'">
				  '.$this->restore_button.'
				  '.$this->delete_permanently_button.'
				</div>';
        }
//		'.$this->delete_button.'
        return '
    	<div class="btn-group" role="group" aria-label="'.__('labels.backend.access.users.user_actions').'">
        '.$this->show_button.'
        '.$this->edit_button.'
		</div>';
    }



        /**
     * @return string
     */
    public function getActionButtonsAssignAttribute()
    {
        

        return '
    	<div class="btn-group" role="group" aria-label="'.__('labels.backend.access.users.user_actions').'">
        '.$this->show_button.'
        '.$this->assign_button.'
		</div>';
    }
}
