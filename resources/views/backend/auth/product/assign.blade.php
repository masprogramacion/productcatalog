@extends('backend.layouts.app')

@section('title', __('labels.backend.access.products.management') . ' | ' . __('labels.backend.access.products.assign_product_division'))

@section('content')




<div class="container">

  <div class="row">
    <div class="col-md-4 order-md-2 mb-4">
    </div>

    <div class="col-md-12 mb-4">
     
        {{ html()->modelForm($product, 'PATCH', route('admin.auth.product.update.assign', $product->id))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-details-product','novalidate'=>'novalidate'])->open() }}
        <hr class="mb-4">
        <h4 class="mb-3">@lang('labels.general.product_description')</h4>
        <div class="row">
          <div class="col-md-5 mb-3">
            {{ html()->label(__('labels.products.form.label.product_name'))->for('name')}}
            {{ html()->text('name')
                                    ->class('form-control')
                                    ->attributes(['disabled'=>'disabled']) }}

          </div>
          <div class="col-md-7 mb-3">
            {{ html()->label(__('labels.products.form.label.description'))->for('description')}}
            {{ html()->textarea('description')
                                      ->class('form-control')
                                      ->attributes(['disabled'=>'disabled'])
                                      }}
          </div>
        </div>
        <div class="row">

        <div class="col-sm-5">
          Categoria
          {{ html()->select('department_id',$departments)
                        ->placeholder('Seleccione un Departamento')
                        ->class('select2 custom-select d-block w-100')
                        ->attributes(['disabled'=>'disabled'])  }}
          <small class="text-muted"></small>
          <div class="invalid-feedback">
          </div>
        </div>

        </div>



       
        <hr class="mb-4">

        <div class="row">
          <div class="col-md-4 mb-3">
             {{ form_cancel(route('admin.dashboard'), __('buttons.general.cancel')) }}
          </div>
          <div class="col-md-4 mb-3">
                <a href="#reassign-modal" class="btn btn-sm btn-warning" data-toggle="modal" data-backdrop="false">
                        <i class="fa fa-edit"></i> Reasignar Categoria
                </a>
          </div>
          <div class="col-md-4 mb-3">
                <a href="#assign-modal" class="btn btn-sm btn-success" data-toggle="modal" data-backdrop="false">
                        <i class="fa fa-check"></i> Confirmar Categoria
                </a>
          </div>


         </div>
         

      </form>
    </div>
  </div>


</div>


<div id="reassign-modal" class="modal fade">
        <div class="modal-dialog">
            {{ html()->modelForm($product, 'PATCH', route('admin.auth.product.update.assign', $product->id))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-product','novalidate'=>'novalidate'])->open() }}

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Reasignar Categoria</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Esta seguro de reasignar la  <span class="badge badge-danger">categoria</span> de este producto  ?</p>
                        <div class="row">

                          <div class="col-sm-12">
                            Categoria:
                            {{ html()->select('department_id',$departments)
                                          ->placeholder('Seleccione un Departamento')
                                          ->class('select2 custom-select d-block w-100')
                                          ->required() }}
                            <small class="text-muted"></small>
                            <div class="invalid-feedback">
                            </div>
                          </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success  pull-left" data-loading-text="Loading&hellip;"><i class="fa fa-check"></i> Reasignar</button>

                    </div>
                </div>
            </form>
        </div>
    </div>


    <div id="assign-modal" class="modal fade">
        <div class="modal-dialog">
            {{ html()->modelForm($product, 'PATCH', route('admin.auth.product.update.assign', $product->id))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-product','novalidate'=>'novalidate'])->open() }}

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Confirmar Categoria</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Esta seguro de confirmar la categoria de este producto?</p>
                        <div class="row">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
                        <button type="submit" class="btn btn-sm btn-success  pull-left" data-loading-text="Loading&hellip;"><i class="fa fa-check"></i> Confirmar</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('after-scripts')
 <script> 

$(document).ready(function(){

  var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

  $('#discount_start_date').datepicker({

            format: '{{ config('app.date_format_js') }}' ,
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fortawesome',
            minDate: today,
            maxDate: function () {
                return $('#discount_end_date').val();
            }
          });
  $('#discount_end_date').datepicker({
    format: '{{ config('app.date_format_js') }}',
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fortawesome',
            minDate: function () {
                return $('#discount_start_date').val();
            } 
  });


  //$(".select2").select2();
  $('[data-toggle="tooltip"]').tooltip();

  $('#collapseInner').on('show.bs.collapse', function( event ) {
    event.stopPropagation();
  })
  $('#collapseInner').on('hide.bs.collapse', function( event ) {
      event.stopPropagation();
  })

  $('#btnSubmit').on("click", validateForm);

  $('#ind_include_discount').change(function() {

    if ($('#discount_percentage').attr('required')) {
        $('#discount_percentage').removeAttr('required');
    } 

    else {
        $('#discount_percentage').attr('required','required');
    }

    });


});


function validateForm() {
   
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName("needs-validation");
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.classList.add("was-validated");
    });

    let product_name = document.querySelector("#name").checkValidity();
    let description = document.querySelector("#description").checkValidity();
    let price = document.querySelector("#price").checkValidity();
    let suggested_price = document.querySelector("#suggested_price").checkValidity();
    let min_remain_shelf_life = document.querySelector("#min_remain_shelf_life").checkValidity();
    let old_material_number = document.querySelector("#old_material_number").checkValidity();
    let vendor_material_number = document.querySelector("#vendor_material_number").checkValidity();
    //let material_tax_type_id = document.querySelector('#material_tax_type_id').checkValidity();
    let purchase_tax_code_id = document.querySelector('#purchase_tax_code_id').checkValidity();
    let iva_code_id = document.querySelector('#iva_code_id').checkValidity();

    let no_errors  = product_name && description && price && suggested_price && min_remain_shelf_life && old_material_number && vendor_material_number  && purchase_tax_code_id && iva_code_id;
    
    //if there is a discount, validate value
    let no_discount_errors = true;
    if($('#ind_include_discount').is(':checked')){
        no_discount_errors = document.querySelector("#discount_percentage").checkValidity();
    }
    //if there is a limited discount, validate start and end date
    let no_date_errors = true;
    if($('#ind_limited_time_discount').is(':checked')){
      let discount_start_date = document.querySelector("#discount_start_date").checkValidity();
      let discount_end_date = document.querySelector("#discount_end_date").checkValidity();
      if(discount_start_date && discount_end_date){
        let tmp_discount_start_date = document.querySelector("#discount_start_date");
        //alert(tmp_discount_start_date);
      }else {
        no_date_errors = false;
      }
    }


    if (no_errors && no_discount_errors && no_date_errors) {
      $('#form-create-product').submit();
      $(this).attr('disabled','disabled');
    }else {

          //var formElements = document.getElementById('form-create-product').querySelectorAll('textarea, input:not([type=hidden])');
          var errorElements = document.getElementById('form-create-product').querySelectorAll('input.form-control:invalid');
          if(errorElements.length > 0){
            //console.log(errorElements);
            //var scrollTop = $(window).scrollTop();
            var elementOffset = $(errorElements[0]).offset().top - 100;
            //console.log(elementOffset);
            //var currentElementOffset = (elementOffset - scrollTop);
            //console.log(currentElementOffset);
            $('html, body').animate({
                scrollTop: elementOffset
              }, 2000, function() {
                $(errorElements[0]).focus();
            });
          }


    }
  }

</script>  
@endpush