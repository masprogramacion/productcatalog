@php

$label_button = 'Aprobar el Producto';

@endphp

  @if($show_update_status_form)

  <div class="container">


    {{ html()->modelForm($product, 'PATCH', route('frontend.auth.product.resend', $product->id))->class('form-horizontal needs-validation')->attributes(['id'=>'form-update-status-product','novalidate'=>'novalidate'])->open() }}
    <input type="hidden"  id="flag_approved" name="flag_approved" value="1">
    <div class="table-responsive">
        <table class="table table-hover">

            <tr>
                    <th>@lang('labels.backend.access.products.tabs.content.status.comments')</th>
                    <td>
                        {{ html()->textarea('comments')
                                                ->class('form-control')
                                                ->attribute('maxlength', 255)
                                                ->required()}}
                        <div class="invalid-feedback">
                        @lang('validation.required', ['attribute' => __('labels.backend.access.products.tabs.content.status.comments')])
                        </div>
                        
                    </td>
            </tr>

        <tr>

          <td>
           
          </td>
          <td>
            <button id="btnApproveProduct" class="btn btn-sm btn-success" type="button"> Enviar Producto</button>
          </td>
        </tr>
    
    </table>
    </div><!-- ends table-responsive --> 
    {{ html()->form()->close() }}


  </div><!-- ends container --> 
  @endif





<div class="container mt-5 mb-5">
	<div class="row">
		<div class="col-md-10 offset-md-1">
			<h4>{{ __('labels.backend.access.products.tabs.content.status.history_transitions') }}</h4>

      <ul class="timeline">
      @foreach($workflow_statuses as $workflow_status)

        @if($workflow_status->status_id==2 || $workflow_status->status_id==3  || $workflow_status->status_id==4   || $workflow_status->status_id==5  )
        <li class="timeline-inverted">
          <div class="timeline-badge {{$workflow_status->color}}"><i class="fa {{$workflow_status->icon}}"></i></div>
        @elseif($workflow_status->status_id==7 || $workflow_status->status_id==8  || $workflow_status->status_id==9   || $workflow_status->status_id==10  )
        <li class="timeline">
          <div class="timeline-badge {{$workflow_status->color}}"><i class="fa {{$workflow_status->icon}}"></i></div>
        @elseif($workflow_status->status_id==11 )
        <li class="timeline-inverted">
          <div class="timeline-badge {{$workflow_status->color}}"><i class="fa {{$workflow_status->icon}}"></i></div>
        @else
        <li>
          <div class="timeline-badge"><i class="fa {{$workflow_status->icon}}"></i></div>
        @endif

          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4 class="timeline-title">{{$workflow_status->current_status_description}}</h4>
              <p><small class="text-muted"><i class="fa {{$workflow_status->icon}}"></i> {{$workflow_status->updated_at}}</small></p>
            </div>
            <div class="timeline-body">
              <p>Usuario: {{$workflow_status->user->full_name}} <br/>Comentarios: {{$workflow_status->comments}}</p>
            </div>
          </div>
        </li>

      @endforeach
      </ul>
		</div>
	</div>
</div>


    <div id="approve-modal" class="modal fade">
        <div class="modal-dialog">
 
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Enviar Producto</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Esta seguro que desea enviar este producto para revision?</p>
                        <div class="row">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-danger cancel" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>
                        <button type="button" class="btn btn-sm btn-success ok  pull-left" data-loading-text="Loading&hellip;"><i class="fa fa-check"></i> Enviar</button>

                    </div>
                </div>

        </div>
    </div>
