<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\User;

/**
 * Class SapDataRelationship.
 */

trait SapDataRelationship
{

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
