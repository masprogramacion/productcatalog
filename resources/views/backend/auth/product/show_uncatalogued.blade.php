@extends('backend.layouts.app')

@section('content')


<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.products.management') }} <small class="text-muted">{{ __('labels.backend.access.products.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.auth.product.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">


        <table class="table table-hover">
            <tr>
                <th>@lang('labels.backend.access.users.tabs.content.overview.name')</th>
                <td>{{ $product->name }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.description')</th>
                <td>{!! $product->description !!}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.brand_id')</th>
                <td>
                @if($brand)
                   {{ $brand->name}}
                @endif
                </td>
            </tr>
            <tr>
                <th>Categoria</th>
                <td>
                @if($department)
                {{ $department->department_name }}
                @endif
                </td>
            </tr>
            <tr>
                <th>CODIGO DE BARRA - GTIN (EAN/UPC)</th>
                <td>{{ $product->gtin_1 }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.created_at')</th>
                <td>{{ $product->created_at }}</td>
            </tr>
            <tr>
                <th>@lang('labels.backend.access.products.tabs.content.overview.updated_at')</th>
                <td>{{ $product->updated_at }}</td>
            </tr>
        </table>


                </div>
            </div><!--col-->
        </div><!--row-->
        <div>
            <div class="col-sm-3">
                <div class="card">
                            <img class="card-img-top" id="preview_image_front_1" name="preview_image_front_1"  src="{{ $product->imageFront }}" alt="@lang('labels.products.form.alt.select_front_image')">
                            <div class="card-body">
                                <h5 class="card-title">Imagen Frontal</h5>
                                <p class="card-text"></p>
                            </div>
                </div>
            </div>
            <div class="col-sm-3">        
                    <div class="card">
                            <img class="card-img-top" id="preview_image_back_1" name="preview_image_back_1" src="{{ $product->imageBack  }}" alt="@lang('labels.products.form.alt.select_back_image')">
                            <div class="card-body">
                                    <h5 class="card-title">Imagen Posterior</h5>
                            </div>
                    </div>
            </div>
        </div>
    </div><!--card-body-->
    <div class="card-footer clearfix">
                <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.dashboard'), __('buttons.general.goback')) }}
                </div><!--col-->

                <div class="col text-right">
                <a class="btn btn-success btn-sm pull-right" href="{{ route('admin.auth.product.edit.uncatalogued', $product) }}" role="button">@lang('buttons.general.crud.edit')</a>
                </div><!--row-->
            </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->





@endsection
@push('after-scripts')
    
    <script>







    </script>
    
@endpush