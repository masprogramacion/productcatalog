<?php

namespace App\Notifications\Backend\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Class ProductStatusUpdated.
 */
class ProductStatusUpdated extends Notification
{
    use Queueable;

    public $data;

    public function __construct(array $data) {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(__('strings.emails.products.subject'))
            ->line(__('strings.emails.products.email_product_updated', ['product_name' => $this->data['product_name'], 'user_info' => $this->data['user_info'] ]))
            ->greeting(__('strings.emails.auth.greeting'))
            ->action(__('labels.frontend.auth.login_button'), route('frontend.auth.login'));
            //->line(__('strings.emails.auth.thank_you_for_using_app'));
    }
}
