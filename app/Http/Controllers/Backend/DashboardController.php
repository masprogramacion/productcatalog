<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Auth\Product;
use App\Repositories\Backend\Auth\ProductRepository;
use App\Models\Catalog\ProductStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * ProductController constructor.
     *
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        
        $user = auth()->user();
        //$products = Product::whereUserId($user->id)->get();
        $product_statuses = ProductStatus::pluck('description as name','id');
        $products_gerente_categoria = Product::where('status_id',3)->orderBy('created_at', 'desc')->get();
        $products_pending_asistente_compras = Product::whereNull('product_assignment_verified_at')->where('status_id',2)->orderBy('id', 'desc')->paginate();
        $products = Product::whereNotNull('product_assignment_verified_at')->whereIn('status_id',array(2,8,9,10))->orderBy('id', 'desc')->paginate();
        $products_uncatalogued =   $this->productRepository->getFilteredPaginated($request, config('xtra.paged.products_uncatalogued'), 'id', 'desc', $user->id);
        //dd($products_pending_asistente_compras);
        if($user->hasRole('gerentecategoria')){
            $departments = $user->departments->pluck('department_id');
            //dump($departments);
            $products_gerente_categoria = Product::where('status_id',3)->whereIn('department_id', $departments)->orderBy('created_at', 'desc')->get();
            //dump($products_gerente_categoria);
            //dd();
        }

        if($user->hasRole('asistentecompra')){
            $departments = $user->departments->pluck('department_id');
            //dump($departments);


            $products_pending_asistente_compras = Product::whereNull('product_assignment_verified_at')->whereIn('department_id', $departments)->whereIn('status_id',array(2))->orderBy('created_at', 'desc')->paginate();
            //DB::enableQueryLog(); 

            //dump($products);
            //dd(DB::getQueryLog()); // Show results of log\

        }     

        $products_planimetria = Product::where('status_id',4)->orderBy('created_at', 'desc')->get();
        $products_finanzas = Product::where('status_id',5)->orderBy('created_at', 'desc')->get();
        $products_approved = Product::where('status_id',6)->orderBy('created_at', 'desc')->get();


        //GET PRODUCTS WITH THE STATUS(ES) ASIGNED TO THE ROLE
        $status_for_role = Product::whereUserId($user->id)->get();
        
        $departments = [];
        $departments = $this->productRepository->getDepartment();


        //dd($products_pending_analista_compras);
        return view('backend.dashboard')
        ->withDepartments($departments)
        ->with('product_statuses', $product_statuses)
        ->with('products_pending_asistente_compras', $products_pending_asistente_compras)
        ->with('products_gerente_categoria', $products_gerente_categoria)
        ->with('products_planimetria', $products_planimetria)
        ->with('products_finanzas', $products_finanzas)
        ->with('products_approved', $products_approved)
        ->with('products_uncatalogued', $products_uncatalogued)
        ->with('products', $products);


        
 
    }


    public function webservice_error()
    {

        return view('backend.system_error');


    }
}
