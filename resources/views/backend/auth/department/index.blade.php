@extends('backend.layouts.app')

@section('content')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.departments.management_category') }} <small class="text-muted">{{ __('labels.backend.access.departments.assigned') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">

            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.access.departments.table.name')</th>
                            <th>@lang('labels.backend.access.departments.table.categories')</th>
                            <th>@lang('labels.backend.access.departments.table.emails')</th>
                            <th>@lang('labels.backend.access.departments.table.created_at')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($department_managers as $manager)
                            <tr>
                                <td>{{ $manager->first_name }} {{ $manager->last_name }}</td>
                                <td>
   
                                    @if($manager->departments->count())
                                    
                                        @foreach($manager->departments as $department)
                                            <i class="fas fa-dot-circle"></i> {{ ucwords($department->department_name) }}<br/>
                                        @endforeach
                                        
                                    @else
                                        @lang('labels.general.none')
                                    @endif

                                </td>
                                <td>{{ $manager->email}}</td>
                                <td>{!! $manager->created_at !!}</td>
                                <td>{!! $manager->manager_action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $department_managers->total() !!} {{ trans_choice('labels.backend.access.departments.table.total', $department_managers->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $department_managers->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->


@endsection