<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\User;

/**
 * Class BankAccountRelationship.
 */

trait BankAccountRelationship
{

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
