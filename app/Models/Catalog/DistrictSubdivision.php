<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class DistrictSubdivision extends Model
{
    //
    public function district()
    {
        return $this->belongsTo('App\Models\Catalog\District');
    }
}
