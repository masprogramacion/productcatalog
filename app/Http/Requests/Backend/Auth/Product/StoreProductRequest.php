<?php

namespace App\Http\Requests\Backend\Auth\Product;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreProductRequest.
 */
class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return $this->user()->isAdmin();
        //TODO this can be used by a regular user as well
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_name' => 'required',
            'description' => 'required',
            'price' => 'required',
            'suggested_price' => 'required|gt:price',
            'department_id' => 'required',
            'min_remain_shelf_life'  => 'required',
            'purchase_tax_code_id' => 'required',
            //'material_tax_type_id' => 'required',
            'iva_code_id' => 'required',
            'brand_id'  => 'required',
            'manufacturer_id'  => 'required',
            'ind_include_discount'  => 'sometimes|nullable',
            'discount_percentage' => 'sometimes|nullable|numeric',
            'ind_limited_time_discount' => 'sometimes|nullable|numeric',
            'discount_start_date' => 'sometimes|nullable|date | date_format:"Y-m-d"',
            'discount_end_date' => 'sometimes|nullable|date | date_format:"Y-m-d"|after_or_equal:discount_start_date',
            'old_material_number' => 'sometimes|nullable' ,
            'vendor_material_number' => 'sometimes|nullable',


            
            // 'order_price_measurement_unit_id' => 'required',

            // 'material_tax_type_id'  => 'required',
            // 'available_deliverable_from'  => 'required|date',
            // 'available_deliverable_until'  => 'required|date',
            // 'ind_regular_vendor'  => 'required',
            // 'iva_code_id'  => 'required',
            // 'purchase_tax_code_id'  => 'required',
            // 'material_tax_code'  => 'required',
            // 'material_type_id'  => 'required',
            // 'material_group_id'  => 'required',
            // 'purchase_organization_id' => 'required',
            // 'purchasing_group_id'=> 'required',
            // 'provider_account_id'=> 'required',

            // 'sku'   => 'required|unique:products,sku,',
            // 'lower_level_pack_measurement_unit_id_1'=> 'required',
            // 'department_id'=> 'required',
            // 'category_id'=> 'required',
            // 'hierarchy_id'=> 'required',
            // 'ind_measurement_unit_is_delivery_or_issue_unit'=> 'required',
            // 'price' => 'required|numeric'
        ];
    }
}

