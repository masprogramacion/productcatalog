<?php

namespace App\Http\Requests\Frontend\User;

use Illuminate\Validation\Rule;
use App\Helpers\Frontend\Auth\Socialite;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateInfoRequest.
 */
class UpdateInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|max:191',
            'first_name'  => 'required|max:191',
            'last_name'  => 'required|max:191',
            'email' => 'sometimes|required|email|max:191',
            'operations_document' => 'sometimes|file|max:5120|mimes:pdf,docx,doc',
        ];

    }


}
