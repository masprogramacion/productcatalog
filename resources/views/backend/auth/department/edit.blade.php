@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.edit'))


@section('content')
{{ html()->modelForm($user, 'PATCH', route('admin.auth.department.update', $user->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.departments.management_category')
                        <small class="text-muted">@lang('labels.backend.access.departments.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">


                    <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.users.name'))->class('col-md-2 form-control-label') }}
                        <div class="col-md-10">{!! $user->first_name !!} {!! $user->last_name !!}</div><!--col-->
                    </div><!--form-group-->



                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.access.departments.table.assigned'))->class('col-md-2 form-control-label') }}

                        <div class="table-responsive col-md-10">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>@lang('labels.backend.access.departments.table.assigned')</th>
                                        <th>@lang('labels.backend.access.departments.table.catalog')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if($departments->count())
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <div class="checkbox d-flex align-items-center">
                                                                {{ html()->label(ucwords($user->name)) }}
                                                            </div>
                                                        </div>
                                                        <div class="card-body">
                                                                @if($user_departments->count())
                                                                
                                                                    @foreach($user_departments as $department)
                                                                        <i class="fas fa-dot-circle"></i> {{ ucwords($department->department_name) }}<br/>
                                                                    @endforeach
                                                                    
                                                                @else
                                                                    @lang('labels.general.none')
                                                                @endif
                                                        </div>
                                                    </div><!--card-->
                                            @endif
                                        </td>
                                        <td>
                                            @if($departments->count())
                                                @foreach($departments as $department)
                                                    <div class="checkbox d-flex align-items-center">
                                                        {{ html()->label(
                                                                html()->checkbox('departments[]', $user_departments->contains($department), $department->id)
                                                                        ->class('switch-input')
                                                                        ->id('department-'.$department->id)
                                                                    . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                                                                ->class('switch switch-label switch-pill switch-primary mr-2')
                                                            ->for('department-'.$department->id) }}
                                                        {{ html()->label(ucwords($department->department_name))->for('department-'.$department->id) }}
                                                    </div>
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                @if($user->hasRole('asistentecompra'))
                    {{ form_cancel(route('admin.auth.department.index_assistant'), __('buttons.general.cancel')) }}
                @endif

                @if($user->hasRole('gerentecategoria'))
                    {{ form_cancel(route('admin.auth.department.index'), __('buttons.general.cancel')) }}
                @endif

                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
