@extends('backend.layouts.app')

@section('title', __('labels.backend.access.catalogs.banks.management') )

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-vendor-links')
@endsection

@section('content')
         <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            @lang('labels.backend.access.catalogs.banks.management')
                            <small class="text-muted"></small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr>
                
                 
                <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">


                        <table class="table table-striped table-bordered" id="banksTable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('labels.backend.access.catalogs.banks.table.name')</th>
                                    <th class="tabledit-toolbar-column"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($banks as $bank)

 
                                <tr id="{{$bank->id}}">
                                    <td><span class="tabledit-span tabledit-identifier">1</span><input class="tabledit-input tabledit-identifier" type="hidden" name="id" value="{{$bank->id}}" disabled=""></td>
                                    <td class="tabledit-view-mode"><span class="tabledit-span" style="display: inline;">{{$bank->name}}</span>
                                    <!--<input class="tabledit-input form-control input-sm" type="text" name="nickname" value="markcell" style="display: none;" disabled=""></td>-->
                                    {{ html()->text('bank_name')
                                        ->class('tabledit-input form-control input-sm')
                                        ->value($bank->name)
                                        ->attributes(['name'=>'bank_name','id'=>'bank_name','style'=>'display: none;', 'disabled'=>''])
                                        ->required() }}
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    <!--
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.access.catalogs.banks.table.name')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($banks as $bank)
                            <tr>
                                <td>{{ html()->text('table_name')
                                        ->class('form-control')
                                        ->value($bank->name)
                                        ->attribute('maxlength', 191)
                                        ->attributes(['name'=>'table_name_'.$bank->id,'id'=>'table_name_'.$bank->id,'maxlength'=>'191'])
                                        ->required() }}</td>
                                <td>{!! $bank->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    -->
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $banks->total() !!} {{ trans_choice('labels.backend.access.catalogs.banks.table.total', $banks->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $banks->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->


            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">

                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
        <div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
        <span class="fa fa-spinner fa-spin fa-3x"></span>
    </div>
</div>
@endsection

@push('after-scripts')
 <script> 





$.ajaxSetup({

headers: {

    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

}

})
// Update record
$(document).on("click", ".update" , function() {
  
  var row_id = $(this).data('id');
  var bank_name = $('#table_name_'+row_id).val();
  //alert(row_id);

  if(bank_name != ''){
    $('.modal').modal('show');
    $.ajax({
      url: '{{ url('admin/auth/catalog/bank/update') }}/' + row_id,
      type: 'post',
      data: { id:row_id, name:bank_name},
        success: function(data) {
            //submitBtn.button('reset');
            
            if (data.result === 'updated') {
                //deleteLogModal.modal('hide');
                alert(data.result)
            }
            else {
                alert('OOPS ! This is a lack of coffee exception!')
            }
            $('.modal').modal('hide');
        },
        error: function(xhr, textStatus, errorThrown) {
            alert('AJAX ERROR ! Check the console !');
            console.error(errorThrown);
            $('.modal').modal('hide');
            //submitBtn.button('reset');
        }
        //success: function(response){
            //alert(response);
        //}
    });
  }else{
    alert('Debe llenar el campo que esta editando');
  }
});

// Delete record
// $(document).on("click", ".delete" , function() {
//   var delete_id = $(this).data('id');
//   var el = this;
//   $.get('{{ url('admin/auth/catalog/bank/update/') }}/' + bank_id , function (data) {

//         $.each(data, function(key, value) {   
//             $('#category_id')
//                 .append($("<option></option>")
//                             .attr("value",key)
//                             .text(value)); 
//         });
//         $('#category_id').prop("disabled", false);


//     });
// });


$('#banksTable').Tabledit({
    url: '{{ url('admin/auth/catalog/bank/update') }}',
    dataType: 'json',
    columns: {
        identifier: [0, 'id'],
        editable: [[1, 'bank_name']]
    },
    buttons: {
        edit: {
            class: 'btn btn-sm btn-primary',
            html: '<i class="fa fa-pencil-alt"></i> &nbsp Editar',
            action: 'edit'
        },
        delete: {
            class: 'btn btn-sm btn-danger',
            html: '<span class="fa fa-trash-alt"></span> &nbsp Eliminar',
            action: 'delete'
        },
        save: {
            class: 'btn btn-sm btn-success',
            html: '<span class="fa fa-check"></span> &nbsp Guardar',
        },
        confirm: {
            class: 'btn btn-sm btn-danger',
            html: '<span class="fa fa-check"></span> &nbsp Confirmar',
        }
    },
    restoreButton:false,
    onAlways: function() {
        console.log('onAlways()');
    },
    onAjax: function(action, serialize) {
        console.log('onAjax(action, serialize)');
        console.log(action);
        console.log(serialize);
    },
    onSuccess: function(data, textStatus, jqXHR) {
        console.log(data);
        console.log('success');
        console.log(jqXHR);
    },
    onFail: function(jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        console.log('erro r failed');
        console.log(errorThrown);
    }
});


$(document).ready(function (){
      


});



</script>  
@endpush
