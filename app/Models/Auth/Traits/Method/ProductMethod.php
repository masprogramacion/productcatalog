<?php

namespace App\Models\Auth\Traits\Method;

use App\Models\Catalog\ProductStatus;
/**
 * Trait ProductMethod.
 */
trait ProductMethod
{

    public function getStatus() {

        return ProductStatus::where('id', '=', $this->status_id)->first();
     }


}
